<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SoftwarePackageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('software_packages')->truncate();
        DB::table('software_packages')->insert([
            [
                'name' => 'Gold',
                'quantity_branch' => '2',
                'quantity_staff' => '2',
                'quantity_acc' => '2',
                'quantity_product' => '2',
                'quantity_bill' => '2',
                'price' => '100000',
            ],
            [
                'name' => 'Platinum',
                'quantity_branch' => '24',
                'quantity_staff' => '24',
                'quantity_acc' => '24',
                'quantity_product' => '24',
                'quantity_bill' => '24',
                'price' => '1400000',
            ],
        ]);
    }
}
