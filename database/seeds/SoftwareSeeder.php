<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SoftwareSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('softwares')->truncate();
        DB::table('softwares')->insert([
            [
                'name' => 'C.E.O Nail',
                'description' => 'Phần mềm quản lý tiệm nail',
            ],
            [
                'name' => 'C.E.O Res',
                'description' => 'Phần mềm quản lý res',
            ],
            [
                'name' => 'C.E.O Coffee',
                'description' => 'Phần mềm quản lý coffee',
            ],
            [
                'name' => 'C.E.O EduCenter',
                'description' => 'Phần mềm quản lý educenter',
            ],
            [
                'name' => 'C.E.O Construct',
                'description' => 'Phần mềm quản lý construct',
            ],
            [
                'name' => 'C.E.O Inventory',
                'description' => 'Phần mềm quản lý inventory',
            ],
        ]);
    }
}
