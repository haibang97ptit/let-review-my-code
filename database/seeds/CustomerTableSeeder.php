<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CustomerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('customers')->truncate();
        DB::table('customers')->insert([
            [
                'phone_number' => '0123445678',
                'email' => 'a@gmail.com',
                'address' => '123, LA Street',
                'birthday' => '2020-09-05',
                'note' => '',
                'state' => 'Maryland',
                'first_name' => 'A',
                'last_name' => 'T',
                'user_id' => '2',
            ],
            [
                'phone_number' => '0223445678',
                'email' => 'b@gmail.com',
                'address' => '123, LA Street',
                'birthday' => '2020-09-05',
                'note' => '',
                'state' => 'Maryland',
                'first_name' => 'B',
                'last_name' => 'T',
                'user_id' => '3',
            ],
            [
                'phone_number' => '0323445678',
                'email' => 'c@gmail.com',
                'address' => '123, LA Street',
                'birthday' => '2020-09-05',
                'note' => '',
                'state' => 'Maryland',
                'first_name' => 'C',
                'last_name' => 'T',
                'user_id' => '2',
            ],
            [
                'phone_number' => '0423445678',
                'email' => 'd@gmail.com',
                'address' => '123, LA Street',
                'birthday' => '2020-09-05',
                'note' => '',
                'state' => 'Maryland',
                'first_name' => 'D',
                'last_name' => 'T',
                'user_id' => '2',
            ],
            [
                'phone_number' => '0523445678',
                'email' => 'e@gmail.com',
                'address' => '123, LA Street',
                'birthday' => '2020-09-05',
                'note' => '',
                'state' => 'Maryland',
                'first_name' => 'E',
                'last_name' => 'T',
                'user_id' => '3',
            ],
            [
                'phone_number' => '0623445678',
                'email' => 'f@gmail.com',
                'address' => '123, LA Street',
                'birthday' => '2020-09-05',
                'note' => '',
                'state' => 'Maryland',
                'first_name' => 'F',
                'last_name' => 'T',
                'user_id' => '2',
            ],
            [
                'phone_number' => '0723445678',
                'email' => 'g@gmail.com',
                'address' => '123, LA Street',
                'birthday' => '2020-09-05',
                'note' => '',
                'state' => 'Maryland',
                'first_name' => 'G',
                'last_name' => 'T',
                'user_id' => '4',
            ],
            [
                'phone_number' => '0823445678',
                'email' => 'h@gmail.com',
                'address' => '123, LA Street',
                'birthday' => '2020-09-05',
                'note' => '',
                'state' => 'Maryland',
                'first_name' => 'H',
                'last_name' => 'T',
                'user_id' => '2',
            ],
            [
                'phone_number' => '0923445678',
                'email' => 'i@gmail.com',
                'address' => '123, LA Street',
                'birthday' => '2020-09-05',
                'note' => '',
                'state' => 'Maryland',
                'first_name' => 'I',
                'last_name' => 'T',
                'user_id' => '4',
            ],
            [
                'phone_number' => '0103445678',
                'email' => 'j@gmail.com',
                'address' => '123, LA Street',
                'birthday' => '2020-09-05',
                'note' => '',
                'state' => 'Maryland',
                'first_name' => 'J',
                'last_name' => 'T',
                'user_id' => '2',
            ],
            [
                'phone_number' => '0113445678',
                'email' => 'k@gmail.com',
                'address' => '123, LA Street',
                'birthday' => '2020-09-05',
                'note' => '',
                'state' => 'Maryland',
                'first_name' => 'K',
                'last_name' => 'T',
                'user_id' => '3',
            ],
        ]);
    }
}
