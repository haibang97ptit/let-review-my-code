<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('states')->truncate();
        DB::table('states')->insert([
            [
                'name' => 'California',
            ],
            [
                'name' => 'Florida',
            ],

            [
                'name' => 'Texas',
            ],

            [
                'name' => 'Hawaii',
            ],

            [
                'name' => 'New Jersey',
            ],

            [
                'name' => 'Arizona',
            ],
            [
                'name' => 'Pennsylvania',
            ],
            [
                'name' => 'North Carolina',
            ],
            [
                'name' => 'Georgia',
            ],
            [
                'name' => 'Colorado',
            ],
            [
                'name' => 'Michigan',
            ],
            [
                'name' => 'Massachusetts',
            ],
            [
                'name' => 'Virginia',
            ],
            [
                'name' => 'Illinois',
            ],
            [
                'name' => 'Ohio',
            ],
            [
                'name' => 'Alaska',
            ],
            [
                'name' => 'Alabama',
            ],
            [
                'name' => 'New York',
            ],
            [
                'name' => 'Washington',
            ],
            [
                'name' => 'Tennessee',
            ],
            [
                'name' => 'Minnesota',
            ],
            [
                'name' => 'Maryland',
            ],
            [
                'name' => 'Oregon',
            ],
            [
                'name' => 'South Carolina',
            ],
            [
                'name' => 'Wisconsin',
            ],
            [
                'name' => 'Missouri',
            ],
            [
                'name' => 'Utah',
            ],
            [
                'name' => 'Indiana',
            ],
            [
                'name' => 'Louisiana',
            ],
            [
                'name' => 'Connecticut',
            ],
            [
                'name' => 'Kentucky',
            ],
            [
                'name' => 'Oklahoma',
            ],
            [
                'name' => 'Nevada',
            ],
            [
                'name' => 'Maine',
            ],
            [
                'name' => 'Montana',
            ],
            [
                'name' => 'Mississippi',
            ],
            [
                'name' => 'New Mexico',
            ],
            [
                'name' => 'Iowa',
            ],
            [
                'name' => 'Rhode Island',
            ],
            [
                'name' => 'Kansas',
            ],
            [
                'name' => 'Nebraska',
            ],
            [
                'name' => 'Wyoming',
            ],
            [
                'name' => 'Arkansas',
            ],
            [
                'name' => 'Vermont',
            ],
            [
                'name' => 'Delaware',
            ],
            [
                'name' => 'West Virginia',
            ],
            [
                'name' => 'Idaho',
            ],
            [
                'name' => 'South Dakota',
            ],
            [
                'name' => 'New Hampshire',
            ],
            [
                'name' => 'North Dakota',
            ],
        ]);
    }
}
