<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RegisterServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('register_services')->truncate();
        for($i = 0; $i < 500; $i++) {
            DB::table('register_services')->insert([
                [
                    'code' => 'TQ/D20201021/7yknYV',
                    'start_date' => '2020-10-09 14:36:00',
                    'end_date' => '2020-05-13 14:36:00',
                    'status_register' => 'paid',
                    'price' => '3960000',
                    'address_domain' => 'abc.com',
                    'id_customer' => '1',
                    'type_service' => 'domain',
                    'type_service_id' => '1',
                    'time_use' => '6',
                ],
            ]);
        }
        for($i = 0; $i < 500; $i++) {
            DB::table('register_services')->insert([
                [
                    'code' => 'TQ/D20201021/7yknYV',
                    'start_date' => '2020-10-09 14:36:00',
                    'end_date' => '2020-05-13 14:36:00',
                    'status_register' => 'paid',
                    'price' => '3960000',
                    'address_domain' => 'abc.com',
                    'id_customer' => '1',
                    'type_service' => 'hosting',
                    'type_service_id' => '1',
                    'time_use' => '6',
                ],
            ]);
        }
        for($i = 0; $i < 500; $i++) {
            DB::table('register_services')->insert([
                [
                    'code' => 'TQ/D20201021/7yknYV',
                    'start_date' => '2020-10-09 14:36:00',
                    'end_date' => '2020-05-13 14:36:00',
                    'status_register' => 'paid',
                    'price' => '3960000',
                    'address_domain' => 'abc.com',
                    'id_customer' => '1',
                    'type_service' => 'vps',
                    'type_service_id' => '1',
                    'time_use' => '6',
                ],
            ]);
        }
        for($i = 0; $i < 500; $i++) {
            DB::table('register_services')->insert([
                [
                    'code' => 'TQ/D20201021/7yknYV',
                    'start_date' => '2020-10-09 14:36:00',
                    'end_date' => '2020-05-13 14:36:00',
                    'status_register' => 'paid',
                    'price' => '3960000',
                    'address_domain' => 'abc.com',
                    'id_customer' => '1',
                    'type_service' => 'email',
                    'type_service_id' => '1',
                    'time_use' => '6',
                ],
            ]);
        }
        for($i = 0; $i < 500; $i++) {
            DB::table('register_services')->insert([
                [
                    'code' => 'TQ/D20201021/7yknYV',
                    'start_date' => '2020-10-09 14:36:00',
                    'end_date' => '2020-05-13 14:36:00',
                    'status_register' => 'paid',
                    'price' => '3960000',
                    'address_domain' => 'abc.com',
                    'id_customer' => '1',
                    'type_service' => 'ssl',
                    'type_service_id' => '1',
                    'time_use' => '6',
                ],
            ]);
        }
        for($i = 0; $i < 500; $i++) {
            DB::table('register_services')->insert([
                [
                    'code' => 'TQ/D20201021/7yknYV',
                    'start_date' => '2020-10-09 14:36:00',
                    'end_date' => '2020-05-13 14:36:00',
                    'status_register' => 'paid',
                    'price' => '3960000',
                    'address_domain' => 'abc.com',
                    'id_customer' => '1',
                    'type_service' => 'website',
                    'type_service_id' => '1',
                    'time_use' => '6',
                ],
            ]);
        }
    }
}
