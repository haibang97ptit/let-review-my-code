<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DomainTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('domains')->truncate();
        DB::table('domains')->insert(array(
            array(
                'name' => '.com',
                'fee_register' => 100000,
                'fee_remain' => 500000,
                'fee_transformation' => 60000,
                'notes' => 'This is my domain',
            ),
            array(
                'name' => '.vn',
                'fee_register' => 50000,
                'fee_remain' => 70000,
                'fee_transformation' => 60000,
                'notes' => 'This is my domain',
            )
        ));
    }
}
