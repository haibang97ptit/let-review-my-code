<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RegisterSoftwareSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('register_softwares')->truncate();
        for($i = 0; $i < 1000; $i++) {
            DB::table('register_softwares')->insert([
                [
                    'code' => 'TQ/D20201021/7yknYV',
                    'start_date' => '2020-10-09 14:36:00',
                    'end_date' => '2020-05-13 14:36:00',
                    'status_register' => 'paid',
                    'price' => '3960000',
                    'address_domain' => 'abc.com',
                    'id_customer' => '1',
                    'id_software' => '1',
                    'id_software_package' => '1',
                    'time_use' => '6',
                    'version' => 'official',
                ],
            ]);
        }
    }
}
