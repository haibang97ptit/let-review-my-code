<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();
        DB::table('users')->insert([
            [
                'name' => 'admin',
                'email' => 'admin@gmail.com',
                'password' => \Illuminate\Support\Facades\Hash::make('12345678'),
                'role_id' => '1',
            ],
            [
                'name' => 'Trần Văn A',
                'email' => 'tva@gmail.com',
                'password' => \Illuminate\Support\Facades\Hash::make('12345678'),
                'role_id' => '2',
            ],
            [
                'name' => 'Trần Văn B',
                'email' => 'tvb@gmail.com',
                'password' => \Illuminate\Support\Facades\Hash::make('12345678'),
                'role_id' => '2',
            ],
            [
                'name' => 'Trần Văn C',
                'email' => 'tvc@gmail.com',
                'password' => \Illuminate\Support\Facades\Hash::make('12345678'),
                'role_id' => '2',
            ],
            [
                'name' => 'A T',
                'email' => 'a@gmail.com',
                'password' => \Illuminate\Support\Facades\Hash::make('12345678'),
                'role_id' => '4',
            ],
            [
                'name' => 'B T',
                'email' => 'b@gmail.com',
                'password' => \Illuminate\Support\Facades\Hash::make('12345678'),
                'role_id' => '4',
            ],
            [
                'name' => 'C T',
                'email' => 'c@gmail.com',
                'password' => \Illuminate\Support\Facades\Hash::make('12345678'),
                'role_id' => '4',
            ],
            [
                'name' => 'D T',
                'email' => 'd@gmail.com',
                'password' => \Illuminate\Support\Facades\Hash::make('12345678'),
                'role_id' => '4',
            ],
            [
                'name' => 'E T',
                'email' => 'e@gmail.com',
                'password' => \Illuminate\Support\Facades\Hash::make('12345678'),
                'role_id' => '4',
            ],
            [
                'name' => 'F T',
                'email' => 'f@gmail.com',
                'password' => \Illuminate\Support\Facades\Hash::make('12345678'),
                'role_id' => '4',
            ],
            [
                'name' => 'G T',
                'email' => 'g@gmail.com',
                'password' => \Illuminate\Support\Facades\Hash::make('12345678'),
                'role_id' => '4',
            ],
            [
                'name' => 'H T',
                'email' => 'h@gmail.com',
                'password' => \Illuminate\Support\Facades\Hash::make('12345678'),
                'role_id' => '4',
            ],
            [
                'name' => 'I T',
                'email' => 'i@gmail.com',
                'password' => \Illuminate\Support\Facades\Hash::make('12345678'),
                'role_id' => '4',
            ],
            [
                'name' => 'J T',
                'email' => 'j@gmail.com',
                'password' => \Illuminate\Support\Facades\Hash::make('12345678'),
                'role_id' => '4',
            ],
            [
                'name' => 'K T',
                'email' => 'k@gmail.com',
                'password' => \Illuminate\Support\Facades\Hash::make('12345678'),
                'role_id' => '4',
            ],
            [
                'name' => 'Nguyễn Thị D',
                'email' => 'ntd@gmail.com',
                'password' => \Illuminate\Support\Facades\Hash::make('12345678'),
                'role_id' => '1',
            ],
            [
                'name' => 'Phan Văn E',
                'email' => 'pve@gmail.com',
                'password' => \Illuminate\Support\Facades\Hash::make('12345678'),
                'role_id' => '1',
            ],
        ]);
    }
}
