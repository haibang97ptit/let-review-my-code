<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->truncate();
        DB::table('permissions')->insert([
            //Customer
            [
                'name' => 'customer-access',
                'description' => 'Truy cập trang khách hàng',
                'feature' => 'customer',
                'permission_type' => 'access',
            ],
            [
                'name' => 'customer-view',
                'description' => 'Xem trang khách hàng',
                'feature' => 'customer',
                'permission_type' => 'view',
            ],
            [
                'name' => 'customer-create',
                'description' => 'Tạo khách hàng',
                'feature' => 'customer',
                'permission_type' => 'create',
            ],
            [
                'name' => 'customer-update',
                'description' => 'Cập nhật khách hàng',
                'feature' => 'customer',
                'permission_type' => 'update',
            ],
            [
                'name' => 'customer-delete',
                'description' => 'Xóa khách hàng',
                'feature' => 'customer',
                'permission_type' => 'delete',
            ],
            [
                'name' => 'customer-search',
                'description' => 'Tìm kiếm khách hàng',
                'feature' => 'customer',
                'permission_type' => 'search',
            ],
            [
                'name' => 'customer-staff-update',
                'description' => 'Cập nhật khách hàng của nhân viên',
                'feature' => 'customer',
                'permission_type' => 'staff-update',
            ],
            //Domain
            [
                'name' => 'domain-access',
                'description' => 'Truy cập trang domain',
                'feature' => 'domain',
                'permission_type' => 'access',
            ],
            [
                'name' => 'domain-view',
                'description' => 'Xem domain',
                'feature' => 'domain',
                'permission_type' => 'view',
            ],
            [
                'name' => 'domain-create',
                'description' => 'Tạo domain',
                'feature' => 'domain',
                'permission_type' => 'create',
            ],
            [
                'name' => 'domain-update',
                'description' => 'Cập nhật domain',
                'feature' => 'domain',
                'permission_type' => 'update',
            ],
            [
                'name' => 'domain-delete',
                'description' => 'Xóa domain',
                'feature' => 'domain',
                'permission_type' => 'delete',
            ],
            [
                'name' => 'domain-search',
                'description' => 'Tìm kiếm domain',
                'feature' => 'domain',
                'permission_type' => 'search',
            ],
            //Hosting
            [
                'name' => 'hosting-access',
                'description' => 'Truy cập trang hosting',
                'feature' => 'hosting',
                'permission_type' => 'access',
            ],
            [
                'name' => 'hosting-view',
                'description' => 'Xem hosting',
                'feature' => 'hosting',
                'permission_type' => 'view',
            ],
            [
                'name' => 'hosting-create',
                'description' => 'Tạo hosting',
                'feature' => 'hosting',
                'permission_type' => 'create',
            ],
            [
                'name' => 'hosting-update',
                'description' => 'Cập nhật hosting',
                'feature' => 'hosting',
                'permission_type' => 'update',
            ],
            [
                'name' => 'hosting-delete',
                'description' => 'Xóa hosting',
                'feature' => 'hosting',
                'permission_type' => 'delete',
            ],
            [
                'name' => 'hosting-search',
                'description' => 'Tìm kiếm hosting',
                'feature' => 'hosting',
                'permission_type' => 'search',
            ],
            //VPS
            [
                'name' => 'vps-access',
                'description' => 'Truy cập VPS',
                'feature' => 'vps',
                'permission_type' => 'access',
            ],
            [
                'name' => 'vps-view',
                'description' => 'Xem VPS',
                'feature' => 'vps',
                'permission_type' => 'view',
            ],
            [
                'name' => 'vps-create',
                'description' => 'Tạo VPS',
                'feature' => 'vps',
                'permission_type' => 'create',
            ],
            [
                'name' => 'vps-update',
                'description' => 'Cập nhật VPS',
                'feature' => 'vps',
                'permission_type' => 'update',
            ],
            [
                'name' => 'vps-delete',
                'description' => 'Xóa VPS',
                'feature' => 'vps',
                'permission_type' => 'delete',
            ],
            [
                'name' => 'vps-search',
                'description' => 'Tìm kiếm VPS',
                'feature' => 'vps',
                'permission_type' => 'search',
            ],
            //Email
            [
                'name' => 'email-access',
                'description' => 'Truy cập trang email',
                'feature' => 'email',
                'permission_type' => 'access',
            ],
            [
                'name' => 'email-view',
                'description' => 'Xem email',
                'feature' => 'email',
                'permission_type' => 'view',
            ],
            [
                'name' => 'email-create',
                'description' => 'Tạo email',
                'feature' => 'email',
                'permission_type' => 'create',
            ],
            [
                'name' => 'email-update',
                'description' => 'Cập nhật email',
                'feature' => 'email',
                'permission_type' => 'update',
            ],
            [
                'name' => 'email-delete',
                'description' => 'Xóa email',
                'feature' => 'email',
                'permission_type' => 'delete',
            ],
            [
                'name' => 'email-search',
                'description' => 'Tìm kiếm email',
                'feature' => 'email',
                'permission_type' => 'search',
            ],
            //SSL
            [
                'name' => 'ssl-access',
                'description' => 'Truy cập trang SSL',
                'feature' => 'ssl',
                'permission_type' => 'access',
            ],
            [
                'name' => 'ssl-view',
                'description' => 'Xem SSL',
                'feature' => 'ssl',
                'permission_type' => 'view',
            ],
            [
                'name' => 'ssl-create',
                'description' => 'Tạo SSL',
                'feature' => 'ssl',
                'permission_type' => 'create',
            ],
            [
                'name' => 'ssl-update',
                'description' => 'Cập nhật SSL',
                'feature' => 'ssl',
                'permission_type' => 'update',
            ],
            [
                'name' => 'ssl-delete',
                'description' => 'Xóa SSL',
                'feature' => 'ssl',
                'permission_type' => 'delete',
            ],
            [
                'name' => 'ssl-search',
                'description' => 'Tìm kiếm SSL',
                'feature' => 'ssl',
                'permission_type' => 'search',
            ],
            //Website
            [
                'name' => 'website-access',
                'description' => 'Truy cập trang Website',
                'feature' => 'website',
                'permission_type' => 'access',
            ],
            [
                'name' => 'website-view',
                'description' => 'Xem Website',
                'feature' => 'website',
                'permission_type' => 'view',
            ],
            [
                'name' => 'website-create',
                'description' => 'Tạo Website',
                'feature' => 'website',
                'permission_type' => 'create',
            ],
            [
                'name' => 'website-update',
                'description' => 'Cập nhật Website',
                'feature' => 'website',
                'permission_type' => 'update',
            ],
            [
                'name' => 'website-delete',
                'description' => 'Xóa Website',
                'feature' => 'website',
                'permission_type' => 'delete',
            ],
            [
                'name' => 'website-search',
                'description' => 'Tìm kiếm Website',
                'feature' => 'website',
                'permission_type' => 'search',
            ],
            //Software
            [
                'name' => 'software-access',
                'description' => 'Truy cập trang phần mềm',
                'feature' => 'software',
                'permission_type' => 'access',
            ],
            [
                'name' => 'software-view',
                'description' => 'Xem phần mềm',
                'feature' => 'software',
                'permission_type' => 'view',
            ],
            [
                'name' => 'software-create',
                'description' => 'Tạo phần mềm',
                'feature' => 'software',
                'permission_type' => 'create',
            ],
            [
                'name' => 'software-update',
                'description' => 'Cập nhật phần mềm',
                'feature' => 'software',
                'permission_type' => 'update',
            ],
            [
                'name' => 'software-delete',
                'description' => 'Xóa phần mềm',
                'feature' => 'software',
                'permission_type' => 'delete',
            ],
            [
                'name' => 'software-search',
                'description' => 'Tìm kiếm phần mềm',
                'feature' => 'software',
                'permission_type' => 'search',
            ],
            //Type-software
            [
                'name' => 'type-software-access',
                'description' => 'Truy cập trang loại phần mềm',
                'feature' => 'type-software',
                'permission_type' => 'access',
            ],
            [
                'name' => 'type-software-view',
                'description' => 'Xem loại phần mềm',
                'feature' => 'type-software',
                'permission_type' => 'view',
            ],
            [
                'name' => 'type-software-create',
                'description' => 'Tạo loại phần mềm',
                'feature' => 'type-software',
                'permission_type' => 'create',
            ],
            [
                'name' => 'type-software-update',
                'description' => 'Cập nhật loại phần mềm',
                'feature' => 'type-software',
                'permission_type' => 'update',
            ],
            [
                'name' => 'type-software-delete',
                'description' => 'Xóa loại phần mềm',
                'feature' => 'type-software',
                'permission_type' => 'delete',
            ],
            [
                'name' => 'type-software-search',
                'description' => 'Tìm kiếm loại phần mềm',
                'feature' => 'type-software',
                'permission_type' => 'search',
            ],
            //Staff
            [
                'name' => 'staff-access',
                'description' => 'Truy cập trang nhân viên',
                'feature' => 'staff',
                'permission_type' => 'access',
            ],
            [
                'name' => 'staff-view',
                'description' => 'Xem nhân viên',
                'feature' => 'staff',
                'permission_type' => 'view',
            ],
            [
                'name' => 'staff-create',
                'description' => 'Tạo nhân viên',
                'feature' => 'staff',
                'permission_type' => 'create',
            ],
            [
                'name' => 'staff-update',
                'description' => 'Cập nhật nhân viên',
                'feature' => 'staff',
                'permission_type' => 'update',
            ],
            [
                'name' => 'staff-delete',
                'description' => 'Xóa nhân viên',
                'feature' => 'staff',
                'permission_type' => 'delete',
            ],
            [
                'name' => 'staff-search',
                'description' => 'Tìm kiếm nhân viên',
                'feature' => 'staff',
                'permission_type' => 'search',
            ],
            //Position (staff)
            [
                'name' => 'position-access',
                'description' => 'Truy cập trang chức vụ nhân viên',
                'feature' => 'type-staff',
                'permission_type' => 'access',
            ],
            [
                'name' => 'position-view',
                'description' => 'Xem chức vụ nhân viên',
                'feature' => 'type-staff',
                'permission_type' => 'view',
            ],
            [
                'name' => 'position-create',
                'description' => 'Tạo chức vụ nhân viên',
                'feature' => 'type-staff',
                'permission_type' => 'create',
            ],
            [
                'name' => 'position-update',
                'description' => 'Cập nhật chức vụ nhân viên',
                'feature' => 'type-staff',
                'permission_type' => 'update',
            ],
            [
                'name' => 'position-delete',
                'description' => 'Xóa chức vụ nhân viên',
                'feature' => 'type-staff',
                'permission_type' => 'delete',
            ],
            [
                'name' => 'position-search',
                'description' => 'Tìm kiếm chức vụ nhân viên',
                'feature' => 'type-staff',
                'permission_type' => 'search',
            ],
            //Cash-book
            [
                'name' => 'cash-book-access',
                'description' => 'Truy cập trang sổ quỹ',
                'feature' => 'cash-book',
                'permission_type' => 'access',
            ],
            [
                'name' => 'cash-book-view',
                'description' => 'Xem sổ quỹ',
                'feature' => 'cash-book',
                'permission_type' => 'view',
            ],
            [
                'name' => 'cash-book-create',
                'description' => 'Tạo sổ quỹ',
                'feature' => 'cash-book',
                'permission_type' => 'create',
            ],
            [
                'name' => 'cash-book-update',
                'description' => 'Cập nhật sổ quỹ',
                'feature' => 'cash-book',
                'permission_type' => 'update',
            ],
            [
                'name' => 'cash-book-delete',
                'description' => 'Xóa sổ quỹ',
                'feature' => 'cash-book',
                'permission_type' => 'delete',
            ],
            [
                'name' => 'cash-book-search',
                'description' => 'Tìm kiếm sổ quỹ',
                'feature' => 'cash-book',
                'permission_type' => 'search',
            ],
            //Revenue
            [
                'name' => 'revenue-access',
                'description' => 'Truy cập trang phiếu thu',
                'feature' => 'revenue',
                'permission_type' => 'access',
            ],
            [
                'name' => 'revenue-view',
                'description' => 'Xem phiếu thu',
                'feature' => 'revenue',
                'permission_type' => 'view',
            ],
            [
                'name' => 'revenue-create',
                'description' => 'Tạo phiếu thu',
                'feature' => 'revenue',
                'permission_type' => 'create',
            ],
            [
                'name' => 'revenue-update',
                'description' => 'Cập nhật phiếu thu',
                'feature' => 'revenue',
                'permission_type' => 'update',
            ],
            [
                'name' => 'revenue-delete',
                'description' => 'Xóa phiếu thu',
                'feature' => 'revenue',
                'permission_type' => 'delete',
            ],
            [
                'name' => 'revenue-search',
                'description' => 'Tìm kiếm phiếu thu',
                'feature' => 'revenue',
                'permission_type' => 'search',
            ],
            //Expenditure
            [
                'name' => 'expenditure-access',
                'description' => 'Truy cập trang phiếu chi',
                'feature' => 'expenditure',
                'permission_type' => 'access',
            ],
            [
                'name' => 'expenditure-view',
                'description' => 'Xem phiếu chi',
                'feature' => 'expenditure',
                'permission_type' => 'view',
            ],
            [
                'name' => 'expenditure-create',
                'description' => 'Tạo phiếu chi',
                'feature' => 'expenditure',
                'permission_type' => 'create',
            ],
            [
                'name' => 'expenditure-update',
                'description' => 'Cập nhật phiếu chi',
                'feature' => 'expenditure',
                'permission_type' => 'update',
            ],
            [
                'name' => 'expenditure-delete',
                'description' => 'Xóa phiếu chi',
                'feature' => 'expenditure',
                'permission_type' => 'delete',
            ],
            [
                'name' => 'expenditure-search',
                'description' => 'Tìm kiếm phiếu chi',
                'feature' => 'expenditure',
                'permission_type' => 'search',
            ],
            //Invoice
            [
                'name' => 'invoice-access',
                'description' => 'Truy cập trang hóa đơn',
                'feature' => 'invoice',
                'permission_type' => 'access',
            ],
            [
                'name' => 'invoice-view',
                'description' => 'Xem hóa đơn',
                'feature' => 'invoice',
                'permission_type' => 'view',
            ],
            [
                'name' => 'invoice-create',
                'description' => 'Tạo hóa đơn',
                'feature' => 'invoice',
                'permission_type' => 'create',
            ],
            [
                'name' => 'invoice-update',
                'description' => 'Cập nhật hóa đơn',
                'feature' => 'invoice',
                'permission_type' => 'update',
            ],
            [
                'name' => 'invoice-delete',
                'description' => 'Xóa hóa đơn',
                'feature' => 'invoice',
                'permission_type' => 'delete',
            ],
            [
                'name' => 'invoice-search',
                'description' => 'Tìm kiếm hóa đơn',
                'feature' => 'invoice',
                'permission_type' => 'search',
            ],
            //Role
            [
                'name' => 'role-access',
                'description' => 'Truy cập trang vai trò',
                'feature' => 'role',
                'permission_type' => 'access',
            ],
            [
                'name' => 'role-view',
                'description' => 'Xem vai trò',
                'feature' => 'role',
                'permission_type' => 'view',
            ],
            [
                'name' => 'role-create',
                'description' => 'Tạo vai trò',
                'feature' => 'role',
                'permission_type' => 'create',
            ],
            [
                'name' => 'role-update',
                'description' => 'Cập nhật vai trò',
                'feature' => 'role',
                'permission_type' => 'update',
            ],
            [
                'name' => 'role-delete',
                'description' => 'Xóa vai trò',
                'feature' => 'role',
                'permission_type' => 'delete',
            ],
            [
                'name' => 'role-search',
                'description' => 'Tìm kiếm vai trò',
                'feature' => 'role',
                'permission_type' => 'search',
            ],
            //Permission
            [
                'name' => 'permission-access',
                'description' => 'Truy cập trang quyền',
                'feature' => 'permission',
                'permission_type' => 'access',
            ],
            [
                'name' => 'permission-view',
                'description' => 'Xem quyền',
                'feature' => 'permission',
                'permission_type' => 'view',
            ],
            [
                'name' => 'permission-create',
                'description' => 'Tạo quyền',
                'feature' => 'permission',
                'permission_type' => 'create',
            ],
            [
                'name' => 'permission-update',
                'description' => 'Cập nhật quyền',
                'feature' => 'permission',
                'permission_type' => 'update',
            ],
            [
                'name' => 'permission-delete',
                'description' => 'Xóa quyền',
                'feature' => 'permission',
                'permission_type' => 'delete',
            ],
            [
                'name' => 'permission-search',
                'description' => 'Tìm kiếm quyền',
                'feature' => 'permission',
                'permission_type' => 'search',
            ],
            //User
            [
                'name' => 'user-access',
                'description' => 'Truy cập trang tài khoản',
                'feature' => 'user',
                'permission_type' => 'access',
            ],
            [
                'name' => 'user-view',
                'description' => 'Xem tài khoản',
                'feature' => 'user',
                'permission_type' => 'view',
            ],
            [
                'name' => 'user-create',
                'description' => 'Tạo tài khoản',
                'feature' => 'user',
                'permission_type' => 'create',
            ],
            [
                'name' => 'user-update',
                'description' => 'Cập nhật tài khoản',
                'feature' => 'user',
                'permission_type' => 'update',
            ],
            [
                'name' => 'user-delete',
                'description' => 'Xóa tài khoản',
                'feature' => 'user',
                'permission_type' => 'delete',
            ],
            [
                'name' => 'user-search',
                'description' => 'Tìm kiếm tài khoản',
                'feature' => 'user',
                'permission_type' => 'search',
            ],
            //Register-service
            [
                'name' => 'register-service-access',
                'description' => 'Truy cập trang đăng ký dịch vụ',
                'feature' => 'register-service',
                'permission_type' => 'access',
            ],
            [
                'name' => 'register-service-view',
                'description' => 'Xem đăng ký dịch vụ',
                'feature' => 'register-service',
                'permission_type' => 'view',
            ],
            [
                'name' => 'register-service-create',
                'description' => 'Tạo đăng ký dịch vụ',
                'feature' => 'register-service',
                'permission_type' => 'create',
            ],
            [
                'name' => 'register-service-update',
                'description' => 'Cập nhật đăng ký dịch vụ',
                'feature' => 'register-service',
                'permission_type' => 'update',
            ],
            [
                'name' => 'register-service-delete',
                'description' => 'Xóa đăng ký dịch vụ',
                'feature' => 'register-service',
                'permission_type' => 'delete',
            ],
            [
                'name' => 'register-service-search',
                'description' => 'Tìm kiếm đăng ký dịch vụ',
                'feature' => 'register-service',
                'permission_type' => 'search',
            ],
            //Register-soft
            [
                'name' => 'register-soft-access',
                'description' => 'Truy cập trang đăng ký phần mềm',
                'feature' => 'register-soft',
                'permission_type' => 'access',
            ],
            [
                'name' => 'register-soft-view',
                'description' => 'Xem đăng ký phần mềm',
                'feature' => 'register-soft',
                'permission_type' => 'view',
            ],
            [
                'name' => 'register-soft-create',
                'description' => 'Tạo đăng ký phần mềm',
                'feature' => 'register-soft',
                'permission_type' => 'create',
            ],
            [
                'name' => 'register-soft-update',
                'description' => 'Cập nhật đăng ký phần mềm',
                'feature' => 'register-soft',
                'permission_type' => 'update',
            ],
            [
                'name' => 'register-soft-delete',
                'description' => 'Xóa đăng ký phần mềm',
                'feature' => 'register-soft',
                'permission_type' => 'delete',
            ],
            [
                'name' => 'register-soft-search',
                'description' => 'Tìm kiếm đăng ký phần mềm',
                'feature' => 'register-soft',
                'permission_type' => 'search',
            ],
            //Cash book management
            [
                'name' => 'cash-book-management-access',
                'description' => 'Truy cập quản lý sổ quỹ',
                'feature' => 'cash-book-management',
                'permission_type' => 'access',
            ],
            //Service management
            [
                'name' => 'service-management-access',
                'description' => 'Truy cập quản lý dịch vụ',
                'feature' => 'service-management',
                'permission_type' => 'access',
            ],
            //Software management
            [
                'name' => 'software-management-access',
                'description' => 'Truy cập quản lý phần mềm',
                'feature' => 'software-management',
                'permission_type' => 'access',
            ],
            //Staff management
            [
                'name' => 'staff-management-access',
                'description' => 'Truy cập quản lý nhân viên',
                'feature' => 'staff-management',
                'permission_type' => 'access',
            ],
            //Account management
            [
                'name' => 'account-management-access',
                'description' => 'Truy cập quản lý tài khoản',
                'feature' => 'account-management',
                'permission_type' => 'access',
            ],
        ]);
    }
}
