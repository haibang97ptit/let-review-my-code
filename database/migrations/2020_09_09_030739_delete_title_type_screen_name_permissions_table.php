<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteTitleTypeScreenNamePermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('permissions')) {
            Schema::table('permissions', function (Blueprint $table) {
                if (Schema::hasColumn('permissions', 'title')) {
                    $table->dropColumn('title');
                }
                if (Schema::hasColumn('permissions', 'type')) {
                    $table->dropColumn('type');
                }
                if (Schema::hasColumn('permissions', 'screen_name')) {
                    $table->dropColumn('screen_name');
                }
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
