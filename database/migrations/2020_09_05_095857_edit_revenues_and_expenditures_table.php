<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditRevenuesAndExpendituresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('expenditures')) {
            Schema::table('expenditures', function (Blueprint $table) {
                if (!Schema::hasColumn('expenditures', 'status')) {
                    $table->string('status')->nullable()->after('price');
                }
            });
        }
        if (Schema::hasTable('revenues')) {
            Schema::table('revenues', function (Blueprint $table) {
                if (!Schema::hasColumn('revenues', 'status')) {
                    $table->string('status')->nullable()->after('price');
                }
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
