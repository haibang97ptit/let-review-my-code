<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTimeUseColumnToRegisterServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('register_services', function (Blueprint $table) {
            if (!Schema::hasColumn('register_services', 'time_use')) {
                $table->string('time_use');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('register_services', function (Blueprint $table) {
            if (Schema::hasColumn('register_services', 'time_use')) {
                $table->dropColumn('time_use');
            }
        });
    }
}
