<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCodeColumnInRegisterSoftwaresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('register_softwares', function (Blueprint $table) {
            if (!Schema::hasColumn('register_softwares', 'code')) {
                $table->string('code')->after('id');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('register_softwares', function (Blueprint $table) {
            if (Schema::hasColumn('register_softwares', 'code')) {
                $table->dropColumn('code');
            }
        });
    }
}
