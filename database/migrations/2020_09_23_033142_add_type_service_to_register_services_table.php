<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeServiceToRegisterServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('register_services', function (Blueprint $table) {
            if (!Schema::hasColumn('register_services', 'type_service')) {
                $table->string('type_service');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('register_services', function (Blueprint $table) {
            if (Schema::hasColumn('register_services', 'type_service')) {
                $table->dropColumn('type_service');
            }
        });
    }
}
