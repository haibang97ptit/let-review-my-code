<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteSixIdServicesInRegisterServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('register_services', function (Blueprint $table) {
            if (Schema::hasColumn('register_services', 'id_domain')) {
                $table->dropColumn('id_domain');
            }
            if (Schema::hasColumn('register_services', 'id_hosting')) {
                $table->dropColumn('id_hosting');
            }
            if (Schema::hasColumn('register_services', 'id_vps')) {
                $table->dropColumn('id_vps');
            }
            if (Schema::hasColumn('register_services', 'id_ssl')) {
                $table->dropColumn('id_ssl');
            }
            if (Schema::hasColumn('register_services', 'id_email')) {
                $table->dropColumn('id_email');
            }
            if (Schema::hasColumn('register_services', 'id_website')) {
                $table->dropColumn('id_website');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('register_services', function (Blueprint $table) {
            //
        });
    }
}
