<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTimeUseColumnToRegisterSoftwaresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('register_softwares', function (Blueprint $table) {
            if (!Schema::hasColumn('register_softwares', 'time_use')) {
                $table->string('time_use')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('register_softwares', function (Blueprint $table) {
            if (Schema::hasColumn('register_softwares', 'time_use')) {
                $table->dropColumn('time_use');
            }
        });
    }
}
