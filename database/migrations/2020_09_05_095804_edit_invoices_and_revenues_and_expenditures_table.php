<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditInvoicesAndRevenuesAndExpendituresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('invoices')) {
            Schema::table('invoices', function (Blueprint $table) {
                if (!Schema::hasColumn('invoices', 'code')) {
                    $table->string('code')->nullable();
                }
                if (!Schema::hasColumn('invoices', 'status')) {
                    $table->string('status')->nullable();
                }

            });
        }

        if (Schema::hasTable('revenues')) {
            Schema::table('revenues', function (Blueprint $table) {
                if (!Schema::hasColumn('revenues', 'code')) {
                    $table->string('code')->nullable();
                }
                if (!Schema::hasColumn('revenues', 'id_staff')) {
                    $table->string('id_staff')->nullable();
                }
                if (!Schema::hasColumn('revenues', 'price')) {
                    $table->string('price')->nullable();
                }
                if (!Schema::hasColumn('revenues', 'description')) {
                    $table->string('description')->nullable();
                }
            });
        }

        if (Schema::hasTable('expenditures')) {
            Schema::table('expenditures', function (Blueprint $table) {
                if (!Schema::hasColumn('expenditures', 'code')) {
                    $table->string('code')->nullable();
                }
                if (!Schema::hasColumn('expenditures', 'id_staff')) {
                    $table->string('id_staff')->nullable();
                }
                if (!Schema::hasColumn('expenditures', 'price')) {
                    $table->string('price')->nullable();
                }
                if (!Schema::hasColumn('expenditures', 'description')) {
                    $table->string('description')->nullable();
                }
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
