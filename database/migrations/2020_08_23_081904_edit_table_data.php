<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditTableData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('register_services')) {
            Schema::table('register_services', function (Blueprint $table) {
                if (!Schema::hasColumn('register_services', 'status_register')) {
                    $table->string('status_register')->nullable()->after('notes');
                }
                if (!Schema::hasColumn('register_services', 'price')) {
                    $table->string('price')->nullable()->after('status_register');
                }
            });
        }

        if (Schema::hasTable('register_soft')) {
            Schema::table('register_soft', function (Blueprint $table) {
                if (!Schema::hasColumn('register_soft', 'status_register')) {
                    $table->string('status_register')->nullable()->after('notes');
                }
                if (!Schema::hasColumn('register_soft', 'price')) {
                    $table->string('price')->nullable()->after('status_register');
                }
            });
        }

        if (Schema::hasTable('invoices')) {
            Schema::table('invoices', function (Blueprint $table) {
                if (!Schema::hasColumn('invoices', 'id_register_soft')) {
                    $table->string('id_register_soft')->nullable()->after('id_register_service');
                }
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('register_services')) {
            Schema::table('register_services', function (Blueprint $table) {
                if (!Schema::hasColumn('register_services', 'status_register')) {
                    $table->dropColumn('status_register');
                }
                if (!Schema::hasColumn('register_services', 'price')) {
                    $table->dropColumn('price');
                }
            });
        }
        if (Schema::hasTable('register_soft')) {
            Schema::table('register_soft', function (Blueprint $table) {
                if (!Schema::hasColumn('register_soft', 'status_register')) {
                    $table->dropColumn('status_register');
                }
                if (!Schema::hasColumn('register_soft', 'price')) {
                    $table->dropColumn('price');
                }
            });
        }
        if (Schema::hasTable('invoices')) {
            Schema::table('invoices', function (Blueprint $table) {
                if (!Schema::hasColumn('invoices', 'id_register_soft')) {
                    $table->dropColumn('id_register_soft');
                }
            });
        }
    }
}
