<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusColumnToRegisterSoftwaresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('register_softwares', function (Blueprint $table) {
            if (!Schema::hasColumn('register_softwares', 'status')) {
                $table->string('status');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('register_softwares', function (Blueprint $table) {
            if (Schema::hasColumn('register_softwares', 'status')) {
                $table->dropColumn('status');
            }
        });
    }
}
