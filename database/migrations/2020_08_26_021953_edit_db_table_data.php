<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditDbTableData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('invoices')) {
            Schema::table('invoices', function (Blueprint $table) {
                if (!Schema::hasColumn('invoices', 'id_staff')) {
                    $table->string('id_staff')->nullable()->after('id_customer');
                }
                if (!Schema::hasColumn('invoices', 'description')) {
                    $table->string('description')->nullable()->after('id_staff');
                }
            });
        }
        if (Schema::hasTable('register_services')) {
            Schema::table('register_services', function (Blueprint $table) {
                if (!Schema::hasColumn('register_services', 'id_staff')) {
                    $table->string('id_staff')->nullable()->after('id_customer');
                }
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('invoices')) {
            Schema::table('invoices', function (Blueprint $table) {
                if (!Schema::hasColumn('invoices', 'id_staff')) {
                    $table->dropColumn('id_staff');
                }
                if (!Schema::hasColumn('invoices', 'description')) {
                    $table->dropColumn('description');
                }
            });
        }
        if (Schema::hasTable('register_services')) {
            Schema::table('register_services', function (Blueprint $table) {
                if (!Schema::hasColumn('register_services', 'id_staff')) {
                    $table->dropColumn('id_staff');
                }
            });
        }
    }
}
