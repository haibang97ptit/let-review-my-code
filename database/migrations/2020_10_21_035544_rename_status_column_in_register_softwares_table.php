<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameStatusColumnInRegisterSoftwaresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('register_softwares', function (Blueprint $table) {
            if (Schema::hasColumn('register_softwares', 'status')) {
                $table->renameColumn('status', 'version');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('register_softwares', function (Blueprint $table) {
            if (Schema::hasColumn('register_softwares', 'status')) {
                $table->renameColumn('version', 'status');
            }
        });
    }
}
