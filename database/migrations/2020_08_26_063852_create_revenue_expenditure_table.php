<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRevenueExpenditureTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cash_books', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code');
            $table->string('status');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('revenues', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code');
            $table->string('id_staff');
            $table->string('price');
            $table->string('description');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('expenditures', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code');
            $table->string('id_staff');
            $table->string('price');
            $table->string('description');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cash_books');
        Schema::dropIfExists('revenues');
        Schema::dropIfExists('expenditures');
    }
}
