<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditDb extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('customers')) {
            Schema::table('customers', function (Blueprint $table) {
                if (!Schema::hasColumn('customers', 'images')) {
                    $table->string('images')->nullable()->after('notes');
                }
            });
        }
        if (Schema::hasTable('domains')) {
            Schema::table('domains', function (Blueprint $table) {
                if (!Schema::hasColumn('domains', 'images')) {
                    $table->string('images')->nullable()->after('notes');
                }
            });
        }
        if (Schema::hasTable('emails')) {
            Schema::table('emails', function (Blueprint $table) {
                if (!Schema::hasColumn('emails', 'images')) {
                    $table->string('images')->nullable()->after('notes');
                }
            });
        }
        if (Schema::hasTable('hosting')) {
            Schema::table('hosting', function (Blueprint $table) {
                if (!Schema::hasColumn('hosting', 'images')) {
                    $table->string('images')->nullable()->after('notes');
                }
            });
        }
        if (Schema::hasTable('softwares')) {
            Schema::table('softwares', function (Blueprint $table) {
                if (!Schema::hasColumn('softwares', 'images')) {
                    $table->string('images')->nullable()->after('notes');
                }
            });
        }
        if (Schema::hasTable('ssls')) {
            Schema::table('ssls', function (Blueprint $table) {
                if (!Schema::hasColumn('ssls', 'images')) {
                    $table->string('images')->nullable()->after('notes');
                }
            });
        }
        if (Schema::hasTable('staffs')) {
            Schema::table('staffs', function (Blueprint $table) {
                if (!Schema::hasColumn('staffs', 'images')) {
                    $table->string('images')->nullable()->after('role');
                }
            });
        }
        if (Schema::hasTable('typesoftwares')) {
            Schema::table('typesoftwares', function (Blueprint $table) {
                if (!Schema::hasColumn('typesoftwares', 'images')) {
                    $table->string('images')->nullable()->after('description');
                }
            });
        }
        if (Schema::hasTable('typestaffs')) {
            Schema::table('typestaffs', function (Blueprint $table) {
                if (!Schema::hasColumn('typestaffs', 'images')) {
                    $table->string('images')->nullable()->after('description');
                }
            });
        }
        if (Schema::hasTable('users')) {
            Schema::table('users', function (Blueprint $table) {
                if (!Schema::hasColumn('users', 'images')) {
                    $table->string('images')->nullable()->after('email');
                }
            });
        }
        if (Schema::hasTable('vpss')) {
            Schema::table('vpss', function (Blueprint $table) {
                if (!Schema::hasColumn('vpss', 'images')) {
                    $table->string('images')->nullable()->after('notes');
                }
            });
        }
        if (Schema::hasTable('websites')) {
            Schema::table('websites', function (Blueprint $table) {
                if (!Schema::hasColumn('websites', 'images')) {
                    $table->string('images')->nullable()->after('notes');
                }
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('customers')) {
            Schema::table('customers', function (Blueprint $table) {
                if (!Schema::hasColumn('customers', 'images')) {
                    $table->dropColumn('images');
                }
            });
        }
        if (Schema::hasTable('domains')) {
            Schema::table('domains', function (Blueprint $table) {
                if (!Schema::hasColumn('domains', 'images')) {
                    $table->dropColumn('images');
                }
            });
        }
        if (Schema::hasTable('emails')) {
            Schema::table('emails', function (Blueprint $table) {
                if (!Schema::hasColumn('emails', 'images')) {
                    $table->dropColumn('images');
                }
            });
        }
        if (Schema::hasTable('hosting')) {
            Schema::table('hosting', function (Blueprint $table) {
                if (!Schema::hasColumn('hosting', 'images')) {
                    $table->dropColumn('images');
                }
            });
        }
        if (Schema::hasTable('softwares')) {
            Schema::table('softwares', function (Blueprint $table) {
                if (!Schema::hasColumn('softwares', 'images')) {
                    $table->dropColumn('images');
                }
            });
        }
        if (Schema::hasTable('ssls')) {
            Schema::table('ssls', function (Blueprint $table) {
                if (!Schema::hasColumn('ssls', 'images')) {
                    $table->dropColumn('images');
                }
            });
        }
        if (Schema::hasTable('staffs')) {
            Schema::table('staffs', function (Blueprint $table) {
                if (!Schema::hasColumn('staffs', 'images')) {
                    $table->dropColumn('images');
                }
            });
        }
        if (Schema::hasTable('typesoftwares')) {
            Schema::table('typesoftwares', function (Blueprint $table) {
                if (!Schema::hasColumn('typesoftwares', 'images')) {
                    $table->dropColumn('images');
                }
            });
        }
        if (Schema::hasTable('typestaffs')) {
            Schema::table('typestaffs', function (Blueprint $table) {
                if (!Schema::hasColumn('typestaffs', 'images')) {
                    $table->dropColumn('images');
                }
            });
        }
        if (Schema::hasTable('users')) {
            Schema::table('users', function (Blueprint $table) {
                if (!Schema::hasColumn('users', 'images')) {
                    $table->dropColumn('images');
                }
            });
        }
        if (Schema::hasTable('vpss')) {
            Schema::table('vpss', function (Blueprint $table) {
                if (!Schema::hasColumn('vpss', 'images')) {
                    $table->dropColumn('images');
                }
            });
        }
        if (Schema::hasTable('websites')) {
            Schema::table('websites', function (Blueprint $table) {
                if (!Schema::hasColumn('websites', 'images')) {
                    $table->dropColumn('images');
                }
            });
        }
    }
}
