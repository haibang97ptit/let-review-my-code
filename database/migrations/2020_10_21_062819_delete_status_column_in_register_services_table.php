<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteStatusColumnInRegisterServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('register_services', function (Blueprint $table) {
            if (Schema::hasColumn('register_services', 'status')) {
                $table->dropColumn('status');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('register_services', function (Blueprint $table) {
            if (!Schema::hasColumn('register_services', 'status')) {
                $table->string('status');
            }
        });
    }
}
