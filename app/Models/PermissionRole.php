<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PermissionRole extends Model
{
    protected $table = 'permission_role';
    protected $fillable = ['id', 'role_id', 'permission_id'];
    use SoftDeletes;
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    public $messages = [];

//    public $rules = [
//        'name' => 'required|unique:permissions',
//        'description' => 'required',
//    ];
//
//    public function __construct()
//    {
//        parent::__construct();
//        $this->messages = [
//            'name.required' => __('permission.name').__('permission.required'),
//            'name.unique' => __('permission.name').__('permission.exist'),
//            'description.required' => __('permission.description').__('permission.required'),
//        ];
//    }
}
