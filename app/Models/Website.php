<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Website extends Model
{
    use SoftDeletes;
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $table = 'websites';
    protected $fillable = ['id', 'name', 'type_website', 'price', 'notes'];

    public $messages = [];
    public $rules = [
        'name' => 'required|unique:websites',
        'type_website' => 'required',
        'price' => 'required',
    ];

    public function __construct()
    {
        parent::__construct();
        $this->messages = [
            'name.required' => __('website.name').__('general.required'),
            'name.unique' => __('website.name').__('general.exist'),
            'type_website.required' => __('website.type-website').__('general.required'),
            'price.required' => __('website.price').__('general.required'),
        ];
    }

    public function getAll($key, $paginate) {
        $websites = Website::where('name', 'like', '%'.$key.'%')
            ->orWhere('type_website', 'like', '%'.$key.'%')
            ->orWhere('price', 'like', '%'.$key.'%')
            ->orderBy('id', 'desc')
            ->paginate($paginate);
        return $websites;
    }
}
