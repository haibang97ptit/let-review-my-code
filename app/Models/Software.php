<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Software extends Model
{
    use SoftDeletes;
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $table = 'softwares';
    protected $fillable = ['id', 'name', 'description'];

    public $messages = [];
    public $rules = [
        'name' => 'required|unique:softwares,name,NULL,id,deleted_at,NULL',
        'description' => 'required',
    ];

    public function __construct()
    {
        parent::__construct();
        $this->messages = [
            'name.required' => __('software.name').__('general.required'),
            'name.unique'=> __('software.name').__('general.exist'),
            'description.required' => __('software.description').__('general.required'),
        ];
    }

    public function getAll($key, $paginate) {
        $softwares = Software::where('name', 'like', '%'.$key.'%')
            ->orderBy('name', 'asc')
            ->paginate($paginate);
        return $softwares;
    }

    public function getAllTrash($key, $paginate) {
        $softwares = Software::onlyTrashed()
            ->where('name', 'like', '%'.$key.'%')
            ->orderBy('name', 'asc')
            ->paginate($paginate);
        return $softwares;
    }
}
