<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SoftwarePackage extends Model
{
    use SoftDeletes;
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $table = 'software_packages';
    protected $fillable = ['id', 'name', 'quantity_branch', 'quantity_staff', 'quantity_acc', 'quantity_product', 'quantity_bill', 'price', 'notes'];

    public $messages = [];
    public $rules = [
        'name' => 'required|unique:software_packages,name,NULL,id,deleted_at,NULL',
        'price' => 'required',
        'quantity_branch' => 'required',
        'quantity_staff' => 'required',
        'quantity_acc' => 'required',
        'quantity_product' => 'required',
        'quantity_bill' => 'required',
    ];

    public function __construct()
    {
        parent::__construct();
        $this->messages = [
            'name.required' => __('software-package.name').__('general.required'),
            'name.unique' => __('software-package.name').__('general.exist'),
            'price.required' => __('software-package.price').__('general.required'),
            'quantity_branch.required' => __('software-package.quantity-branch').__('general.required'),
            'quantity_staff.required' => __('software-package.quantity-staff').__('general.required'),
            'quantity_acc.required' => __('software-package.quantity-acc').__('general.required'),
            'quantity_product.required' => __('software-package.quantity-product').__('general.required'),
            'quantity_bill.required' => __('software-package.quantity-bill').__('general.required'),
        ];
    }

    public function getAll($key, $paginate) {
        $softwarePackages = SoftwarePackage::where('name', 'like', '%'.$key.'%')
            ->orWhere('quantity_branch', 'like', '%'.$key.'%')
            ->orWhere('quantity_staff', 'like', '%'.$key.'%')
            ->orWhere('quantity_acc', 'like', '%'.$key.'%')
            ->orWhere('quantity_product', 'like', '%'.$key.'%')
            ->orWhere('quantity_bill', 'like', '%'.$key.'%')
            ->orWhere('price', 'like', '%'.$key.'%')
            ->orderBy('name', 'asc')
            ->paginate($paginate);
        return $softwarePackages;
    }

    public function getAllTrash($key, $paginate) {
        $softwarePackages = SoftwarePackage::onlyTrashed()
            ->where('name', 'like', '%'.$key.'%')
            ->orderBy('name', 'asc')
            ->paginate($paginate);
        return $softwarePackages;
    }

}
