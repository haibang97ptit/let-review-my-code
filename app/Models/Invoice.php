<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Invoice extends Model
{
    use SoftDeletes;
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $message = [];
    public $rules = [
//        'code' => 'required',


    ];
    protected $table = 'invoices';
    protected $fillable = ['id', 'code','price','VAT_price','id_staff','total','id_customer','id_register_service','id_register_soft'];
    public function __construct()
    {
        parent::__construct();
        $this->message = [
//            'service_name.required' => 'Vui lòng chọn khách hàng',

        ];
    }

}
