<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CashBook extends Model
{
//    use SoftDeletes;
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $table = 'cash_books';
    protected $fillable = ['id', 'code', 'type'];

    public function getAll($key, $paginate) {
        $cashBook1 = CashBook::join('register_services', 'register_services.code', '=', 'cash_books.code')
            ->join('customers as c', 'c.id', '=', 'register_services.id_customer')
            ->where('cash_books.code', 'like', '%'.$key.'%')
            ->select('register_services.*', 'c.first_name as first_name',
                'c.last_name as last_name', 'c.email as customer_email', 'cash_books.*')
            ->orderBy('register_services.id', 'desc')
            ->get();
        $cashBook2 = CashBook::join('register_softwares', 'register_softwares.code', '=', 'cash_books.code')
            ->join('customers', 'customers.id', '=', 'register_softwares.id_customer')
            ->join('softwares', 'softwares.id', '=', 'register_softwares.id_software')
            ->where('cash_books.code', 'like', '%'.$key.'%')
            ->select('customers.first_name as first_name', 'customers.last_name as last_name', 'customers.email as customer_email',
                'softwares.name as software_name', 'register_softwares.*', 'cash_books.*')
            ->orderBy('register_softwares.id', 'desc')
            ->get();
        $cashBook = collect();
        foreach ($cashBook1 as $a)
            $cashBook->push($a);
        foreach ($cashBook2 as $b)
            $cashBook->push($b);
//        dd($cashBook->paginate(10));
        return $cashBook->paginate($paginate);
    }

}
