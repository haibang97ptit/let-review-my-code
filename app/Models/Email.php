<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Email extends Model
{
    use SoftDeletes;
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $table = 'emails';
    protected $fillable = ['id', 'name', 'price', 'capacity', 'email_number', 'email_forwarder', 'email_list', 'parked_domains', 'notes'];

    public $messages = [];
    public $rules = [
        'name' => 'required|unique:emails',
        'price' => 'required',
        'capacity' => 'required',
        'email_number' => 'required',
        'email_forwarder' => 'required',
        'email_list' => 'required',
        'parked_domains' => 'required',
    ];

    public function __construct()
    {
        parent::__construct();
        $this->messages = [
            'name.required' => __('email.name').__('general.required'),
            'name.unique' => __('email.name').__('general.exist'),
            'price.required' => __('email.price').__('general.required'),
            'capacity.required' => __('email.capacity').__('general.required'),
            'email_number.required' => __('email.email-number').__('general.required'),
            'email_forwarder.required' => __('email.email-forwarder').__('general.required'),
            'email_list.required' => __('email.email-list').__('general.required'),
            'parked_domains.required' => __('email.parked-domains').__('general.required'),
        ];
    }

    public function getAll($key, $paginate) {
        $emails = Email::where('name', 'like', '%'.$key.'%')
            ->orWhere('price', 'like', '%'.$key.'%')
            ->orWhere('capacity', 'like', '%'.$key.'%')
            ->orWhere('email_number', 'like', '%'.$key.'%')
            ->orWhere('email_forwarder', 'like', '%'.$key.'%')
            ->orWhere('email_list', 'like', '%'.$key.'%')
            ->orWhere('parked_domains', 'like', '%'.$key.'%')
            ->orderBy('id', 'desc')
            ->paginate($paginate);
        return $emails;
    }
}
