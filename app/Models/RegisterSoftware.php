<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RegisterSoftware extends Model
{
    use SoftDeletes;
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $table = 'register_softwares';
    protected $fillable = ['id', 'start_date', 'end_date', 'id_customer', 'notes', 'status_register', 'price', 'address_domain', 'id_software', 'id_software_package', 'time_use', 'version'];

    public $messages = [];
    public $rules = [
        'id_customer' => 'required',
        'address_domain' => 'required',
        'id_software' => 'required',
        'id_software_package' => 'required',
        'time_use' => 'required',
        'start_date' => 'required',
    ];

    public function __construct()
    {
        parent::__construct();
        $this->messages = [
            'id_customer.required' => __('register-software.customer').__('general.required'),
            'address_domain.required' => __('register-software.domain-address').__('general.required'),
            'id_software.required' => __('register-software.software').__('general.required'),
            'id_software_package.required' => __('register-software.software-package').__('general.required'),
            'time_use.required' => __('register-software.time-use').__('general.required'),
            'start_date.required' => __('register-software.start-date').__('general.required'),
        ];
    }

    public function getAll($paginate) {
        $registerSoftwares = RegisterSoftware::join('customers', 'customers.id', '=', 'register_softwares.id_customer')
            ->join('softwares', 'softwares.id', '=', 'register_softwares.id_software')
            ->select('customers.first_name as first_name', 'customers.last_name as last_name', 'customers.email as customer_email',
                'softwares.name as software_name', 'register_softwares.*')
            ->orderBy('register_softwares.id', 'desc')
            ->paginate($paginate);
        return $registerSoftwares;
    }

    public function createCode() {
        $dt = Carbon::now();
        $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $code = 'TQ-SW-'.substr(str_shuffle($permitted_chars), 0, 5);
        return $code;
    }

    public function getSearchRow($request, $paginate) {
        $registerSoftwares = RegisterSoftware::join('customers as c', 'c.id', '=', 'register_softwares.id_customer')
            ->join('softwares as s', 's.id', '=', 'register_softwares.id_software')
            ->where('register_softwares.id_customer', '=', $request->customer_select)
            ->orWhere('register_softwares.id_software', '=', $request->software_select)
            ->orWhere('register_softwares.version', '=', $request->version_select)
            ->orWhere('register_softwares.status_register', '=', $request->status_select)
            ->select('c.first_name as first_name', 'c.last_name as last_name', 'c.email as customer_email',
                's.name as software_name', 'register_softwares.*')
            ->orderBy('register_softwares.id', 'desc')
            ->paginate($paginate);
        return $registerSoftwares;
    }

    public function getAllTrash($paginate) {
        $registerSoftwares = RegisterSoftware::onlyTrashed()
            ->join('customers', 'customers.id', '=', 'register_softwares.id_customer')
            ->join('softwares', 'softwares.id', '=', 'register_softwares.id_software')
            ->select('customers.first_name as first_name', 'customers.last_name as last_name', 'customers.email as customer_email',
                'softwares.name as software_name', 'register_softwares.*')
            ->orderBy('register_softwares.id', 'desc')
            ->paginate($paginate);
        return $registerSoftwares;
    }

    public function getTrashSearchRow($request, $paginate) {
        $registerSoftwares = RegisterSoftware::onlyTrashed()
            ->join('customers as c', 'c.id', '=', 'register_softwares.id_customer')
            ->join('softwares as s', 's.id', '=', 'register_softwares.id_software')
            ->where('register_softwares.id_customer', '=', $request->customer_select)
            ->orWhere('register_softwares.id_software', '=', $request->software_select)
            ->orWhere('register_softwares.version', '=', $request->version_select)
            ->orWhere('register_softwares.status_register', '=', $request->status_select)
            ->select('c.first_name as first_name', 'c.last_name as last_name', 'c.email as customer_email',
                's.name as software_name', 'register_softwares.*')
            ->orderBy('register_softwares.id', 'desc')
            ->paginate($paginate);
        return $registerSoftwares;
    }
}
