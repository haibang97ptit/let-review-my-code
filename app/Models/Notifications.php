<?php

namespace App\Models;

use App\Notifications\NotificationDatabase;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Pusher\Pusher;

class Notifications extends Model
{
    use SoftDeletes;
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $table = 'notifications';

    public function createNotify($data){

        $notify['title'] = $data['title'];
        $notify['content'] = $data['content'];
        $notify['code'] = $data['code'];
        $notify['department'] = $data['department'];

        $options = array(
            'cluster' => 'ap1',
            'encrypted' => true
        );
        $pusher = new Pusher(
            env('PUSHER_APP_KEY'),
            env('PUSHER_APP_SECRET'),
            env('PUSHER_APP_ID'),
            $options
        );
        $pusher->trigger('Notify', 'staff.' . $notify['department'], $notify);

        $all_staffs = Staff::where('department_code', $notify['department'])->get();
        foreach ($all_staffs as $staff) {
            $staff->notify(new NotificationDatabase($notify));
        }
    }
}
