<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ServiceUse extends Model
{
    public function getAll($paginate) {
        $registerSoftwares = RegisterSoftware::join('customers', 'customers.id', '=', 'register_softwares.id_customer')
            ->join('softwares', 'softwares.id', '=', 'register_softwares.id_software')
            ->where('register_softwares.status_register', '=', 'paid')
            ->select('customers.first_name as first_name', 'customers.last_name as last_name', 'customers.email as customer_email',
                'softwares.name as software_name', 'register_softwares.*')
            ->orderBy('register_softwares.id', 'desc');
        $registerSoftwares = $registerSoftwares->addSelect(DB::raw("'software' as type"));
        $registerSoftwares = $registerSoftwares->get();
        $registerServices = RegisterService::join('customers as c', 'c.id', '=', 'register_services.id_customer')
            ->where('register_services.status_register', '=', 'paid')
            ->select('register_services.*', 'c.first_name as first_name',
                'c.last_name as last_name', 'c.email as customer_email')
            ->orderBy('register_services.id', 'desc');
        $registerServices = $registerServices->addSelect(DB::raw("'service' as type"));
        $registerServices = $registerServices->get();
        $serviceUses = collect();
        foreach ($registerSoftwares as $a)
            $serviceUses->push($a);
        foreach ($registerServices as $b)
            $serviceUses->push($b);
        return $serviceUses->paginate($paginate);
    }

    public function filterDomain($paginate, $key) {
        $domains = RegisterService::join('customers as c', 'c.id', '=', 'register_services.id_customer')
            ->where('register_services.status_register', '=', 'paid')
            ->where('type_service', '=', 'domain')
            ->where(function ($query) use ($key) {
                $query->where('c.first_name', 'like', '%'.$key.'%')
                    ->orWhere('c.last_name', 'like', '%'.$key.'%')
                    ->orWhere('c.email', 'like', '%'.$key.'%')
                    ->orWhere('register_services.address_domain', 'like', '%'.$key.'%');
            })
            ->select('register_services.*', 'c.first_name as first_name',
                'c.last_name as last_name', 'c.email as customer_email')
            ->orderBy('register_services.id', 'desc');
        $domains = $domains->addSelect(DB::raw("'service' as type"));
        $domains = $domains->paginate($paginate);
        return $domains;
    }

    public function filterHosting($paginate, $key) {
        $hostings = RegisterService::join('customers as c', 'c.id', '=', 'register_services.id_customer')
            ->where('register_services.status_register', '=', 'paid')
            ->where('register_services.type_service', '=', 'hosting')
            ->where(function ($query) use ($key) {
                $query->where('c.first_name', 'like', '%'.$key.'%')
                    ->orWhere('c.last_name', 'like', '%'.$key.'%')
                    ->orWhere('c.email', 'like', '%'.$key.'%')
                    ->orWhere('register_services.address_domain', 'like', '%'.$key.'%');
            })
            ->select('register_services.*', 'c.first_name as first_name',
                'c.last_name as last_name', 'c.email as customer_email')
            ->orderBy('register_services.id', 'desc');
        $hostings = $hostings->addSelect(DB::raw("'service' as type"));
        $hostings = $hostings->paginate($paginate);
        return $hostings;
    }

    public function filterVps($paginate, $key) {
        $vpses = RegisterService::join('customers as c', 'c.id', '=', 'register_services.id_customer')
            ->where('register_services.status_register', '=', 'paid')
            ->where('type_service', '=', 'vps')
            ->where(function ($query) use ($key) {
                $query->where('c.first_name', 'like', '%'.$key.'%')
                    ->orWhere('c.last_name', 'like', '%'.$key.'%')
                    ->orWhere('c.email', 'like', '%'.$key.'%')
                    ->orWhere('register_services.address_domain', 'like', '%'.$key.'%');
            })
            ->select('register_services.*', 'c.first_name as first_name',
                'c.last_name as last_name', 'c.email as customer_email')
            ->orderBy('register_services.id', 'desc');
        $vpses = $vpses->addSelect(DB::raw("'service' as type"));
        $vpses = $vpses->paginate($paginate);
        return $vpses;
    }

    public function filterEmail($paginate, $key) {
        $emails = RegisterService::join('customers as c', 'c.id', '=', 'register_services.id_customer')
            ->where('register_services.status_register', '=', 'paid')
            ->where('type_service', '=', 'email')
            ->where(function ($query) use ($key) {
                $query->where('c.first_name', 'like', '%'.$key.'%')
                    ->orWhere('c.last_name', 'like', '%'.$key.'%')
                    ->orWhere('c.email', 'like', '%'.$key.'%')
                    ->orWhere('register_services.address_domain', 'like', '%'.$key.'%');
            })
            ->select('register_services.*', 'c.first_name as first_name',
                'c.last_name as last_name', 'c.email as customer_email')
            ->orderBy('register_services.id', 'desc');
        $emails = $emails->addSelect(DB::raw("'service' as type"));
        $emails = $emails->paginate($paginate);
        return $emails;
    }

    public function filterSsl($paginate, $key) {
        $ssls = RegisterService::join('customers as c', 'c.id', '=', 'register_services.id_customer')
            ->where('register_services.status_register', '=', 'paid')
            ->where('type_service', '=', 'ssl')
            ->where(function ($query) use ($key) {
                $query->where('c.first_name', 'like', '%'.$key.'%')
                    ->orWhere('c.last_name', 'like', '%'.$key.'%')
                    ->orWhere('c.email', 'like', '%'.$key.'%')
                    ->orWhere('register_services.address_domain', 'like', '%'.$key.'%');
            })
            ->select('register_services.*', 'c.first_name as first_name',
                'c.last_name as last_name', 'c.email as customer_email')
            ->orderBy('register_services.id', 'desc');
        $ssls = $ssls->addSelect(DB::raw("'service' as type"));
        $ssls = $ssls->paginate($paginate);
        return $ssls;
    }

    public function filterWebsite($paginate, $key) {
        $websites = RegisterService::join('customers as c', 'c.id', '=', 'register_services.id_customer')
            ->where('register_services.status_register', '=', 'paid')
            ->where('type_service', '=', 'website')
            ->where(function ($query) use ($key) {
                $query->where('c.first_name', 'like', '%'.$key.'%')
                    ->orWhere('c.last_name', 'like', '%'.$key.'%')
                    ->orWhere('c.email', 'like', '%'.$key.'%')
                    ->orWhere('register_services.address_domain', 'like', '%'.$key.'%');
            })
            ->select('register_services.*', 'c.first_name as first_name',
                'c.last_name as last_name', 'c.email as customer_email')
            ->orderBy('register_services.id', 'desc');
        $websites = $websites->addSelect(DB::raw("'service' as type"));
        $websites = $websites->paginate($paginate);
        return $websites;
    }

    public function filterSoftware($paginate, $key) {
        $softwares = RegisterSoftware::join('customers', 'customers.id', '=', 'register_softwares.id_customer')
            ->join('softwares', 'softwares.id', '=', 'register_softwares.id_software')
            ->where('register_softwares.status_register', '=', 'paid')
            ->where(function ($query) use ($key) {
                $query->where('customers.first_name', 'like', '%'.$key.'%')
                    ->orWhere('customers.last_name', 'like', '%'.$key.'%')
                    ->orWhere('customers.email', 'like', '%'.$key.'%')
                    ->orWhere('register_softwares.address_domain', 'like', '%'.$key.'%');
            })
            ->select('customers.first_name as first_name', 'customers.last_name as last_name', 'customers.email as customer_email',
                'softwares.name as software_name', 'register_softwares.*')
            ->orderBy('register_softwares.id', 'desc');
        $softwares = $softwares->addSelect(DB::raw("'software' as type"));
        $softwares = $softwares->paginate($paginate);
        return $softwares;
    }
}
