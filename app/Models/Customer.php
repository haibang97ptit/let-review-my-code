<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class Customer extends Model
{
    use SoftDeletes;

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $table = 'customers';
    protected $fillable = ['id', 'phone_number', 'email', 'address', 'birthday', 'note', 'state', 'first_name', 'last_name', 'user_id'];

    public $messages = [];
    public $rules = [
        'phone_number' => 'required',
        'email' => 'required|unique:customers,email,NULL,id,deleted_at,NULL',
        'address' => 'required',
        'birthday' => 'required',
        'state' => 'required',
        'first_name' => 'required',
        'last_name' => 'required',
        'password' => 'required',
    ];

    public function __construct()
    {
        parent::__construct();
        $this->messages = [
            'phone_number.required' => __('customer.phone-number') . __('general.required'),
            'email.required' => __('customer.email') . __('general.required'),
            'address.required' => __('customer.address') . __('general.required'),
            'birthday.required' => __('customer.birthday') . __('general.required'),
            'state.required' => __('customer.state') . __('general.required'),
            'first_name.required' => __('customer.first-name') . __('general.required'),
            'last_name.required' => __('customer.last-name') . __('general.required'),
            'password.required' => __('customer.password') . __('general.required'),
        ];
    }

    public function getAll($key, $paginate) {
        $customers = Customer::where('first_name', 'like', '%'.$key.'%')
            ->orWhere('last_name', 'like', '%'.$key.'%')
            ->orWhere('birthday', 'like', '%'.$key.'%')
            ->orWhere('email', 'like', '%'.$key.'%')
            ->orWhere('phone_number', 'like', '%'.$key.'%')
            ->orWhere('state', 'like', '%'.$key.'%')
            ->orWhere('address', 'like', '%'.$key.'%')
            ->paginate($paginate);
        return $customers;
    }

    public function getAllTrash($key, $paginate) {
        $customers = Customer::onlyTrashed()->where('first_name', 'like', '%'.$key.'%')->paginate($paginate);
        return $customers;
    }

}

