<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;

class RegisterService extends Model
{
    use SoftDeletes;
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $table = 'register_services';
    protected $fillable = ['id', 'code', 'start_date', 'end_date', 'notes', 'status_register', 'price', 'address_domain', 'id_customer', 'type_service', 'type_service_id', 'time_use'];

    public function createCode($service) {
        $dt = Carbon::now();
        $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $code = 'TQ-'.$service.'-'.substr(str_shuffle($permitted_chars), 0, 5);
        return $code;
    }

    public function getAll($paginate) {
        $register_services = RegisterService::join('customers as c', 'c.id', '=', 'register_services.id_customer')
            ->select('register_services.*', 'c.first_name as first_name',
                'c.last_name as last_name', 'c.email as customer_email')
            ->orderBy('register_services.id', 'desc')
            ->paginate($paginate);
        return $register_services;
    }

    public function getSearchRow($customer, $service, $status, $paginate) {
        $register_services = RegisterService::join('customers as c', 'c.id', '=', 'register_services.id_customer')
            ->select('register_services.*', 'c.first_name as first_name',
                'c.last_name as last_name', 'c.email as customer_email')
            ->where('type_service', 'like', '%'.$service.'%')
            ->where('id_customer', 'like', '%'.$customer.'%')
            ->where('status_register', '=', $status)
            ->orderBy('register_services.id', 'desc')
            ->paginate($paginate);
        return $register_services;
    }

    public function getAllTrash($paginate) {
        $register_services = RegisterService::onlyTrashed()
            ->join('customers as c', 'c.id', '=', 'register_services.id_customer')
            ->select('register_services.*', 'c.first_name as first_name',
                'c.last_name as last_name', 'c.email as customer_email')
            ->orderBy('register_services.id', 'desc')
            ->paginate($paginate);
        return $register_services;
    }

    public function getTrashSearchRow($customer, $service, $status, $paginate) {
        $register_services = RegisterService::onlyTrashed()
            ->join('customers as c', 'c.id', '=', 'register_services.id_customer')
            ->select('register_services.*', 'c.first_name as first_name',
                'c.last_name as last_name', 'c.email as customer_email')
            ->where('type_service', 'like', '%'.$service.'%')
            ->where('id_customer', 'like', '%'.$customer.'%')
            ->where('status_register', '=', $status)
            ->orderBy('register_services.id', 'desc')
            ->paginate($paginate);
        return $register_services;
    }

    public function getToUpdate($id) {
        $register_services = RegisterService::where('register_services.id', '=', $id)
            ->join('customers as c', 'c.id', '=', 'register_services.id_customer')
            ->select('register_services.*', 'c.first_name as first_name',
                'c.last_name as last_name', 'c.email as customer_email')
            ->first();
        return $register_services;
    }

    public function registerDomain($request, $dt) {
        //Save data
        $this->id_customer = $request->id_customer;
        $this->notes = $request->notes;

        //Create code
        $code = $this->createCode("D");
        $this->code = $code;
        $this->address_domain = $request->domain_address_domain;

        //Create start date, end date with time use
        $startDate = $request->domain_start_date.$dt->format(' H:i');
        $this->time_use = $request->domain_time_use;
        $dt2 = Carbon::parse($startDate);
        $dt2->addMonth($request->domain_time_use);
        $endDate = $dt2->format('Y-m-d H:i');

        //Add start, end date
        $this->start_date = $startDate;
        $this->end_date = $endDate;

        //Price
        $price = $request->domain_time_use * ($request->domain_fee_register + $request->domain_fee_remain + $request->domain_fee_transformation);
        $this->price = $price;
        $this->type_service = "domain";
        $this->type_service_id = $request->id_domain;
        $this->status_register = "unpaid";
        $this->save();
    }

    public function registerHosting($request, $dt) {
        //Save data
        $this->id_customer = $request->id_customer;
        $this->notes = $request->notes;

        //Create code
        $code = $this->createCode("H");
        $this->code = $code;
        $this->address_domain = $request->hosting_address_domain;

        //Create start date, end date with time use
        $startDate = $request->hosting_start_date.$dt->format(' H:i');
        $this->time_use = $request->hosting_time_use;
        $dt2 = Carbon::parse($startDate);
        $dt2->addMonth($request->hosting_time_use);
        $endDate = $dt2->format('Y-m-d H:i');

        //Add start, end date
        $this->start_date = $startDate;
        $this->end_date = $endDate;

        //Price
        $this->price = $request->hosting_time_use * $request->hosting_price;
        $this->type_service = "hosting";
        $this->type_service_id = $request->id_hosting;
        $this->status_register = "unpaid";
        $this->save();
    }

    public function registerVps($request, $dt) {
        //Save data
        $this->id_customer = $request->id_customer;
        $this->notes = $request->notes;

        //Create code
        $code = $this->createCode("V");
        $this->code = $code;
        $this->address_domain = $request->vps_address_domain;

        //Create start date, end date with time use
        $startDate = $request->vps_start_date.$dt->format(' H:i');
        $this->time_use = $request->vps_time_use;
        $dt2 = Carbon::parse($startDate);
        $dt2->addMonth($request->vps_time_use);
        $endDate = $dt2->format('Y-m-d H:i');

        //Add start, end date
        $this->start_date = $startDate;
        $this->end_date = $endDate;

        //Price
        $this->price = $request->vps_time_use * $request->vps_price;
        $this->type_service = "vps";
        $this->type_service_id = $request->id_vps;
        $this->status_register = "unpaid";
        $this->save();
    }

    public function registerEmail($request, $dt) {
        //Save data
        $this->id_customer = $request->id_customer;
        $this->notes = $request->notes;

        //Create code
        $code = $this->createCode("E");
        $this->code = $code;
        $this->address_domain = $request->email_address_domain;

        //Create start date, end date with time use
        $startDate = $request->email_start_date.$dt->format(' H:i');
        $this->time_use = $request->email_time_use;
        $dt2 = Carbon::parse($startDate);
        $dt2->addMonth($request->email_time_use);
        $endDate = $dt2->format('Y-m-d H:i');

        //Add start, end date
        $this->start_date = $startDate;
        $this->end_date = $endDate;

        //Price
        $this->price = $request->email_time_use * $request->email_price;
        $this->type_service = "email";
        $this->type_service_id = $request->id_email;
        $this->status_register = "unpaid";
        $this->save();
    }

    public function registerSsl($request, $dt) {
        //Save data
        $this->id_customer = $request->id_customer;
        $this->notes = $request->notes;

        //Create code
        $code = $this->createCode("S");
        $this->code = $code;
        $this->address_domain = $request->ssl_address_domain;

        //Create start date, end date with time use
        $startDate = $request->ssl_start_date.$dt->format(' H:i');
        $this->time_use = $request->ssl_time_use;
        $dt2 = Carbon::parse($startDate);
        $dt2->addMonth($request->ssl_time_use);
        $endDate = $dt2->format('Y-m-d H:i');

        //Add start, end date
        $this->start_date = $startDate;
        $this->end_date = $endDate;

        //Price
        $this->price = $request->ssl_time_use * $request->ssl_price;
        $this->type_service = "ssl";
        $this->type_service_id = $request->id_ssl;
        $this->status_register = "unpaid";
        $this->save();
    }

    public function registerWebsite($request, $dt) {
        //Save data
        $this->id_customer = $request->id_customer;
        $this->notes = $request->notes;

        //Create code
        $code = $this->createCode("W");
        $this->code = $code;
        $this->address_domain = $request->website_address_domain;

        //Create start date, end date with time use
        $startDate = $request->website_start_date.$dt->format(' H:i');
        $this->time_use = $request->website_time_use;
        $dt2 = Carbon::parse($startDate);
        $dt2->addMonth($request->website_time_use);
        $endDate = $dt2->format('Y-m-d H:i');

        //Add start, end date
        $this->start_date = $startDate;
        $this->end_date = $endDate;

        //Price
        $this->price = $request->website_time_use * $request->website_price;
        $this->type_service = "website";
        $this->type_service_id = $request->id_website;
        $this->status_register = "unpaid";
        $this->save();
    }

}
