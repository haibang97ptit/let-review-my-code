<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class ListRequest extends Model
{

    use SoftDeletes;

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    protected $table = 'request';
    protected $fillable = ['id', 'code', 'id_department_send', 'id_department_receive', 'id_customer', 'title', 'status', 'service', 'person_create'];

    public $messages = [];
    public $rules = [
        'department_receive' => 'required',
        'id_customer' => 'required',
        'title' => 'required',
        'service' => 'required',
    ];


    public function __construct()
    {
        parent::__construct();
        $this->messages = [
            'department_receive.required' => __('support.department_receive') . __('general.required'),
            'id_customer.required' => __('support.customer') . __('general.required'),
            'title.required' => __('support.title') . __('general.required'),
            'status.required' => __('support.status') . __('general.required'),
            'service.required' => __('support.service') . __('general.required'),
        ];
    }

    public function createCode($department_send, $department_receive)
    {
        $department_send = str_replace(substr($department_send, -3, 3), "", $department_send);
        $department_receive = str_replace(substr($department_receive, -3, 3), "", $department_receive);
        $codeCheck = $department_send . $department_receive . '0001';

        while ($this->checkCodeExist($codeCheck) !== false) {
            $number = substr($this->checkCodeExist($codeCheck), -4, 4);
            $newNumber = $number + 1;
            if ($newNumber < 10) {
                $newNumber = '000' . $newNumber;
            } elseif ($newNumber < 100) {
                $newNumber = '00' . $newNumber;
            } elseif ($newNumber < 1000) {
                $newNumber = '0' . $newNumber;
            }
            $codeCheck = $department_send . $department_receive . '' . $newNumber;
        }
        return $this->code = $codeCheck;
    }

    public function checkCodeExist($code)
    {
        $codes = ListRequest::withTrashed()->where('code', '=', $code)->first();
        if ($codes !== null) {
            return $codes->code;
        }
        return false;
    }

    public function getAll($department, $paginate)
    {
        $listRequest = ListRequest::where('id_department_receive', 'like', $department)->orWhere('id_department_send', 'like', $department)->orderBy('id', 'DESC')
            ->paginate($paginate);
        return $listRequest;
    }

    public function getAllByColumn($department, $query, $paginate)
    {
        if($paginate == null){
            $paginate = 999;
        }
        $listRequest = ListRequest::where('status', 'like', $query)->where('id_department_send', 'like', $department)->orWhere([['id_department_receive', 'like', $department], ['status', 'like', $query]])
            ->orderBy('id', 'DESC')
            ->paginate($paginate);
        return $listRequest;
    }

    public function updateColumn($code, $col, $key)
    {
        DB::table('request')->where('code', 'like', $code)->update([$col => $key]);
    }
}
