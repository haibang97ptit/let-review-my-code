<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MessagesDetail extends Model
{
    use SoftDeletes;

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $table = 'messages_detail';

    public $messages = [];
    public $rules = [
        'content_support' => 'required|max:500',
    ];
    public function __construct()
    {
        parent::__construct();
        $this->messages = [
            'content_support.required' => __('support.content_support') . __('general.required'),
            'content_support.max' => __('support.content_support') . __('general.max'),
        ];
    }



}
