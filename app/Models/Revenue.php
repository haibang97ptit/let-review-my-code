<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Revenue extends Model
{
    use SoftDeletes;
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $message = [];
    public $rules = [
//        'code' => 'required',


    ];
    protected $table = 'revenues';
    protected $fillable = ['id', 'code','id_staff','description','price'];
    public function __construct()
    {
        parent::__construct();
        $this->message = [
//            'service_name.required' => 'Vui lòng chọn khách hàng',

        ];
    }
}
