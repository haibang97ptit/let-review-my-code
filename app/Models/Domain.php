<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Domain extends Model
{
    use SoftDeletes;
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $table = 'domains';
    protected $fillable = ['id', 'name', 'fee_register', 'fee_remain', 'fee_transformation', 'notes', 'images'];

    public $messages = [];
    public $rules = [
        'name' => 'required|unique:domains',
        'fee_register' => 'required',
        'fee_remain' => 'required',
        'fee_transformation' => 'required',
    ];

    public function __construct()
    {
        parent::__construct();
        $this->messages = [
            'name.required' => __('domain.name').__('general.required'),
            'name.unique' => __('domain.name').__('general.exist'),
            'fee_register.required' => __('domain.fee-register').__('general.required'),
            'fee_remain.required' => __('domain.fee-remain').__('general.required'),
            'fee_transformation.required' => __('domain.fee-transformation').__('general.required'),
        ];
    }

    public function getAll($key, $paginate) {
        $domains = Domain::where('name', 'like', '%'.$key.'%')
            ->orWhere('fee_register', 'like', '%'.$key.'%')
            ->orWhere('fee_remain', 'like', '%'.$key.'%')
            ->orWhere('fee_transformation', 'like', '%'.$key.'%')
            ->orderBy('id', 'desc')
            ->paginate($paginate);
        return $domains;
    }
}
