<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VPS extends Model
{
    //
    use SoftDeletes;
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $table = 'vpss';
    protected $fillable = ['id', 'name', 'price', 'cpu', 'capacity', 'ram', 'bandwith', 'technical', 'operating_system', 'backup', 'panel', 'notes'];

    public $messages = [];
    public $rules = [
        'name' => 'required|unique:vpss',
        'price' => 'required',
        'cpu' => 'required',
        'capacity' => 'required',
        'bandwith' => 'required',
        'ram' => 'required',
        'technical' => 'required',
        'operating_system' => 'required',
        'backup' => 'required',
        'panel' => 'required',
    ];

    public function __construct()
    {
        parent::__construct();
        $this->messages = [
            'name.required' => __('vps.name').__('general.required'),
            'name.unique' => __('vps.name').__('general.exist'),
            'price.required' => __('vps.price').__('general.required'),
            'cpu.required' => __('vps.cpu').__('general.required'),
            'capacity.required' => __('vps.capacity').__('general.required'),
            'ram.required' => __('vps.ram').__('general.required'),
            'bandwith.required' => __('vps.bandwidth').__('general.required'),
            'technical.required' => __('vps.technical').__('general.required'),
            'operating_system.required' => __('vps.operating-system').__('general.required'),
            'backup.required' => __('vps.backup').__('general.required'),
            'panel.required' => __('vps.panel').__('general.required'),
        ];
    }

    public function getAll($key, $paginate) {
        $vpses = VPS::where('name', 'like', '%'.$key.'%')
            ->orWhere('price', 'like', '%'.$key.'%')
            ->orWhere('cpu', 'like', '%'.$key.'%')
            ->orWhere('capacity', 'like', '%'.$key.'%')
            ->orWhere('ram', 'like', '%'.$key.'%')
            ->orWhere('bandwith', 'like', '%'.$key.'%')
            ->orWhere('technical', 'like', '%'.$key.'%')
            ->orWhere('operating_system', 'like', '%'.$key.'%')
            ->orWhere('backup', 'like', '%'.$key.'%')
            ->orWhere('panel', 'like', '%'.$key.'%')
            ->orderBy('id', 'desc')
            ->paginate($paginate);
        return $vpses;
    }
}
