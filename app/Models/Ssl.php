<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ssl extends Model
{
    use SoftDeletes;
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $table = 'ssls';
    protected $fillable = ['id', 'name', 'price', 'insurance_policy', 'domain_number', 'reliability', 'green_bar', 'sans', 'notes'];

    public $messages = [];
    public $rules = [
        'name' => 'required|unique:ssls',
        'price' => 'required',
        'insurance_policy' => 'required',
        'domain_number' => 'required',
        'reliability' => 'required',
        'green_bar' => 'required',
        'sans' => 'required',
    ];

    public function __construct()
    {
        parent::__construct();
        $this->messages = [
            'name.required' => __('ssl.name').__('general.required'),
            'name.unique' => __('ssl.name').__('general.exist'),
            'price.required' => __('ssl.price').__('general.required'),
            'insurance_policy.required' => __('ssl.insurance-policy').__('general.required'),
            'domain_number.required' => __('ssl.domain-number').__('general.required'),
            'reliability.required' => __('ssl.reliability').__('general.required'),
            'green_bar.required' => __('ssl.green-bar').__('general.required'),
            'sans.required' => __('ssl.sans').__('general.required'),
        ];
    }

    public function getAll($key, $paginate) {
        $ssls = Ssl::where('name', 'like', '%'.$key.'%')
            ->orWhere('price', 'like', '%'.$key.'%')
            ->orWhere('insurance_policy', 'like', '%'.$key.'%')
            ->orWhere('domain_number', 'like', '%'.$key.'%')
            ->orWhere('reliability', 'like', '%'.$key.'%')
            ->orWhere('green_bar', 'like', '%'.$key.'%')
            ->orWhere('sans', 'like', '%'.$key.'%')
            ->orderBy('id', 'desc')
            ->paginate($paginate);
        return $ssls;
    }
}
