<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Hosting extends Model
{
    use SoftDeletes;
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $table = 'hostings';
    protected $fillable = ['id', 'name', 'price', 'capacity', 'bandwith', 'sub_domain', 'email', 'ftp', 'database', 'adddon_domain', 'park_domain', 'notes'];

    public $messages = [];
    public $rules = [
        'name' => 'required|unique:hostings',
        'price' => 'required',
        'capacity' => 'required',
        'bandwith' => 'required',
        'sub_domain' => 'required',
        'email' => 'required',
        'ftp' => 'required',
        'database' => 'required',
        'adddon_domain' => 'required',
        'park_domain' => 'required',
    ];

    public function __construct()
    {
        parent::__construct();
        $this->messages = [
            'name.required' => __('hosting.name').__('general.required'),
            'name.unique' => __('hosting.name').__('general.exist'),
            'price.required' => __('hosting.price').__('general.required'),
            'capacity.required' => __('hosting.capacity').__('general.required'),
            'bandwith.required' => __('hosting.bandwidth').__('general.required'),
            'sub_domain.required' => __('hosting.sub-domain').__('general.required'),
            'email.required' => __('hosting.email').__('general.required'),
            'ftp.required' => __('hosting.ftp').__('general.required'),
            'database.required' => __('hosting.database').__('general.required'),
            'adddon_domain.required' => __('hosting.adddon-domain').__('general.required'),
            'park_domain.required' => __('hosting.park-domain').__('general.required'),
        ];
    }

    public function getAll($key, $paginate) {
        $hostings = Hosting::where('name', 'like', '%'.$key.'%')
            ->orWhere('price', 'like', '%'.$key.'%')
            ->orWhere('capacity', 'like', '%'.$key.'%')
            ->orWhere('bandwith', 'like', '%'.$key.'%')
            ->orWhere('sub_domain', 'like', '%'.$key.'%')
            ->orWhere('email', 'like', '%'.$key.'%')
            ->orWhere('ftp', 'like', '%'.$key.'%')
            ->orWhere('database', 'like', '%'.$key.'%')
            ->orWhere('adddon_domain', 'like', '%'.$key.'%')
            ->orWhere('park_domain', 'like', '%'.$key.'%')
            ->orderBy('id', 'desc')
            ->paginate($paginate);
        return $hostings;
    }
}
