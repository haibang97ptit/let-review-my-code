<?php


namespace App\Business;


use App\Models\SoftwarePackage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class SoftwareLogic extends BaseLogic
{

    /**
     * @inheritDoc
     */
    public function model()
    {
        return SoftwarePackage::class;
    }
    public function getlistsearch(Request $request){
        $query=SoftwarePackage::select('softwares.*')
            ->wherenull('deleted_at');
        if ($request){
            if (isset($request->name)){
                $query->where('softwares.name','LIKE','%'.$request->name.'%');
            }
            if (isset($request->page) && is_numeric($request->page)) {
                $query->offset($request->page * Config::get('constants.pagination'));
            }
        }
        return $query->paginate(4);

    }
}
