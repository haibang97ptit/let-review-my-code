<?php


namespace App\Business;


use App\Models\Customer;
use App\Models\RegisterService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

use Illuminate\Support\Facades\Auth;

class RegisterServicesLogic extends BaseLogic
{

    /**
     * @inheritDoc
     */
    public function model()
    {
        return RegisterService::class;
    }

    public function getListRegisterServices()
    {
        $query = $this->model
            ->join('customers as c', 'c.id', 'register_services.id_customer')
            ->select('register_services.*', 'c.first_name as customer_name','c.email as customer_email');
        $query->orderBy('register_services.id', 'ASC');

        return $query->paginate(Config::get('constants.pagination'));
    }










    public function getIndexRegisterServices($register_id)
    {
        return $query = RegisterService::join('customers as c', 'c.id', 'register_services.id_customer')
            ->leftJoin('domains as d', 'd.id', 'register_services.id_domain')
            ->leftJoin('hostings as h', 'h.id', 'register_services.id_hosting')
            ->leftJoin('vpss as v', 'v.id', 'register_services.id_vps')
            ->leftJoin('emails as e', 'e.id', 'register_services.id_email')
            ->leftJoin('ssls as s', 's.id', 'register_services.id_ssl')
            ->leftJoin('websites as w', 'w.id', 'register_services.id_website')
            ->select('register_services.*', 'c.name as customer_name',
                'w.name as website_name',
                's.name as ssl_name',
                'e.name as email_name',
                'v.name as vps_name',
                'h.name as hosting_name',
                'd.name as domain_name')
            ->whereNull('register_services.deleted_at')
            ->whereNull('c.deleted_at')
            ->whereNull('d.deleted_at')
            ->whereNull('h.deleted_at')
            ->whereNull('v.deleted_at')
            ->whereNull('e.deleted_at')
            ->whereNull('s.deleted_at')
            ->whereNull('w.deleted_at')
            //dựa theo id của registerservices để lấy thông tin
            ->where('register_services.id', $register_id)
            ->first();
    }
    public function getaddressdomain(){
        return $query=RegisterService::select('register_services.address_domain')->distinct()->pluck('address_domain');
    }
    public function getlistsearch(Request $request){
        $query=$this->model
            ->join('customers as c', 'c.id', 'register_services.id_customer')
            ->leftJoin('domains as d', 'd.id', 'register_services.id_domain')
            ->leftJoin('hostings as h', 'h.id', 'register_services.id_hosting')
            ->leftJoin('vpss as v', 'v.id', 'register_services.id_vps')
            ->leftJoin('emails as e', 'e.id', 'register_services.id_email')
            ->leftJoin('ssls as s', 's.id', 'register_services.id_ssl')
            ->leftJoin('websites as w', 'w.id', 'register_services.id_website')
            ->select('register_services.*', 'c.name as customer_name',
                'w.name as website_name',
                's.name as ssl_name',
                'e.name as email_name',
                'v.name as vps_name',
                'h.name as hosting_name',
                'd.name as domain_name')
            ->whereNull('register_services.deleted_at')
            ->whereNull('c.deleted_at')
            ->whereNull('d.deleted_at')
            ->whereNull('h.deleted_at')
            ->whereNull('v.deleted_at')
            ->whereNull('e.deleted_at')
            ->whereNull('s.deleted_at')
            ->whereNull('w.deleted_at');
        if (Auth::user()->id_role == 3) {
            $query->join('users as u', 'u.id', 'staffs.id_user')
                ->where('staffs.id_user', Auth::user()->id)
                ->whereNull('u.deleted_at');
        }

        if ($request){
            if (isset($request->code)){
                $query->where('register_services.code','LIKE','%'.$request->code.'%');
            }

            if (isset($request->service)){
                if($request->service=='domain'){
                    $query->whereNotNull('id_domain');
                }
                if($request->service=='hosting'){
                    $query->whereNotNull('id_hosting');
                }
                if($request->service=='vps'){
                    $query->whereNotNull('id_vps');
                }
                if($request->service=='email'){
                    $query->whereNotNull('id_email');
                }
                if($request->service=='ssl'){
                    $query->whereNotNull('id_ssl');
                }
                if($request->service=='website'){
                    $query->whereNotNull('id_website');
                }
            }

            if (isset($request->customer)){
                $query->where('register_services.id_customer','LIKE','%'.$request->customer.'%');
            }
            if (isset($request->address_domain)){
                $query->where('register_services.address_domain','LIKE','%'.$request->address_domain.'%');
            }
            if (isset($request->page) && is_numeric($request->page)) {
                $query->offset($request->page * Config::get('constants.pagination'));
            }

//            $query->paginatione(Config::get('constants.pagination'));
        }
//        dd($request);
        return $query->paginate(Config::get('constants.pagination'));

    }

}
