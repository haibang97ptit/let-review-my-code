<?php


namespace App\Business;


use App\Models\Customer;
use App\Models\Staff;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class StaffLogic extends BaseLogic
{

    /**
     * @inheritDoc
     */
    public function model()
    {
        return Staff::class;
    }
    public function getphone(){
        return $query=Staff::select('staffs.phone_number')->distinct()->pluck('phone_number');
    }
    public function getemail(){
        return $query=Staff::select('staffs.email')->distinct()->pluck('email');
    }
    public function getlistsearch(Request $request){
        $query=Staff::select('staffs.*')
            ->wherenull('deleted_at');
        if ($request){
            if(isset($request->name)){
                $query->where('staffs.name','LIKE','%'. $request->name.'%');
            }
            if(isset($request->phone_number)){
                $query->where('staffs.phone_number','LIKE','%'.$request->phone_number.'%');
            }
            if (isset($request->email)){
                $query->where('staffs.email','LIKE','%'.$request->email.'%');
            }
            if (isset($request->page) && is_numeric($request->page)) {
                $query->offset($request->page * Config::get('constants.pagination'));
            }
//            $query->paginate(Config::get('constants.pagination'));
        }
//        dd($query->paginate(Config::get('constants.pagination')));
        return $query->paginate(Config::get('constants.pagination'));

    }
}
