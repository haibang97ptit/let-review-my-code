<?php


namespace App\Business;


use App\Models\Customer;
use App\Models\RegisterService;
use App\Models\RegisterSoftware;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class RegisterSoftLogic extends BaseLogic
{

    public function model()
    {
        return RegisterSoftware::class;
    }
    public function getlistregistersoft(){
        $query=$this->model
            ->join('customers as c','c.id','register_soft.id_customer')
            ->join('softwares as sw','sw.id','register_soft.id_software')
            ->join('typesoftwares as ts','ts.id','register_soft.id_typesoftware')
            ->join('users as u','u.id','register_soft.id_staff')
            ->select('register_soft.*','c.name as customer_name','sw.name as software',
            'ts.name as typesoftware','u.name as staff_name','sw.price as price',
            'c.email as customer_email')
            ->wherenull('register_soft.deleted_at')
            ->wherenull('c.deleted_at')
            ->where('register_soft.status_register','=','success')
        ;
        $query->orderBy('register_soft.id','ASC');
        return $query->paginate(Config::get('constants.pagination'));
    }
    public function getdetailregistersoft($id){
        return $query=RegisterSoftware::join('customers as c','c.id','register_soft.id_customer')
            ->join('softwares as sw','sw.id','register_soft.id_software')
            ->join('typesoftwares as ts','ts.id','register_soft.id_typesoftware')
            ->join('users as u','u.id','register_soft.id_staff')
            ->select('register_soft.*','c.name as customer_name','sw.name as software',
                'ts.name as typesoftware','u.name as staff_name',
            'sw.price as price')
            ->wherenull('register_soft.deleted_at')
            ->wherenull('c.deleted_at')
            ->where('register_soft.id',$id)->first();
    }
    public function getaddressdomain(){
        return $query=RegisterSoftware::select('register_soft.address_domain')->distinct()->pluck('address_domain');
    }
    public function getlistsearchsoft(Request $request){
        $query=$this->model
            ->join('customers as c','c.id','register_soft.id_customer')
            ->join('softwares as sw','sw.id','register_soft.id_software')
            ->join('typesoftwares as ts','ts.id','register_soft.id_typesoftware')
            ->join('users as u','u.id','register_soft.id_staff')
            ->select('register_soft.*','c.name as customer_name','sw.name as software',
                'ts.name as typesoftware','u.name as staff_name',
                'c.email as customer_email')
            ->wherenull('register_soft.deleted_at')
            ->wherenull('c.deleted_at');
        if ($request){
            if (isset($request->customer)){
                $query->where('register_soft.id_customer','LIKE','%'.$request->customer.'%');
            }
            if (isset($request->typesoftware)){
                $query->where('register_soft.id_typesoftware','LIKE','%'.$request->typesoftware.'%');
            }
            if (isset($request->software)){
                $query->where('register_soft.id_software','LIKE','%'.$request->software.'%');
            }
            if (isset($request->status)){
                $query->where('register_soft.status','LIKE','%'.$request->status.'%');
            }
            if (isset($request->page) && is_numeric($request->page)) {
                $query->offset($request->page * Config::get('constants.pagination'));
            }
//            $query->paginate(Config::get('constants.pagination'));
        }

        return $query->paginate(Config::get('constants.pagination'));

    }

}
