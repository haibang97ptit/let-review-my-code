<?php


namespace App\Business;


use App\Models\CashBook;


class CashBookLogic extends BaseLogic
{

    /**
     * @inheritDoc
     */
    public function model()
    {
        return CashBook::class;
    }
//    public function totalprice($start){
//        $query=$this->model->whereMonth('cash_books.created_at','=',$start)->sum('cash_books.price') ;
//        $expenditure=$this->model->where('cash_books.status','=','expenditure')->whereMonth('cash_books.created_at','=',$start)->sum('cash_books.price') ;
//        $query=$query-$expenditure;
//        return $query;
//    }
}
