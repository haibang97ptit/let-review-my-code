<?php


namespace App\Business;


use App\Models\Software;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class TypeSofftwareLogic extends BaseLogic
{

    /**
     * @inheritDoc
     */
    public function model()
    {
        return Software::class;
    }
    public function getlistsearch(Request $request){
        $query=Software::select('typesoftwares.*')
            ->wherenull('deleted_at');
        if ($request){
            if (isset($request->name)){
                $query->where('typesoftwares.name','LIKE','%'.$request->name.'%')
                ->orwhere('typesoftwares.description','LIKE','%'.$request->name.'%');
            }
            if (isset($request->page) && is_numeric($request->page)) {
                $query->offset($request->page * Config::get('constants.pagination'));
            }
        }
        return $query->paginate(Config::get('constants.pagination'));

    }
}
