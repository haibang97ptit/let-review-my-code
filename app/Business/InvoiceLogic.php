<?php


namespace App\Business;


use App\Models\Invoice;
use Illuminate\Support\Facades\Config;

class InvoiceLogic extends BaseLogic
{
    public function model()
    {
        return Invoice::class;
    }
    public function getListInvoice()
    {
        $query = $this->model
            ->join('customers as c', 'c.id', 'invoices.id_customer');
//            ->Leftjoin('register_services as sv', 'sv.id', 'invoices.id_register_service')
//            ->Leftjoin('register_soft as rsw', 'rsw.id', 'invoices.id_register_soft')
//            ->Leftjoin('domains as d', 'd.id', 'sv.id_domain')
//            ->Leftjoin('hostings as h', 'h.id', 'sv.id_hosting')
//            ->Leftjoin('vpss as v', 'v.id', 'sv.id_vps')
//            ->Leftjoin('emails as e', 'e.id', 'sv.id_email')
//            ->Leftjoin('ssls as s', 's.id', 'sv.id_ssl')
//            ->Leftjoin('websites as w', 'w.id', 'sv.id_website')
//            ->Leftjoin('softwares as sw','sw.id','rsw.id_software')
//            ->Leftjoin('typesoftwares as tsw','tsw.id','rsw.id_typesoftware')
//            ->select('invoices.*', 'c.first_name as customer_name',
//                'sv.price as price_service','rsw.price as price_service',
//                 'w.name as website_name',
//                 's.name as ssl_name',
//                 'e.name as email_name',
//                 'v.name as vps_name',
//                 'h.name as hosting_name',
//                 'd.name as domain_name',
//                'sw.name as soft_name','tsw.name as typesoftware_name',
//            'sv.address_domain as address_domain1','rsw.address_domain as address_domain'
//                )
//            ->whereNull('invoices.deleted_at')
//            ->whereNull('c.deleted_at')
//            ->whereNull('d.deleted_at')
//            ->whereNull('h.deleted_at')
//            ->whereNull('v.deleted_at')
//            ->whereNull('e.deleted_at')
//            ->whereNull('s.deleted_at')
//            ->whereNull('w.deleted_at');

        //DESC GIẢM DÂN
        //ASC TĂNG DẦN
        $query->orderBy('invoices.id','DESC');

        return $query->paginate(Config::get('constants.pagination'));
    }
    public function getListRevenue_expenditure()
    {
        $query = $this->model
            ->join('customers as c', 'c.id', 'invoices.id_customer')
            ->select('invoices.*', 'c.name as customer_name' )
            ->whereNull('invoices.deleted_at')
            ->whereNull('c.deleted_at')
            ->where('invoices.status','=','success');

        //DESC GIẢM DÂN
        //ASC TĂNG DẦN
        $query->orderBy('invoices.id','DESC');

        return $query->paginate(Config::get('constants.pagination'));
    }

    public function getIndexInvoice($invoice_id)
    {
        return $query = Invoice::join('customers as c', 'c.id', 'invoices.id_customer')
            ->Leftjoin('register_services as sv', 'sv.id', 'invoices.id_register_service')
            ->Leftjoin('register_soft as rsw', 'rsw.id', 'invoices.id_register_soft')
            ->Leftjoin('domains as d', 'd.id', 'sv.id_domain')
            ->Leftjoin('hostings as h', 'h.id', 'sv.id_hosting')
            ->Leftjoin('vpss as v', 'v.id', 'sv.id_vps')
            ->Leftjoin('emails as e', 'e.id', 'sv.id_email')
            ->Leftjoin('ssls as s', 's.id', 'sv.id_ssl')
            ->Leftjoin('websites as w', 'w.id', 'sv.id_website')
            ->Leftjoin('softwares as sw','sw.id','rsw.id_software')
            ->Leftjoin('typesoftwares as tsw','tsw.id','rsw.id_typesoftware')
            ->select('invoices.*', 'c.name as customer_name','c.email as customer_email',
                'sv.price as price_service','rsw.price as price_service',
                'w.name as website_name',
                's.name as ssl_name',
                'e.name as email_name',
                'v.name as vps_name',
                'h.name as hosting_name',
                'd.name as domain_name',
                'sw.name as soft_name','tsw.name as typesoftware_name',
                'sv.address_domain as address_domain1','rsw.address_domain as address_domain'
            )
            ->whereNull('invoices.deleted_at')
            ->whereNull('c.deleted_at')
            ->whereNull('d.deleted_at')
            ->whereNull('h.deleted_at')
            ->whereNull('v.deleted_at')
            ->whereNull('e.deleted_at')
            ->whereNull('s.deleted_at')
            ->whereNull('w.deleted_at')
            ->where('invoices.status','=','fail')
            ->where('invoices.id', $invoice_id)
            ->first();
    }

}
