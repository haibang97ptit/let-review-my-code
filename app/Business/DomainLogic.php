<?php


namespace App\Business;


use App\Models\Domain;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class DomainLogic extends BaseLogic
{

    /**
     * @inheritDoc
     */
    public function model()
    {
        return Domain::class;
    }
    public function getfeeregister(){
        return $query=Domain::select('domains.fee_register')->distinct()->pluck('fee_register');
    }
    public function getfeeremain(){
        return $query=Domain::select('domains.fee_remain')->distinct()->pluck('fee_remain');
    }
    public function getfeetranformation(){
        return $query=Domain::select('domains.fee_transformation')->distinct()->pluck('fee_transformation');
    }
    public function getlistsearch(Request $request){
        $query=Domain::select('domains.*')
            ->wherenull('deleted_at');
        if ($request){
            if (isset($request->name) ){
                $query->where('domains.name','LIKE','%'.$request->name.'%');
            }
            if (isset($request->fee_register)){
                $query->where('domains.fee_register','LIKE','%'.$request->fee_register.'%');
            }
            if (isset($request->fee_remain)){
                $query->where('domains.fee_remain','LIKE','%'.$request->fee_remain.'%');
            }
            if (isset($request->fee_tranformation)){
                $query->where('domains.fee_tranformation','LIKE','%'.$request->fee_tranformation.'%');
            }
            if (isset($request->page) && is_numeric($request->page)) {
                $query->offset($request->page * Config::get('constants.pagination'));
            }
        }
        return $query->paginate(Config::get('constants.pagination'));
    }
}
