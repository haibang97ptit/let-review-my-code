<?php


namespace App\Business;


use App\Models\Website;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class WebsiteLogic extends BaseLogic
{

    /**
     * @inheritDoc
     */
    public function model()
    {
       return Website::class;
    }
    public function gettype(){
        return $query=Website::select('websites.type_website')->distinct()->pluck('type_website');
    }
    public function getlistsearch(Request $request){
        $query=Website::select('websites.*')
            ->wherenull('deleted_at');
        if($request){
            if (isset($request->name)){
                $query->where('websites.name','LIKE','%'.$request->name.'%');
            }
            if (isset($request->type_website)){
                $query->where('websites.type_website','LIKE','%'.$request->type_website.'%');
            }
            if (isset($request->page) && is_numeric($request->page)) {
                $query->offset($request->page * Config::get('constants.pagination'));
            }
        }
        return $query->paginate(Config::get('constants.pagination'));
    }
}
