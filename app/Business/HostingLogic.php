<?php


namespace App\Business;


use App\Models\Hosting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class HostingLogic extends BaseLogic
{

    /**
     * @inheritDoc
     */
    public function model()
    {
        return Hosting::class;
    }
    public function getcapacity(){
        return $query=Hosting::select('hostings.capacity')->distinct()->pluck('capacity');
    }
    public function getbandwith(){
        return $query=Hosting::select('hostings.bandwith')->distinct()->pluck('bandwith');
    }
    public function getsubdomain(){
        return $query=Hosting::select('hostings.sub_domain')->distinct()->pluck('sub_domain');
    }
    public function getemail(){
        return $query=Hosting::select('hostings.email')->distinct()->pluck('email');
    }
    public function getftp(){
        return $query=Hosting::select('hostings.ftp')->distinct()->pluck('ftp');
    }
    public function getdatabase(){
        return $query=Hosting::select('hostings.database')->distinct()->pluck('database');
    }
    public function getadddondomain(){
        return $query=Hosting::select('hostings.adddon_domain')->distinct()->pluck('adddon_domain');
    }
    public function getparkdomain(){
        return $query=Hosting::select('hostings.park_domain')->distinct()->pluck('park_domain');
    }
    public function getlistsearch(Request $request){
        $query=Hosting::select('hostings.*')
            ->wherenull('deleted_at');
        if ($request){
            if (isset($request->name)){
                $query->where('hostings.name','LIKE','%'.$request->name.'%');
            }
            if (isset($request->capacity)){
                $query->where('hostings.capacity','LIKE','%'.$request->capacity.'%');
            }
            if (isset($request->bandwith)){
                $query->where('hostings.bandwith','LIKE','%'.$request->bandwith.'%');
            }
            if (isset($request->sub_domain)){
                $query->where('hostings.sub_domain','LIKE','%'.$request->sub_domain.'%');
            }
            if (isset($request->email)){
                $query->where('hostings.email','LIKE','%'.$request->email.'%');
            }
            if (isset($request->ftp)){
                $query->where('hostings.ftp','LIKE','%'.$request->ftp.'%');
            }
            if (isset($request->database)){
                $query->where('hostings.database','LIKE','%'.$request->database.'%');
            }
            if (isset($request->adddon_domain)){
                $query->where('hostings.adddon_domain','LIKE','%'.$request->adddon_domain.'%');
            }
            if (isset($request->park_domain)){
                $query->where('hostings.park_domain','LIKE','%'.$request->park_domain.'%');
            }
            if (isset($request->page) && is_numeric($request->page)) {
                $query->offset($request->page * Config::get('constants.pagination'));
            }
        }
        return $query->paginate(Config::get('constants.pagination'));
    }

}
