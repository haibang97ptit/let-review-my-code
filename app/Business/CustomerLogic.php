<?php


namespace App\Business;


use App\Models\Customer;
use Illuminate\Support\Facades\Config;
use Illuminate\Http\Request;

class CustomerLogic extends BaseLogic
{

    /**
     * @inheritDoc
     */
    public function model()
    {
        return Customer::class;
    }

    public function getphone(){
       return $query = Customer::select('customers.phone_number')->distinct()->pluck('phone_number');
    }

    public function getemail(){
        return $query = Customer::select('customers.email')->distinct()->pluck('email');
    }

    public function getlistsearch(Request $request){
        $query = Customer::select('customers.*')
            ->wherenull('deleted_at');
        if ($request){
            if(isset($request->name)){
                $query->where('customers.name','LIKE','%'. $request->name.'%');
            }
            if(isset($request->phone_number)){
                $query->where('customers.phone_number','LIKE','%'.$request->phone_number.'%');
            }
            if (isset($request->email)){
                $query->where('customers.email','LIKE','%'.$request->email.'%');
            }
            if (isset($request->page) && is_numeric($request->page)) {
                $query->offset($request->page * Config::get('constants.pagination'));
            }
//            $query->paginate(Config::get('constants.pagination'));
        }
//        dd($query->paginate(Config::get('constants.pagination')));
        return $query->paginate(Config::get('constants.pagination'));

    }
}
