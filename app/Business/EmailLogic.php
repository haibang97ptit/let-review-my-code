<?php


namespace App\Business;


use App\Models\Email;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class EmailLogic extends BaseLogic
{

    /**
     * @inheritDoc
     */
    public function model()
    {
        return Email::class;
    }
    public function getcapacity(){
        return $query=Email::select('emails.capacity')->distinct()->pluck('capacity');
    }
    public function getemailnumber(){
        return $query=Email::select('emails.email_number')->distinct()->pluck('email_number');
    }
    public function getemailforwarder(){
        return $query=Email::select('emails.email_forwarder')->distinct()->pluck('email_forwarder');
    }
    public function getemaillist(){
        return $query=Email::select('emails.email_list')->distinct()->pluck('email_list');
    }
    public function getlistsearch(Request $request){
        $query=Email::select('emails.*')
            ->wherenull('deleted_at');
        if ($request){
            if (isset($request->name)){
                $query->where('emails.name','LIKE','%'.$request->name.'%');
            }
            if (isset($request->capacity)){
                $query->where('emails.capacity','LIKE','%'.$request->capacity.'%');
            }
            if (isset($request->email_number)){
                $query->where('emails.email_number','LIKE','%'.$request->email_number.'%');
            }
            if (isset($request->email_forwarder)){
                $query->where('emails.email_forwarder','LIKE','%'.$request->email_forwarder.'%');
            }
            if (isset($request->email_list)){
                $query->where('emails.email_list','LIKE','%'.$request->email_list.'%');
            }
            if (isset($request->page) && is_numeric($request->page)) {
                $query->offset($request->page * Config::get('constants.pagination'));
            }
        }
        return $query->paginate(Config::get('constants.pagination'));
    }
}
