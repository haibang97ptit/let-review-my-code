<?php


namespace App\Business;


use App\Models\Ssl;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class SslLogic extends BaseLogic
{

    /**
     * @inheritDoc
     */
    public function model()
    {
        return Ssl::class;
    }
    public function getinsurancepolicy(){
        return $query=Ssl::select('ssls.insurance_policy')->distinct()->pluck('insurance_policy');
    }
    public function getdomainnumber(){
        return $query=Ssl::select('ssls.domain_number')->distinct()->pluck('domain_number');
    }
    public function getreliability(){
        return $query=Ssl::select('ssls.reliability')->distinct()->pluck('reliability');
    }
    public function getgreenbar(){
        return $query=Ssl::select('ssls.green_bar')->distinct()->pluck('green_bar');
    }
    public function getlistsearch(Request $request){
        $query=Ssl::select('ssls.*')
            ->wherenull('deleted_at');
        if ($request){
            if (isset($request->name)){
                $query->where('ssls.name','LIKE','%'.$request->name,'%');
            }
            if (isset($request->insurance_policy)){
                $query->where('ssls.insurance_policy','LIKE','%'.$request->insurance_policy,'%');
            }
            if (isset($request->domain_number)){
                $query->where('ssls.domain_number','LIKE','%'.$request->domain_number,'%');
            }
            if (isset($request->reliability)){
                $query->where('ssls.reliability','LIKE','%'.$request->reliability,'%');
            }
            if (isset($request->green_bar)){
                $query->where('ssls.green_bar','LIKE','%'.$request->green_bar,'%');
            }
            if (isset($request->page) && is_numeric($request->page)) {
                $query->offset($request->page * Config::get('constants.pagination'));
            }
        }
        return $query->paginate(Config::get('constants.pagination'));
    }

}
