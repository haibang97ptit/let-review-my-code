<?php


namespace App\Business;


use App\Models\VPS;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class VpsLogic extends BaseLogic
{

    /**
     * @inheritDoc
     */
    public function model()
    {
        return VPS::class;
    }
    public function getcpu(){
        return $query=VPS::select('vpss.cpu')->distinct()->pluck('cpu');
    }
    public function getcapacity(){
        return $query=VPS::select('vpss.capacity')->distinct()->pluck('capacity');
    }
    public function getram(){
        return $query=VPS::select('vpss.ram')->distinct()->pluck('ram');
    }
    public function getbandwidth(){
        return $query=VPS::select('vpss.bandwith')->distinct()->pluck('bandwith');
    }
    public function gettechnical(){
        return $query=VPS::select('vpss.technical')->distinct()->pluck('technical');
    }
    public function getoperatingsystem(){
        return $query=VPS::select('vpss.operating_system')->distinct()->pluck('operating_system');
    }
    public function getbackup(){
        return $query=VPS::select('vpss.backup')->distinct()->pluck('backup');
    }
    public function getlistsearch(Request $request){
        $query=VPS::select('vpss.*')
            ->wherenull('deleted_at');
        if ($request){
            if (isset($request->name)){
                $query->where('vpss.name','LIKE','%'.$request->name.'%');
            }
            if (isset($request->capacity)){
                $query->where('vpss.capacity','LIKE','%'.$request->capacity.'%');
            }
            if (isset($request->cpu)){
                $query->where('vpss.cpu','LIKE','%'.$request->cpu.'%');
            }
            if (isset($request->ram)){
                $query->where('vpss.ram','LIKE','%'.$request->ram.'%');
            }
            if (isset($request->bandwith)){
                $query->where('vpss.bandwith','LIKE','%'.$request->bandwith.'%');
            }
            if (isset($request->technical)){
                $query->where('vpss.technical','LIKE','%'.$request->technical.'%');
            }
            if (isset($request->operating_system)){
                $query->where('vpss.operating_system','LIKE','%'.$request->operating_system.'%');
            }
            if (isset($request->backup)){
                $query->where('vpss.backup','LIKE','%'.$request->backup.'%');
            }
            if (isset($request->page) && is_numeric($request->page)) {
                $query->offset($request->page * Config::get('constants.pagination'));
            }
        }
        return $query->paginate(Config::get('constants.pagination'));
    }
}
