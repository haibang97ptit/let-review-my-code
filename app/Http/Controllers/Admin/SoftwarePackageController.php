<?php

namespace App\Http\Controllers\Admin;

use App\Models\SoftwarePackage;
use Illuminate\Http\Request;

class SoftwarePackageController extends AdminController
{
    public function index() {
//        $this->authorize('software-access');

        $key = isset($request->key) ? $request->key : '';
        $softwarePackage = new SoftwarePackage();
        $softwarePackages = $softwarePackage->getAll($key, 4);
        if (isset($request->amount)) {
            $softwarePackages = $softwarePackage->getAll($key, $request->amount);
        }
        return view('admin.software-package.index',compact('softwarePackages'));
    }

    //Show row with key
    public function searchRow(Request $request) {
        $softwarePackage = new SoftwarePackage();
        $softwarePackages = $softwarePackage->getAll($request->key, 4);
        if ($request->amount !== null) {
            $softwarePackages = $softwarePackage->getAll($request->key, $request->amount);
        }
        return view('admin.software-package.card', compact('softwarePackages'));
    }

    //Show create form
    public function createForm(Request $request) {
        $softwarePackage = SoftwarePackage::find($request->id);
        return view('admin.software-package.update-add', compact('softwarePackage'));
    }

    //Save to db
    public function store(Request $request) {
        try {
            $softwarePackage = new SoftwarePackage();
            if ($request->id)  {
                $softwarePackage = SoftwarePackage::find($request->id);
                $softwarePackage->rules['name'] = 'required|unique:software_packages,name,'.$softwarePackage->id.',id,deleted_at,NULL';
            }
            //Validation
            $validator = $this->validateInput($request->all(), $softwarePackage->rules, $softwarePackage->messages);
            if ($validator->fails()) {
                return redirect()->back()->withInput()->withErrors($validator);
            }
            SoftwarePackage::updateOrCreate(['id' => $request->id], $request->all());
            return redirect(route('admin.software-packages.index'))->with('success', __('general.create-success'));
        } catch (\Exception $exception) {
            return redirect(route('admin.software-packages.index'))->with('fail', __('general.create-fail'));
        }
    }

    //Delete one row
    public function destroy(Request $request) {
        try {
            SoftwarePackage::findOrFail($request->id)->delete();
            return redirect()->back()->with('success', __('general.delete-success'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('fail', __('general.delete-fail'));
        }
    }

    //Delete row selected
    public function destroySelect(Request $request) {
        try {
            $allVals = explode(',', $request->allValsDelete[0]);
            foreach ($allVals as $item) {
                SoftwarePackage::findOrFail($item)->delete();
            }
            return redirect()->back()->with('success', __('general.delete-success'));
        }
        catch (\Exception $exception) {
            return redirect()->back()->with('fail', __('general.delete-fail'));
        }
    }

    //Trash index
    public function trashIndex(Request $request) {
        $key = isset($request->key) ? $request->key : '';
        $softwarePackage = new SoftwarePackage();
        $softwarePackages = $softwarePackage->getAllTrash($key, 4);
        if (isset($request->amount)) {
            $softwarePackages = $softwarePackage->getAllTrash($key, $request->amount);
        }
        return view('admin.software-package.trash.index',compact('softwarePackages'));
    }

    //Show row - trash
    public function trashSearchRow(Request $request) {
        $softwarePackage = new SoftwarePackage();
        $softwarePackages = $softwarePackage->getAllTrash($request->key, 4);
        if ($request->amount !== null) {
            $softwarePackages = $softwarePackage->getAllTrash($request->key, $request->amount);
        }
        return view('admin.software-package.trash.card', compact('softwarePackages'));
    }

    //Force delete 1 row
    public function forceDelete(Request $request) {
        try {
            SoftwarePackage::withTrashed()->find($request->id)->forceDelete();
            return redirect()->back()->with('success', __('general.delete-success'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('fail', __('general.delete-fail'));
        }
    }

    //Force delete row selected
    public function forceDeleteSelect(Request $request) {
        try {
            $allVals = explode(',', $request->allVals[0]);
            foreach ($allVals as $item) {
                SoftwarePackage::withTrashed()->find($item)->forceDelete();
            }
            return redirect()->back()->with('success', __('general.delete-success'));
        }
        catch (\Exception $exception) {
            return redirect()->back()->with('fail', __('general.delete-fail'));
        }
    }

    //Restore from trash
    public function restore(Request $request) {
        try {
            SoftwarePackage::withTrashed()->find($request->id)->restore();
            return redirect(route('admin.software-packages.index'))->with('success', __('general.restore-success'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('fail', __('general.restore-fail'));
        }
    }

    //Restore row selected
    public function restoreSelect(Request $request) {
        try {
            $allVals = explode(',', $request->allVals[0]);
            foreach ($allVals as $item) {
                SoftwarePackage::withTrashed()->find($item)->restore();
            }
            return redirect()->back()->with('success', __('general.restore-success'));
        }
        catch (\Exception $exception) {
            return redirect()->back()->with('fail', __('general.restore-fail'));
        }
    }

}
