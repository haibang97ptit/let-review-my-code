<?php

namespace App\Http\Controllers\Admin;

use App\Models\Domain;
use Illuminate\Http\Request;

class DomainController extends AdminController
{
    public function index() {
//        $this->authorize('software-access');

        $key = isset($request->key) ? $request->key : '';
        $domain = new Domain();
        $domains = $domain->getAll($key, 4);
        if (isset($request->amount)) {
            $domains = $domain->getAll($key, $request->amount);
        }
        return view('admin.domain.index',compact('domains'));
    }

    //Show row with key
    public function searchRow(Request $request) {
        $domain = new Domain();
        $domains = $domain->getAll($request->key, 4);
        if ($request->amount !== null) {
            $domains = $domain->getAll($request->key, $request->amount);
        }
        return view('admin.domain.card', compact('domains'));
    }

    //Show create form
    public function createForm(Request $request) {
        $domain = Domain::find($request->id);
        return view('admin.domain.update-add', compact('domain'));
    }

    //Save to db
    public function store(Request $request) {
        try {
            $check = 0;
            $domain = new Domain();
            if ($request->id)  {
                $domain = Domain::find($request->id);
                $domain->rules['name'] = 'required|unique:domains,name,'.$domain->id.',id,deleted_at,NULL';
                $check = 1;
            }
            //Validation
            $validator = $this->validateInput($request->all(), $domain->rules, $domain->messages);
            if ($validator->fails()) {
                return redirect()->back()->withInput()->withErrors($validator);
            }
            Domain::updateOrCreate(['id' => $request->id], $request->all());
            if ($check == 0) {
                return redirect(route('admin.domains.index'))->with('success', __('general.create-success'));
            }
            return redirect(route('admin.domains.index'))->with('success', __('general.update-success'));
        } catch (\Exception $exception) {
            return redirect(route('admin.domains.index'))->with('fail', __('general.manipulation-fail'));
        }
    }

    //Delete one row
    public function destroy(Request $request) {
        try {
            Domain::findOrFail($request->id)->delete();
            return redirect()->back()->with('success', __('general.delete-success'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('fail', __('general.delete-fail'));
        }
    }

    //Delete row selected
    public function destroySelect(Request $request) {
        try {
            $allVals = explode(',', $request->allValsDelete[0]);
            foreach ($allVals as $item) {
                Domain::findOrFail($item)->delete();
            }
            return redirect()->back()->with('success', __('general.delete-success'));
        }
        catch (\Exception $exception) {
            return redirect()->back()->with('fail', __('general.delete-fail'));
        }
    }
}
