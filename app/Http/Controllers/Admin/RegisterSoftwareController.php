<?php

namespace App\Http\Controllers\Admin;

use App\Models\CashBook;
use App\Models\Customer;
use App\Models\RegisterSoftware;
use App\Models\Software;
use App\Models\SoftwarePackage;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use League\Flysystem\Exception;

class RegisterSoftwareController extends AdminController
{
    //Index
    public function index(Request $request) {
        $registerSoftware = new RegisterSoftware();
        $registerSoftwares = $registerSoftware->getAll(10);
        if (isset($request->amount)) {
            $registerSoftwares = $registerSoftware->getAll($request->amount);
        }
        $customers = Customer::get();
        $softwares = Software::get();
        return view('admin.register-software.index')
            ->with(compact('registerSoftwares'))
            ->with(compact('customers'))
            ->with(compact('softwares'));
    }

    //Search row
    public function searchRow(Request $request) {
        $registerSoftware = new RegisterSoftware();
        $registerSoftwares = $registerSoftware->getSearchRow($request,10);
        if ($request->amount !== null) {
            $registerSoftwares = $registerSoftware->getSearchRow($request, $request->amount);
        }
        $customers = Customer::get();
        $softwares = Software::get();
        return view('admin.register-software.index')
            ->with(compact('registerSoftwares'))
            ->with(compact('customers'))
            ->with(compact('softwares'));
    }

    //Show create form
    public function createForm(Request $request)
    {
        $registerSoftware = RegisterSoftware::find($request->id);
        $customers = Customer::get();
        $softwares = Software::get();
        $softwarePackages = SoftwarePackage::get();
        return view('admin.register-software.update-add')
            ->with(compact('customers'))
            ->with(compact('softwares'))
            ->with(compact('softwarePackages'))
            ->with(compact('registerSoftware'));
    }

    //Save to db
    public function store(Request $request) {
        try {
            $check = 0;
            $dt = Carbon::now('+07:00');
            $registerSoftware = new RegisterSoftware();
            if ($request->id) {
                $registerSoftware = RegisterSoftware::find($request->id);
                $check = 1;
            }
            //Validation
            $validator = $this->validateInput($request->all(), $registerSoftware->rules, $registerSoftware->messages);
            if ($validator->fails()) {
                return redirect()->back()->withInput()->withErrors($validator);
            }
            $registerSoftware->id_customer = $request->id_customer;
            $registerSoftware->code = $registerSoftware->createCode();
            $registerSoftware->address_domain = $request->address_domain;
            $registerSoftware->id_software = $request->id_software;
            $registerSoftware->id_software_package = $request->id_software_package;

            //Version: trial | official
            $registerSoftware->version = $request->version;
            $registerSoftware->notes = $request->notes;
            $registerSoftware->start_date = $request->start_date.' '.$dt->format('H:i');

            //Add data with official
            if ($request->version == "official") {
                $registerSoftware->time_use = $request->time_use;
                $registerSoftware->price = SoftwarePackage::find($request->id_software_package)->price * $request->time_use;
                //Set end date (extend)
                if ($request->end_date) {
                    $registerSoftware->start_date = $request->start_date;
                    $dt2 = Carbon::parse($request->end_date);
                    $dt2->addMonth($registerSoftware->time_use);
                    $registerSoftware->end_date = $dt2->format('Y-m-d H:i');
                    $check = 2;
                }
                else {
                    $dt2 = Carbon::parse($registerSoftware->start_date);
                    $dt2->addMonth($registerSoftware->time_use);
                    $registerSoftware->end_date = $dt2->format('Y-m-d H:i');
                }
                $registerSoftware->status_register = "unpaid";
            }
            else {
                $registerSoftware->status_register = "paid";
            }
            $registerSoftware->save();
            if ($check == 0) {
                $resultText = "register-success";
            }
            elseif ($check == 1) {
                $resultText = "update-success";
            }
            else {
                $resultText = "extend-success";
            }
            return redirect(route('admin.register-softwares.index'))->with('success', __('general.'.$resultText));
        }
        catch (\Exception $exception) {
            return redirect(route('admin.register-softwares.index'))->with('fail', __('general.manipulation-fail'));
        }
    }

    //Delete one row
    public function destroy(Request $request) {
        try {
            RegisterSoftware::findOrFail($request->id)->delete();
            return redirect()->back()->with('success', __('general.delete-success'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('fail', __('general.delete-fail'));
        }
    }

    //Delete row selected
    public function destroySelect(Request $request) {
        try {
            $allVals = explode(',', $request->allValsDelete[0]);
            foreach ($allVals as $item) {
                RegisterSoftware::findOrFail($item)->delete();
            }
            return redirect()->back()->with('success', __('general.delete-success'));
        }
        catch (\Exception $exception) {
            return redirect()->back()->with('fail', __('general.delete-fail'));
        }
    }

    //Trash index
    public function trashIndex(Request $request) {
        $registerSoftware = new RegisterSoftware();
        $registerSoftwares = $registerSoftware->getAllTrash(10);
        if (isset($request->amount)) {
            $registerSoftwares = $registerSoftware->getAllTrash($request->amount);
        }
        $customers = Customer::get();
        $softwares = Software::get();
        return view('admin.register-software.trash.index')
            ->with(compact('registerSoftwares'))
            ->with(compact('customers'))
            ->with(compact('softwares'));
    }

    //Show row - trash
    public function trashSearchRow(Request $request) {
        $registerSoftware = new RegisterSoftware();
        $registerSoftwares = $registerSoftware->getTrashSearchRow($request,10);
        if ($request->amount !== null) {
            $registerSoftwares = $registerSoftware->getTrashSearchRow($request, $request->amount);
        }
        $customers = Customer::get();
        $softwares = Software::get();
        return view('admin.register-software.trash.index')
            ->with(compact('registerSoftwares'))
            ->with(compact('customers'))
            ->with(compact('softwares'));
    }

    //Force delete 1 row
    public function forceDelete(Request $request) {
        try {
            RegisterSoftware::withTrashed()->find($request->id)->forceDelete();
            return redirect()->back()->with('success', __('general.delete-success'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('fail', __('general.delete-fail'));
        }
    }

    //Force delete row selected
    public function forceDeleteSelect(Request $request) {
        try {
            $allVals = explode(',', $request->allVals[0]);
            foreach ($allVals as $item) {
                RegisterSoftware::withTrashed()->find($item)->forceDelete();
            }
            return redirect()->back()->with('success', __('general.delete-success'));
        }
        catch (\Exception $exception) {
            return redirect()->back()->with('fail', __('general.delete-fail'));
        }
    }

    //Restore from trash
    public function restore(Request $request) {
        try {
            RegisterSoftware::withTrashed()->find($request->id)->restore();
            return redirect()->back()->with('success', __('general.restore-success'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('fail', __('general.restore-fail'));
        }
    }

    //Restore row selected
    public function restoreSelect(Request $request) {
        try {
            $allVals = explode(',', $request->allVals[0]);
            foreach ($allVals as $item) {
                RegisterSoftware::withTrashed()->find($item)->restore();
            }
            return redirect()->back()->with('success', __('general.restore-success'));
        }
        catch (\Exception $exception) {
            return redirect()->back()->with('fail', __('general.restore-fail'));
        }
    }

    //Change status
    public function changeStatus(Request $request) {
        try {
            $registerSoftware = RegisterSoftware::find($request->id);
            $registerSoftware->status_register = $request->status;
            $registerSoftware->save();
            if ($request->status == "paid") {
                $cashBook = new CashBook();
                $cashBook->code = $registerSoftware->code;
                $cashBook->type = "software";
                $cashBook->save();
            }
            else {
                CashBook::where('code', '=', $registerSoftware->code)->delete();
            }
        }
        catch (\Exception $exception) {
            return $exception;
        }
    }

}
