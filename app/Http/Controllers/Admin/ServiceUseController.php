<?php

namespace App\Http\Controllers\Admin;

use App\Models\Customer;
use App\Models\RegisterService;
use App\Models\RegisterSoftware;
use App\Models\ServiceUse;
use Illuminate\Http\Request;

class ServiceUseController extends AdminController
{
    //Index
    public function index(Request $request)
    {
        $serviceUse = new ServiceUse();
        $serviceUses = $serviceUse->getAll(10);
        if (isset($request->amount)) {
            $serviceUses = $serviceUse->getAll($request->amount);
        }
        $customers = Customer::get();
        return view('admin.service-use.index', compact('serviceUses'), compact('customers'));
    }

    //Filter
    public function filter(Request $request) {
        $key = isset($request->key) ? $request->key : '';
        $serviceUse = new ServiceUse();
        $customers = Customer::get();
        if ($request->type == "domain") {
            $serviceUses = $serviceUse->filterDomain(10, $key);
            if (isset($request->amount)) {
                $serviceUses = $serviceUse->filterDomain($request->amount, $key);
            }
            return view('admin.service-use.index', compact('serviceUses'), compact('customers'));
        }
        elseif ($request->type == "hosting") {
            $serviceUses = $serviceUse->filterHosting(10, $key);
            if (isset($request->amount)) {
                $serviceUses = $serviceUse->filterHosting($request->amount, $key);
            }
            return view('admin.service-use.index', compact('serviceUses'), compact('customers'));
        }
        elseif ($request->type == "vps") {
            $serviceUses = $serviceUse->filterVps(10, $key);
            if (isset($request->amount)) {
                $serviceUses = $serviceUse->filterVps($request->amount, $key);
            }
            return view('admin.service-use.index', compact('serviceUses'), compact('customers'));
        }
        elseif ($request->type == "email") {
            $serviceUses = $serviceUse->filterEmail(10, $key);
            if (isset($request->amount)) {
                $serviceUses = $serviceUse->filterEmail($request->amount, $key);
            }
            return view('admin.service-use.index', compact('serviceUses'), compact('customers'));
        }
        elseif ($request->type == "ssl") {
            $serviceUses = $serviceUse->filterSsl(10, $key);
            if (isset($request->amount)) {
                $serviceUses = $serviceUse->filterSsl($request->amount, $key);
            }
            return view('admin.service-use.index', compact('serviceUses'), compact('customers'));
        }
        elseif ($request->type == "website") {
            $serviceUses = $serviceUse->filterWebsite(10, $key);
            if (isset($request->amount)) {
                $serviceUses = $serviceUse->filterWebsite($request->amount, $key);
            }
            return view('admin.service-use.index', compact('serviceUses'), compact('customers'));
        }
        else {
            $serviceUses = $serviceUse->filterSoftware(10, $key);
            if (isset($request->amount)) {
                $serviceUses = $serviceUse->filterSoftware($request->amount, $key);
            }
            return view('admin.service-use.index', compact('serviceUses'), compact('customers'));
        }
    }

    //Show row with key
    public function searchRow(Request $request) {
        $serviceUse = new ServiceUse();
        $customers = Customer::get();
        if ($request->type == "domain") {
            $serviceUses = $serviceUse->filterDomain(10, $request->key);
            if ($request->amount !== null) {
                $serviceUses = $serviceUse->filterDomain($request->amount, $request->key);
            }
            return view('admin.service-use.body', compact('serviceUses'), compact('customers'));
        }
        elseif ($request->type == "hosting") {
            $serviceUses = $serviceUse->filterHosting(10, $request->key);
            if ($request->amount !== null) {
                $serviceUses = $serviceUse->filterHosting($request->amount, $request->key);
            }
            return view('admin.service-use.body', compact('serviceUses'), compact('customers'));
        }
        elseif ($request->type == "vps") {
            $serviceUses = $serviceUse->filterVps(10, $request->key);
            if ($request->amount !== null) {
                $serviceUses = $serviceUse->filterVps($request->amount, $request->key);
            }
            return view('admin.service-use.body', compact('serviceUses'), compact('customers'));
        }
        elseif ($request->type == "email") {
            $serviceUses = $serviceUse->filterEmail(10, $request->key);
            if ($request->amount !== null) {
                $serviceUses = $serviceUse->filterEmail($request->amount, $request->key);
            }
            return view('admin.service-use.body', compact('serviceUses'), compact('customers'));
        }
        elseif ($request->type == "ssl") {
            $serviceUses = $serviceUse->filterSsl(10, $request->key);
            if ($request->amount !== null) {
                $serviceUses = $serviceUse->filterSsl($request->amount, $request->key);
            }
            return view('admin.service-use.body', compact('serviceUses'), compact('customers'));
        }
        elseif ($request->type == "website") {
            $serviceUses = $serviceUse->filterWebsite(10, $request->key);
            if ($request->amount !== null) {
                $serviceUses = $serviceUse->filterWebsite($request->amount, $request->key);
            }
            return view('admin.service-use.body', compact('serviceUses'), compact('customers'));
        }
        else {
            $serviceUses = $serviceUse->filterSoftware(10, $request->key);
            if ($request->amount !== null) {
                $serviceUses = $serviceUse->filterSoftware($request->amount, $request->key);
            }
            return view('admin.service-use.body', compact('serviceUses'), compact('customers'));
        }
    }
}
