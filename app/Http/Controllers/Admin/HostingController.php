<?php

namespace App\Http\Controllers\Admin;

use App\Models\Hosting;
use Illuminate\Http\Request;

class HostingController extends AdminController
{
    public function index() {
//        $this->authorize('software-access');

        $key = isset($request->key) ? $request->key : '';
        $hosting = new Hosting();
        $hostings = $hosting->getAll($key, 4);
        if (isset($request->amount)) {
            $hostings = $hosting->getAll($key, $request->amount);
        }
        return view('admin.hosting.index',compact('hostings'));
    }

    //Show row with key
    public function searchRow(Request $request) {
        $hosting = new Hosting();
        $hostings = $hosting->getAll($request->key, 4);
        if ($request->amount !== null) {
            $hostings = $hosting->getAll($request->key, $request->amount);
        }
        return view('admin.hosting.card', compact('hostings'));
    }

    //Show create form
    public function createForm(Request $request) {
        $hosting = Hosting::find($request->id);
        return view('admin.hosting.update-add', compact('hosting'));
    }

    //Save to db
    public function store(Request $request) {
        try {
            $check = 0;
            $hosting = new Hosting();
            if ($request->id)  {
                $hosting = Hosting::find($request->id);
                $hosting->rules['name'] = 'required|unique:hostings,name,'.$hosting->id.',id,deleted_at,NULL';
                $check = 1;
            }
            //Validation
            $validator = $this->validateInput($request->all(), $hosting->rules, $hosting->messages);
            if ($validator->fails()) {
                return redirect()->back()->withInput()->withErrors($validator);
            }
            Hosting::updateOrCreate(['id' => $request->id], $request->all());
            if ($check == 0) {
                return redirect(route('admin.hostings.index'))->with('success', __('general.create-success'));
            }
            return redirect(route('admin.hostings.index'))->with('success', __('general.update-success'));
        } catch (\Exception $exception) {
            return redirect(route('admin.hostings.index'))->with('fail', __('general.manipulation-fail'));
        }
    }

    //Delete one row
    public function destroy(Request $request) {
        try {
            Hosting::findOrFail($request->id)->delete();
            return redirect()->back()->with('success', __('general.delete-success'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('fail', __('general.delete-fail'));
        }
    }

    //Delete row selected
    public function destroySelect(Request $request) {
        try {
            $allVals = explode(',', $request->allValsDelete[0]);
            foreach ($allVals as $item) {
                Hosting::findOrFail($item)->delete();
            }
            return redirect()->back()->with('success', __('general.delete-success'));
        }
        catch (\Exception $exception) {
            return redirect()->back()->with('fail', __('general.delete-fail'));
        }
    }

}
