<?php

namespace App\Http\Controllers\Admin;

use App\Models\Customer;
use App\Models\Role;
use App\Models\State;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class CustomerController extends AdminController
{
    //Index
    public function index(Request $request)
    {
        $this->authorize('customer-access');

        $key = isset($request->key) ? $request->key : '';
        $customer = new Customer();
        $customers = $customer->getAll($key, 10);
        if (isset($request->amount)) {
            $customers = $customer->getAll($key, $request->amount);
        }
        return view('admin.customer.index', compact('customers'));
    }

    //Show row with key
    public function searchRow(Request $request) {
        $customer = new Customer();
        $customers = $customer->getAll($request->key, 10);
        if ($request->amount !== null) {
            $customers = $customer->getAll($request->key, $request->amount);
        }
        return view('admin.customer.search-row', compact('customers'));
    }

    //Create form
    public function createForm(Request $request) {
        $customer = Customer::find($request->id);
        $states = State::orderBy('name', 'asc')->get();
        return view('admin.customer.update-add', compact('states'), compact('customer'));
    }

    //Save to db
    public function store(Request $request) {
        try {
            $customer = new Customer();

            $user = new User();
            if ($request->id) {
                //Create if user deleted
                $user = User::where('name', '=', $request->name)->first();
                if ($user == null) {
                    $user = new User();
                };
                $user->rules['email'] = 'required|unique:users,email,'.$user->id.',id,deleted_at,NULL';
                $customer = Customer::find($request->id);
                $customer->rules['phone_number'] = 'required';
                $customer->rules['email'] = 'required|unique:customers,email,'.$customer->id.',id,deleted_at,NULL';
                $customer->rules['address'] = 'required';
                $customer->rules['birthday'] = 'required';
                $customer->rules['state'] = 'required';
                $customer->rules['first_name'] = 'required';
                $customer->rules['last_name'] = 'required';
                $customer->rules['password'] = '';
            }
            //Validation
            $validator = $this->validateInput($request->all(), $customer->rules, $customer->messages);
            if ($validator->fails()) {
                return redirect()->back()->withInput()->withErrors($validator);
            }
            $customer->phone_number = $request->phone_number;
            $customer->email = $request->email;
            $customer->address = $request->address;
            $customer->birthday = $request->birthday;
            $customer->state = $request->state;
            $customer->first_name = $request->first_name;
            $customer->last_name = $request->last_name;
            $customer->note = isset($request->note) ? $request->note : '';
            $customer->user_id = Auth::user()->id;
            //Add account's user
            if (($request->password != null & $request->id != null) || $request->id == null) {
                //Add account with viewer role
                $role = Role::where('name', '=', 'viewer')->first();
                $role_id = isset($role) ? $role->id : null;
                //Validation user
                $validator = $this->validateInput(['name' => $request->first_name, 'email' => $request->email, 'password' => $request->password, 'role_id' => $role_id], $user->rules, $user->messages);
                if ($validator->fails()) {
                    return redirect()->back()->withInput()->withErrors($validator);
                }
                //Save user table
                $user->name = $request->first_name . ' ' . $request->last_name;
                $user->email = $request->email;
                $user->password = Hash::make($request->password);
                $user->role_id = $role_id;
                $user->save();
            }
            $customer->save();
            return redirect(route('admin.customers.index'))->with('success', __('general.create-success'));
        } catch (\Exception $exception) {
            return redirect(route('admin.customers.index'))->with('fail', __('general.create-fail'));
        }
    }

    //Delete 1 row
    public function destroy(Request $request) {
        try {
            $customer = Customer::find($request->id);
            User::where('email', '=', $customer->email)->delete();
            $customer->delete();
            return redirect()->back()->with('success', __('general.delete-success'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('fail', __('general.delete-fail'));
        }
    }

    //Delete row selected
    public function destroySelect(Request $request) {
        try {
            $allVals = explode(',', $request->allValsDelete[0]);
            foreach ($allVals as $item) {
                $customer = Customer::find($item);
                User::where('email', '=', $customer->email)->delete();
                $customer->delete();
            }
            return redirect()->back()->with('success', __('general.delete-success'));
        }
        catch (\Exception $exception) {
            return redirect()->back()->with('fail', __('general.delete-fail'));
        }
    }

    //Trash index
    public function trashIndex(Request $request) {
        $key = isset($request->key) ? $request->key : '';
        $customer = new Customer();
        $customers = $customer->getAllTrash($key, 10);
        if (isset($request->amount)) {
            $customers = $customer->getAllTrash($key, $request->amount);
        }
        return view('admin.customer.trash.index', compact('customers'));
    }

    //Show row - trash
    public function trashSearchRow(Request $request) {
        $customer = new Customer();
        $customers = $customer->getAllTrash($request->key, 10);
        if ($request->amount !== null) {
            $customers = $customer->getAllTrash($request->key, $request->amount);
        }
        return view('admin.customer.trash.search-row', compact('customers'));
    }

    //Force delete 1 row
    public function forceDelete(Request $request) {
        try {
            $customer = Customer::withTrashed()->find($request->id);
            User::withTrashed()->where('email', '=', $customer->email)->forceDelete();
            $customer->forceDelete();
            return redirect()->back()->with('success', __('general.delete-success'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('fail', __('general.delete-fail'));
        }
    }

    //Force delete row selected
    public function forceDeleteSelect(Request $request) {
        try {
            $allVals = explode(',', $request->allValsDelete[0]);
            foreach ($allVals as $item) {
                $customer = Customer::withTrashed()->find($item);
                User::withTrashed()->where('email', '=', $customer->email)->forceDelete();
                $customer->forceDelete();
            }
            return redirect()->back()->with('success', __('general.delete-success'));
        }
        catch (\Exception $exception) {
            return redirect()->back()->with('fail', __('general.delete-fail'));
        }
    }

    //Restore from trash
    public function restore(Request $request) {
        try {
            $customer = Customer::withTrashed()->find($request->id);
            User::withTrashed()->where('email', '=', $customer->email)->restore();
            $customer->restore();
            return redirect(route('admin.customers.index'))->with('success', __('general.restore-success'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('fail', __('general.restore-fail'));
        }
    }
}
