<?php

namespace App\Http\Controllers\Admin;

use App\Models\Email;
use Illuminate\Http\Request;

class EmailController extends AdminController
{
    public function index() {
//        $this->authorize('software-access');

        $key = isset($request->key) ? $request->key : '';
        $email = new Email();
        $emails = $email->getAll($key, 4);
        if (isset($request->amount)) {
            $emails = $email->getAll($key, $request->amount);
        }
        return view('admin.email.index',compact('emails'));
    }

    //Show row with key
    public function searchRow(Request $request) {
        $email = new Email();
        $emails = $email->getAll($request->key, 4);
        if ($request->amount !== null) {
            $emails = $email->getAll($request->key, $request->amount);
        }
        return view('admin.email.card', compact('emails'));
    }

    //Show create form
    public function createForm(Request $request) {
        $email = Email::find($request->id);
        return view('admin.email.update-add', compact('email'));
    }

    //Save to db
    public function store(Request $request) {
        try {
            $check = 0;
            $email = new Email();
            if ($request->id)  {
                $email = Email::find($request->id);
                $email->rules['name'] = 'required|unique:emails,name,'.$email->id.',id,deleted_at,NULL';
                $check = 1;
            }
            //Validation
            $validator = $this->validateInput($request->all(), $email->rules, $email->messages);
            if ($validator->fails()) {
                return redirect()->back()->withInput()->withErrors($validator);
            }
            Email::updateOrCreate(['id' => $request->id], $request->all());
            if ($check == 0) {
                return redirect(route('admin.emails.index'))->with('success', __('general.create-success'));
            }
            return redirect(route('admin.emails.index'))->with('success', __('general.update-success'));
        } catch (\Exception $exception) {
            return redirect(route('admin.emails.index'))->with('fail', __('general.manipulation-fail'));
        }
    }

    //Delete one row
    public function destroy(Request $request) {
        try {
            Email::findOrFail($request->id)->delete();
            return redirect()->back()->with('success', __('general.delete-success'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('fail', __('general.delete-fail'));
        }
    }

    //Delete row selected
    public function destroySelect(Request $request) {
        try {
            $allVals = explode(',', $request->allValsDelete[0]);
            foreach ($allVals as $item) {
                Email::findOrFail($item)->delete();
            }
            return redirect()->back()->with('success', __('general.delete-success'));
        }
        catch (\Exception $exception) {
            return redirect()->back()->with('fail', __('general.delete-fail'));
        }
    }
}
