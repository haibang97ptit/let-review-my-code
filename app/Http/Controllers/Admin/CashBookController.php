<?php

namespace App\Http\Controllers\Admin;

use App\Business\CashBookLogic;
use App\Charts\CashChart;
use App\Charts\SampleChart;
use App\Models\CashBook;
use App\Models\Expenditure;
use App\Models\Revenue;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Config;

class CashBookController extends Controller
{
    public function index(Request $request) {
//        $this->authorize('cash-book-access');
        $cashBook1 = CashBook::join('register_services as rs', 'rs.code', '=', 'cash_books.code')->get();
        $cashBook2 = CashBook::join('register_softwares as rsw', 'rsw.code', '=', 'cash_books.code')->get();

        $fees = new Collection();
        for ($i = 1; $i <= 12; $i++) {
            $count = 0;
            foreach ($cashBook1 as $a) {
                $month = Carbon::parse($a->start_date);
                if ($month->month == $i) {
                    $count += $a->price;
                }
            }
            foreach ($cashBook2 as $b) {
                $month = Carbon::parse($b->start_date);
                if ($month->month == $i) {
                    $count += $b->price;
                }
            }
            $fees->push($count);
        }

        $chart = new CashChart();
        $chart->labels(['Tháng 1', 'Tháng 2', 'Tháng 3', 'Tháng 4', 'Tháng 5', 'Tháng 6', 'Tháng 7', 'Tháng 8', 'Tháng 9', 'Tháng 10', 'Tháng 11', 'Tháng 12']);
        $chart->dataset('Doanh thu', 'line', [$fees[0], $fees[1], $fees[2], $fees[3], $fees[4], $fees[5], $fees[6], $fees[7], $fees[8], $fees[9], $fees[10], $fees[11]])
            ->color('#eb4034')
            ->fill(false);
        $chart->title("Doanh thu trong năm", '40', '#eb4034');


        $key = isset($request->key) ? $request->key : '';
        $cashBook = new CashBook();
        $cashBooks = $cashBook->getAll($key, 10);
        if (isset($request->amount)) {
            $cashBooks = $cashBook->getAll($key, $request->amount);
        }

        return view('admin.cash-book.index', compact('chart'), compact('cashBooks'));
    }

    //Show row with key
    public function searchRow(Request $request) {
        $cashBook = new CashBook();
        $cashBooks = $cashBook->getAll($request->key, 10);
        if ($request->amount !== null) {
            $cashBooks = $cashBook->getAll($request->key, $request->amount);
        }
        return view('admin.cash-book.body', compact('cashBooks'));
    }

    //Delete one row
    public function destroy(Request $request)
    {
        try {
            CashBook::findOrFail($request->id)->delete();
            return redirect()->back()->with('success', __('general.delete-success'));
        } catch (\Exception $e) {
            return redirect()->back()->with('fail', __('general.delete-fail'));
        }
    }

    //Delete row selected
    public function destroySelect(Request $request) {
        try {
            $allVals = explode(',', $request->allValsDelete[0]);
            foreach ($allVals as $item) {
                CashBook::findOrFail($item)->delete();
            }
            return redirect()->back()->with('success', __('general.delete-success'));
        }
        catch (\Exception $exception) {
            return redirect()->back()->with('fail', __('general.delete-fail'));
        }
    }
}
