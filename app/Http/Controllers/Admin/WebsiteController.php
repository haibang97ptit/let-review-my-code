<?php

namespace App\Http\Controllers\Admin;

use App\Models\Website;
use Illuminate\Http\Request;

class WebsiteController extends AdminController
{
    public function index() {
//        $this->authorize('software-access');

        $key = isset($request->key) ? $request->key : '';
        $website = new Website();
        $websites = $website->getAll($key, 4);
        if (isset($request->amount)) {
            $websites = $website->getAll($key, $request->amount);
        }
        return view('admin.website.index',compact('websites'));
    }

    //Show row with key
    public function searchRow(Request $request) {
        $website = new Website();
        $websites = $website->getAll($request->key, 4);
        if ($request->amount !== null) {
            $websites = $website->getAll($request->key, $request->amount);
        }
        return view('admin.website.card', compact('websites'));
    }

    //Show create form
    public function createForm(Request $request) {
        $website = Website::find($request->id);
        return view('admin.website.update-add', compact('website'));
    }

    //Save to db
    public function store(Request $request) {
        try {
            $check = 0;
            $website = new Website();
            if ($request->id)  {
                $website = Website::find($request->id);
                $website->rules['name'] = 'required|unique:websites,name,'.$website->id.',id,deleted_at,NULL';
                $check = 1;
            }
            //Validation
            $validator = $this->validateInput($request->all(), $website->rules, $website->messages);
            if ($validator->fails()) {
                return redirect()->back()->withInput()->withErrors($validator);
            }
            Website::updateOrCreate(['id' => $request->id], $request->all());
            if ($check == 0) {
                return redirect(route('admin.websites.index'))->with('success', __('general.create-success'));
            }
            return redirect(route('admin.websites.index'))->with('success', __('general.update-success'));
        } catch (\Exception $exception) {
            return redirect(route('admin.websites.index'))->with('fail', __('general.manipulation-fail'));
        }
    }

    //Delete one row
    public function destroy(Request $request) {
        try {
            Website::findOrFail($request->id)->delete();
            return redirect()->back()->with('success', __('general.delete-success'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('fail', __('general.delete-fail'));
        }
    }

    //Delete row selected
    public function destroySelect(Request $request) {
        try {
            $allVals = explode(',', $request->allValsDelete[0]);
            foreach ($allVals as $item) {
                Website::findOrFail($item)->delete();
            }
            return redirect()->back()->with('success', __('general.delete-success'));
        }
        catch (\Exception $exception) {
            return redirect()->back()->with('fail', __('general.delete-fail'));
        }
    }

}
