<?php

namespace App\Http\Controllers\Admin;

use App\Models\VPS;
use Illuminate\Http\Request;

class VPSController extends AdminController
{
    public function index() {
//        $this->authorize('software-access');

        $key = isset($request->key) ? $request->key : '';
        $vps = new VPS();
        $vpses = $vps->getAll($key, 4);
        if (isset($request->amount)) {
            $vpses = $vps->getAll($key, $request->amount);
        }
        return view('admin.vps.index',compact('vpses'));
    }

    //Show row with key
    public function searchRow(Request $request) {
        $vps = new VPS();
        $vpses = $vps->getAll($request->key, 4);
        if ($request->amount !== null) {
            $vpses = $vps->getAll($request->key, $request->amount);
        }
        return view('admin.vps.card', compact('vpses'));
    }

    //Show create form
    public function createForm(Request $request) {
        $vps = VPS::find($request->id);
        return view('admin.vps.update-add', compact('vps'));
    }

    //Save to db
    public function store(Request $request) {
        try {
            $check = 0;
            $vps = new VPS();
            if ($request->id)  {
                $vps = VPS::find($request->id);
                $vps->rules['name'] = 'required|unique:vpss,name,'.$vps->id.',id,deleted_at,NULL';
                $check = 1;
            }
            //Validation
            $validator = $this->validateInput($request->all(), $vps->rules, $vps->messages);
            if ($validator->fails()) {
                return redirect()->back()->withInput()->withErrors($validator);
            }
            VPS::updateOrCreate(['id' => $request->id], $request->all());
            if ($check == 0) {
                return redirect(route('admin.vpses.index'))->with('success', __('general.create-success'));
            }
            return redirect(route('admin.vpses.index'))->with('success', __('general.update-success'));
        } catch (\Exception $exception) {
            return redirect(route('admin.vpses.index'))->with('fail', __('general.manipulation-fail'));
        }
    }

    //Delete one row
    public function destroy(Request $request) {
        try {
            VPS::findOrFail($request->id)->delete();
            return redirect()->back()->with('success', __('general.delete-success'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('fail', __('general.delete-fail'));
        }
    }

    //Delete row selected
    public function destroySelect(Request $request) {
        try {
            $allVals = explode(',', $request->allValsDelete[0]);
            foreach ($allVals as $item) {
                VPS::findOrFail($item)->delete();
            }
            return redirect()->back()->with('success', __('general.delete-success'));
        }
        catch (\Exception $exception) {
            return redirect()->back()->with('fail', __('general.delete-fail'));
        }
    }
}
