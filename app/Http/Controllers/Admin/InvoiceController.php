<?php

namespace App\Http\Controllers\Admin;

use App\Business\RegisterServicesLogic;
use App\Business\InvoiceLogic;
use App\Helpers\Helper;
use App\Models\CashBook;
use App\Models\ConstantsModel;
use App\Models\Customer;
use App\Models\Invoice;
use App\Models\RegisterService;
use App\Models\RegisterSoftware;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;

class InvoiceController extends AdminController
{
    const UNDERLINE = '_';
    public function index(Request $request) {
        $this->authorize('invoice-access');

        $logic_register_services = new InvoiceLogic();
        $invoices = $logic_register_services->getListInvoice();

//        $invoices = Invoice::paginate(Config::get('constants.pagination'));
        $customer = Customer::all();
        $status = ConstantsModel::INVOICE;
        return view('admin.invoice.index', compact('invoices','status','customer'));
    }

    public function show(Request $request)
    {
        $logic_invoice = new  InvoiceLogic();
        $invoice = $logic_invoice->getIndexInvoice($request->id);
        $customer_name = $invoice->customer_name;
        return view('admin.invoice.show', compact('invoice', 'customer_name'));
    }
    public function store(Request $request){
//        dd($request->all());
        $invoice=Invoice::find($request->id);
        $invoice->status='success';
        $cash_book=new CashBook();
        $cash_book->status='invoice';
        $cash_book->code=$invoice->code;
        $cash_book->price=$invoice->total;
        try {
//trong mã code sv->service; sw->software
            if ($invoice->id_register_service!=null){
                $register_service=RegisterService::find($invoice->id_register_service);
                $register_service->status_register='success';
                $register_service->save();
            }else{
                $invoice->id_register_service=null;
            }
            if ($invoice->id_register_soft!=null){
                $register_soft=RegisterSoftware::find($invoice->id_register_soft);
                $register_soft->status_register='success';
                $register_soft->save();
            }else{
                $invoice->id_register_soft=null;
            }
            $cash_book->save();
            $invoice->save();

            return redirect(route('admin.invoices.index'))->with('success', 'Success');
        } catch (\Exception $e) {
            return redirect(route('admin.invoices.index'))->with('fail', 'Fail');

        }
    }

    public function destroy(Request $request){

        try {

            $invoice=Invoice::find($request->id);
            if ($invoice == null) {
                throw new \Exception();
            }
            $register_service=RegisterService::find($invoice->id_register_service);
            $register_soft=RegisterSoftware::find($invoice->id_register_soft);

            if ($register_service->id!=null){

                $register_service->delete();
            }else{
                if ($register_soft->id!=null){
                    $register_soft->delete();
                }
            }

            $invoice->delete();
            return redirect(route('admin.invoices.index'))->with('success','success');
        }catch (\Exception $exception){
            return redirect(route('admin.invoices.index'))->with('fail','fail'.$exception);
        }
    }


}
