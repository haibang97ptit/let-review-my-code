<?php

namespace App\Http\Controllers\Admin;

use App\Models\Ssl;
use Illuminate\Http\Request;

class SslController extends AdminController
{
    public function index() {
//        $this->authorize('software-access');

        $key = isset($request->key) ? $request->key : '';
        $ssl = new Ssl();
        $ssls = $ssl->getAll($key, 4);
        if (isset($request->amount)) {
            $ssls = $ssl->getAll($key, $request->amount);
        }
        return view('admin.ssl.index',compact('ssls'));
    }

    //Show row with key
    public function searchRow(Request $request) {
        $ssl = new Ssl();
        $ssls = $ssl->getAll($request->key, 4);
        if ($request->amount !== null) {
            $ssls = $ssl->getAll($request->key, $request->amount);
        }
        return view('admin.ssl.card', compact('ssls'));
    }

    //Show create form
    public function createForm(Request $request) {
        $ssl = Ssl::find($request->id);
        return view('admin.ssl.update-add', compact('ssl'));
    }

    //Save to db
    public function store(Request $request) {
        try {
            $check = 0;
            $ssl = new Ssl();
            if ($request->id)  {
                $ssl = Ssl::find($request->id);
                $ssl->rules['name'] = 'required|unique:ssls,name,'.$ssl->id.',id,deleted_at,NULL';
                $check = 1;
            }
            //Validation
            $validator = $this->validateInput($request->all(), $ssl->rules, $ssl->messages);
            if ($validator->fails()) {
                return redirect()->back()->withInput()->withErrors($validator);
            }
            Ssl::updateOrCreate(['id' => $request->id], $request->all());
            if ($check == 0) {
                return redirect(route('admin.ssls.index'))->with('success', __('general.create-success'));
            }
            return redirect(route('admin.ssls.index'))->with('success', __('general.update-success'));
        } catch (\Exception $exception) {
            return redirect(route('admin.ssls.index'))->with('fail', __('general.manipulation-fail'));
        }
    }

    //Delete one row
    public function destroy(Request $request) {
        try {
            Ssl::findOrFail($request->id)->delete();
            return redirect()->back()->with('success', __('general.delete-success'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('fail', __('general.delete-fail'));
        }
    }

    //Delete row selected
    public function destroySelect(Request $request) {
        try {
            $allVals = explode(',', $request->allValsDelete[0]);
            foreach ($allVals as $item) {
                Ssl::findOrFail($item)->delete();
            }
            return redirect()->back()->with('success', __('general.delete-success'));
        }
        catch (\Exception $exception) {
            return redirect()->back()->with('fail', __('general.delete-fail'));
        }
    }
}
