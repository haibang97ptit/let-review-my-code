<?php

namespace App\Http\Controllers\Admin;

use App\Models\MessagesDetail;
use App\Models\Customer;
use App\Models\Department;
use App\Models\ListRequest;
use App\Models\Notifications;
use App\Models\RegisterService;
use App\Models\RegisterSoftware;
use App\Models\Software;
use App\Models\Staff;
use App\Notifications\NotificationDatabase;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Pusher\Pusher;

class SupportController extends AdminController
{


    //Update status
    public function updateStatus(Request $request)
    {
        $query = $request->get('query');
        $code = $query[0];
        $key = $query[1];
        $list_request = new ListRequest();
        $list_request->updateColumn($code, "status", $key);
    }

    //Index
    public function listRequest(Request $request)
    {
        $user = Auth::user();
        $user_staff = DB::table('staffs')->where('email', 'like', $user->email)->first();
        $notifications = DB::table('notifications')->get();
        $Support = new ListRequest();
        $listSupport = $Support->getAll($user_staff->department_code, 10);
        if (isset($request->amount)) {
            $listSupport = $Support->getAll($user_staff->department_code, $request->amount);
        }
        return view('admin.support.index', compact('listSupport', 'notifications'));
    }

    //Create new request
    public function createRequest()
    {
        $departments = Department::all();
        $customers = Customer::get();
        return view('admin.support.create', compact('departments', 'customers'));
    }

    //New request
    public function newRequest(Request $request)
    {
        $user = Auth::user();
        $user_staff = DB::table('staffs')->where('email', 'like', $user->email)->first();
        $Support = new ListRequest();

        $listSupport = $Support->getAllByColumn($user_staff->department_code, __('support.new'), 10);
        if (isset($request->amount)) {
            $listSupport = $Support->getAllByColumn($user_staff->department_code, __('support.new'), $request->amount);
        }

        foreach ($listSupport as $item) {
            $name = DB::table('departments')->where('code', 'like', $item->id_department_send)->select('name')->get();
            $item->id_department_send = $name[0]->name;
        }

        return view('admin.support.new', compact('listSupport'));
    }

    //Answers request
    public function answersRequest(Request $request)
    {
        $user = Auth::user();
        $user_staff = DB::table('staffs')->where('email', 'like', $user->email)->first();
        $Support = new ListRequest();

        $listSupport = $Support->getAllByColumn($user_staff->department_code, __('support.replied'), 10);
        if (isset($request->amount)) {
            $listSupport = $Support->getAllByColumn($user_staff->department_code, __('support.replied'), $request->amount);
        }

        foreach ($listSupport as $item) {
            $name = DB::table('departments')->where('code', 'like', $item->id_department_send)->select('name')->get();
            $item->id_department_send = $name[0]->name;
        }
        return view('admin.support.answers', compact('listSupport'));
    }

    //Processing request
    public function processingRequest(Request $request)
    {
        $user = Auth::user();
        $user_staff = DB::table('staffs')->where('email', 'like', $user->email)->first();
        $Support = new ListRequest();

        $listSupport = $Support->getAllByColumn($user_staff->department_code, __('support.processing'), 10);
        if (isset($request->amount)) {
            $listSupport = $Support->getAllByColumn($user_staff->department_code, __('support.processing'), $request->amount);
        }

        foreach ($listSupport as $item) {
            $name = DB::table('departments')->where('code', 'like', $item->id_department_send)->select('name')->get();
            $item->id_department_send = $name[0]->name;
        }
        Session::put('support.processing', $listSupport->count());
        return view('admin.support.processing', compact('listSupport'));
    }

    //Closed
    public function closedRequest(Request $request)
    {
        $user = Auth::user();
        $user_staff = DB::table('staffs')->where('email', 'like', $user->email)->first();
        $Support = new ListRequest();

        $listSupport = $Support->getAllByColumn($user_staff->department_code, __('support.done'), 10);
        if (isset($request->amount)) {
            $listSupport = $Support->getAllByColumn($user_staff->department_code, __('support.done'), $request->amount);
        }

        foreach ($listSupport as $item) {
            $name = DB::table('departments')->where('code', 'like', $item->id_department_send)->select('name')->get();
            $item->id_department_send = $name[0]->name;
        }
        return view('admin.support.closed', compact('listSupport'));
    }

    //Detail
    public function detailRequest(Request $request)
    {

        $id = $request->id;
        $detail = DB::table('request')->where('code', 'like', $id)->first();
        $name_department = DB::table('departments')->where('code', 'like', $detail->id_department_send)->select('name')->first();
        $detail->id_department_send = $name_department->name;
        $messages = DB::table('messages_detail')->where('code_request', 'like', $id)->get();
        $customers = Customer::where('id', 'like', $detail->id_customer)->first();

        //Service
        $register_services = RegisterService::where('id_customer', 'like', $detail->id_customer)->get();
        //Software
        $registerSoftwares = RegisterSoftware::where('id_customer', 'like', $detail->id_customer)
            ->join('softwares', 'softwares.id', '=', 'id_software')
            ->get();
        //dd($registerSoftwares);
        return view('admin.support.detail', compact('detail', 'messages', 'register_services', 'customers', 'registerSoftwares'));
    }

    //Search customer
    public function searchCustomer(Request $request)
    {
        $key = isset($request->key) ? $request->key : 'NONE';
        $customers = Customer::where('customers.phone_number', 'like', '%' . $key . '%')->orWhere('customers.email', 'like', '%' . $key . '%')->limit(5)->get();
        return response()->json(['success' => true, 'data' => $customers]);
    }

    //Send request
    public function sendRequest(Request $request)
    {

        $new_request = new ListRequest();
        $new_messages = new MessagesDetail();
        $departments = new Department();
        $user = Auth::user();


        $validator = $this->validateInput($request->all(), $new_request->rules, $new_request->messages);
        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $user_staff = DB::table('staffs')->where('email', 'like', $user->email)->first();
        $department_send = $user_staff->department_code;

        $new_request->createCode($department_send, $request->department_receive);
        $new_request->id_department_send = $department_send;
        $new_request->id_department_receive = $request->department_receive;
        $new_request->id_customer = $request->id_customer;
        $new_request->title = $request->title;
        $new_request->status = __('support.new');
        $new_request->service = $request->service;
        $new_request->person_create = $user->name;

        //Validation message detail
        $validator = $this->validateInput(['content_support' => $request->content_support], $new_messages->rules, $new_messages->messages);
        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        $new_messages->code_request = $new_request->code;
        $new_messages->department_send = $departments->getName($department_send);
        $new_messages->person_send = $user->name;
        $new_messages->content = $request->content_support;
        if ($request->hasFile('file_upload')) {
            $name_file = time() . '_' . $request->file_upload->getClientOriginalName();
            $request->file_upload->storeAs('public/support', $name_file);
            $new_messages->file_upload = $name_file;
        }
        $new_messages->save();
        $new_request->save();

        $data['title'] = __('support.department'). " ". $departments->getName($department_send);
        $data['content'] = $request->title;
        $data['department'] = $request->department_receive;
        $data['code'] = $new_request->code;

        $notifications = new Notifications();
        $notifications->createNotify($data);

//        $options = array(
//            'cluster' => 'ap1',
//            'encrypted' => true
//        );
//        $pusher = new Pusher(
//            env('PUSHER_APP_KEY'),
//            env('PUSHER_APP_SECRET'),
//            env('PUSHER_APP_ID'),
//            $options
//        );
//        $pusher->trigger('Notify', 'staff.' . $data['department'], $data);
//
//
//        $all_staffs = Staff::where('department_code', $data['department'])->get();
//        foreach ($all_staffs as $staff) {
//            $staff->notify(new NotificationDatabase($data));
//        }
//        Notification::send($all_staffs,new NotificationDatabase($data));

        return redirect()->route('admin.support.index');
    }

    //Reply request
    public function replyRequest(Request $request)
    {
        $new_messages = new MessagesDetail();
        $departments = new Department();
        $user = Auth::user();

        $user_staff = DB::table('staffs')->where('email', 'like', $user->email)->first();
        $id_current_department = $user_staff->department_code;

        $new_messages->code_request = $request->code_support;
        $new_messages->department_send = $departments->getName($id_current_department);
        $new_messages->person_send = $user->name;
        $new_messages->content = $request->content_support;
        if ($request->hasFile('file_upload')) {
            $name_file = time() . '_' . $request->file_upload->getClientOriginalName();
            $request->file_upload->storeAs('public/support', $name_file);
            $new_messages->file_upload = $name_file;

        }
        $new_messages->save();


        $thisRequest = ListRequest::where('code', 'like', $request->code_support)->first();
        $id_department_send = $thisRequest->id_department_send;
        $id_department_receive = $thisRequest->id_department_receive;
        $name_department_send = $departments->getName($id_department_send);
        $messages_detail = MessagesDetail::where('code_request', 'like', $request->code_support)->get();

        foreach ($messages_detail as $key => $messages) {
            if ($messages->department_send !== $name_department_send) {
                $thisRequest->updateColumn($request->code_support, "status", __('support.replied'));
                $id_current_department == $id_department_send ? $department_receive = $id_department_receive:$department_receive = $id_department_send;

                $data['title'] = __('support.department'). " ". $departments->getName($id_current_department);
                $data['content'] = "Vừa trả lời hỗ trợ #" . $request->code_support;
                $data['department'] = $department_receive;
                $data['code'] = $request->code_support;

                $notifications = new Notifications();
                $notifications->createNotify($data);
                return redirect()->back();
            }
        }



        return redirect()->back();
    }
}
