<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Pusher\Pusher;

class SendNotifyController extends AdminController
{
    public function index()
    {
        return view('admin.support.send_notify');
    }

    public function sendNotify(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'content' => 'required'
        ]);

        $data['title'] = $request->input('title');
        $data['content'] = $request->input('content');

        $options = array(
            'cluster' => 'ap1',
            'encrypted' => true
        );

        $pusher = new Pusher(
            env('PUSHER_APP_KEY'),
            env('PUSHER_APP_SECRET'),
            env('PUSHER_APP_ID'),
            $options
        );

        $pusher->trigger('Notify', 'send-message', $data);

        return redirect()->route('send');
    }
    public function markAsRead($id,$code){
//        dd($id,$code);
        DB::table('notifications')->where('id',$id)->update(['read_at'=>Carbon::now()]);
        return redirect()->route('admin.support.detail',$code);
    }

    public function createNotifications(){

    }
}
