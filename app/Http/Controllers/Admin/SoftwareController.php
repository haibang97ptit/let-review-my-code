<?php

namespace App\Http\Controllers\Admin;

use App\Models\Software;
use Illuminate\Http\Request;

class SoftwareController extends AdminController
{
    //Index
    public function index() {
//        $this->authorize('type-software-access');

        $key = isset($request->key) ? $request->key : '';
        $software = new Software();
        $softwares = $software->getAll($key, 10);
        if (isset($request->amount)) {
            $softwares = $software->getAll($key, $request->amount);
        }
        return view('admin.software.index',compact('softwares'));
    }

    //Show row with key
    public function searchRow(Request $request) {
        $software = new Software();
        $softwares = $software->getAll($request->key, 10);
        if ($request->amount !== null) {
            $softwares = $software->getAll($request->key, $request->amount);
        }
        return view('admin.software.body', compact('softwares'));
    }

    //Show create form
    public function createForm(Request $request) {
        $software = Software::find($request->id);
        return view('admin.software.update-add', compact('software'));
    }

    //Save to db
    public function store(Request $request) {
        try {
            $software = new Software();
            if ($request->id)  {
                $software = Software::find($request->id);
                $software->rules['name'] = 'required|unique:softwares,name,'.$software->id.',id,deleted_at,NULL';
            }
            //Validation
            $validator = $this->validateInput($request->all(), $software->rules, $software->messages);
            if ($validator->fails()) {
                return redirect()->back()->withInput()->withErrors($validator);
            }
            Software::updateOrCreate(['id' => $request->id], $request->all());
            return redirect(route('admin.softwares.index'))->with('success', __('general.create-success'));
        } catch (\Exception $exception) {
            return redirect(route('admin.softwares.index'))->with('fail', __('general.create-fail'));
        }
    }

    //Delete one row
    public function destroy(Request $request) {
        try {
            Software::findOrFail($request->id)->delete();
            return redirect()->back()->with('success', __('general.delete-success'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('fail', __('general.delete-fail'));
        }
    }

    //Delete row selected
    public function destroySelect(Request $request) {
        try {
            $allVals = explode(',', $request->allValsDelete[0]);
            foreach ($allVals as $item) {
                Software::findOrFail($item)->delete();
            }
            return redirect()->back()->with('success', __('general.delete-success'));
        }
        catch (\Exception $exception) {
            return redirect()->back()->with('fail', __('general.delete-fail'));
        }
    }

    //Trash index
    public function trashIndex(Request $request) {
        $key = isset($request->key) ? $request->key : '';
        $software = new Software();
        $softwares = $software->getAllTrash($key, 10);
        if (isset($request->amount)) {
            $softwares = $software->getAllTrash($key, $request->amount);
        }
        return view('admin.software.trash.index',compact('softwares'));
    }

    //Show row - trash
    public function trashSearchRow(Request $request) {
        $software = new Software();
        $softwares = $software->getAllTrash($request->key, 10);
        if ($request->amount !== null) {
            $softwares = $software->getAllTrash($request->key, $request->amount);
        }
        return view('admin.software.trash.body', compact('softwares'));
    }

    //Force delete 1 row
    public function forceDelete(Request $request) {
        try {
            Software::withTrashed()->find($request->id)->forceDelete();
            return redirect()->back()->with('success', __('general.delete-success'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('fail', __('general.delete-fail'));
        }
    }

    //Force delete row selected
    public function forceDeleteSelect(Request $request) {
        try {
            $allVals = explode(',', $request->allVals[0]);
            foreach ($allVals as $item) {
                Software::withTrashed()->find($item)->forceDelete();
            }
            return redirect()->back()->with('success', __('general.delete-success'));
        }
        catch (\Exception $exception) {
            return redirect()->back()->with('fail', __('general.delete-fail'));
        }
    }

    //Restore from trash
    public function restore(Request $request) {
        try {
            Software::withTrashed()->find($request->id)->restore();
            return redirect(route('admin.softwares.index'))->with('success', __('general.restore-success'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('fail', __('general.restore-fail'));
        }
    }

    //Restore row selected
    public function restoreSelect(Request $request) {
        try {
            $allVals = explode(',', $request->allVals[0]);
            foreach ($allVals as $item) {
                Software::withTrashed()->find($item)->restore();
            }
            return redirect()->back()->with('success', __('general.restore-success'));
        }
        catch (\Exception $exception) {
            return redirect()->back()->with('fail', __('general.restore-fail'));
        }
    }

}
