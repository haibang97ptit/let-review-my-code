<?php

namespace App\Http\Controllers\Admin;

use App\Business\InvoiceLogic;
use App\Helpers\Helper;
use App\Models\CashBook;
use App\Models\ConstantsModel;
use App\Models\Customer;
use App\Models\Expenditure;
use App\Models\Revenue;
use App\Models\Staff;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;

class RevenueExpenditureController extends AdminController
{
    const UNDERLINE = '_';
    public function index(Request $request) {
        $logic_register_services = new InvoiceLogic();
        $invoices= $logic_register_services->getListRevenue_expenditure();

        $customer=Customer::all();
        $status = ConstantsModel::INVOICE;
        return view('admin.revenue_expenditure.index', compact('invoices','status','customer'));
    }

    public function indexrevenue(Request $request) {
        $this->authorize('revenue-access');

        $revenue=Revenue::join('staffs','staffs.id','revenues.id_staff')
            ->select('revenues.*','staffs.name as name')
            ->wherenull('revenues.deleted_at')
            ->paginate(Config::get('constants.pagination'));
        return view('admin.revenue.index',compact('revenue'));
    }

    public function indexexpenditure(Request $request) {
        $this->authorize('expenditure-access');

        $expenditure=Expenditure::join('staffs','staffs.id','expenditures.id_staff')
            ->select('expenditures.*','staffs.name as name')
            ->wherenull('expenditures.deleted_at')
            ->paginate(Config::get('constants.pagination'));

        return view('admin.expenditure.index',compact('expenditure'));
    }
    public function revenue(Request $request){
        $revenue = ($request->id) ? Revenue::find($request->id) : new Revenue();
        $staff=Staff::all();
        return view('admin.revenue.edit-add', compact('revenue','staff'));
    }
    public function expenditure(Request $request){
        $expenditure = ($request->id) ? Expenditure::find($request->id) : new Expenditure();
        $staff=Staff::all();
        return view('admin.expenditure.edit-add', compact('expenditure','staff'));
    }
    public function postrevenue(Request $request){
        $revenue=new Revenue();
        if ($request->id){
            $revenue=Revenue::find($request->id);
        }
        $validator=$this->validateInput($request->all(), $revenue->rules, $revenue->message);
        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        $revenue->fill($request->all());
        try {
            $revenue->save();
            $revenue_update=Revenue::find($revenue->id);
            if ($revenue_update->id){
                $code=Helper::generateCodeById($revenue_update->id). self::UNDERLINE .Helper::generateCodeById($revenue_update->id_staff). self::UNDERLINE .'re';
                $revenue_update->update([
                    'code' => $code,
                ]);
            }
            return redirect(route('admin.revenues.index'))->with('success', 'Success');
        }catch (\Exception $exception){
            return  redirect(route('admin.revenues.index'))->with('fail', 'Fail');

        }
    }
    public function postexpenditure(Request $request){
        $expenditure=new Expenditure();
        if ($request->id){
            $expenditure=Expenditure::find($request->id);
        }
        $validator = $this->validateInput($request->all(), $expenditure->rules, $expenditure->message);
        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        $request['price']=$request->price;
        $expenditure->fill($request->all());
        try {
            $expenditure->save();
         $expenditure_update=Expenditure::find($expenditure->id);
         if ($expenditure_update->id){
             $code=Helper::generateCodeById($expenditure_update->id). self::UNDERLINE .Helper::generateCodeById($expenditure_update->id_staff). self::UNDERLINE .'ex';
             $expenditure_update->update([
                 'code' => $code,
             ]);
         }
            return redirect(route('admin.expenditures.index'))->with('success', 'Success');
        }catch (\Exception $exception){
            return  redirect(route('admin.expenditures.index'))->with('fail', 'Fail'.$exception);

        }
    }
    public function pay(Request $request){
        if ($request->get('query')){
            $query=$request->get('query');
            $expenditure = Expenditure::find($query);
           $staff=Staff::find($expenditure->id_staff);
            $output='
                                                    ';
            echo $output;
        }

    }
}
