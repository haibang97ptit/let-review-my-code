<?php

namespace App\Http\Controllers\Admin;

use App\Business\RegisterServicesLogic;
use App\Helpers\Helper;
use App\Models\CashBook;
use App\Models\ConstantsModel;
use App\Models\Customer;
use App\Models\Domain;
use App\Models\Email;
use App\Models\Hosting;
use App\Models\Invoice;
use App\Models\RegisterService;
use App\Models\Ssl;
use App\Models\VPS;
use App\Models\Website;
use Cassandra\Custom;
use Egulias\EmailValidator\Exception\DotAtEnd;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Carbon\Carbon;

class RegisterServiceController extends AdminController
{
    //Index
    public function index(Request $request)
    {
        $this->authorize('register-service-access');

        $register_service = new RegisterService();
        $register_services = $register_service->getAll(10);
        if (isset($request->amount)) {
            $register_services = $register_service->getAll($request->amount);
        }
        $customers = Customer::get();
        return view('admin.register-service.index', compact('register_services'), compact('customers'));
    }

    //Show row with key
    public function searchRow(Request $request) {
        $register_service = new RegisterService();
        $register_services = $register_service->getSearchRow($request->customer_select, $request->service_select, $request->status_select, 10);
        if ($request->amount !== null) {
            $register_services = $register_service->getSearchRow($request->customer_select, $request->service_select, $request->status_select, $request->amount);
        }
        $customers = Customer::get();
        return view('admin.register-service.index')
            ->with(compact('register_services'))
            ->with(compact('customers'));
    }

    //Create form
    public function createForm(Request $request)
    {
        $customers = Customer::get();
        $domains = Domain::all();
        $hostings = Hosting::all();
        $vpses = VPS::all();
        $emails = Email::all();
        $ssls = Ssl::all();
        $websites = Website::all();
        return view('admin.register-service.add')
            ->with(compact('customers'))
            ->with(compact('domains'))
            ->with(compact('hostings'))
            ->with(compact('vpses'))
            ->with(compact('emails'))
            ->with(compact('ssls'))
            ->with(compact('websites'));
    }

    //Save db
    public function store(Request $request) {
        //Get hour now
        $dt = Carbon::now('+07:00');
        if ($request->id_domain) {
            try {
                $registerService = new RegisterService();
                $registerService->registerDomain($request, $dt);
            }
            catch (\Exception $exception) {
                return redirect(route('admin.register-services.index'))->with('fail', __('general.register-fail'));
            }
        }
        if ($request->id_hosting) {
            try {
                $registerService = new RegisterService();
                $registerService->registerHosting($request, $dt);
            }
            catch (\Exception $exception) {
                return redirect(route('admin.register-services.index'))->with('fail', __('general.register-fail'));
            }
        }
        if ($request->id_vps) {
            try {
                $registerService = new RegisterService();
                $registerService->registerVps($request, $dt);
            }
            catch (\Exception $exception) {
                return redirect(route('admin.register-services.index'))->with('fail', __('general.register-fail'));
            }
        }
        if ($request->id_email) {
            try {
                $registerService = new RegisterService();
                $registerService->registerEmail($request, $dt);
            }
            catch (\Exception $exception) {
                return redirect(route('admin.register-services.index'))->with('fail', __('general.register-fail'));
            }
        }
        if ($request->id_ssl) {
            try {
                $registerService = new RegisterService();
                $registerService->registerSsl($request, $dt);
            }
            catch (\Exception $exception) {
                return redirect(route('admin.register-services.index'))->with('fail', __('general.register-fail'));
            }
        }
        if ($request->id_website) {
            try {
                $registerService = new RegisterService();
                $registerService->registerWebsite($request, $dt);
            }
            catch (\Exception $exception) {
                return redirect(route('admin.register-services.index'))->with('fail', __('general.register-fail'));
            }
        }
        return redirect(route('admin.register-services.index'))->with('success', __('general.register-success'));
    }

    //Update form
    public function update(Request $request) {
        $rsUpdate = new RegisterService();
        $register_service = $rsUpdate->getToUpdate($request->id);
        $customers = Customer::get();
        return view('admin.register-service.update', compact('register_service'), compact('customers'));
    }

    //Update (store)
    public function updateStore(Request $request) {
        try {
            $dt = Carbon::now('+07:00');
            $check = 0;
            $regiterService = new RegisterService();
            if ($request->type_service == "domain") {
                $regiterService->code = $regiterService->createCode("D");
            }
            elseif ($request->type_service == "hosting") {
                $regiterService->code = $regiterService->createCode("H");
            }
            elseif ($request->type_service == "vps") {
                $regiterService->code = $regiterService->createCode("V");
            }
            elseif ($request->type_service == "email") {
                $regiterService->code = $regiterService->createCode("E");
            }
            elseif ($request->type_service == "ssl") {
                $regiterService->code = $regiterService->createCode("S");
            }
            else {
                $regiterService->code = $regiterService->createCode("W");
            }
            //Create start date, end date with time use
            $dt2 = Carbon::parse($request->end_date_extend);
            $dt2->addMonth($request->time_use);
            $endDate = $dt2->format('Y-m-d H:i');
            //Add start, end date
            $regiterService->start_date = $request->start_date_extend;
            $regiterService->end_date = $endDate;
            //Check update
            if ($request->id) {
                $regiterService = RegisterService::find($request->id);
                //Create start date, end date with time use
                $startDate = $request->start_date.$dt->format(' H:i');
                $dt2 = Carbon::parse($startDate);
                $dt2->addMonth($request->time_use);
                $endDate = $dt2->format('Y-m-d H:i');
                //Add start, end date
                $regiterService->start_date = $startDate;
                $regiterService->end_date = $endDate;
                $check = 1;
            }
            $regiterService->id_customer = $request->id_customer;
            $regiterService->type_service = $request->type_service;
            $regiterService->type_service_id = $request->type_service_id;
            $regiterService->time_use = $request->time_use;
            $regiterService->address_domain = $request->address_domain;
            $regiterService->price = $request->price;
            $regiterService->status_register = "unpaid";
            $regiterService->notes = $request->note;
            $regiterService->save();
            if ($check == 0) {
                $resultText = "extend-success";
            }
            else {
                $resultText = "update-success";
            }
            return redirect(route('admin.register-services.index'))->with('success', __('general.'.$resultText));
        }
        catch (\Exception $exception) {
            return redirect(route('admin.register-services.index'))->with('fail', __('general.manipulation-fail'));
        }
    }

    //Delete one row
    public function destroy(Request $request) {
        try {
            RegisterService::findOrFail($request->id)->delete();
            return redirect()->back()->with('success', __('general.delete-success'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('fail', __('general.delete-fail'));
        }
    }

    //Delete row selected
    public function destroySelect(Request $request) {
        try {
            $allVals = explode( ',', $request->allValsDelete[0]);
            foreach ($allVals as $item) {
                RegisterService::findOrFail($item)->delete();
            }
            return redirect()->back()->with('success', __('general.delete-success'));
        }
        catch (\Exception $exception) {
            return redirect()->back()->with('fail', __('general.delete-fail'));
        }
    }

    //Trash index
    public function trashIndex(Request $request) {
        $register_service = new RegisterService();
        $register_services = $register_service->getAllTrash(10);
        if (isset($request->amount)) {
            $register_services = $register_service->getAllTrash($request->amount);
        }
        $customers = Customer::get();
        return view('admin.register-service.trash.index', compact('register_services'), compact('customers'));
    }

    //Show row - trash
    public function trashSearchRow(Request $request) {
        $register_service = new RegisterService();
        $register_services = $register_service->getTrashSearchRow($request->customer_select, $request->service_select, $request->status_select, 10);
        if ($request->amount !== null) {
            $register_services = $register_service->getTrashSearchRow($request->customer_select, $request->service_select, $request->status_select, $request->amount);
        }
        $customers = Customer::get();
        return view('admin.register-service.trash.index', compact('register_services'), compact('customers'));
    }

    //Force delete 1 row
    public function forceDelete(Request $request) {
        try {
            RegisterService::withTrashed()->find($request->id)->forceDelete();
            return redirect()->back()->with('success', __('general.delete-success'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('fail', __('general.delete-fail'));
        }
    }

    //Force delete row selected
    public function forceDeleteSelect(Request $request) {
        try {
            $allVals = explode(',', $request->allValsDelete[0]);
            foreach ($allVals as $item) {
                RegisterService::withTrashed()->find($item)->forceDelete();
            }
            return redirect()->back()->with('success', __('general.delete-success'));
        }
        catch (\Exception $exception) {
            return redirect()->back()->with('fail', __('general.delete-fail'));
        }
    }

    //Restore from trash
    public function restore(Request $request) {
        try {
            RegisterService::withTrashed()->find($request->id)->restore();
            return redirect(route('admin.register-services.index'))->with('success', __('general.restore-success'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('fail', __('general.restore-fail'));
        }
    }

    //Change status
    public function changeStatus(Request $request) {
        try {
            $registerService = RegisterService::find($request->id);
            $registerService->status_register = $request->status;
            $registerService->save();
            if ($request->status == "paid") {
                $cashBook = new CashBook();
                $cashBook->code = $registerService->code;
                $cashBook->type = "service";
                $cashBook->save();
            }
            else {
                CashBook::where('code', '=', $registerService->code)->delete();
            }
        }
        catch (\Exception $exception) {
            return $exception;
        }
    }


}
