<?php

namespace App\Notifications;

use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Auth;

class CustomDbChannel
{

    public function send($notifiable, Notification $notification)
    {
        $data = $notification->toDatabase($notifiable);

        return $notifiable->routeNotificationFor('database')->create([
            'id' => $notification->id,
            //customize here
            'department' => $data['department'], //<-- comes from toDatabase() Method below
            'notifiable_id'=> \Auth::user()->id,

            'type' => get_class($notification),
            'data' => $data,
            'read_at' => null,
        ]);
    }

}
