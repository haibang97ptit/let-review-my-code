<?php

namespace App\Providers;

use App\Models\ListRequest;
use App\Models\Permission;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
        \App\Models\Customer::class => \App\Policies\CustomerPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {

        view()->composer('*', function ($view)
        {
            if(Auth::check()){
                $user = Auth::user();
                $user_staff = DB::table('staffs')->where('email','like',$user->email)->first();
                $listSupport = new ListRequest();

                $new_request = $listSupport->getAllByColumn($user_staff->department_code,__('support.new'),null)->count();
                $processing_request = $listSupport->getAllByColumn($user_staff->department_code,__('support.processing'),null)->count();
                $replied_request = $listSupport->getAllByColumn($user_staff->department_code,__('support.replied'),null)->count();


                //...with this variable
                $view->with(compact('new_request','processing_request','replied_request'));
            }
        });

        $this->registerPolicies();
        if (!$this->app->runningInConsole()) {
            foreach (Permission::get() as $permission) {
                Gate::define($permission->name, function ($user) use ($permission) {
                    return $user->hasPermission($permission);
                });
            }
        }
    }
}
