<?php

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/


Broadcast::channel('App.User.{id}', function ($user, $id) {
    return (int) $user->id === (int) $id;
});

Broadcast::channel('staff.{department_code}', function ($user, $department_code) {
    $user_department = \App\Models\Staff::where('email','like',$user->email)->first();
    return (string) $user_department->department_code === (string) $department_code;
});

