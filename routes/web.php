<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect(route('admin.index'));
});

// Admin pages
Route::namespace('Admin')->prefix('/admin')->group(function (): void {

    // Login
    Route::get('/login', 'LoginController@getLogin')->name('login');
    Route::post('/login', 'LoginController@postLogin')->name('postLogin');

    //Logout
    Route::post('/logout', 'LoginController@postLogout')->name('logout');
    Route::get('/','HomeController@index')->name('admin.index');

    //Roles
    Route::prefix('/roles')->group(function (): void {
        Route::get('/', 'RoleController@index')->name('admin.roles.index');
        Route::post('/', 'RoleController@searchRow')->name('admin.roles.search-row');
        Route::get('/create', 'RoleController@createForm')->name('admin.roles.create');
        Route::post('/store', 'RoleController@store')->name('admin.roles.store');
        Route::post('/destroy', 'RoleController@destroy')->name('admin.roles.destroy');
        Route::post('/destroy-select', 'RoleController@destroySelect')->name('admin.roles.destroy-select');
    });

    //Permission
    Route::prefix('/permissions')->group(function (): void {
        Route::get('/', 'PermissionController@index')->name('admin.permissions.index');
        Route::post('/', 'PermissionController@searchRow')->name('admin.permissions.search-row');
        Route::get('/create', 'PermissionController@createForm')->name('admin.permissions.create');
        Route::post('/store', 'PermissionController@store')->name('admin.permissions.store');
        Route::post('/destroy', 'PermissionController@destroy')->name('admin.permissions.destroy');
        Route::post('/destroy-select', 'PermissionController@destroySelect')->name('admin.permissions.destroy-select');
    });

    //User
    Route::prefix('/users')->group(function (): void {
        Route::get('/', 'UserController@index')->name('admin.users.index');
        Route::post('/', 'UserController@searchRow')->name('admin.users.search-row');
        Route::get('/create', 'UserController@createForm')->name('admin.users.create');
        Route::post('/store', 'UserController@store')->name('admin.users.store');
        Route::post('/destroy', 'UserController@destroy')->name('admin.users.destroy');
        Route::post('/destroy-select', 'UserController@destroySelect')->name('admin.users.destroy-select');
    });

    //Position (staff)
    Route::prefix('/positions')->group(function(): void {
        Route::get('/', 'PositionController@index')->name('admin.positions.index');
        Route::post('/', 'PositionController@searchRow')->name('admin.positions.search-row');
        Route::get('/create', 'PositionController@createForm')->name('admin.positions.create');
        Route::post('/store', 'PositionController@store')->name('admin.positions.store');
        Route::post('/destroy', 'PositionController@destroy')->name('admin.positions.destroy');
        Route::post('/destroy-select', 'PositionController@destroySelect')->name('admin.positions.destroy-select');
    });

    //Departments
    Route::prefix('/departments')->group(function (): void {
        Route::get('/', 'DepartmentController@index')->name('admin.departments.index');
        Route::post('/', 'DepartmentController@searchRow')->name('admin.departments.search-row');
        Route::get('/create', 'DepartmentController@createForm')->name('admin.departments.create');
        Route::post('/store', 'DepartmentController@store')->name('admin.departments.store');
        Route::post('/destroy', 'DepartmentController@destroy')->name('admin.departments.destroy');
        Route::post('/destroy-select', 'DepartmentController@destroySelect')->name('admin.departments.destroy-select');
        Route::get('/trash', 'DepartmentController@trashIndex')->name('admin.departments.trash-index');
        Route::post('/trash', 'DepartmentController@trashSearchRow')->name('admin.departments.trash-search-row');
        Route::post('/trash/force', 'DepartmentController@forceDelete')->name('admin.departments.trash-force');
        Route::post('/trash/force-select', 'DepartmentController@forceDeleteSelect')->name('admin.departments.trash-force-select');
        Route::post('/trash/restore', 'DepartmentController@restore')->name('admin.departments.trash-restore');
    });

    //Staff
    Route::prefix('/staffs')->group(function(): void {
        Route::get('/', 'StaffController@index')->name('admin.staffs.index');
        Route::post('/', 'StaffController@searchRow')->name('admin.staffs.search-row');
        Route::get('/create', 'StaffController@createForm')->name('admin.staffs.create');
        Route::post('/store', 'StaffController@store')->name('admin.staffs.store');
        Route::post('/destroy', 'StaffController@destroy')->name('admin.staffs.destroy');
        Route::post('/destroy-select', 'StaffController@destroySelect')->name('admin.staffs.destroy-select');
    });

    //Customer
    Route::prefix('/customers')->group(function (): void {
        Route::get('/', 'CustomerController@index')->name('admin.customers.index');
        Route::post('/', 'CustomerController@searchRow')->name('admin.customers.search-row');
        Route::get('/create', 'CustomerController@createForm')->name('admin.customers.create');
        Route::post('/store', 'CustomerController@store')->name('admin.customers.store');
        Route::post('/destroy', 'CustomerController@destroy')->name('admin.customers.destroy');
        Route::post('/destroy-select', 'CustomerController@destroySelect')->name('admin.customers.destroy-select');
        Route::get('/trash', 'CustomerController@trashIndex')->name('admin.customers.trash-index');
        Route::post('/trash', 'CustomerController@trashSearchRow')->name('admin.customers.trash-search-row');
        Route::post('/trash/force', 'CustomerController@forceDelete')->name('admin.customers.trash-force');
        Route::post('/trash/force-select', 'CustomerController@forceDeleteSelect')->name('admin.customers.trash-force-select');
        Route::post('/trash/restore', 'CustomerController@restore')->name('admin.customers.trash-restore');
    });

    //Register services
    Route::prefix('/register-services')->group(function(): void {
        Route::get('/', 'RegisterServiceController@index')->name('admin.register-services.index');
        Route::post('/', 'RegisterServiceController@searchRow')->name('admin.register-services.search-row');
        Route::get('/create', 'RegisterServiceController@createForm')->name('admin.register-services.create');
        Route::get('/update/{id}', 'RegisterServiceController@update')->name('admin.register-services.update');
        Route::post('/update', 'RegisterServiceController@updateStore')->name('admin.register-services.update-store');
        Route::post('/store', 'RegisterServiceController@store')->name('admin.register-services.store');
        Route::post('/destroy', 'RegisterServiceController@destroy')->name('admin.register-services.destroy');
        Route::post('/destroy-select', 'RegisterServiceController@destroySelect')->name('admin.register-services.destroy-select');
        Route::get('/trash', 'RegisterServiceController@trashIndex')->name('admin.register-services.trash-index');
        Route::post('/trash', 'RegisterServiceController@trashSearchRow')->name('admin.register-services.trash-search-row');
        Route::post('/trash/force', 'RegisterServiceController@forceDelete')->name('admin.register-services.trash-force');
        Route::post('/trash/force-select', 'RegisterServiceController@forceDeleteSelect')->name('admin.register-services.trash-force-select');
        Route::post('/trash/restore', 'RegisterServiceController@restore')->name('admin.register-services.trash-restore');
        Route::post('/change-status', 'RegisterServiceController@changeStatus')->name('admin.register-services.change-status');
    });

    //Software package
    Route::prefix('/software-packages')->group(function(): void {
        Route::get('/', 'SoftwarePackageController@index')->name('admin.software-packages.index');
        Route::post('/', 'SoftwarePackageController@searchRow')->name('admin.software-packages.search-row');
        Route::get('/create', 'SoftwarePackageController@createForm')->name('admin.software-packages.create');
        Route::post('/store', 'SoftwarePackageController@store')->name('admin.software-packages.store');
        Route::post('/destroy', 'SoftwarePackageController@destroy')->name('admin.software-packages.destroy');
        Route::post('/destroy-select', 'SoftwarePackageController@destroySelect')->name('admin.software-packages.destroy-select');
        Route::get('/trash', 'SoftwarePackageController@trashIndex')->name('admin.software-packages.trash-index');
        Route::post('/trash', 'SoftwarePackageController@trashSearchRow')->name('admin.software-packages.trash-search-row');
        Route::post('/trash/force', 'SoftwarePackageController@forceDelete')->name('admin.software-packages.trash-force');
        Route::post('/trash/force-select', 'SoftwarePackageController@forceDeleteSelect')->name('admin.software-packages.trash-force-select');
        Route::post('/trash/restore', 'SoftwarePackageController@restore')->name('admin.software-packages.trash-restore');
        Route::post('/trash/restore-select', 'SoftwarePackageController@restoreSelect')->name('admin.software-packages.trash-restore-select');
    });

    //Softwares
    Route::prefix('/softwares')->group(function(): void {
        Route::get('/', 'SoftwareController@index')->name('admin.softwares.index');
        Route::post('/', 'SoftwareController@searchRow')->name('admin.softwares.search-row');
        Route::get('/create', 'SoftwareController@createForm')->name('admin.softwares.create');
        Route::post('/store', 'SoftwareController@store')->name('admin.softwares.store');
        Route::post('/destroy', 'SoftwareController@destroy')->name('admin.softwares.destroy');
        Route::post('/destroy-select', 'SoftwareController@destroySelect')->name('admin.softwares.destroy-select');
        Route::get('/trash', 'SoftwareController@trashIndex')->name('admin.softwares.trash-index');
        Route::post('/trash', 'SoftwareController@trashSearchRow')->name('admin.softwares.trash-search-row');
        Route::post('/trash/force', 'SoftwareController@forceDelete')->name('admin.softwares.trash-force');
        Route::post('/trash/force-select', 'SoftwareController@forceDeleteSelect')->name('admin.softwares.trash-force-select');
        Route::post('/trash/restore', 'SoftwareController@restore')->name('admin.softwares.trash-restore');
        Route::post('/trash/restore-select', 'SoftwareController@restoreSelect')->name('admin.softwares.trash-restore-select');
    });

    //Register softwares
    Route::prefix('/register-softwares')->group(function(): void {
        Route::get('/', 'RegisterSoftwareController@index')->name('admin.register-softwares.index');
        Route::post('/', 'RegisterSoftwareController@searchRow')->name('admin.register-softwares.search-row');
        Route::get('/create', 'RegisterSoftwareController@createForm')->name('admin.register-softwares.create');
        Route::get('/update', 'RegisterSoftwareController@createForm')->name('admin.register-softwares.update');
        Route::post('/store', 'RegisterSoftwareController@store')->name('admin.register-softwares.store');
        Route::post('/destroy', 'RegisterSoftwareController@destroy')->name('admin.register-softwares.destroy');
        Route::post('/destroy-select', 'RegisterSoftwareController@destroySelect')->name('admin.register-softwares.destroy-select');
        Route::get('/trash', 'RegisterSoftwareController@trashIndex')->name('admin.register-softwares.trash-index');
        Route::post('/trash', 'RegisterSoftwareController@trashSearchRow')->name('admin.register-softwares.trash-search-row');
        Route::post('/trash/force', 'RegisterSoftwareController@forceDelete')->name('admin.register-softwares.trash-force');
        Route::post('/trash/force-select', 'RegisterSoftwareController@forceDeleteSelect')->name('admin.register-softwares.trash-force-select');
        Route::post('/trash/restore', 'RegisterSoftwareController@restore')->name('admin.register-softwares.trash-restore');
        Route::post('/trash/restore-select', 'RegisterSoftwareController@restoreSelect')->name('admin.register-softwares.trash-restore-select');
        Route::post('/change-status', 'RegisterSoftwareController@changeStatus')->name('admin.register-softwares.change-status');
    });

    //Services-use
    Route::prefix('/services-use')->group(function(): void{
        Route::get('/', 'ServiceUseController@index')->name('admin.services-use.index');
        Route::get('/filter', 'ServiceUseController@filter')->name('admin.services-use.filter');
        Route::post('/filter/search-row', 'ServiceUseController@searchRow')->name('admin.services-use.filter-search-row');
        Route::get('/update-register-softwares', 'RegisterSoftwareController@createForm')->name('admin.services-use.update-register-softwares');
        Route::get('/update-register-services/{id}', 'RegisterServiceController@update')->name('admin.register-services.update-register-services');
    });

    //Domain
    Route::prefix('/domains')->group(function(): void {
        Route::get('/', 'DomainController@index')->name('admin.domains.index');
        Route::post('/', 'DomainController@searchRow')->name('admin.domains.search-row');
        Route::get('/create', 'DomainController@createForm')->name('admin.domains.create');
        Route::post('/store', 'DomainController@store')->name('admin.domains.store');
        Route::post('/destroy', 'DomainController@destroy')->name('admin.domains.destroy');
        Route::post('/destroy-select', 'DomainController@destroySelect')->name('admin.domains.destroy-select');
    });

    //Hosting
    Route::prefix('/hostings')->group(function(): void {
        Route::get('/', 'HostingController@index')->name('admin.hostings.index');
        Route::post('/', 'HostingController@searchRow')->name('admin.hostings.search-row');
        Route::get('/create', 'HostingController@createForm')->name('admin.hostings.create');
        Route::post('/store', 'HostingController@store')->name('admin.hostings.store');
        Route::post('/destroy', 'HostingController@destroy')->name('admin.hostings.destroy');
        Route::post('/destroy-select', 'HostingController@destroySelect')->name('admin.hostings.destroy-select');
    });

    //VPS
    Route::prefix('/vpses')->group(function(): void {
        Route::get('/', 'VPSController@index')->name('admin.vpses.index');
        Route::post('/', 'VPSController@searchRow')->name('admin.vpses.search-row');
        Route::get('/create', 'VPSController@createForm')->name('admin.vpses.create');
        Route::post('/store', 'VPSController@store')->name('admin.vpses.store');
        Route::post('/destroy', 'VPSController@destroy')->name('admin.vpses.destroy');
        Route::post('/destroy-select', 'VPSController@destroySelect')->name('admin.vpses.destroy-select');
    });

    //Email
    Route::prefix('/emails')->group(function(): void {
        Route::get('/', 'EmailController@index')->name('admin.emails.index');
        Route::post('/', 'EmailController@searchRow')->name('admin.emails.search-row');
        Route::get('/create', 'EmailController@createForm')->name('admin.emails.create');
        Route::post('/store', 'EmailController@store')->name('admin.emails.store');
        Route::post('/destroy', 'EmailController@destroy')->name('admin.emails.destroy');
        Route::post('/destroy-select', 'EmailController@destroySelect')->name('admin.emails.destroy-select');
    });

    //SSL
    Route::prefix('/ssls')->group(function(): void {
        Route::get('/', 'SslController@index')->name('admin.ssls.index');
        Route::post('/', 'SslController@searchRow')->name('admin.ssls.search-row');
        Route::get('/create', 'SslController@createForm')->name('admin.ssls.create');
        Route::post('/store', 'SslController@store')->name('admin.ssls.store');
        Route::post('/destroy', 'SslController@destroy')->name('admin.ssls.destroy');
        Route::post('/destroy-select', 'SslController@destroySelect')->name('admin.ssls.destroy-select');
    });

    //Website
    Route::prefix('/websites')->group(function(): void{
        Route::get('/', 'WebsiteController@index')->name('admin.websites.index');
        Route::post('/', 'WebsiteController@searchRow')->name('admin.websites.search-row');
        Route::get('/create', 'WebsiteController@createForm')->name('admin.websites.create');
        Route::post('/store', 'WebsiteController@store')->name('admin.websites.store');
        Route::post('/destroy', 'WebsiteController@destroy')->name('admin.websites.destroy');
        Route::post('/destroy-select', 'WebsiteController@destroySelect')->name('admin.websites.destroy-select');
    });

    Route::prefix('/cash-books')->group(function(): void{
        Route::get('/', 'CashBookController@index')->name('admin.cash-books.index');
        Route::post('/', 'CashBookController@searchRow')->name('admin.cash-books.search-row');
        Route::post('/destroy', 'CashBookController@destroy')->name('admin.cash-books.destroy');
        Route::post('/destroy-select', 'CashBookController@destroySelect')->name('admin.cash-books.destroy-select');
    });

    //Invoices
    Route::prefix('/invoices')->group(function(): void{
        Route::get('/', 'InvoiceController@index')->name('admin.invoices.index');
        Route::get('/{id}/show', 'InvoiceController@show')->name('admin.invoices.show');
        Route::get('/create', 'InvoiceController@entry')->name('admin.invoices.create');
        Route::get('/{id}/edit', 'InvoiceController@entry')->name('admin.invoices.edit');
        Route::post('/store', 'InvoiceController@store')->name('admin.invoices.store');
        Route::post('/{id}/destroy', 'InvoiceController@destroy')->name('admin.invoices.destroy');
        Route::get('/search', 'InvoiceController@search')->name('admin.invoices.search');
        Route::post('/pay', 'InvoiceController@pay')->name('admin.invoices.pay');
    });

    //Support
    Route::prefix('/support')->group(function (): void {
        Route::get('/', 'SupportController@listRequest')->name('admin.support.index');
        Route::get('/create', 'SupportController@createRequest')->name('admin.support.create');
        Route::post('/create', 'SupportController@searchCustomer')->name('admin.support.search_customer');
        Route::post('/send', 'SupportController@sendRequest')->name('admin.support.send');
        Route::post('/reply', 'SupportController@replyRequest')->name('admin.support.reply');
        Route::post('/update', 'SupportController@updateStatus')->name('admin.support.update');
        Route::get('/new', 'SupportController@newRequest')->name('admin.support.new');
        Route::get('/answers', 'SupportController@answersRequest')->name('admin.support.answers');
        Route::get('/processing', 'SupportController@processingRequest')->name('admin.support.processing');
        Route::get('/closed', 'SupportController@closedRequest')->name('admin.support.closed');
        Route::get('/detail/{id}', 'SupportController@detailRequest')->name('admin.support.detail');
    });

    // Notify
    Route::get('/markAsRead/{id}/{code}', 'SendNotifyController@markAsRead')->name('admin.notify.mark_read');
    Route::get('/send', 'SendNotifyController@index')->name('send');
    Route::post('/postNotify', 'SendNotifyController@sendNotify')->name('postNotify');


















































    //expenditure management

    Route::prefix('/expenditures')->group(function(): void{
        Route::get('/', 'RevenueExpenditureController@indexexpenditure')->name('admin.expenditures.index');
        Route::get('/create', 'RevenueExpenditureController@expenditure')->name('admin.expenditures.create');
        Route::get('/{id}/edit', 'RevenueExpenditureController@expenditure')->name('admin.expenditures.edit');
        Route::post('/store', 'RevenueExpenditureController@postexpenditure')->name('admin.expenditures.store');
        Route::post('/{id}/destroy', 'RevenueExpenditureController@destroy')->name('admin.expenditures.destroy');
        Route::get('/search', 'RevenueExpenditureController@search')->name('admin.expenditures.search');
    });
    //revenues management

    Route::prefix('/revenues')->group(function(): void{
        Route::get('/', 'RevenueExpenditureController@indexrevenue')->name('admin.revenues.index');
        Route::get('/create', 'RevenueExpenditureController@revenue')->name('admin.revenues.create');
        Route::get('/{id}/edit', 'RevenueExpenditureController@expenditure')->name('admin.revenues.edit');
        Route::post('/store', 'RevenueExpenditureController@postrevenue')->name('admin.revenues.store');
        Route::post('/{id}/destroy', 'RevenueExpenditureController@destroy')->name('admin.revenues.destroy');
        Route::get('/search', 'RevenueExpenditureController@search')->name('admin.revenues.search');
        Route::post('/pay', 'RevenueExpenditureController@pay')->name('admin.revenues.pay');
    });
    //cash_books management




    //product management
    Route::prefix('/products')->group(function (): void {
        Route::get('/', 'ProductController@index')->name('admin.products.index');
        Route::get('/{id}/show', 'ProductController@show')->name('admin.products.show');
        Route::get('/create', 'ProductController@entry')->name('admin.products.create');
//        Route::get('/{id}/edit', 'RoleController@entry')->name('admin.roles.edit');
//        Route::post('/store', 'RoleController@store')->name('admin.roles.store');
        Route::post('/{id}/destroy', 'ProductController@destroy')->name('admin.products.destroy');
    });


    //unit management
    Route::prefix('/units')->group(function (): void {
        Route::get('/', 'UnitController@index')->name('admin.units.index');
        Route::get('/{id}/show', 'UnitController@show')->name('admin.units.show');
        Route::get('/create', 'UnitController@entry')->name('admin.units.create');

//        Route::get('/{id}/edit', 'RoleController@entry')->name('admin.roles.edit');
//        Route::post('/store', 'RoleController@store')->name('admin.roles.store');
    });

    //purpose management
    Route::prefix('/purposes')->group(function (): void {
        Route::get('/', 'PurposeController@index')->name('admin.purposes.index');
        Route::get('/{id}/show', 'PurposeController@show')->name('admin.purposes.show');
        Route::get('/create', 'PurposeController@entry')->name('admin.purposes.create');
//        Route::get('/{id}/edit', 'RoleController@entry')->name('admin.roles.edit');
//        Route::post('/store', 'RoleController@store')->name('admin.roles.store');
    });
    //purchase management
    Route::prefix('/purchases')->group(function (): void {
        Route::get('/', 'PurchaseController@index')->name('admin.purchases.index');
        Route::get('/{id}/show', 'PurchaseController@show')->name('admin.purchases.show');
        Route::get('/create', 'PurchaseController@entry')->name('admin.purchases.create');
//        Route::get('/{id}/edit', 'RoleController@entry')->name('admin.roles.edit');
//        Route::post('/store', 'RoleController@store')->name('admin.roles.store');
    });
    Route::get('/print', function () {
        return view('template.print');
    })->name('admin.print');
//    Route::view('/print', 'template.print');
});
