<?php
return [
    'id' => 'STT',
    'list' => 'Danh sách ssl',
    'ssl' => 'SSL',
    'name' => 'Tên ssl',
    'note' => 'Ghi chú',
    'create' => 'Tạo mới ssl',
    'update' => 'Cập nhật ssl',
    'price' => 'Giá',
    'insurance-policy' => 'Insurance policy',
    'domain-number' => 'Domain number',
    'reliability' => 'Reliability',
    'green-bar' => 'Green bar',
    'sans' => 'Sans',
];
