<?php
return [
    'id' => 'STT',
    'list' => 'Danh sách khách hàng',
    'name' => 'Họ và tên',
    'email' =>' Email',
    'address' => 'Địa chỉ',
    'birthday' => 'Ngày sinh',
    'notes' => 'Ghi chú',
    'customer' => 'Khách hàng',
    'first-name' => 'Tên',
    'last-name' => 'Họ',
    'create' => 'Tạo khách hàng',
    'state' => 'Bang',
    'password' => 'Mật khẩu',
    'note' => 'Ghi chú',
    'phone-number' => 'Số điện thoại',
];
