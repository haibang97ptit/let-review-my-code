<?php
return [
    'id' => 'STT',
    'list' => 'Danh sách đăng ký',
    'customer' => 'Khách hàng',
    'software' => 'Phần mềm',
    'software-package' => 'Gói phần mềm',
    'domain-address' => 'Địa chỉ domain',
    'start-date' => 'Thời gian bắt đầu',
    'end-date' => 'Thời gian kết thúc',
    'status-register' => 'Trạng thái',
    'register-software' => 'Đăng ký phần mềm',
    'note' => 'Ghi chú',
    'choose-customer' => 'Chọn khách hàng',
    'choose-software' => 'Chọn phần mềm',
    'choose-software-package' => 'Chọn gói phần mềm',
    'trial' => 'Dùng thử',
    'official' => 'Chính thức',
    'time-use' => 'Thời gian sử dụng',
    'months' => 'tháng',
    'info' => 'Thông tin đăng ký',
    'email' => 'Email',
    'price' => 'Giá',
    'version' => 'Phiên bản',
    'choose-version' => 'Chọn phiên bản',
    'choose-status' => 'Chọn trạng thái',
    'paid' => 'Đã thanh toán',
    'unpaid' => 'Chưa thanh toán',
    'code' => 'Mã đơn hàng',
    'update' => 'Cập nhật đơn hàng',
    'extend' => 'Gia hạn đơn hàng',
];
