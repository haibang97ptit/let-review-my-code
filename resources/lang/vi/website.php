<?php
return [
    'id' => 'STT',
    'list' => 'Danh sách website',
    'website' => 'Website',
    'name' => 'Tên website',
    'note' => 'Ghi chú',
    'create' => 'Tạo mới website',
    'update' => 'Cập nhật website',
    'price' => 'Giá',
    'type-website' => 'Loại website',
];
