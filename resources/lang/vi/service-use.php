<?php
return [
    'id' => 'STT',
    'customer' => 'Khách hàng',
    'service-software' => 'Dịch vụ/Phần mềm',
    'domain-address' => 'Địa chỉ domain',
    'start-date' => 'Thời gian bắt đầu',
    'end-date' => 'Thời gian kết thúc',
    'status' => 'Trạng thái',
    'active' => 'Đang hoạt động',
    'not-active' => 'Quá hạn',
    'list' => 'Danh sách dịch vụ đang sử dụng',
    'info' => 'Thông tin dịch vụ',
];
