<?php
return [
    'id' => 'STT',
    'list' => 'Danh sách VPS',
    'vps' => 'VPS',
    'name' => 'Tên VPS',
    'note' => 'Ghi chú',
    'create' => 'Tạo mới VPS',
    'update' => 'Cập nhật VPS',
    'capacity' => 'Dung lượng',
    'price' => 'Giá',
    'cpu' => 'CPU',
    'ram' => 'Ram',
    'bandwidth' => 'Băng thông',
    'technical' => 'Công nghệ ảo hóa',
    'operating-system' => 'Hệ điều hành',
    'backup' => 'Back up',
    'panel' => 'Panel',
];
