<?php
return [
    'id' => 'STT',
    'list' => 'Danh sách hosting',
    'hosting' => 'Hosting',
    'name' => 'Tên hosting',
    'note' => 'Ghi chú',
    'create' => 'Tạo mới hosting',
    'update' => 'Cập nhật hosting',
    'price' => 'Giá',
    'capacity' => 'Dung lượng',
    'bandwidth' => 'Băng thông',
    'sub-domain' => 'Sub domain',
    'email' => 'Email',
    'ftp' => 'FTP',
    'database' => 'Database',
    'adddon-domain' => 'Adddon domain',
    'park-domain' => 'Park domain',
];
