<?php
return [
    'id' => 'STT',
    'list' => 'Danh sách domain',
    'domain' => 'Domain',
    'fee-register' => 'Phí đăng ký',
    'fee-remain' => 'Phí duy trì',
    'fee-transformation' => 'Phí chuyển',
    'name' => 'Tên miền',
    'note' => 'Ghi chú',
    'create' => 'Tạo mới domain',
    'update' => 'Cập nhật domain',
];
