<?php
return [
    'id' => 'STT',
    'staff' => 'Nhân viên',
    'list' => 'Danh sách nhân viên',
    'position' => 'Chức vụ',
    'name' => 'Họ và tên',
    'email' => 'Email',
    'address' => 'Địa chỉ',
    'phone-number' => 'Số điện thoại',
    'birthday' => 'Ngày sinh',
    'salary' => 'Lương',
    'start-time' => 'Ngày vào làm',
    'create' => 'Tạo nhân viên',
    'password' => 'Mật khẩu',
    'department' => 'Phòng ban',
];
