<?php
return [
    'id' => 'STT',
    'list' => 'Danh sách email',
    'email' => 'Email',
    'note' => 'Ghi chú',
    'create' => 'Tạo mới email',
    'update' => 'Cập nhật email',
    'name' => 'Tên email',
    'price' => 'Giá',
    'capacity' => 'Dung lượng',
    'email-number' => 'Email number',
    'email-forwarder' => 'Email forwarder',
    'email-list' => 'Email list',
    'parked-domains' => 'Parked domains',
];
