<?php
return [
    'id' => 'STT',
    'role' => 'Vai trò',
    'create' => 'Tạo vai trò',
    'permission' => 'Quyền',
    'list' => 'Danh sách vai trò',
    'name' => 'Tên vai trò',
    'role_require' => 'Vai trò không được để trống',
    'select-all' => 'Chọn tất cả',
    'delete-all' => 'Xóa tất cả',
];
