<table class="table table-bordered table-striped table-hover" style="text-align: left">
    <tr>
        <td>{{ __('ssl.name') }}</td>
        <td class="td-software">{{ $ssl->name }}</td>
    </tr>
    <tr>
        <td>{{ __('ssl.price') }}</td>
        <td class="td-software">{{ $ssl->price }}</td>
    </tr>
    <tr>
        <td>{{ __('ssl.insurance-policy') }}</td>
        <td class="td-software">{{ $ssl->insurance_policy }}</td>
    </tr>
    <tr>
        <td>{{ __('ssl.domain-number') }}</td>
        <td class="td-software">{{ $ssl->domain_number }}</td>
    </tr>
    <tr>
        <td>{{ __('ssl.green-bar') }}</td>
        <td class="td-software">{{ $ssl->green_bar }}</td>
    </tr>
    <tr>
        <td>{{ __('ssl.sans') }}</td>
        <td class="td-software">{{ $ssl->sans }}</td>
    </tr>
    <tr>
        <td>{{ __('ssl.note') }}</td>
        <td class="td-software">{{ $ssl->notes }}</td>
    </tr>
</table>
