<div style="width: 120%">
    <table>
        <tr>
            <td>{{ __('ssl.price') }}</td>
            <td class="td-software">{{ $ssl->price }}</td>
        </tr>
        <tr>
            <td>{{ __('ssl.insurance-policy') }}</td>
            <td class="td-software">{{ $ssl->insurance_policy }}</td>
        </tr>
        <tr>
            <td>{{ __('ssl.domain-number') }}</td>
            <td class="td-software">{{ $ssl->domain_number }}</td>
        </tr>
        <tr>
            <td>{{ __('ssl.green-bar') }}</td>
            <td class="td-software">{{ $ssl->green_bar }}</td>
        </tr>
        <tr>
            <td>{{ __('ssl.sans') }}</td>
            <td class="td-software">{{ $ssl->sans }}</td>
        </tr>
    </table>
</div>

