@extends('layout.master')
@section('css')
    <link rel="stylesheet" href="{{ asset('../css/default.css') }}">
@endsection
@section('content')
    <div class="body-content">
        <div class="card">
            <div class="card-header card-header-new">
                @if(isset($ssl->id))
                    {{ __('ssl.update') }}
                @else
                    {{ __('ssl.create') }}
                @endif
            </div>

            <div class="card-body">
                <form method="POST" action="{{ route("admin.ssls.store") }}" enctype="multipart/form-data">
                    @csrf
                    <input style="display: none" name="id" value="{{ isset($ssl->id) ? $ssl->id : old('id') }}">
                    <div class="clearfix">
                        <div class="form-group" style="float: left; width: 49%">
                            <label class="required" for="name">{{ __('ssl.name') }} <span class="content-required">*</span></label>
                            <input class="form-control" type="text" name="name" id="name" value="{{ isset($ssl->name) ? $ssl->name : old('name') }}">
                        </div>
                        <div class="form-group" style="float: right; width: 49%">
                            <label class="required" for="price">{{ __('ssl.price') }} <span class="content-required">*</span></label>
                            <input class="form-control" type="number" name="price" id="price" value="{{ isset($ssl->price) ? $ssl->price : old('price') }}">
                        </div>
                    </div>
                    <div class="clearfix">
                        <div class="form-group" style="float: left; width: 49%">
                            <label class="required" for="insurance_policy">{{ __('ssl.insurance-policy') }} <span class="content-required">*</span></label>
                            <input class="form-control" type="text" name="insurance_policy" id="insurance_policy" value="{{ isset($ssl->insurance_policy) ? $ssl->insurance_policy : old('insurance_policy') }}">
                        </div>
                        <div class="form-group" style="float: right; width: 49%">
                            <label class="required" for="domain_number">{{ __('ssl.domain-number') }} <span class="content-required">*</span></label>
                            <input class="form-control" type="number" name="domain_number" id="domain_number" value="{{ isset($ssl->domain_number) ? $ssl->domain_number : old('domain_number') }}">
                        </div>
                    </div>
                    <div class="clearfix">
                        <div class="form-group" style="float: left; width: 49%">
                            <label class="required" for="reliability">{{ __('ssl.reliability') }} <span class="content-required">*</span></label>
                            <input class="form-control" type="text" name="reliability" id="reliability" value="{{ isset($ssl->reliability) ? $ssl->reliability : old('reliability') }}">
                        </div>
                        <div class="form-group" style="float: right; width: 49%">
                            <label class="required" for="green_bar">{{ __('ssl.green-bar') }} <span class="content-required">*</span></label>
                            <input class="form-control" type="text" name="green_bar" id="green_bar" value="{{ isset($ssl->green_bar) ? $ssl->green_bar : old('green_bar') }}">
                        </div>
                    </div>
                    <div class="clearfix">
                        <div class="form-group" style="float: left; width: 49%">
                            <label class="required" for="sans">{{ __('ssl.sans') }} <span class="content-required">*</span></label>
                            <input class="form-control" type="text" name="sans" id="sans" value="{{ isset($ssl->sans) ? $ssl->sans : old('sans') }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="required" for="notes">{{ __('ssl.note') }}</label>
                        <input class="form-control" type="text" name="notes" id="notes" value="{{ isset($ssl->notes) ? $ssl->notes : old('notes') }}">
                    </div>
                    <div class="form-group">
                        <button class="btn btn-secondary" type="button" onclick="goBack()">{{ __('general.back') }}</button>
                        <button class="btn btn-danger" type="submit">{{ __('general.save') }}</button>
                    </div>
                </form>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul style="margin-bottom: 0px">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
    </div>
@stop
