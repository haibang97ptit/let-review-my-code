<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="https://skywalkapps.github.io/bootstrap-notifications/stylesheets/bootstrap-notifications.css">
<div class="container">
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-9" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Demo App</a>
            </div>

            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="dropdown dropdown-notifications">
                        <a href="#notifications-panel" class="dropdown-toggle" data-toggle="dropdown">
                            <i data-count="0" class="glyphicon glyphicon-bell notification-icon"></i>
                        </a>

                        <div class="dropdown-container">
                            <div class="dropdown-toolbar">
                                <div class="dropdown-toolbar-actions">
                                    <a href="#">Mark all as read</a>
                                </div>
                                <h3 class="dropdown-toolbar-title">Notifications (<span class="notif-count">0</span>)</h3>
                            </div>
                            <ul class="dropdown-menu">
                            </ul>
                            <div class="dropdown-footer text-center">
                                <a href="#">View All</a>
                            </div>
                        </div>
                    </li>
                    <li><a href="#">Timeline</a></li>
                    <li><a href="#">Friends</a></li>
                </ul>
            </div>
        </div>
    </nav>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form action="{{route('postNotify')}}" method="post">
        @csrf
        <label for="fname">Title</label>
        <input type="text" id="title" name="title" placeholder="Your Title..">

        <label for="subject">Content</label>
        <textarea id="content" name="content" placeholder="Write something.." style="height:200px"></textarea>

        <input type="submit" value="Submit">

    </form>
</div>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
{{--<script src="https://js.pusher.com/4.3/pusher.min.js"></script>--}}


{{--<script type="text/javascript">--}}
{{--    var notificationsWrapper   = $('.dropdown-notifications');--}}
{{--    var notificationsToggle    = notificationsWrapper.find('a[data-toggle]');--}}
{{--    var notificationsCountElem = notificationsToggle.find('i[data-count]');--}}
{{--    var notificationsCount     = parseInt(notificationsCountElem.data('count'));--}}
{{--    var notifications          = notificationsWrapper.find('ul.dropdown-menu');--}}


{{--    // Enable pusher logging - don't include this in production--}}
{{--    Pusher.logToConsole = true;--}}

{{--    var pusher = new Pusher('{{env('PUSHER_APP_KEY')}}', {--}}
{{--        cluster: 'ap1',--}}
{{--        encrypted: true--}}
{{--    });--}}

{{--    // Subscribe to the channel we specified in our Laravel Event--}}
{{--    var channel = pusher.subscribe('Notify');--}}

{{--    // Bind a function to a Event (the full Laravel class)--}}
{{--    channel.bind('send-notify', function(data) {--}}
{{--        var existingNotifications = notifications.html();--}}
{{--        var avatar = Math.floor(Math.random() * (71 - 20 + 1)) + 20;--}}
{{--        var newNotificationHtml = `--}}
{{--          <li class="notification active">--}}
{{--              <div class="media">--}}
{{--                <div class="media-left">--}}
{{--                  <div class="media-object">--}}
{{--                    <img src="https://api.adorable.io/avatars/71/`+avatar+`.png" class="img-circle" alt="50x50" style="width: 50px; height: 50px;">--}}
{{--                  </div>--}}
{{--                </div>--}}
{{--                <div class="media-body">--}}
{{--                  <strong class="notification-title">`+data.title+`</strong>--}}
{{--                  <p class="notification-desc">`+data.content+`</p>--}}
{{--                  <div class="notification-meta">--}}
{{--                    <small class="timestamp">about a minute ago</small>--}}
{{--                  </div>--}}
{{--                </div>--}}
{{--              </div>--}}
{{--          </li>--}}
{{--        `;--}}
{{--        notifications.html(newNotificationHtml + existingNotifications);--}}

{{--        notificationsCount += 1;--}}
{{--        notificationsCountElem.attr('data-count', notificationsCount);--}}
{{--        notificationsWrapper.find('.notif-count').text(notificationsCount);--}}
{{--        notificationsWrapper.show();--}}
{{--    });--}}
{{--</script>--}}
<style>
    /* Style inputs with type="text", select elements and textareas */
    input[type=text], select, textarea {
        width: 100%; /* Full width */
        padding: 12px; /* Some padding */
        border: 1px solid #ccc; /* Gray border */
        border-radius: 4px; /* Rounded borders */
        box-sizing: border-box; /* Make sure that padding and width stays in place */
        margin-top: 6px; /* Add a top margin */
        margin-bottom: 16px; /* Bottom margin */
        resize: vertical /* Allow the user to vertically resize the textarea (not horizontally) */
    }

    /* Style the submit button with a specific background color etc */
    input[type=submit] {
        background-color: #4CAF50;
        color: white;
        padding: 12px 20px;
        border: none;
        border-radius: 4px;
        cursor: pointer;
    }

    /* When moving the mouse over the submit button, add a darker green color */
    input[type=submit]:hover {
        background-color: #45a049;
    }

    /* Add a background color and some padding around the form */
    .container {
        border-radius: 5px;
        background-color: #f2f2f2;
        padding: 20px;
        width: 500px;
        margin: 0 auto;
    }
    .alert-danger { color: red}
</style>
