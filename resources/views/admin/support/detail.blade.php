@extends('layout.master')
@section('css')
    <link rel="stylesheet" href="{{ asset('../css/default.css') }}">
@endsection
@section('content')
    <div class="content">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active" aria-current="page"><h2>Chi Tiết Hỗ Trợ: {{ $detail->title }} [#{{ $detail->code }}]</h2></li>
            </ol>
        </nav>
        <div class="d-flex justify-content-end pb-3">
            <button id="change_to_processing" type="button" class="btn btn-xs btn-success">{{ __('support.to_process') }}</button>
            <button id="change_to_done" type="button" class="ml-1 btn btn-xs btn-secondary">{{ __('support.to_done') }}</button>
        </div>
        <div class="card p-2">
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                    <tr>
                        <th>{{ __('support.id') }}</th>
                        <th>{{ __('support.department_send') }}</th>
                        <th>{{ __('support.status') }}</th>
                        <th>{{ __('support.date') }}</th>
                        <th>{{ __('support.service') }}</th>
                        <th>{{ __('support.staff') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{ $detail->code }}</td>
                            <td>
                                {{ $detail->id_department_send }}

                            </td>
                            @if($detail->status == __('support.new'))
                                <td><button type="button" class="btn btn-xs btn-primary">{{ $detail->status }}</button></td>
                            @endif
                            @if($detail->status ==  __('support.replied'))
                                <td><button type="button" class="btn btn-xs btn-warning">{{ $detail->status }}</button></td>
                            @endif
                            @if($detail->status == __('support.processing'))
                                <td><button type="button" class="btn btn-xs btn-success">{{ $detail->status }}</button></td>
                            @endif
                            @if($detail->status == __('support.done'))
                                <td><button type="button" class="btn btn-xs btn-secondary">{{ $detail->status }}</button></td>
                            @endif
                            <td>
                                {{ $detail->created_at }}
                            </td>
                            <td>{{ $detail->service }}</td>
                            <td>{{ $detail->person_create }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <button type="button" class="btn btn-info w-100 mb-3" data-toggle="collapse" href="#register-service-table" aria-expanded="false" aria-controls="register-service-table">
            {{__('support.customer')}}: {{ $customers->first_name.' '.$customers->last_name }} | {{ $customers->email }} | {{ $customers->phone_number }}
            <i class="float-right fas fa-angle-down"></i>
        </button>
        <div id="register-service-table" class="collapse">
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th class="align-middle">{{ __('support.id') }}</th>
                        <th class="align-middle">
                            <p>{{ __('register-service.service') }}</p>
                        </th>
                        <th class="align-middle">
                            <p>{{ __('register-service.start-date') }}</p>
                            <p class="small-p">{{ __('register-service.end-date') }}</p>
                        </th>
                        <th class="align-middle">{{ __('register-software.version') }}</th>
                        <th class="align-middle">{{ __('register-service.status') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    <!-- Services -->
                    @foreach($register_services as $item => $register_service)
                        <tr >

                            <td class="align-middle">{{ $register_service->code }}</td>
                            <td class="align-middle">
                                <p>{{ $register_service->type_service }}</p>
                                <p class="small-p">{{ $register_service->address_domain }}</p>
                            </td>
                            <td class="align-middle">
                                <p>{{ date('Y-m-d H:i', strtotime($register_service->start_date)) }}</p>
                                <p class="small-p">{{ date('Y-m-d H:i', strtotime($register_service->end_date) )}}</p>
                            </td>
                            <td class="align-middle">
                                <span class="badge badge-success">{{ __('register-software.official') }}</span>
                            </td>
                            <td class="align-middle">
                                @if($register_service->status_register == "paid")
                                    <span class="badge badge-primary">{{ __('general.paid') }}</span>
                                @else
                                    <span class="badge badge-warning">{{ __('general.unpaid') }}</span>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    <!-- Softwares -->
                    @foreach($registerSoftwares as $item => $registerSoftware)
                        <tr >
                            <td class="align-middle">{{ $registerSoftware->code }}</td>
                            <td class="align-middle">
                                <p>{{ $registerSoftware->name }}</p>
                                <p class="small-p">{{ $registerSoftware->address_domain }}</p>
                            </td>

                            <td class="align-middle">
                                <p>{{ $registerSoftware->start_date == "" ? "" : date('Y-m-d H:i', strtotime($registerSoftware->start_date)) }}</p>
                                <p class="small-p">{{ $registerSoftware->end_date == "" ? "" : date('Y-m-d H:i', strtotime($registerSoftware->end_date) )}}</p>
                            </td>
                            <td class="align-middle">
                                @if($registerSoftware->version == "official")
                                    <span class="badge badge-success">{{ __('register-software.official') }}</span>
                                @else
                                    <span class="badge badge-dark">{{ __('register-software.trial') }}</span>
                                @endif
                            </td>
                            <td class="align-middle">
                                @if($registerSoftware->status_register == "paid")
                                    <span class="badge badge-success">Đã thanh toán</span>
                                @else
                                    <span class="badge badge-warning">Chưa thanh toán</span>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        @foreach($messages as $item)
            @if($detail->id_department_send !== $item->department_send)
            <div class="card p-3 border-top border-danger">
                <div class="text-danger pb-2"><strong>{{$item->person_send}} - {{ $item->department_send }}</strong>  <i class="float-right">[{{$item->created_at}}]</i></div>
                <div>
                    {!! $item->content !!}
                </div>
            </div>
            @else
                <div class="card p-3 border-top border-info">
                    <div class="text-info pb-2"><strong>{{$item->person_send}} - {{ $item->department_send }}</strong>  <i class="float-right">[{{$item->created_at}}]</i></div>
                    <div>
                        {!! $item->content !!}
                    </div>
                    @if($item->file_upload)
                        <small><i class="fas fa-paperclip mr-2"></i><a href="{{asset('storage/support/'.$item->file_upload)}}" target="_blank">{{$item->file_upload}}</a></small>
                    @endif
                </div>
            @endif
        @endforeach
        @if($detail->status !== __('support.done'))
            <form class="" action="{{ route("admin.support.reply") }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="py-3">
                    <input name="code_support" value="{{ $detail->code }}" style="display: none">
                    <textarea class="form-control" id="content-support" name="content_support"></textarea>
                    <input type="file" class="my-2 form-control-file" id="file_upload" name="file_upload">
                    <div class="d-flex w-100 justify-content-end">
                        <button type="submit" class="my-1 btn btn-primary">Gửi</button>
                    </div>
                </div>
            </form>
        @endif
    </div>

    <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
    <script>
        CKEDITOR.replace( 'content-support' );
    </script>
    <script>
        $('#change_to_processing').on('click',function () {
            let query = ['{{$detail->code}}','{{ __('support.processing') }}'];
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{ route('admin.support.update') }}",
                type: "POST",
                data: {query: query},
                success:function(data) {
                    window.location.reload();
                }

            })
        })
        $('#change_to_done').on('click',function () {
            let query = ['{{$detail->code}}','{{ __('support.done') }}'];
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{ route('admin.support.update') }}",
                type: "POST",
                data: {query: query},
                success:function(data) {
                    window.location.reload();
                }

            })
        })
    </script>
@stop
