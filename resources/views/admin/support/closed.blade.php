@extends('layout.master')
@section('css')
    <link rel="stylesheet" href="{{ asset('../css/default.css') }}">
@endsection
@section('content')
    <div class="content">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active" aria-current="page"><h2>Danh Sách Yêu Cầu Hỗ Trợ Đã Đóng</h2></li>
            </ol>
        </nav>
        <div class="card">
            <div class="d-flex flex-column flex-md-row  p-2">
                <div class="d-flex mr-sm-auto">
                    <div class="input-group" >
                        <div class="input-group-prepend">
                            <label class="input-group-text" for="showRow">{{ __('general.show') }}</label>
                        </div>
                        <select class="custom-select" id="showRow">
                            <option
                                value="10" {{ isset($_GET['amount']) ? ($_GET['amount'] === '10' ? 'selected' : '') : '' }}>
                                10
                            </option>
                            <option
                                value="25" {{ isset($_GET['amount']) ? ($_GET['amount'] === '25' ? 'selected' : '') : '' }}>
                                25
                            </option>
                            <option
                                value="50" {{ isset($_GET['amount']) ? ($_GET['amount'] === '50' ? 'selected' : '') : '' }}>
                                50
                            </option>
                            <option
                                value="100" {{ isset($_GET['amount']) ? ($_GET['amount'] === '100' ? 'selected' : '') : '' }}>
                                100
                            </option>
                        </select>
                        @can('permission-delete')
                            <button id="btn-delete"
                                    class="mx-2 btn btn-danger"
                                    type="button" class="delete btn btn-danger" data-toggle="modal"
                                    data-target="#deleteModal">
                                {{ __('general.delete') }}
                            </button>
                            <form action="{{ route('admin.permissions.destroy-select') }}" method="POST">
                                @csrf
                                <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog"
                                     aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title"
                                                    id="exampleModalLabel">{{ __('permission.permission') }}</h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <input type="text" id="allValsDelete" name="allValsDelete[]"
                                                       style="display: none">
                                                <p>{{ __('general.confirm_delete') }}</p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                        data-dismiss="modal">{{ __('general.close') }}</button>
                                                <button type="submit"
                                                        class="btn btn-danger">{{ __('general.delete') }}</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        @endcan
                    </div>
                </div>
                @can('permission-search')
                <div class="mt-2 mt-md-0 ml-sm-auto">
                    <input id="search" class="form-control" type="search" placeholder="{{ __('general.search') }}"
                       value="{{ isset($_GET['key']) ? $_GET['key'] : '' }}">
                </div>
                @endcan
            </div>
        </div>
        <div class="card p-2">
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                    <tr>
                        <th style="width: 10px"><input type="checkbox" id="check-all"></th>
                        <th>{{ __('support.id') }}</th>
                        <th>{{ __('support.title') }}</th>
                        <th>{{ __('support.department_send') }}</th>
                        <th>{{ __('support.status') }}</th>
                        <th>{{ __('support.date') }}</th>
                        <th>{{ __('support.service') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($listSupport as $item => $request)
                        <tr class="li" style="cursor: pointer;">
                            <td class="checkbox" style="width: 10px"><input type="checkbox" class="btn-check" value="1"></td>
                            <td class="code" >{{ $request->code }}</td>
                            <td >{{ $request->title }}</td>
                            <td >
                                {{ $request->id_department_send }}
                            </td>
                            @if($request->status == __('support.done'))
                                <td ><button type="button" class="btn btn-xs btn-secondary">{{ $request->status }}</button></td>
                            @endif
                            <td >
                                {{ $request->created_at }}
                            </td>
                            <td >{{ $request->service }}</td>
                        </tr>

                    @empty
                        <tr>
                            <td class="checkbox" style="text-align: center" colspan="7">{{ __('general.nodata') }}</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>
        <div class="clearfix">
            <div style="float: right">
                {!! $listSupport->links() !!}
            </div>
        </div>
    </div>
    <script>
        $("td:not(.checkbox)").on('click',function () {
            let code = $(this).closest('tr').find('.code').html();
            window.location.href ="/admin/support/detail/"+code;
        })
        $(document).on('click', '.pagination a', function (event) {
            event.preventDefault();
            var amountRow = $("#showRow :selected").val();
            var key = $("#search").val();
            var url = $(this).attr('href') + '&amount=' + amountRow + '&key=' + key;
            window.location.assign(url);
        });

        $("#showRow").on('change', function () {
            var amountRow = $("#showRow :selected").val();
            var url = '{{ route('admin.support.closed').'?amount=:amount' }}';
            url = url.replace(':amount', amountRow);
            window.location.assign(url);
        });
    </script>
@stop
