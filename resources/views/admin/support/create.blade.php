@extends('layout.master')
@section('css')
    <link rel="stylesheet" href="{{ asset('../css/default.css') }}">
@endsection
@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <div class="content">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active" aria-current="page"><h2>Gửi Yêu Cầu Hỗ Trợ</h2></li>
            </ol>
        </nav>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul style="margin-bottom: 0px">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="card">
            <form class="p-3" action="{{ route("admin.support.send") }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-group row">
                    <label for="department_receive" class="col-sm-2 col-form-label">Phòng ban <span class="content-required">*</span></label>
                    <div class="col-sm-10">
                        <select id="department_receive" class="form-control" name="department_receive">
                            <option disabled selected="selected">Chọn phòng ban</option>
                            @forelse ($departments as $department)
                                <option value="{{ $department->code }}">{{ $department->name }}</option>
                            @empty
                                <option>No department</option>
                            @endforelse
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="service-needing-assistance" class="col-sm-2 col-form-label">Dịch vụ cần hỗ trợ <span class="content-required">*</span></label>
                    <div class="col-sm-10">
                        <select id="service-needing-assistance" class="form-control" name="service">
                            <option disabled selected="selected">Vui lòng chọn dịch vụ cần hỗ trợ</option>
                            <option>Domain</option>
                            <option>Hosting</option>
                            <option>Software</option>
                            <option>Other...</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="customer-select" class="col-sm-2 col-form-label">{{ __('register-service.customer') }} <span class="content-required">*</span></label>
                    <div class="col-sm-10">
                        <select class="form-control" id="customer-select" name="id_customer">
                            <option></option>
                            @foreach($customers as $customer)
                                <option value="{{ $customer->id }}">{{ $customer->first_name.' '.$customer->last_name.' - '.$customer->email }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="title" class="col-sm-2 col-form-label">Tiêu đề <span class="content-required">*</span></label>
                    <div class="col-sm-10">
                        <input id="title" name="title" class="form-control" type="text" placeholder="Tiêu đề cần hỗ trợ">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="content-support" class="col-sm-2 col-form-label">Nội dung <span class="content-required">*</span></label>
                    <div class="col-sm-10">
                        <textarea class="form-control" id="content-support" name="content_support"></textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label" for="file_upload">File đính kèm</label>
                    <div class="col-sm-10">
                        <input type="file" class="form-control-file" id="file_upload" name="file_upload">
                    </div>
                </div>
                <button type="submit" class="btn btn-primary float-right">Gửi</button>
            </form>
        </div>
    </div>

    <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
    <script>
        //CKeditor
        CKEDITOR.replace( 'content-support' );
            $("#customer-select").select2({
                placeholder: "{{ __('register-service.choose-customer') }}",
                allowClear: true,
                width: "100%",
            });
    </script>
@stop
