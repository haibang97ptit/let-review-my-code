@extends('layout.master')
@section('content')
    <div class="content">
        <h1 class="titleheader" >Quản Lý hóa đơn</h1>
        <div class="row">
            @if(session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            @if(session('fail'))
                <div class="alert alert-danger">
                    {{ session('fail') }}
                </div>
            @endif
        </div>
        <p>
{{--            <a href="{{route('admin.invoices.create')}}" class="btn btn-primary">{{ __('general.create') }}</a>--}}
        </p>

        <div class="panel panel-default">

            {{--search--}}

            <div class="panel panel-default">
{{--                <div class="card">--}}
{{--                    <div class="accordion md-accordion accordion-blocks" id="accordion"--}}
{{--                         role="tablist"--}}
{{--                         aria-multiselectable="true" style="height: 40px">--}}
{{--                        <h5 class="panel-title">--}}
{{--                            <div id="color1" class=" panel-heading custom-panel-search" >--}}
{{--                                <!-- Card header -->--}}
{{--                                <div  role="tab" id="heading79">--}}

{{--                                    <!--Options-->--}}

{{--                                    <!-- Heading -->--}}
{{--                                    <a class="collapsed bar-search" role="button" data-toggle="collapse" data-parent="#accordion"--}}
{{--                                       href="#addstudent" aria-expanded="false" aria-controls="schedule" style="font-size: 20px">--}}
{{--                                        <h5>--}}
{{--                                            <label class="registerservice" style="color: #007bff;margin-top: 0.5%; margin-left: 1%">Tìm kiếm</label>--}}

{{--                                        </h5>--}}
{{--                                    </a>--}}
{{--                                </div>--}}
{{--                                <!-- Card body -->--}}
{{--                            </div>--}}

{{--                        </h5>--}}
{{--                    </div>--}}

{{--                    <form method="get" class="search_frm" action="{{route('admin.invoices.search')}}">--}}
{{--                        @csrf--}}

{{--                        <div id="addstudent" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">--}}
{{--                            <div class="panel-body">--}}
{{--                                <div class="col-md-12">--}}
{{--                                    <div class="row">--}}
{{--                                        <div class="col-mobile col-sm-6 col-md-6 form-group">--}}
{{--                                            <label>Địa chỉ domain</label>--}}
{{--                                            <select class="js-example-basic-single form-control" name="address_domain">--}}
{{--                                                <option value="" disabled selected>Chọn địa chỉ domain</option>--}}
{{--                                                @foreach($invoices as $i)--}}
{{--                                                    <option--}}
{{--                                                        value="{{$i}}">{{$i}}</option>--}}
{{--                                                @endforeach--}}
{{--                                            </select>--}}
{{--                                        </div>--}}
{{--                                        <div  class="col-mobile col-sm-6 col-md-6 form-group">--}}
{{--                                            <label>Khách hàng</label>--}}
{{--                                            <select class="js-example-basic-single form-control" name="customer">--}}
{{--                                                <option value="" disabled selected>Chọn khách hàng</option>--}}
{{--                                                @foreach($customer as $i)--}}
{{--                                                    <option--}}
{{--                                                        value="{{$i->id}}">{{$i->name}}-{{$i->email}}</option>--}}
{{--                                                @endforeach--}}
{{--                                            </select>--}}
{{--                                        </div>--}}
{{--                                        <div class="form-group col-md-6">--}}
{{--                                            <div class="col-xs-6 form-group">--}}
{{--                                                <label>Gói phần mềm</label>--}}
{{--                                                <select class="js-example-basic-single form-control" name="software">--}}
{{--                                                    <option value="" disabled selected>Chọn gói phần mềm</option>--}}
{{--                                                    @foreach($software as $i)--}}
{{--                                                        <option--}}
{{--                                                            value="{{$i->id}}">{{$i->name}}</option>--}}
{{--                                                    @endforeach--}}
{{--                                                </select>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <div class="form-group col-md-6">--}}
{{--                                            <div class="col-xs-6 form-group">--}}
{{--                                                <label>Loại phần mềm</label>--}}
{{--                                                <select class="js-example-basic-single form-control" name="typesoftware">--}}
{{--                                                    <option value="" disabled selected>Chọn loại phần mềm</option>--}}
{{--                                                    @foreach($typesoftware as $i)--}}
{{--                                                        <option--}}
{{--                                                            value="{{$i->id}}">{{$i->name}}</option>--}}
{{--                                                    @endforeach--}}
{{--                                                </select>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <div class="form-group col-md-6">--}}
{{--                                            <div class="col-xs-6 form-group">--}}
{{--                                                <label>Trạng thái</label>--}}
{{--                                                <select class="js-example-basic-single form-control" name="status">--}}
{{--                                                    <option value="" disabled selected>Chọn trạng thái</option>--}}
{{--                                                    <option value="Chính thức">Chính thức</option>--}}
{{--                                                    <option value="Dùng thử">Dùng thử</option>--}}

{{--                                                </select>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <div class="form-row" style="margin-bottom: 10px">--}}
{{--                                        <br>--}}
{{--                                        <button type="submit"--}}
{{--                                                class="btn btn-primary search-schedule" >{{__('general.search')}}</button>--}}

{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                    </form>--}}
{{--                </div>--}}
            </div>
            {{--end search--}}
            <div class="panel panel-default">

                <div class="panel-body table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr class="table_header">
                            <th class="thstyleform" >Tên Khách hàng</th>
                            <th class="thstyleform" >Mã đơn hàng</th>
                            <th class="thstyleform" >Sản phẩm</th>
{{--                            <th class="thstyleform" >Địa chỉ domain</th>--}}
                            <th class="thstyleform" >Tổng tiền</th>
                            <th class="thstyleform" >Ngày Tạo</th>


                            <th>&nbsp;</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($invoices as $i)
                            <tr>
                                <td class="thstyleform">{{$i->customer_name}}
                                    <p class="pstyleform1">{{$i->customer_email}}</p>
                                </td>
                                <td class="thstyleform">{{$i->code}}</td>
                                <td class="thstyleform">{{$i->domain_name}}{{$i->hosting_name}}{{$i->vps_name}}{{$i->email_name}}{{$i->ssl_name}}{{$i->website_name}}{{$i->soft_name}} {{$i->typesoftware_name}}</td>
{{--                                <td class="thstyleform">{{$i->address_domain}}{{$i->address_domain1}}</td>--}}
                                <td class="thstyleform">{{$i->price}}</td>
                                <td class="thstyleform">{{date('Y-m-d ',strtotime($i->created_at))}}</td>




                                <td class="thstyleform">
                                    @if($i->status=='fail')
                                        <button id="button-pay" value="{{$i->id}}"
                                                class="btn btn-xs btn-warning button-pay" data-toggle="modal"
                                                data-target="#exampleModal{{$i->id}}">Chưa thanh toán
                                        </button>
                                    @else
                                        <button class="btn btn-xs btn-success">Hoàn thành</button>
                                    @endif
                                        <div class="modal fade" id="exampleModal{{$i->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <form action="{{route('admin.invoices.store')}}" method="post">
                                                            <input name="id" hidden value="{{$i->id}}">
                                                        @csrf
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">Thanh toán</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body" id="dataModalform">
                                                            {{--lấy dữ liệu truyền vào đây--}}
                                                            <p>Bạn có chắc ?</p>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Quay lại</button>
                                                            <button type="submit" class="btn btn-primary">Lưu</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>

                                    {{--                                form thanh toán--}}

                                    <a href="{{ route('admin.invoices.show', $i->id) }}" class="btn btn-xs btn-info">{{ __('general.view') }}</a>
{{--                                        <a href="{{route('admin.invoices.edit', [$i->id])}}"--}}
{{--                                           class="btn btn-xs btn-success">{{ __('general.edit') }}</a>--}}
                                    <form style="display: inline-block" action="{{ route('admin.invoices.destroy', [$i->id]) }}" method="post" onsubmit="return confirm('Chúng tôi sẽ hủy đăng ký khi bạn xóa hóa đơn này! Bạn có chắc chắn muốn xóa? ');">
                                        @csrf
                                        <button type="submit" class="btn btn-xs btn-danger">{{ __('general.delete') }}</button>
                                    </form>
                                </td>

                            </tr>
                        @empty
                            <tr>
                                <td colspan="9" class="text-center">{{ __('general.nodata') }}</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                    <div class="text-right">
                        {{ $invoices->links() }}
                    </div>
                </div>
            </div>
        </div>

        <script>
            $(document).ready(function() {
                $('.mdb-select').materialSelect();
            });
        </script>
        <script>
            // In your Javascript (external .js resource or <script> tag)
            $(document).ready(function() {
                $('.js-example-basic-single').select2();
            });

            // Reset search
            $(".btn_clear").click(function (e) {
                e.preventDefault();
                $('.search-dropdown-select2').each(function (index, value) {
                    $(this).val('').trigger('change');
                });
                $('.search_frm').find('input[type=text], input[type=search], select, textarea').val('');
            });

            $('#myModal').on('shown.bs.modal', function () {
                $('#myInput').trigger('focus')
            });


@stop
