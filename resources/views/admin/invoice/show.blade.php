@extends('layout.master')

@section('content')



    <section class="content">
        <div class="">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h1 class="titleheader" >Chi tiết hóa đơn</h1>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table class="table table-bordered table-striped">
                                <tr>
                                    <th>ID</th>
                                    <td>{{ $invoice->id }}</td>
                                </tr>
                                <tr>
                                    <th>Mã code</th>
                                    <td>{{ $invoice->code }}</td>
                                </tr>
                                <tr>
                                    <th>Giá</th>
                                    <td>{{ $invoice->price }}</td>
                                </tr>
                                <tr>
                                    <th>Dịch vụ/ Phần mềm</th>
                                    <td>{{$invoice->domain_name}}{{$invoice->hosting_name}}{{$invoice->vps_name}}{{$invoice->email_name}}{{$invoice->ssl_name}}{{$invoice->website_name}}{{$invoice->soft_name}} {{$invoice->typesoftware_name}}</td>
                                </tr>
                                <tr>
                                    <th>Khách hàng</th>
                                    <td>{{ $invoice->customer_name }}-{{$invoice->customer_email}}</td>
                                </tr>
                                <tr>
                                    <th>Địa chỉ đồ main</th>
                                    <td>{{$invoice->address_domain}}{{$invoice->address_domain1}}</td>
                                </tr>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->

                    <a href="{{ route('admin.invoices.index') }}" class="btn btn-default">{{ __('general.back') }}</a>


                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>

@stop
