<div style="width: 120%">
    <table>
        <tr>
            <td>{{ __('software-package.price') }}</td>
            <td class="td-software">{{ $softwarePackage->price }}</td>
        </tr>
        <tr>
            <td>{{ __('software-package.branch') }}</td>
            <td class="td-software">{{ $softwarePackage->quantity_branch }}</td>
        </tr>
        <tr>
            <td>{{ __('software-package.staff') }}</td>
            <td class="td-software">{{ $softwarePackage->quantity_staff }}</td>
        </tr>
        <tr>
            <td>{{ __('software-package.account') }}</td>
            <td class="td-software">{{ $softwarePackage->quantity_acc }}</td>
        </tr>
        <tr>
            <td>{{ __('software-package.product') }}</td>
            <td class="td-software">{{ $softwarePackage->quantity_product }}</td>
        </tr>
        <tr>
            <td>{{ __('software-package.quantity-bill') }}</td>
            <td class="td-software">{{ $softwarePackage->quantity_bill }}</td>
        </tr>
    </table>
</div>

