<div class="card">
    <div class="card-header">
        <div style="float: right">
            <button type="button" id="check-all" class="btn btn-xs btn-primary">{{ __('software-package.select-all') }}</button>
            <button type="button" id="cancel-check-all" class="btn btn-xs btn-secondary">{{ __('software-package.cancel-select-all') }}</button>
        </div>
    </div>
    <div class="card-body">
        <div id="software-package-card">
            @include('admin.software-package.card')
        </div>
    </div>
</div>
