<table class="table table-bordered table-striped table-hover" style="text-align: left">
    <tr>
        <td>{{ __('software-package.name') }}</td>
        <td>{{ $softwarePackage->name }}</td>
    </tr>
    <tr>
        <td>{{ __('software-package.price') }}</td>
        <td>{{ $softwarePackage->price }}</td>
    </tr>
    <tr>
        <td>{{ __('software-package.quantity-branch') }}</td>
        <td>{{ $softwarePackage->quantity_branch }}</td>
    </tr>
    <tr>
        <td>{{ __('software-package.quantity-staff') }}</td>
        <td>{{ $softwarePackage->quantity_staff }}</td>
    </tr>
    <tr>
        <td>{{ __('software-package.quantity-acc') }}</td>
        <td>{{ $softwarePackage->quantity_acc }}</td>
    </tr>
    <tr>
        <td>{{ __('software-package.quantity-product') }}</td>
        <td>{{ $softwarePackage->quantity_product }}</td>
    </tr>
    <tr>
        <td>{{ __('software-package.quantity-bill') }}</td>
        <td>{{ $softwarePackage->quantity_bill }}</td>
    </tr>
    <tr>
        <td>{{ __('software-package.notes') }}</td>
        <td>{{ $softwarePackage->notes }}</td>
    </tr>
</table>
