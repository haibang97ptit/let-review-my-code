@extends('layout.master')
@section('css')
    <link rel="stylesheet" href="{{ asset('../css/default.css') }}">
@endsection
@section('content')
    <div class="body-content">
        <div class="card">
            <div class="card-header card-header-new">
                {{ __('software-package.create') }}
            </div>

            <div class="card-body">
                <form method="POST" action="{{ route("admin.software-packages.store") }}" enctype="multipart/form-data">
                    @csrf
                    <input style="display: none" name="id" value="{{ isset($softwarePackage->id) ? $softwarePackage->id : old('id') }}">
                    <div class="clearfix">
                        <div class="form-group" style="float: left; width: 49%">
                            <label class="required" for="name">{{ __('software-package.name') }} <span class="content-required">*</span></label>
                            <input class="form-control" type="text" name="name" id="name" value="{{ isset($softwarePackage->name) ? $softwarePackage->name : old('name') }}">
                        </div>
                        <div class="form-group" style="float: right; width: 49%">
                            <label class="required" for="price">{{ __('software-package.price') }} <span class="content-required">*</span></label>
                            <input class="form-control" type="number" name="price" id="price" value="{{ isset($softwarePackage->price) ? $softwarePackage->price : old('price') }}">
                        </div>
                    </div>
                    <div class="clearfix">
                        <div class="form-group" style="float: left; width: 49%">
                            <label class="required" for="quantity_branch">{{ __('software-package.quantity-branch') }} <span class="content-required">*</span></label>
                            <input class="form-control" type="number" name="quantity_branch" id="quantity_branch" value="{{ isset($softwarePackage->quantity_branch) ? $softwarePackage->quantity_branch : old('quantity_branch') }}">
                        </div>
                        <div class="form-group" style="float: right; width: 49%">
                            <label class="required" for="quantity_staff">{{ __('software-package.quantity-staff') }} <span class="content-required">*</span></label>
                            <input class="form-control" type="number" name="quantity_staff" id="quantity_staff" value="{{ isset($softwarePackage->quantity_staff) ? $softwarePackage->quantity_staff : old('quantity_staff') }}">
                        </div>
                    </div>
                    <div class="clearfix">
                        <div class="form-group" style="float: left; width: 49%">
                            <label class="required" for="quantity_acc">{{ __('software-package.quantity-acc') }} <span class="content-required">*</span></label>
                            <input class="form-control" type="number" name="quantity_acc" id="quantity_acc" value="{{ isset($softwarePackage->quantity_acc) ? $softwarePackage->quantity_acc : old('quantity_acc') }}">
                        </div>
                        <div class="form-group" style="float: right; width: 49%">
                            <label class="required" for="quantity_product">{{ __('software-package.quantity-product') }} <span class="content-required">*</span></label>
                            <input class="form-control" type="number" name="quantity_product" id="quantity_product" value="{{ isset($softwarePackage->quantity_product) ? $softwarePackage->quantity_product : old('quantity_product') }}">
                        </div>
                    </div>
                    <div class="clearfix">
                        <div class="form-group" style="float: left; width: 49%">
                            <label class="required" for="quantity_bill">{{ __('software-package.quantity-bill') }} <span class="content-required">*</span></label>
                            <input class="form-control" type="number" name="quantity_bill" id="quantity_bill" value="{{ isset($softwarePackage->quantity_bill) ? $softwarePackage->quantity_bill : old('quantity_bill') }}">
                        </div>
                        <div class="form-group" style="float: right; width: 49%">
                            <label class="required" for="notes">{{ __('software-package.notes') }}</label>
                            <input class="form-control" type="text" name="notes" id="notes" value="{{ isset($softwarePackage->notes) ? $softwarePackage->notes : old('notes') }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-secondary" type="button" onclick="goBack()">{{ __('general.back') }}</button>
                        <button class="btn btn-danger" type="submit">{{ __('general.save') }}</button>
                    </div>
                </form>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul style="margin-bottom: 0px">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
    </div>
@stop
