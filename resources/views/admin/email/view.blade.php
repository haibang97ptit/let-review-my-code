<table class="table table-bordered table-striped table-hover" style="text-align: left">
    <tr>
        <td>{{ __('email.name') }}</td>
        <td class="td-software">{{ $email->name }}</td>
    </tr>
    <tr>
        <td>{{ __('email.price') }}</td>
        <td class="td-software">{{ $email->price }}</td>
    </tr>
    <tr>
        <td>{{ __('email.capacity') }}</td>
        <td class="td-software">{{ $email->capacity }}</td>
    </tr>
    <tr>
        <td>{{ __('email.email-number') }}</td>
        <td class="td-software">{{ $email->email_number }}</td>
    </tr>
    <tr>
        <td>{{ __('email.email-forwarder') }}</td>
        <td class="td-software">{{ $email->email_forwarder }}</td>
    </tr>
    <tr>
        <td>{{ __('email.email-list') }}</td>
        <td class="td-software">{{ $email->email_list }}</td>
    </tr>
    <tr>
        <td>{{ __('email.parked-domains') }}</td>
        <td class="td-software">{{ $email->parked_domains }}</td>
    </tr>
    <tr>
        <td>{{ __('email.note') }}</td>
        <td class="td-software">{{ $email->notes }}</td>
    </tr>
</table>
