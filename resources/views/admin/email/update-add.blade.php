@extends('layout.master')
@section('css')
    <link rel="stylesheet" href="{{ asset('../css/default.css') }}">
@endsection
@section('content')
    <div class="body-content">
        <div class="card">
            <div class="card-header card-header-new">
                @if(isset($email->id))
                    {{ __('email.update') }}
                @else
                    {{ __('email.create') }}
                @endif
            </div>

            <div class="card-body">
                <form method="POST" action="{{ route("admin.emails.store") }}" enctype="multipart/form-data">
                    @csrf
                    <input style="display: none" name="id" value="{{ isset($email->id) ? $email->id : old('id') }}">
                    <div class="clearfix">
                        <div class="form-group" style="float: left; width: 49%">
                            <label class="required" for="name">{{ __('email.name') }} <span class="content-required">*</span></label>
                            <input class="form-control" type="text" name="name" id="name" value="{{ isset($email->name) ? $email->name : old('name') }}">
                        </div>
                        <div class="form-group" style="float: right; width: 49%">
                            <label class="required" for="price">{{ __('email.price') }} <span class="content-required">*</span></label>
                            <input class="form-control" type="number" name="price" id="price" value="{{ isset($email->price) ? $email->price : old('price') }}">
                        </div>
                    </div>
                    <div class="clearfix">
                        <div class="form-group" style="float: left; width: 49%">
                            <label class="required" for="capacity">{{ __('email.capacity') }} <span class="content-required">*</span></label>
                            <input class="form-control" type="text" name="capacity" id="capacity" value="{{ isset($email->capacity) ? $email->capacity : old('capacity') }}">
                        </div>
                        <div class="form-group" style="float: right; width: 49%">
                            <label class="required" for="email_number">{{ __('email.email-number') }} <span class="content-required">*</span></label>
                            <input class="form-control" type="number" name="email_number" id="email_number" value="{{ isset($email->email_number) ? $email->email_number : old('email_number') }}">
                        </div>
                    </div>
                    <div class="clearfix">
                        <div class="form-group" style="float: left; width: 49%">
                            <label class="required" for="email_forwarder">{{ __('email.email-forwarder') }} <span class="content-required">*</span></label>
                            <input class="form-control" type="number" name="email_forwarder" id="email_forwarder" value="{{ isset($email->email_forwarder) ? $email->email_forwarder : old('email_forwarder') }}">
                        </div>
                        <div class="form-group" style="float: right; width: 49%">
                            <label class="required" for="email_list">{{ __('email.email-list') }} <span class="content-required">*</span></label>
                            <input class="form-control" type="number" name="email_list" id="email_list" value="{{ isset($email->email_list) ? $email->email_list : old('email_list') }}">
                        </div>
                    </div>
                    <div class="clearfix">
                        <div class="form-group" style="float: left; width: 49%">
                            <label class="required" for="parked_domains">{{ __('email.parked-domains') }} <span class="content-required">*</span></label>
                            <input class="form-control" type="number" name="parked_domains" id="parked_domains" value="{{ isset($email->parked_domains) ? $email->parked_domains : old('parked_domains') }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="required" for="notes">{{ __('email.note') }}</label>
                        <input class="form-control" type="text" name="notes" id="notes" value="{{ isset($email->notes) ? $email->notes : old('notes') }}">
                    </div>
                    <div class="form-group">
                        <button class="btn btn-secondary" type="button" onclick="goBack()">{{ __('general.back') }}</button>
                        <button class="btn btn-danger" type="submit">{{ __('general.save') }}</button>
                    </div>
                </form>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul style="margin-bottom: 0px">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
    </div>
@stop
