<div class="card-group">
    @foreach($emails as $email)
        <div class="col col-sm-12 col-lg-3">
            <div class="card card-4-col">
                <div class="card-header card-header-new" style="font-size: 20px; font-weight: bold; text-align: center;">
                    {{ $email->name }}
                </div>
                <div class="card-body card-body-email" style="background-color: #fafafa; overflow-x: scroll">
                    @include('admin.email.information')
                </div>
                <div class="card-footer" style="background-color: #f2f2f2; text-align: center">
                    <input type="checkbox" class="align-middle btn-check" value="{{ $email->id }}" style="display: none">
                    <button type="button" class="btn btn-xs btn-info" data-toggle="modal" data-target="#viewModal{{ $email->id }}">
                        {{ __('general.view') }}
                    </button>
                    <div class="modal fade" id="viewModal{{ $email->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">{{ __('email.email') }}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    @include('admin.email.view')
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('general.close') }}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <a class="btn btn-xs btn-success" href="{{ route('admin.emails.create').'?id='.$email->id }}">{{ __('general.update') }}</a>
                    <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#deleteItemModal{{ $email->id }}">
                        {{ __('general.delete') }}
                    </button>
                    <form action="{{ route('admin.emails.destroy') }}" method="POST">
                        @csrf
                        <div class="modal fade" id="deleteItemModal{{ $email->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">{{ __('email.email') }}</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <input type="text" name="id" value="{{ $email->id }}" style="display: none">
                                        <p class="text-confirm">{{ __('general.confirm-delete') }}</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('general.close') }}</button>
                                        <button type="submit" class="btn btn-danger">{{ __('general.delete') }}</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endforeach
</div>
<div class="d-flex justify-content-center">
    {!! $emails->links() !!}
</div>
