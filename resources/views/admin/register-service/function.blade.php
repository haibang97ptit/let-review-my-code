<button type="button" class="btn btn-xs btn-info" data-toggle="modal" data-target="#viewModal{{ $register_service->id }}">
    {{ __('general.view') }}
</button>

<a href="{{ route('admin.register-services.update', $register_service->id) }}" class="btn btn-xs btn-success">{{ __('general.update') }}</a>

<button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#deleteItemModal{{ $register_service->id }}">
    {{ __('general.delete') }}
</button>

<div class="modal fade" id="viewModal{{ $register_service->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">{{ __('register-service.service-info') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-striped table-hover">
                    <tbody>
                    <tr>
                        <th>{{ __('register-service.code') }}</th>
                        <td>{{ $register_service->code }}</td>
                    </tr>
                    <tr>
                        <th>{{ __('register-service.customer') }}</th>
                        <td>{{ $register_service->first_name.' '.$register_service->last_name }}</td>
                    </tr>
                    <tr>
                        <th>{{ __('register-service.email') }}</th>
                        <td>{{ $register_service->customer_email }}</td>
                    </tr>
                    <tr>
                        <th>{{ __('register-service.service') }}</th>
                        <td>{{ $register_service->type_service }}</td>
                    </tr>
                    <tr>
                        <th>{{ __('register-service.domain-address') }}</th>
                        <td>{{ $register_service->address_domain }}</td>
                    </tr>
                    <tr>
                        <th>{{ __('register-service.price') }}</th>
                        <td>{{ $register_service->price }}</td>
                    </tr>
                    <tr>
                        <th>{{ __('register-service.start-date') }}</th>
                        <td>{{ date('d/m/Y', strtotime($register_service->start_date)) }}</td>
                    </tr>
                    <tr>
                        <th>{{ __('register-service.end-date') }}</th>
                        <td>{{ date('d/m/Y', strtotime($register_service->end_date)) }}</td>
                    </tr>
                    <tr>
                        <th>{{ __('register-service.note') }}</th>
                        <td>{{ $register_service->notes }}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('general.close') }}</button>
            </div>
        </div>
    </div>
</div>

<form action="{{ route('admin.register-services.destroy') }}" method="POST">
    @csrf
    <div class="modal fade" id="deleteItemModal{{ $register_service->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{ __('register-service.service') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="text" name="id" value="{{ $register_service->id }}" style="display: none">
                    <p>{{ __('general.confirm-delete') }}</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('general.close') }}</button>
                    <button type="submit" class="btn btn-danger">{{ __('general.delete') }}</button>
                </div>
            </div>
        </div>
    </div>
</form>
