<div id="register-service-table">
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
            <tr>
                <th class="align-middle" style="width: 10px"><input type="checkbox" id="check-all"></th>
                <th class="align-middle">{{ __('register-service.id') }}</th>
                <th class="align-middle">{{ __('register-service.customer') }}</th>
                <th class="align-middle">
                    <p>{{ __('register-service.service') }}</p>
                    <p class="small-p">{{ __('register-service.domain-address') }}</p>
                </th>
                <th class="align-middle">
                    <p>{{ __('register-service.start-date') }}</p>
                    <p class="small-p">{{ __('register-service.end-date') }}</p>
                </th>
                <th class="align-middle">{{ __('register-service.status') }}</th>
                <th class="align-middle"></th>
            </tr>
            </thead>
            <tbody>
            @forelse($register_services as $item => $register_service)
                <tr >
                    <td class="align-middle"><input type="checkbox" class="btn-check" value="{{ $register_service->id }}"></td>
                    <td class="align-middle">{{ $register_services->firstItem() + $item }}</td>
                    <td class="align-middle">
                        <p>{{ $register_service->first_name.' '.$register_service->last_name }}</p>
                        <p class="small-p">{{ $register_service->customer_email }}</p>
                    </td>
                    <td class="align-middle">
                        <p>{{ $register_service->type_service }}</p>
                        <p class="small-p">{{ $register_service->address_domain }}</p>
                    </td>
                    <td class="align-middle">
                        <p>{{ date('Y-m-d H:i', strtotime($register_service->start_date)) }}</p>
                        <p class="small-p">{{ date('Y-m-d H:i', strtotime($register_service->end_date) )}}</p>
                    </td>
                    <td class="align-middle">
                        <input class="status-toggle" {{ $register_service->status_register == "paid" ? "checked" : "" }} data-id="{{ $register_service->id }}" type="checkbox" data-size="mini" data-width="100%" data-height="20%" data-onstyle="success" data-offstyle="warning" data-toggle="toggle" data-on="{{ __('general.paid') }}" data-off="{{ __('general.unpaid') }}">
                    </td>
                    <td class="align-middle">
                        @include('admin.register-service.function')
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="7" class="text-center">{{ __('general.nodata') }}</td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>
    <div class="clearfix">
        <div style="float: right">
            {!! $register_services->links() !!}
        </div>
    </div>
</div>
