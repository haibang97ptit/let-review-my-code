@if($register_service->type_service == "domain")
    @php /** @var TYPE_NAME $register_service */
        $domain = \App\Models\Domain::findOrFail($register_service->type_service_id) @endphp
    <div class="form-row">
        <div class="form-group col-md-6">
            <label>{{ __('register-service.domain-name') }} <span class="content-required">*</span></label>
            <input type="text" class="form-control" value="{{ $domain->name }}" readonly>
        </div>
        <div class="form-group col-md-6">
            <label>{{ __('register-service.fee-register') }} <span class="content-required">*</span></label>
            <input type="text" class="form-control" value="{{ $domain->fee_register }}" readonly>
        </div>
    </div>

    <div class="form-row">
        <div class="form-group col-md-6">
            <label>{{ __('register-service.fee-remain') }} <span class="content-required">*</span></label>
            <input type="text" class="form-control" value="{{ $domain->fee_remain }}" readonly>
        </div>
        <div class="form-group col-md-6">
            <label>{{ __('register-service.fee-transformation') }} <span class="content-required">*</span></label>
            <input type="text" class="form-control" value="{{ $domain->fee_transformation }}" readonly>
        </div>
    </div>
@elseif($register_service->type_service == "hosting")
    @php /** @var TYPE_NAME $register_service */
        $hosting = \App\Models\Hosting::findOrFail($register_service->type_service_id) @endphp
    <div class="form-row">
        <div class="form-group col-md-6">
            <label>{{ __('register-service.hosting-name') }} <span class="content-required">*</span></label>
            <input type="text" class="form-control" value="{{ $hosting->name }}" readonly>
        </div>
        <div class="form-group col-md-6">
            <label>{{ __('register-service.price') }} <span class="content-required">*</span></label>
            <input type="text" class="form-control" value="{{ $hosting->price }}" readonly>
        </div>
    </div>

    <div class="form-row">
        <div class="form-group col-md-6">
            <label>{{ __('register-service.capacity') }} <span class="content-required">*</span></label>
            <input type="text" class="form-control" value="{{ $hosting->capacity }}" readonly>
        </div>
        <div class="form-group col-md-6">
            <label>{{ __('register-service.bandwidth') }} <span class="content-required">*</span></label>
            <input type="text" class="form-control" value="{{ $hosting->bandwith }}" readonly>
        </div>
    </div>

    <div class="form-row">
        <div class="form-group col-md-6">
            <label>{{ __('register-service.sub-domain') }} <span class="content-required">*</span></label>
            <input type="text" class="form-control" value="{{ $hosting->sub_domain }}" readonly>
        </div>
        <div class="form-group col-md-6">
            <label>{{ __('register-service.email') }} <span class="content-required">*</span></label>
            <input type="text" class="form-control" value="{{ $hosting->email }}" readonly>
        </div>
    </div>

    <div class="form-row">
        <div class="form-group col-md-6">
            <label>{{ __('register-service.ftp') }} <span class="content-required">*</span></label>
            <input type="text" class="form-control" value="{{ $hosting->ftp }}" readonly>
        </div>
        <div class="form-group col-md-6">
            <label>{{ __('register-service.database') }} <span class="content-required">*</span></label>
            <input type="text" class="form-control" value="{{ $hosting->database }}" readonly>
        </div>
    </div>

    <div class="form-row">
        <div class="form-group col-md-6">
            <label>{{ __('register-service.adddon-domain') }} <span class="content-required">*</span></label>
            <input type="text" class="form-control" value="{{ $hosting->adddon_domain }}" readonly>
        </div>
        <div class="form-group col-md-6">
            <label>{{ __('register-service.park-domain') }} <span class="content-required">*</span></label>
            <input type="text" class="form-control" value="{{ $hosting->park_domain }}" readonly>
        </div>
    </div>
@elseif($register_service->type_service == "vps")
    @php /** @var TYPE_NAME $register_service */
        $vps = \App\Models\VPS::findOrFail($register_service->type_service_id) @endphp
    <div class="form-row">
        <div class="form-group col-md-6">
            <label>{{ __('register-service.vps-name') }} <span class="content-required">*</span></label>
            <input type="text" class="form-control" value="{{ $vps->name }}" readonly>
        </div>
        <div class="form-group col-md-6">
            <label>{{ __('register-service.price') }} <span class="content-required">*</span></label>
            <input type="text" class="form-control" value="{{ $vps->price }}" readonly>
        </div>
    </div>

    <div class="form-row">
        <div class="form-group col-md-6">
            <label>{{ __('register-service.cpu') }} <span class="content-required">*</span></label>
            <input type="text" class="form-control" value="{{ $vps->cpu }}" readonly>
        </div>
        <div class="form-group col-md-6">
            <label>{{ __('register-service.capacity') }} <span class="content-required">*</span></label>
            <input type="text" class="form-control" value="{{ $vps->capacity }}" readonly>
        </div>
    </div>

    <div class="form-row">
        <div class="form-group col-md-6">
            <label>{{ __('register-service.ram') }} <span class="content-required">*</span></label>
            <input type="text" class="form-control" value="{{ $vps->ram }}" readonly>
        </div>
        <div class="form-group col-md-6">
            <label>{{ __('register-service.bandwidth') }} <span class="content-required">*</span></label>
            <input type="text" class="form-control" value="{{ $vps->bandwith }}" readonly>
        </div>
    </div>

    <div class="form-row">
        <div class="form-group col-md-6">
            <label>{{ __('register-service.technical') }} <span class="content-required">*</span></label>
            <input type="text" class="form-control" value="{{ $vps->technical }}" readonly>
        </div>
        <div class="form-group col-md-6">
            <label>{{ __('register-service.operating-system') }} <span class="content-required">*</span></label>
            <input type="text" class="form-control" value="{{ $vps->operating_system }}" readonly>
        </div>
    </div>

    <div class="form-row">
        <div class="form-group col-md-6">
            <label>{{ __('register-service.backup') }} <span class="content-required">*</span></label>
            <input type="text" class="form-control" value="{{ $vps->backup }}" readonly>
        </div>
        <div class="form-group col-md-6">
            <label>{{ __('register-service.panel') }} <span class="content-required">*</span></label>
            <input type="text" class="form-control" value="{{ $vps->panel }}" readonly>
        </div>
    </div>
@elseif($register_service->type_service == "email")
    @php /** @var TYPE_NAME $register_service */
        $email = \App\Models\Email::findOrFail($register_service->type_service_id) @endphp
    <div class="form-row">
        <div class="form-group col-md-6">
            <label>{{ __('register-service.email-name') }} <span class="content-required">*</span></label>
            <input type="text" class="form-control" value="{{ $email->name }}" readonly>
        </div>
        <div class="form-group col-md-6">
            <label>{{ __('register-service.price') }} <span class="content-required">*</span></label>
            <input type="text" class="form-control" value="{{ $email->price }}" readonly>
        </div>
    </div>

    <div class="form-row">
        <div class="form-group col-md-6">
            <label>{{ __('register-service.capacity') }} <span class="content-required">*</span></label>
            <input type="text" class="form-control" value="{{ $email->capacity }}" readonly>
        </div>
        <div class="form-group col-md-6">
            <label>{{ __('register-service.email-number') }} <span class="content-required">*</span></label>
            <input type="text" class="form-control" value="{{ $email->email_number }}" readonly>
        </div>
    </div>

    <div class="form-row">
        <div class="form-group col-md-6">
            <label>{{ __('register-service.email-forwarder') }} <span class="content-required">*</span></label>
            <input type="text" class="form-control" value="{{ $email->email_forwarder }}" readonly>
        </div>
        <div class="form-group col-md-6">
            <label>{{ __('register-service.email-list') }} <span class="content-required">*</span></label>
            <input type="text" class="form-control" value="{{ $email->email_list }}" readonly>
        </div>
    </div>

    <div class="form-row">
        <div class="form-group col-md-6">
            <label>{{ __('register-service.parked-domains') }} <span class="content-required">*</span></label>
            <input type="text" class="form-control" value="{{ $email->parked_domains }}" readonly>
        </div>
    </div>
@elseif($register_service->type_service == "ssl")
    @php /** @var TYPE_NAME $register_service */
        $ssl = \App\Models\Ssl::findOrFail($register_service->type_service_id) @endphp
    <div class="form-row">
        <div class="form-group col-md-6">
            <label>{{ __('register-service.ssl-name') }} <span class="content-required">*</span></label>
            <input type="text" class="form-control" value="{{ $ssl->name }}" readonly>
        </div>
        <div class="form-group col-md-6">
            <label>{{ __('register-service.price') }} <span class="content-required">*</span></label>
            <input type="number" class="form-control" value="{{ $ssl->price }}" readonly>
        </div>
    </div>

    <div class="form-row">
        <div class="form-group col-md-6">
            <label>{{ __('register-service.insurance-policy') }} <span class="content-required">*</span></label>
            <input type="text" class="form-control" value="{{ $ssl->insurance_policy }}" readonly>
        </div>
        <div class="form-group col-md-6">
            <label>{{ __('register-service.domain-number') }} <span class="content-required">*</span></label>
            <input type="text" class="form-control" value="{{ $ssl->domain_number }}" readonly>
        </div>
    </div>

    <div class="form-row">
        <div class="form-group col-md-6">
            <label>{{ __('register-service.reliability') }} <span class="content-required">*</span></label>
            <input type="text" class="form-control" value="{{ $ssl->reliability }}" readonly>
        </div>
        <div class="form-group col-md-6">
            <label>{{ __('register-service.green-bar') }} <span class="content-required">*</span></label>
            <input type="text" class="form-control" value="{{ $ssl->green_bar }}" readonly>
        </div>
    </div>

    <div class="form-row">
        <div class="form-group col-md-6">
            <label>{{ __('register-service.sans') }} <span class="content-required">*</span></label>
            <input type="text" class="form-control" value="{{ $ssl->sans }}" readonly>
        </div>
    </div>
@elseif($register_service->type_service == "website")
    @php /** @var TYPE_NAME $register_service */
        $website = \App\Models\Website::findOrFail($register_service->type_service_id) @endphp
    <div class="form-row">
        <div class="form-group col-md-6">
            <label>{{ __('register-service.website-name') }} <span class="content-required">*</span></label>
            <input type="text" class="form-control" value="{{ $website->name }}" readonly>
        </div>
        <div class="form-group col-md-6">
            <label>{{ __('register-service.website-type') }} <span class="content-required">*</span></label>
            <input type="text" class="form-control" value="{{ $website->type_website }}" readonly>
        </div>
    </div>

    <div class="form-row">
        <div class="form-group col-md-6">
            <label>{{ __('register-service.price') }} <span class="content-required">*</span></label>
            <input type="text" class="form-control" value="{{ $website->price }}" readonly>
        </div>
    </div>
@endif
