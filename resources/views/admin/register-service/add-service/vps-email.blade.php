<div class="form-row">
    <div class="form-group col-md-6">
        <div class="card card-color service-vps">
            <div class="card-header" role="tab" id="headingVPS">
                <a data-toggle="collapse" href="#collapseVPS" aria-expanded="true" aria-controls="collapseVPS">
                    <h5 class="mt-1 mb-0 service-click" data-service="vps">
                        <label>{{ __('register-service.vps') }}</label>
                    </h5>
                </a>
            </div>
        </div>
    </div>

    <div class="form-group col-md-6">
        <div class="card card-color service-email">
            <div class="card-header" role="tab" id="headingEmail">
                <a data-toggle="collapse" href="#collapseEmail" aria-expanded="true" aria-controls="collapseEmail">
                    <h5 class="mt-1 mb-0 service-click" data-service="email">
                        <label>{{ __('register-service.email') }}</label>
                    </h5>
                </a>
            </div>
        </div>
    </div>
</div>
<div class="form-group col-md-12" style="margin-bottom: 0px">
    <div class="collapse" id="collapseVPS" aria-labelledby="headingVPS" data-parent="#accordionService">
        <div class="card-body border border-primary" style="margin-bottom: 30px">

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label>{{ __('register-service.vps-name') }} <span class="content-required">*</span></label>
                    <select onchange="selectVPS(this)" type="text" class="form-control" name="id_vps" id="id_vps">
                        <option value="">{{ __('register-service.choose-vps') }}</option>
                        @foreach($vpses as $vps)
                        <option value="{{ $vps->id }}"
                                data-price="{{ $vps->price }}"
                                data-cpu="{{ $vps->cpu }}"
                                data-capacity="{{ $vps->capacity }}"
                                data-ram="{{ $vps->ram }}"
                                data-bandwith="{{ $vps->bandwith }}"
                                data-technical="{{ $vps->technical }}"
                                data-operating-system="{{ $vps->operating_system }}"
                                data-backup="{{ $vps->backup }}"
                                data-panel="{{ $vps->panel }}"
                                data-notes="{{ $vps->notes }}">
                            {{ $vps->name }}
                        </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-md-6">
                    <label>{{ __('register-service.price') }} <span class="content-required">*</span></label>
                    <input id="vps_price" type="text" class="form-control" name="vps_price" readonly>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label>{{ __('register-service.cpu') }} <span class="content-required">*</span></label>
                    <input id="vps_cpu" type="text" class="form-control" name="vps_cpu" readonly>
                </div>
                <div class="form-group col-md-6">
                    <label>{{ __('register-service.capacity') }} <span class="content-required">*</span></label>
                    <input id="vps_capacity" type="text" class="form-control" name="vps_capacity" readonly>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label>{{ __('register-service.ram') }} <span class="content-required">*</span></label>
                    <input id="vps_ram" type="text" class="form-control" name="vps_ram" readonly>
                </div>
                <div class="form-group col-md-6">
                    <label>{{ __('register-service.bandwidth') }} <span class="content-required">*</span></label>
                    <input id="vps_bandwith" type="text" class="form-control" name="vps_bandwith" readonly>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label>{{ __('register-service.technical') }} <span class="content-required">*</span></label>
                    <input id="vps_technical" type="text" class="form-control" name="vps_technical" readonly>
                </div>
                <div class="form-group col-md-6">
                    <label>{{ __('register-service.operating-system') }} <span class="content-required">*</span></label>
                    <input id="vps_operating_system" type="text" class="form-control" name="vps_operating_system" readonly>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label>{{ __('register-service.backup') }} <span class="content-required">*</span></label>
                    <input id="vps_backup" type="text" class="form-control" name="vps_backup" readonly>
                </div>
                <div class="form-group col-md-6">
                    <label>{{ __('register-service.panel') }} <span class="content-required">*</span></label>
                    <input id="vps_panel" type="text" class="form-control" name="vps_panel" readonly>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label>{{ __('register-service.domain-address') }} <span class="content-required">*</span></label>
                    <input id="vps_address_domain" type="text" class="form-control" name="vps_address_domain">
                </div>
                <div class="form-group col-md-6">
                    <label class="">{{ __('register-service.time-use') }} <span class="content-required">*</span></label>
                    <select class="form-control" name="vps_time_use">
                        <option value="6" {{ (old('vps_time_use') == 6) ? 'selected' : '' }}>6 tháng</option>
                        <option value="12" {{ (old('vps_time_use') == 12) ? 'selected' : '' }}>12 tháng</option>
                        <option value="18" {{ (old('vps_time_use') == 18) ? 'selected' : '' }}>18 tháng</option>
                        <option value="24" {{ (old('vps_time_use') == 24) ? 'selected' : '' }}>24 tháng</option>
                    </select>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label>{{ __('register-service.start-date') }} <span class="content-required">*</span></label>
                    <input id="vps_start_date" type="date" class="form-control" name="vps_start_date" value="{{ old('vps_start_date') }}">
                </div>
            </div>

        </div>
    </div>
</div>
<div class="form-group col-md-12" style="margin-bottom: 0px">
    <div class="collapse" id="collapseEmail" aria-labelledby="headingEmail" data-parent="#accordionService">
        <div class="card-body border border-primary" style="margin-bottom: 30px">

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label>{{ __('register-service.email-name') }} <span class="content-required">*</span></label>
                    <select onchange="selectEmail(this)" type="text" class="form-control" name="id_email" id="id_email">
                        <option value="">{{ __('register-service.choose-email') }}</option>
                        @foreach($emails as $email)
                        <option value="{{ $email->id }}"
                                data-price="{{ $email->price }}"
                                data-capacity="{{ $email->capacity }}"
                                data-email-number="{{ $email->email_number }}"
                                data-email-forwarder="{{ $email->email_forwarder }}"
                                data-email-list="{{ $email->email_list }}"
                                data-parked-domains="{{ $email->parked_domains }}"
                                data-notes="{{ $email->notes }}">
                            {{ $email->name }}
                        </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-md-6">
                    <label>{{ __('register-service.price') }} <span class="content-required">*</span></label>
                    <input id="email_price" type="text" class="form-control" name="email_price" readonly>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label>{{ __('register-service.capacity') }} <span class="content-required">*</span></label>
                    <input id="email_capacity" type="text" class="form-control" name="email_capacity" readonly>
                </div>
                <div class="form-group col-md-6">
                    <label>{{ __('register-service.email-number') }} <span class="content-required">*</span></label>
                    <input id="email_number" type="text" class="form-control" name="email_number" readonly>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label>{{ __('register-service.email-forwarder') }} <span class="content-required">*</span></label>
                    <input id="email_forwarder" type="text" class="form-control" name="email_forwarder" readonly>
                </div>
                <div class="form-group col-md-6">
                    <label>{{ __('register-service.email-list') }} <span class="content-required">*</span></label>
                    <input id="email_list" type="text" class="form-control" name="email_list" readonly>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label>{{ __('register-service.parked-domains') }} <span class="content-required">*</span></label>
                    <input id="email_parked_domains" type="text" class="form-control" name="email_parked_domains" readonly>
                </div>
                <div class="form-group col-md-6">
                    <label>{{ __('register-service.domain-address') }} <span class="content-required">*</span></label>
                    <input id="email_address_domain" type="text" class="form-control" name="email_address_domain">
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label class="">{{ __('register-service.time-use') }} <span class="content-required">*</span></label>
                    <select class="form-control" name="email_time_use">
                        <option value="6" {{ (old('email_time_use') == 6) ? 'selected' : '' }}>6 tháng</option>
                        <option value="12" {{ (old('email_time_use') == 12) ? 'selected' : '' }}>12 tháng</option>
                        <option value="18" {{ (old('email_time_use') == 18) ? 'selected' : '' }}>18 tháng</option>
                        <option value="24" {{ (old('email_time_use') == 24) ? 'selected' : '' }}>24 tháng</option>
                    </select>
                </div>
                <div class="form-group col-md-6">
                    <label>{{ __('register-service.start-date') }} <span class="content-required">*</span></label>
                    <input id="email_start_date" type="date" class="form-control" name="email_start_date" value="{{ old('email_start_date') }}">
                </div>
            </div>

        </div>
    </div>
</div>
