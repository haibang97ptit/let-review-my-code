<div class="form-row">
    <div class="form-group col-md-6">
        <div class="card card-color service-ssl">
            <div class="card-header" role="tab" id="headingSSL">
                <a data-toggle="collapse" href="#collapseSSL" aria-expanded="true" aria-controls="collapseSSL">
                    <h5 class="mt-1 mb-0 service-click" data-service="ssl">
                        <label>{{ __('register-service.ssl') }}</label>
                    </h5>
                </a>
            </div>
        </div>
    </div>

    <div class="form-group col-md-6">
        <div class="card card-color service-website">
            <div  class="card-header" role="tab" id="headingWebsite">
                <a data-toggle="collapse" href="#collapseWebsite" aria-expanded="true" aria-controls="collapseWebsite">
                    <h5 class="mt-1 mb-0 service-click" data-service="website">
                        <label>{{ __('register-service.website') }}</label>
                    </h5>
                </a>
            </div>
        </div>
    </div>
</div>
<div class="form-group col-md-12" style="margin-bottom: 0px">
    <div class="collapse" id="collapseSSL" aria-labelledby="headingSSL" data-parent="#accordionService">
        <div class="card-body border border-primary" style="margin-bottom: 30px">

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label>{{ __('register-service.ssl-name') }} <span class="content-required">*</span></label>
                    <select onchange="selectSSL(this)" type="text" class="form-control" name="id_ssl" id="id_ssl">
                        <option value="">{{ __('register-service.choose-ssl') }}</option>
                        @foreach($ssls as $ssl)
                            <option value="{{ $ssl->id }}"
                                    data-price="{{ $ssl->price }}"
                                    data-insurance-policy="{{ $ssl->insurance_policy }}"
                                    data-domain-number="{{ $ssl->domain_number }}"
                                    data-reliability="{{ $ssl->reliability }}"
                                    data-green-bar="{{ $ssl->green_bar }}"
                                    data-sans="{{ $ssl->sans }}"
                                    data-notes="{{ $ssl->notes }}">
                                {{ $ssl->name }}
                            </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-md-6">
                    <label>{{ __('register-service.price') }} <span class="content-required">*</span></label>
                    <input id="ssl_price" type="number" class="form-control" name="ssl_price" readonly>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label>{{ __('register-service.insurance-policy') }} <span class="content-required">*</span></label>
                    <input id="ssl_insurance_policy" type="text" class="form-control" name="ssl_insurance_policy" readonly>
                </div>
                <div class="form-group col-md-6">
                    <label>{{ __('register-service.domain-number') }} <span class="content-required">*</span></label>
                    <input id="ssl_domain_number" type="text" class="form-control" name="ssl_domain_number" readonly>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label>{{ __('register-service.reliability') }} <span class="content-required">*</span></label>
                    <input id="ssl_reliability" type="text" class="form-control" name="ssl_reliability" readonly>
                </div>
                <div class="form-group col-md-6">
                    <label>{{ __('register-service.green-bar') }} <span class="content-required">*</span></label>
                    <input id="ssl_green_bar" type="text" class="form-control" name="ssl_green_bar" readonly>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label>{{ __('register-service.sans') }} <span class="content-required">*</span></label>
                    <input id="ssl_sans" type="text" class="form-control" name="ssl_sans" readonly>
                </div>
                <div class="form-group col-md-6">
                    <label>{{ __('register-service.domain-address') }} <span class="content-required">*</span></label>
                    <input id="ssl_address_domain" type="text" class="form-control" name="ssl_address_domain">
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label class="">{{ __('register-service.time-use') }} <span class="content-required">*</span></label>
                    <select class="form-control" name="ssl_time_use">
                        <option value="6" {{ (old('ssl_time_use') == 6) ? 'selected' : '' }}>6 tháng</option>
                        <option value="12" {{ (old('ssl_time_use') == 12) ? 'selected' : '' }}>12 tháng</option>
                        <option value="18" {{ (old('ssl_time_use') == 18) ? 'selected' : '' }}>18 tháng</option>
                        <option value="24" {{ (old('ssl_time_use') == 24) ? 'selected' : '' }}>24 tháng</option>
                    </select>
                </div>
                <div class="form-group col-md-6">
                    <label>{{ __('register-service.start-date') }} <span class="content-required">*</span></label>
                    <input id="ssl_start_date" type="date" class="form-control" name="ssl_start_date" value="{{ old('ssl_start_date') }}">
                </div>
            </div>

        </div>
    </div>
</div>
<div class="form-group col-md-12" style="margin-bottom: 0px">
    <div class="collapse" id="collapseWebsite" aria-labelledby="headingWebsite" data-parent="#accordionService">
        <div class="card-body border border-primary" style="margin-bottom: 30px">

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label>{{ __('register-service.website-name') }} <span class="content-required">*</span></label>
                    <select onchange="selectWebsite(this)" class="form-control" name="id_website" id="id_website">
                        <option value="">{{ __('register-service.choose-website') }}</option>
                        @foreach($websites as $website)
                            <option value="{{ $website->id }}"
                                    data-type-website="{{ $website->type_website }}"
                                    data-price="{{ $website->price }}"
                                    data-notes="{{ $website->notes }}">
                                {{ $website->name }}
                            </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-md-6">
                    <label>{{ __('register-service.website-type') }} <span class="content-required">*</span></label>
                    <input id="website_type" type="text" class="form-control" name="website_type" readonly>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label>{{ __('register-service.price') }} <span class="content-required">*</span></label>
                    <input id="website_price" type="text" class="form-control" name="website_price" readonly>
                </div>
                <div class="form-group col-md-6">
                    <label>{{ __('register-service.domain-address') }} <span class="content-required">*</span></label>
                    <input id="website_address_domain" type="text" class="form-control" name="website_address_domain">
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label class="">{{ __('register-service.time-use') }} <span class="content-required">*</span></label>
                    <select class="form-control" name="website_time_use">
                        <option value="6" {{ (old('website_time_use') == 6) ? 'selected' : '' }}>6 tháng</option>
                        <option value="12" {{ (old('website_time_use') == 12) ? 'selected' : '' }}>12 tháng</option>
                        <option value="18" {{ (old('website_time_use') == 18) ? 'selected' : '' }}>18 tháng</option>
                        <option value="24" {{ (old('website_time_use') == 24) ? 'selected' : '' }}>24 tháng</option>
                    </select>
                </div>
                <div class="form-group col-md-6">
                    <label>{{ __('register-service.start-date') }} <span class="content-required">*</span></label>
                    <input id="website_start_date" type="date" class="form-control" name="website_start_date" value="{{ old('website_start_date') }}">
                </div>
            </div>

        </div>
    </div>
</div>
