<div class="form-row">
    <div class="form-group col-md-6">
        <div class="card card-color service-domain">
            <div class="card-header" role="tab" id="headingDomain">
                <a data-toggle="collapse" href="#collapseDomain" aria-expanded="true" aria-controls="collapseDomain">
                    <h5 class="mt-1 mb-0 service-click" data-service="domain">
                        <label>{{ __('register-service.domain') }}</label>
                    </h5>
                </a>
            </div>
        </div>
    </div>

    <div class="form-group col-md-6">
        <div class="card card-color service-hosting">
            <div class="card-header" role="tab" id="headingHosting">
                <a data-toggle="collapse" href="#collapseHosting" aria-expanded="true" aria-controls="collapseHosting">
                    <h5 class="mt-1 mb-0 service-click" data-service="hosting">
                        <label>{{ __('register-service.hosting') }}</label>
                    </h5>
                </a>
            </div>
        </div>
    </div>
</div>
<div class="form-group col-md-12" style="margin-bottom: 0px">
    <div class="collapse" id="collapseDomain" aria-labelledby="headingDomain" data-parent="#accordionService">
        <div class="card-body border border-primary" style="margin-bottom: 30px">

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label>{{ __('register-service.domain-name') }} <span class="content-required">*</span></label>
                    <select onchange="selectDomain(this)" class="form-control" name="id_domain" id="id_domain">
                        <option value="">{{ __('register-service.choose-domain') }}</option>
                        @foreach($domains as $domain)
                            <option value="{{ $domain->id  }}" {{ ($domain->id == old('id_domain')) ? 'selected' : '' }} data-fee-register="{{ $domain->fee_register }}" data-fee-remain="{{ $domain->fee_remain }}" data-fee-transformation="{{ $domain->fee_transformation }}">
                                {{ $domain->name }}
                            </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-md-6">
                    <label>{{ __('register-service.fee-register') }} <span class="content-required">*</span></label>
                    <input id="fee-register" type="text" class="form-control" name="domain_fee_register" value="{{ old('domain_fee_register') }}" readonly>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label>{{ __('register-service.fee-remain') }} <span class="content-required">*</span></label>
                    <input id="fee-remain" type="text" class="form-control" name="domain_fee_remain" value="{{ old('domain_fee_remain') }}" readonly>
                </div>
                <div class="form-group col-md-6">
                    <label>{{ __('register-service.fee-transformation') }} <span class="content-required">*</span></label>
                    <input id="fee-transformation" type="text" class="form-control " name="domain_fee_transformation" value="{{ old('domain_fee_transformation') }}" readonly>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label>{{ __('register-service.domain-address') }} <span class="content-required">*</span></label>
                    <input id="domain_address_domain" type="text" class="form-control" name="domain_address_domain" value="{{ old('domain_address_domain') }}">
                </div>
                <div class="form-group col-md-6">
                    <label class="">{{ __('register-service.time-use') }} <span class="content-required">*</span></label>
                    <select class="form-control" name="domain_time_use">
                        <option value="6" {{ (old('domain_time_use') == 6) ? 'selected' : '' }}>6 tháng</option>
                        <option value="12" {{ (old('domain_time_use') == 12) ? 'selected' : '' }}>12 tháng</option>
                        <option value="18" {{ (old('domain_time_use') == 18) ? 'selected' : '' }}>18 tháng</option>
                        <option value="24" {{ (old('domain_time_use') == 24) ? 'selected' : '' }}>24 tháng</option>
                    </select>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label>{{ __('register-service.start-date') }} <span class="content-required">*</span></label>
                    <input id="domain_start_date" type="date" class="form-control" name="domain_start_date" value="{{ old('domain_start_date') }}">
                </div>
            </div>

        </div>
    </div>
</div>
<div class="form-group col-md-12" style="margin-bottom: 0px">
    <div class="collapse" id="collapseHosting" aria-labelledby="headingHosting" data-parent="#accordionService">
        <div class="card-body border border-primary" style="margin-bottom: 30px">

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label>{{ __('register-service.hosting-name') }} <span class="content-required">*</span></label>
                    <select onchange="selectHosting(this)" type="text" class="form-control" name="id_hosting" id="id_hosting">
                        <option value="">{{ __('register-service.choose-hosting') }}</option>
                        @foreach($hostings as $hosting)
                            <option value="{{ $hosting->id }}"
                                    data-price="{{ $hosting->price }}"
                                    data-capacity="{{ $hosting->capacity }}"
                                    data-bandwith="{{ $hosting->bandwith }}"
                                    data-sub-domain="{{ $hosting->sub_domain }}"
                                    data-email="{{ $hosting->email }}"
                                    data-ftp="{{ $hosting->ftp }}"
                                    data-database="{{ $hosting->database }}"
                                    data-adddon-domain="{{ $hosting->adddon_domain }}"
                                    data-park-domain="{{ $hosting->park_domain }}"
                                    data-notes="{{ $hosting->notes }}">
                                {{ $hosting->name }}
                            </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-md-6">
                    <label>{{ __('register-service.price') }} <span class="content-required">*</span></label>
                    <input id="hosting_price" type="text" class="form-control" name="hosting_price" readonly>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label>{{ __('register-service.capacity') }} <span class="content-required">*</span></label>
                    <input id="hosting_capacity" type="text" class="form-control" name="hosting_capacity" readonly>
                </div>
                <div class="form-group col-md-6">
                    <label>{{ __('register-service.bandwidth') }} <span class="content-required">*</span></label>
                    <input id="hosting_bandwith" type="text" class="form-control" name="hosting_bandwith" readonly>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label>{{ __('register-service.sub-domain') }} <span class="content-required">*</span></label>
                    <input id="hosting_sub_domain" type="text" class="form-control" name="hosting_sub_domain" readonly>
                </div>
                <div class="form-group col-md-6">
                    <label>{{ __('register-service.email') }} <span class="content-required">*</span></label>
                    <input id="hosting_email" type="text" class="form-control" name="hosting_email" readonly>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label>{{ __('register-service.ftp') }} <span class="content-required">*</span></label>
                    <input id="hosting_ftp" type="text" class="form-control" name="hosting_ftp" readonly>
                </div>
                <div class="form-group col-md-6">
                    <label>{{ __('register-service.database') }} <span class="content-required">*</span></label>
                    <input id="hosting_database" type="text" class="form-control" name="hosting_database" readonly>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label>{{ __('register-service.adddon-domain') }} <span class="content-required">*</span></label>
                    <input id="hosting_adddon_domain" type="text" class="form-control" name="hosting_adddon_domain" readonly>
                </div>
                <div class="form-group col-md-6">
                    <label>{{ __('register-service.park-domain') }} <span class="content-required">*</span></label>
                    <input id="hosting_park_domain" type="text" class="form-control" name="hosting_park_domain" readonly>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label>{{ __('register-service.domain-address') }} <span class="content-required">*</span></label>
                    <input id="hosting_address_domain" type="text" class="form-control" name="hosting_address_domain">
                </div>
                <div class="form-group col-md-6">
                    <label class="">{{ __('register-service.time-use') }} <span class="content-required">*</span></label>
                    <select class="form-control" name="hosting_time_use">
                        <option value="6" {{ (old('hosting_time_use') == 6) ? 'selected' : '' }}>6 tháng</option>
                        <option value="12" {{ (old('hosting_time_use') == 12) ? 'selected' : '' }}>12 tháng</option>
                        <option value="18" {{ (old('hosting_time_use') == 18) ? 'selected' : '' }}>18 tháng</option>
                        <option value="24" {{ (old('hosting_time_use') == 24) ? 'selected' : '' }}>24 tháng</option>
                    </select>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label>{{ __('register-service.start-date') }} <span class="content-required">*</span></label>
                    <input id="hosting_start_date" type="date" class="form-control" name="hosting_start_date" value="{{ old('hosting_start_date') }}">
                </div>
            </div>

        </div>
    </div>
</div>
