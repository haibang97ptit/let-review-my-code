@extends('layout.master')
@section('css')
    <link rel="stylesheet" href="{{ asset('../css/default.css') }}">
@endsection
@section('content')
    <div class="body-content">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                @if(isset($_GET['type']))
                    <li class="breadcrumb-item active" aria-current="page"><h2>{{ __('register-service.extend') }}</h2></li>
                @else
                    <li class="breadcrumb-item active" aria-current="page"><h2>{{ __('register-service.update') }}</h2></li>
                @endif
            </ol>
        </nav>

        <div class="card">
            <div class="card-body">
                <form method="POST" action="{{ route("admin.register-services.update-store") }}" enctype="multipart/form-data">
                    @csrf
                    <input style="display: none" name="id" value="{{ $register_service->id }}" {{ isset($_GET['type']) ? 'disabled' : '' }}>
                    <input style="display: none" name="id_customer" value="{{ $register_service->id_customer }}">
                    <input style="display: none" name="type_service_id" value="{{ $register_service->type_service_id }}">
                    <input style="display: none" type="text" name="start_date_extend" value="{{ $register_service->start_date }}">
                    <input style="display: none" type="text" name="end_date_extend" value="{{ $register_service->end_date }}">
                    <div class="clearfix">
                        <div class="form-group left-col">
                            <label>{{ __('register-software.customer') }} <span class="content-required">*</span></label>
                            <select class="form-control" id="customer-select" name="id_customer">
                                <option></option>
                                @foreach($customers as $customer)
                                    <option value="{{ $customer->id }}" {{ (isset($register_service->id_customer) && $register_service->id_customer == $customer->id) ? "selected" : "" }}>{{ $customer->first_name.' '.$customer->last_name.' - '.$customer->email }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group right-col">
                            <label class="required" for="address_domain">{{ __('register-service.domain-address') }} <span class="content-required">*</span></label>
                            <input class="form-control" type="text" name="address_domain" id="address_domain" value="{{ $register_service->address_domain }}">
                        </div>
                    </div>
                    <div class="clearfix">
                        <div style="float: left; width: 49%" class="form-group">
                            <label class="required">{{ __('register-service.service') }} <span class="content-required">*</span></label>
                            <div class="accordion" id="accordionExample">
                                <input class="form-control" type="text" name="type_service" value="{{ $register_service->type_service }}" readonly data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            </div>
                        </div>
                        <div style="float: right; width: 49%" class="form-group">
                            <label class="required" for="time_use">{{ __('register-service.time-use') }} <span class="content-required">*</span></label>
                            <select class="form-control" name="time_use">
                                <option value="6" {{ ($register_service->time_use == 6) ? 'selected' : '' }}>6 tháng</option>
                                <option value="12" {{ ($register_service->time_use == 12) ? 'selected' : '' }}>12 tháng</option>
                                <option value="18" {{ ($register_service->time_use == 18) ? 'selected' : '' }}>18 tháng</option>
                                <option value="24" {{ ($register_service->time_use == 24) ? 'selected' : '' }}>24 tháng</option>
                            </select>
                        </div>
                    </div>
                    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                        <div class="card-body col-md-12 border border-primary" style="margin-bottom: 20px">
                            @include('admin.register-service.service-update-detail')
                        </div>
                    </div>
                    <div class="clearfix">
                        <div style="float: left; width: 49%" class="form-group">
                            <label class="required" for="start_date">{{ __('register-service.start-date') }} <span class="content-required">*</span></label>
                            @if(isset($_GET['type']))
                                <input class="form-control" type="date" name="start_date" id="start_date" value="{{ date('Y-m-d', strtotime($register_service->start_date)) }}" disabled>
                            @else
                                <input class="form-control" type="date" name="start_date" id="start_date" value="{{ date('Y-m-d', strtotime($register_service->start_date)) }}">
                            @endif
                        </div>
                        <div style="float: right; width: 49%" class="form-group">
                            <label class="required" for="price">{{ __('register-service.price') }} <span class="content-required">*</span></label>
                            <input class="form-control" type="number" name="price" id="price" value="{{ $register_service->price }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="required" for="note">{{ __('register-service.note') }}</label>
                        <input class="form-control" type="text" name="note" id="note" value="{{ isset($customer->note) ? $customer->note : old('note') }}">
                    </div>
                    <div class="form-group">
                        <button class="btn btn-secondary" type="button" onclick="goBack()">{{ __('general.back') }}</button>
                        <button class="btn btn-danger" type="submit">{{ __('general.save') }}</button>
                    </div>
                </form>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul style="margin-bottom: 0px">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function() {

            $("#customer-select").select2({
                placeholder: "{{ __('register-software.choose-customer') }}",
                allowClear: true,
                width: "100%",
            });

        });
    </script>
@stop
