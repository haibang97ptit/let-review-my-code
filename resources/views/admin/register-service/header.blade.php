<div class="clearfix">
    <div style="float: left">
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <label class="input-group-text" for="showRow">{{ __('general.show') }}</label>
            </div>
            <select class="custom-select" id="showRow">
                <option value="10" {{ isset($_GET['amount']) ? ($_GET['amount'] === '10' ? 'selected' : '') : '' }}>10</option>
                <option value="25" {{ isset($_GET['amount']) ? ($_GET['amount'] === '25' ? 'selected' : '') : '' }}>25</option>
                <option value="50" {{ isset($_GET['amount']) ? ($_GET['amount'] === '50' ? 'selected' : '') : '' }}>50</option>
                <option value="100" {{ isset($_GET['amount']) ? ($_GET['amount'] === '100' ? 'selected' : '') : '' }}>100</option>
            </select>
            @can('customer-delete')
                <button id="btn-delete" style="margin-left: 5px" type="button" class="delete btn btn-danger" data-toggle="modal" data-target="#deleteModal">
                    {{ __('general.delete') }}
                </button>
                <form action="{{ route('admin.register-services.destroy-select') }}" method="POST">
                    @csrf
                    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">{{ __('register-service.service') }}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <input type="text" id="allValsDelete" name="allValsDelete[]" style="display: none">
                                    <p class="text-confirm">{{ __('general.confirm-delete') }}</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('general.close') }}</button>
                                    <button type="submit" class="btn btn-danger">{{ __('general.delete') }}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            @endcan
        </div>
    </div>

    <div class="clearfix" style="float: right">
        <div style="float: left">
            <a style="margin-right: 5px" class="btn btn-warning" href="{{ route('admin.register-services.trash-index') }}" role="button"><i class="fa fa-trash"></i></a>
        </div>
        @can('customer-search')
            <button class="btn btn-info" type="button" data-toggle="collapse" data-target="#collapseSearch" aria-expanded="false" aria-controls="collapseSearch">
                {{ __('general.search') }}
            </button>
        @endcan
    </div>
</div>
<form action="{{ route('admin.register-services.search-row') }}" method="POST">
    @csrf
    <div class="collapse" id="collapseSearch">
        <div class="card card-body">
            <div class="clearfix">
                <div style="float: left; width: 30%">
                    <select class="form-control" id="customer-select" name="customer_select">
                        <option></option>
                        @foreach($customers as $customer)asd
                        <option value="{{ $customer->id }}">{{ $customer->first_name.' '.$customer->last_name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="clearfix" style="float: right; width: 65%">
                    <div style="float: left; width: 46%">
                        <select class="form-control" id="service-select" name="service_select">
                            <option></option>
                            <option value="domain">{{ __('register-service.domain') }}</option>
                            <option value="hosting">{{ __('register-service.hosting') }}</option>
                            <option value="vps">{{ __('register-service.vps') }}</option>
                            <option value="email">{{ __('register-service.email') }}</option>
                            <option value="ssl">{{ __('register-service.ssl') }}</option>
                            <option value="website">{{ __('register-service.website') }}</option>
                        </select>
                    </div>
                    <div style="float: right; width: 46%">
                        <select class="form-control" id="status-select" name="status_select">
                            <option></option>
                            <option value="paid">{{ __('register-service.paid') }}</option>
                            <option value="unpaid">{{ __('register-service.unpaid') }}</option>
                        </select>
                    </div>
                </div>
            </div>
            <div style="margin-top: 15px">
                <button style="float: right" type="submit" class="btn btn-danger">Xác nhận</button>
            </div>
        </div>
    </div>
</form>
