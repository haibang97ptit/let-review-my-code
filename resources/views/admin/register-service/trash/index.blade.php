@extends('layout.master')
@section('css')
    <link rel="stylesheet" href="{{ asset('../css/default.css') }}">
@endsection
@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @can('customer-view')
        <div class="body-content">

            <div class="clearfix">
                @if(session('success'))
                    <div class="alert-crud alert alert-success" style="margin-bottom: 15px">
                        {{ session('success') }}
                    </div>
                @endif
                @if(session('fail'))
                    <div class="alert-crud alert alert-danger" style="margin-bottom: 15px">
                        {{ session('fail') }}
                    </div>
                @endif
            </div>

            <div class="card">
                <div class="card-header card-header-new">
                    {{ __('register-service.list') }}
                </div>
                <div class="card-body">
                    @include('admin.register-service.trash.header')
                    @include('admin.register-service.trash.body')
                </div>
            </div>
        </div>
    @endcan

    <script type="text/javascript">
        $(document).ready(function() {

            $("#customer-select").select2({
                placeholder: "{{ __('register-service.choose-customer') }}",
                allowClear: true
            });

            $("#service-select").select2({
                placeholder: "{{ __('register-service.choose-service') }}",
                allowClear: true
            });

            $("#status-select").select2({
                placeholder: "{{ __('register-service.choose-status') }}",
                allowClear: true
            });

            $("#check-all").click(function () {
                $('input:checkbox').not(this).prop('checked', this.checked);
            });

            $("#btn-delete").on('click', function () {
                var allVals = [];
                $(".btn-check").not("#check-all").each(function() {
                    if ($(this).is(":checked")) {
                        allVals.push($(this).val());
                    }
                });
                // console.log(allVals);
                $('#allValsDelete').val(allVals);
            });

            $(document).on('click', '.pagination a', function (event) {
                event.preventDefault();
                var amountRow = $("#showRow :selected").val();
                var key = $("#search").val();
                var url = $(this).attr('href') + '&amount=' + amountRow;
                window.location.assign(url);
            });

            $("#showRow").on('change', function () {
                var amountRow = $("#showRow :selected").val();
                var url = '{{ route('admin.register-services.trash-index').'?amount=:amount' }}';
                url = url.replace(':amount', amountRow);
                window.location.assign(url);
            });

        });
    </script>
@stop

