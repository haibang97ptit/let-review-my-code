@extends('layout.master')
@section('css')
    <link rel="stylesheet" href="{{ asset('../css/default.css') }}">
@endsection
@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <div class="body-content">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active" aria-current="page"><h2>{{ __('register-service.register-service') }}</h2></li>
            </ol>
        </nav>

        <div class="card">
            <div class="card-body">
                <form method="POST" action="{{ route("admin.register-services.store") }}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label>{{ __('register-service.customer') }} <span class="content-required">*</span></label>
                            <select class="form-control" id="customer-select" name="id_customer">
                                <option></option>
                                @foreach($customers as $customer)
                                    <option value="{{ $customer->id }}">{{ $customer->first_name.' '.$customer->last_name.' - '.$customer->email }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-row" style="margin-bottom: 20px">
                        <div class="form-group col-md-12">
                            <label>{{ __('register-service.note') }}</label>
                            <textarea rows="5" type="text" class="form-control" name="notes"></textarea>
                        </div>
                    </div>

                    <div class="accordion" id="accordionService">
                        <!-- Domain - host -->
                        @include('admin.register-service.add-service.domain-host')
                        <!-- VPS - Email -->
                        @include('admin.register-service.add-service.vps-email')
                        <!-- SSL - Website -->
                        @include('admin.register-service.add-service.ssl-website')
                    </div>

                    <div class="form-group">
                        <button id="btn-submit" class="btn btn-danger" type="button">{{ __('general.register') }}</button>
                    </div>
                </form>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul style="margin-bottom: 0px">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function() {

            $("#customer-select").select2({
                placeholder: "{{ __('register-service.choose-customer') }}",
                allowClear: true,
                width: "100%",
            });

            $('.service-click').each(function () {
                $(this).on('click', function () {
                    var cls = ".service-" + $(this).data('service');
                    var backColor = $(cls).css('background-color');
                    if (backColor === 'rgb(233, 236, 239)') {
                        $(cls).css('background-color', 'LightGray');
                    }
                    else {
                        $('.card-color').css('background-color', 'LightGray');
                        $(cls).css('background-color', '#e9ecef');
                    }
                })
            });

            $("#btn-submit").on('click', function () {
                var id_domain = $("#id_domain").val();
                var id_hosting = $("#id_hosting").val();
                var id_vps = $("#id_vps").val();
                var id_email = $("#id_email").val();
                var id_ssl = $("#id_ssl").val();
                var id_website = $("#id_website").val();
                var id_customer = $("#customer-select").val();
                var count = 0;
                var countSubmit = 0;

                if (id_domain !== "") {
                    count++;
                    var domain_address_domain = $("#domain_address_domain").val();
                    var domain_start_date = $("#domain_start_date").val();
                    if (domain_address_domain === "" || domain_start_date === "") {
                        countSubmit++;
                        alertify.error("Domain không đủ thông tin");
                    }
                }
                if (id_hosting !== "") {
                    count++;
                    var hosting_address_domain = $("#hosting_address_domain").val();
                    var hosting_start_date = $("#hosting_start_date").val();
                    if (hosting_address_domain === "" || hosting_start_date === "") {
                        countSubmit++;
                        alertify.error("Hosting không đủ thông tin");
                    }
                }
                if (id_vps !== "") {
                    count++;
                    var vps_address_domain = $("#vps_address_domain").val();
                    var vps_start_date = $("#vps_start_date").val();
                    if (vps_address_domain === "" || vps_start_date === "") {
                        countSubmit++;
                        alertify.error("VPS không đủ thông tin");
                    }
                }
                if (id_email !== "") {
                    count++;
                    var email_address_domain = $("#email_address_domain").val();
                    var email_start_date = $("#email_start_date").val();
                    if (email_address_domain === "" || email_start_date === "") {
                        countSubmit++;
                        alertify.error("Email không đủ thông tin");
                    }
                }
                if (id_ssl !== "") {
                    count++;
                    var ssl_address_domain = $("#ssl_address_domain").val();
                    var ssl_start_date = $("#ssl_start_date").val();
                    if (ssl_address_domain === "" || ssl_start_date === "") {
                        countSubmit++;
                        alertify.error("SSL không đủ thông tin");
                    }
                }
                if (id_website !== "") {
                    count++;
                    var website_address_domain = $("#website_address_domain").val();
                    var website_start_date = $("#website_start_date").val();
                    if (website_address_domain === "" || website_start_date === "") {
                        countSubmit++;
                        alertify.error("Website không đủ thông tin");
                    }
                }
                if ( id_customer === "") {
                    countSubmit++;
                    alertify.error("Hãy chọn khách hàng");
                }
                if (count === 0) {
                    countSubmit++;
                    alertify.error("Hãy chọn dịch vụ");
                }
                if (countSubmit === 0) {
                    $("form").submit();
                }
            });

        });
    </script>

@stop

