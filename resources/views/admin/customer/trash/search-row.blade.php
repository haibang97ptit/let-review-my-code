<div class="table-responsive">
    <table class="table table-bordered table-striped table-hover">
        <thead>
        <tr>
            <th style="width: 10px"><input type="checkbox" id="check-all"></th>
            <th>{{ __('customer.id') }}</th>
            <th>{{ __('customer.name') }}</th>
            <th>{{ __('customer.birthday') }}</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @forelse($customers as $item => $customer)
            <tr>
                <td class="align-middle"><input type="checkbox" class="btn-check" value="{{ $customer->id }}"></td>
                <td class="align-middle">{{ $customers->firstItem() + $item }}</td>
                <td class="align-middle">
                    <p>{{ $customer->first_name.' '.$customer->last_name }}</p>
                    <p style="color: gray; font-size: 13px">{{ $customer->phone_number.' - '.$customer->email }}</p>
                </td>
                <td class="align-middle">{{ date('d/m/Y', strtotime($customer->birthday)) }}</td>
                <td class="align-middle">
                    <button type="button" class="btn btn-xs btn-info" data-toggle="modal" data-target="#viewModal{{ $customer->id }}">
                        {{ __('general.view') }}
                    </button>
                    <div class="modal fade" id="viewModal{{ $customer->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">{{ __('customer.customer') }}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <table class="table table-bordered table-striped table-hover">
                                        <tbody>
                                        <tr>
                                            <th>{{ __('customer.name') }}</th>
                                            <td>{{ $customer->first_name.' '.$customer->last_name }}</td>
                                        </tr>
                                        <tr>
                                            <th>{{ __('customer.birthday') }}</th>
                                            <td>{{ date('d/m/Y', strtotime($customer->birthday)) }}</td>
                                        </tr>
                                        <tr>
                                            <th>{{ __('customer.phone-number') }}</th>
                                            <td>{{ $customer->phone_number }}</td>
                                        </tr>
                                        <tr>
                                            <th>{{ __('customer.email') }}</th>
                                            <td>{{ $customer->email }}</td>
                                        </tr>
                                        <tr>
                                            <th>{{ __('customer.state') }}</th>
                                            <td>{{ $customer->state }}</td>
                                        </tr>
                                        <tr>
                                            <th>{{ __('customer.address') }}</th>
                                            <td>{{ $customer->address }}</td>
                                        </tr>
                                        <tr>
                                            <th>{{ __('customer.note') }}</th>
                                            <td>{{ $customer->note }}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('general.close') }}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button type="button" class="btn btn-xs btn-success" data-toggle="modal" data-target="#restoreItemModal{{ $customer->id }}">
                        {{ __('general.restore') }}
                    </button>
                    @can('customer-delete')
                        <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#deleteItemModal{{ $customer->id }}">
                            {{ __('general.delete') }}
                        </button>
                        <form action="{{ route('admin.customers.trash-force') }}" method="POST">
                            @csrf
                            <div class="modal fade" id="deleteItemModal{{ $customer->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">{{ __('customer.customer') }}</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <input type="text" name="id" value="{{ $customer->id }}" style="display: none">
                                            <p>{{ __('general.confirm-delete') }}</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('general.close') }}</button>
                                            <button type="submit" class="btn btn-danger">{{ __('general.delete') }}</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    @endcan
                    <form action="{{ route('admin.customers.trash-restore') }}" method="POST">
                        @csrf
                        <div class="modal fade" id="restoreItemModal{{ $customer->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">{{ __('customer.customer') }}</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <input type="text" name="id" value="{{ $customer->id }}" style="display: none">
                                        <p>{{ __('general.confirm-restore') }}</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('general.close') }}</button>
                                        <button type="submit" class="btn btn-success">{{ __('general.restore') }}</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </td>
            </tr>
        @empty
            <tr>
                <td style="text-align: center" colspan="5">{{ __('general.nodata') }}</td>
            </tr>
        @endforelse
        </tbody>
    </table>
</div>
<div class="clearfix">
    <div style="float: right">
        {!! $customers->links() !!}
    </div>
</div>
