@extends('layout.master')
@section('css')
    <link rel="stylesheet" href="{{ asset('../css/default.css') }}">
@endsection
@section('content')
    <div class="body-content">
        <div class="card">
            <div class="card-header card-header-new">
                {{ __('customer.create') }}
            </div>

            <div class="card-body">
                <form method="POST" action="{{ route("admin.customers.store") }}" enctype="multipart/form-data">
                    @csrf
                    <input style="display: none" name="id" value="{{ isset($customer->id) ? $customer->id : null }}">
                    <div class="clearfix">
                        <div style="float: left; width: 49%" class="form-group">
                            <label class="required" for="first_name">{{ __('customer.first-name') }} <label class="content-required">*</label></label>
                            <input class="form-control" type="text" name="first_name" id="first_name" value="{{ (isset($customer->first_name) ? $customer->first_name : old('first_name')) }}">
                        </div>
                        <div style="float: right; width: 49%" class="form-group">
                            <label class="required" for="last_name">{{ __('customer.last-name') }} <label class="content-required">*</label></label>
                            <input class="form-control" type="text" name="last_name" id="last_name" value="{{ isset($customer->last_name) ? $customer->last_name : old('last_name') }}">
                        </div>
                    </div>
                    <div class="clearfix">
                        <div style="float: left; width: 49%" class="form-group">
                            <label class="required" for="email">{{ __('customer.email') }} <label class="content-required">*</label></label>
                            <input class="form-control" type="email" name="email" id="email" value="{{ isset($customer->email) ? $customer->email : old('email') }}">
                        </div>
                        <div style="float: right; width: 49%" class="form-group">
                            <label class="required" for="password">{{ __('customer.password') }} <label class="content-required">*</label></label>
                            <input class="form-control" type="password" name="password" id="password">
                        </div>
                    </div>
                    <div class="clearfix">
                        <div style="float: left; width: 49%" class="form-group">
                            <label class="required" for="phone_number">{{ __('customer.phone-number') }} <label class="content-required">*</label></label>
                            <input class="form-control" type="number" name="phone_number" id="phone_number" value="{{ isset($customer->phone_number) ? $customer->phone_number : old('phone_number') }}">
                        </div>
                        <div style="float: right; width: 49%" class="form-group">
                            <label class="required" for="birthday">{{ __('customer.birthday') }} <label class="content-required">*</label></label>
                            <input class="form-control" type="date" name="birthday" id="birthday" value="{{ isset($customer->birthday) ? $customer->birthday : old('birthday') }}">
                        </div>
                    </div>
                    <div class="clearfix">
                        <div style="float: left; width: 49%" class="form-group">
                            <label class="required" for="state">{{ __('customer.state') }} <label class="content-required">*</label></label>
                            <select class="form-control" name="state" id="state">
                                @foreach($states as $state)
                                    <option value="{{ $state->name }}" {{ ($state->name == (isset($customer->state) ? $customer->state : old('state'))) ? 'selected' : '' }}>{{ $state->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div style="float: right; width: 49%" class="form-group">
                            <label class="required" for="address">{{ __('customer.address') }} <label class="content-required">*</label></label>
                            <input class="form-control" type="text" name="address" id="address" value="{{ isset($customer->address) ? $customer->address : old('address') }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="required" for="note">{{ __('customer.note') }}</label>
                        <input class="form-control" type="text" name="note" id="note" value="{{ isset($customer->note) ? $customer->note : old('note') }}">
                    </div>
                    <div class="form-group">
                        <button class="btn btn-secondary" type="button" onclick="goBack()">{{ __('general.back') }}</button>
                        <button class="btn btn-danger" type="submit">{{ __('general.save') }}</button>
                    </div>
                </form>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul style="margin-bottom: 0px">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
    </div>
@stop
