@inject('request', 'Illuminate\Http\Request')
@extends('layout.master')
@section('content')
    <div class="content">
        <h1 class="titleheader">Quản Lý Phiếu Chi</h1>
        <div class="row">
            @if(session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            @if(session('fail'))
                <div class="alert alert-danger">
                    {{ session('fail') }}
                </div>
            @endif
        </div>
        <p>
            <a href="{{ route('admin.expenditures.create') }}" class="btn btn-success">{{ __('general.create') }}</a>
        </p>
        <form action="{{ route('admin.expenditures.search') }}" method="GET">
            @csrf
            <div class="input-group mb-3">

                <input type="text" class="form-control" placeholder="Tìm kiếm" name="name" {{$request->name ? $request->name : ''}}>
                <div class="input-group-append">
                    <button class="fa fa-search" type="submit"></button>
                </div>

            </div>
        </form>
        <div class="panel panel-default">
            <div class="panel-heading">

            </div>

            <div class="panel-body table-responsive">
                <table class="table table-bordered table-striped">
                    <thead >
                    <tr class="table_header">
                        <th class="thstyleform">STT</th>
                        <th class="thstyleform">Tổng tiền</th>
                        <th class="thstyleform">Mô tả</th>

                        <th class="thstyleform">&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($expenditure as $i)
                        <tr>
                            <td class="thstyleform">{{$i->id}}</td>
                            <td class="thstyleform">{{ $i->price }}</td>
                            <td class="thstyleform">{{ $i->description }}</td>
                            {{--                            style="text-overflow: ellipsis;width: 300px;height: 51px;overflow: hidden;white-space: nowrap;"--}}
                            {{--                            <td>{{ strip_tags(html_entity_decode($type->description)) }}</td>--}}

                            <td class="thstyleform">
                                <button id="button-pay{{$i->id}}" value="{{$i->id}}"
                                        class="btn btn-xs btn-primary button-pay" data-toggle="modal"
                                        data-target="#exampleModal{{$i->id}}">Xuất phiếu
                                </button>
                                <div class="modal fade" id="exampleModal{{$i->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <form action="{{route('admin.cash_books.store')}}" method="post">
                                                <input name="id" hidden value="{{$i->id}}">
                                                @csrf
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Phiếu chi</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body dataModalform" id="dataModalform">
                                                    {{--lấy dữ liệu truyền vào đây--}}
                                                    <input id="id" name="id"  type="hidden" value="{{$i->id}}">
                                                    <input id="id_staff" name="id_staff"  type="hidden" value="{{$i->id_staff}}">
                                                    <div class="row">
                                                        <div class="form-group col-md-12">
                                                            <div class="col-xs-12 form-group">
                                                            <p>Công ty HiTech Thiên Quân</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-row">
                                                        <div class="form-group col-md-6">
                                                            <div class="col-xs-12 form-group">

                                                                <p type="text" id="staff_name"  name="staff_name"
                                                                   value=""> <label>Nhân viên: </label>{{$i->name}}</p>
                                                                <p class="help-block text-danger"></p>

                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <div class="col-xs-12 form-group">

                                                                <p  id="price"  name="price" >
                                                                    <label>Tổng tiền: </label>  {{$i->price}}</p>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-md-6">
                                                            <div class="col-xs-12 form-group">

                                                                <p  id="description"  name="description" >
                                                                    <label>Lý do :</label>  {{$i->description}}</p>
                                                                <p class="help-block text-danger"></p>

                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-md-6">
                                                            <div class="col-xs-12 form-group">

                                                                <p id="total"
                                                                   name="total">
                                                                    <label>Ngày xuất phiếu : </label>  {{date('d/m/Y ')}}</p>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Quay lại</button>
                                                    <button type="submit" class="btn btn-primary">Xuất phiếu</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
{{--                                @can('')--}}
{{--                                    <a href="{{route('admin.expenditures.show', $i->id)}}"--}}
{{--                                       class="btn btn-xs btn-info">{{ __('general.view') }}</a>--}}
{{--                                @endcan--}}
{{--                                @can('')--}}
                                    <a href="{{route('admin.expenditures.edit', [$i->id])}}"
                                       class="btn btn-xs btn-success">{{ __('general.edit') }}</a>
{{--                                @endcan--}}
{{--                                @can('')--}}
                                    <form style="display: inline-block" action="{{route('admin.expenditures.destroy', [$i->id])}}"
                                          method="post" onsubmit="return confirm('Bạn có chắc chắn muốn xóa?');">
                                        @csrf
                                        <button type="submit" class="btn btn-xs btn-danger">{{ __('general.delete') }}</button>
                                    </form>
{{--                                @endcan--}}
                            </td>

                        </tr>
                    @empty
                        <tr>
                            <td colspan="9" class="text-center">{{ __('general.nodata') }}</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
                <div class="text-right">
                    {{ $expenditure->links() }}
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $('.mdb-select').materialSelect();
        });


        $(document).ready(function() {
            $('.js-example-basic-single').select2();
        });



        $('.myModal').on('shown.bs.modal', function () {
            $('.myInput').trigger('focus')
        });


    </script>

@stop
