<div class="btn-group dropleft">
    <button type="button" class="btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        {{ __('general.function') }}
    </button>
    <div class="dropdown-menu">
        <div class="dropdown-item">
            <button type="button" class="btn btn-sm btn-info" data-toggle="modal" data-target="#viewSWModal{{ $serviceUse->id }}">
                {{ __('general.view') }}
            </button>
            <a href="{{ route('admin.services-use.update-register-softwares').'?id='.$serviceUse->id }}" class="btn btn-sm btn-success">{{ __('general.update') }}</a>
            <button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#deleteSWItemModal{{ $serviceUse->id }}">
                {{ __('general.delete') }}
            </button>
            <a href="{{ route('admin.services-use.update-register-softwares').'?id='.$serviceUse->id.'&type=extend' }}" class="btn btn-sm btn-secondary">{{ __('general.extend') }}</a>
        </div>
    </div>
</div>

<div class="modal fade" id="viewSWModal{{ $serviceUse->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">{{ __('service-use.info') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-striped table-hover">
                    <tbody>
                    <tr>
                        <th>{{ __('register-software.code') }}</th>
                        <td>{{ $serviceUse->code }}</td>
                    </tr>
                    <tr>
                        <th>{{ __('register-software.customer') }}</th>
                        <td>{{ $serviceUse->first_name.' '.$serviceUse->last_name }}</td>
                    </tr>
                    <tr>
                        <th>{{ __('register-software.email') }}</th>
                        <td>{{ $serviceUse->customer_email }}</td>
                    </tr>
                    <tr>
                        <th>{{ __('register-software.software') }}</th>
                        <td>{{ $serviceUse->software_name }}</td>
                    </tr>
                    <tr>
                        <th>{{ __('register-software.domain-address') }}</th>
                        <td>{{ $serviceUse->address_domain }}</td>
                    </tr>
                    <tr>
                        <th>{{ __('register-software.price') }}</th>
                        <td>{{ $serviceUse->price }}</td>
                    </tr>
                    <tr>
                        <th>{{ __('register-software.start-date') }}</th>
                        <td>{{ $serviceUse->start_date }}</td>
                    </tr>
                    <tr>
                        <th>{{ __('register-software.end-date') }}</th>
                        <td>{{ $serviceUse->end_date }}</td>
                    </tr>
                    <tr>
                        <th>{{ __('register-software.note') }}</th>
                        <td>{{ $serviceUse->notes }}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('general.close') }}</button>
            </div>
        </div>
    </div>
</div>

<form action="{{ route('admin.register-softwares.destroy') }}" method="POST">
    @csrf
    <div class="modal fade" id="deleteSWItemModal{{ $serviceUse->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{ __('service-use.info') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="text" name="id" value="{{ $serviceUse->id }}" style="display: none">
                    <p>{{ __('general.confirm-delete') }}</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('general.close') }}</button>
                    <button type="submit" class="btn btn-danger">{{ __('general.delete') }}</button>
                </div>
            </div>
        </div>
    </div>
</form>
