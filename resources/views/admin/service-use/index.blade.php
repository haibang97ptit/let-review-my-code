@extends('layout.master')
@section('css')
    <link rel="stylesheet" href="{{ asset('../css/default.css') }}">
@endsection
@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @can('customer-view')
        <div class="body-content">

            <div class="clearfix">
                @if(session('success'))
                    <div class="alert-crud alert alert-success" style="margin-bottom: 15px">
                        {{ session('success') }}
                    </div>
                @endif
                @if(session('fail'))
                    <div class="alert-crud alert alert-danger" style="margin-bottom: 15px">
                        {{ session('fail') }}
                    </div>
                @endif
            </div>

            <div class="card">
                <div class="card-header card-header-new">
                    {{ __('service-use.list') }}
                </div>
                <div class="card-body">
                    @include('admin.service-use.header')
                    @include('admin.service-use.body')
                </div>
            </div>
        </div>
    @endcan

    <script type="text/javascript">
        $(document).ready(function() {

            $("#check-all").click(function () {
                $('input:checkbox').not(this).prop('checked', this.checked);
            });

            $("#btn-delete").on('click', function () {
                var allVals = [];
                $(".btn-check").not("#check-all").each(function() {
                    if ($(this).is(":checked")) {
                        allVals.push($(this).val());
                    }
                });
                // console.log(allVals);
                $('#allValsDelete').val(allVals);
            });

            $(document).on('click', '.pagination a', function (event) {
                event.preventDefault();
                var amountRow = $("#showRow :selected").val();
                var type = "{{ isset($_GET['type']) ? $_GET['type'] : "" }}";
                var url = $(this).attr('href') + '&amount=' + amountRow + "&type=" + type;
                // var url = $(this).attr('href') + '&amount=' + amountRow;
                window.location.assign(url);
            });

            $("#showRow").on('change', function () {
                var amountRow = $("#showRow :selected").val();
                var type = "{{ isset($_GET['type']) ? $_GET['type'] : "" }}";
                var url = '{{ URL::current().'?amount=:amount&type=:type' }}';
                url = url.replace(':amount', amountRow);
                url = url.replace(':type', type);
                url = url.replace('&amp;', '&');
                // console.log(url);
                window.location.assign(url);
            });

            $("#search").on('keyup', function () {
                var key = $(this).val();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                    }
                });
                $.ajax({
                    type: 'POST',
                    url: '{{ route('admin.services-use.filter-search-row') }}',
                    data: {
                        key: key,
                        amount: {{ isset($_GET['amount']) ? $_GET['amount'] : 'null' }},
                        type: "{{ isset($_GET['type']) ? $_GET['type'] : 'null' }}",
                    },
                    success: function (response) {
                        console.log(response);
                        $("#service-use-table").empty();
                        $("#service-use-table").html(response);
                    },
                });
            });

        });
    </script>
@stop

