<div class="btn-group dropleft">
    <button type="button" class="btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        {{ __('general.function') }}
    </button>
    <div class="dropdown-menu">
        <div class="dropdown-item">
            <button type="button" class="btn btn-sm btn-info" data-toggle="modal" data-target="#viewSModal{{ $serviceUse->id }}">
                {{ __('general.view') }}
            </button>
            <a href="{{ route('admin.register-services.update-register-services', $serviceUse->id) }}" class="btn btn-sm btn-success">{{ __('general.update') }}</a>
            <button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#deleteSItemModal{{ $serviceUse->id }}">
                {{ __('general.delete') }}
            </button>
            <a href="{{ route('admin.register-services.update-register-services', $serviceUse->id).'?type=extend' }}" class="btn btn-sm btn-secondary">{{ __('general.extend') }}</a>
        </div>
    </div>
</div>

<div class="modal fade" id="viewSModal{{ $serviceUse->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">{{ __('service-use.info') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-striped table-hover">
                    <tbody>
                    <tr>
                        <th>{{ __('register-service.code') }}</th>
                        <td>{{ $serviceUse->code }}</td>
                    </tr>
                    <tr>
                        <th>{{ __('register-service.customer') }}</th>
                        <td>{{ $serviceUse->first_name.' '.$serviceUse->last_name }}</td>
                    </tr>
                    <tr>
                        <th>{{ __('register-service.email') }}</th>
                        <td>{{ $serviceUse->customer_email }}</td>
                    </tr>
                    <tr>
                        <th>{{ __('register-service.service') }}</th>
                        <td>{{ $serviceUse->type_service }}</td>
                    </tr>
                    <tr>
                        <th>{{ __('register-service.domain-address') }}</th>
                        <td>{{ $serviceUse->address_domain }}</td>
                    </tr>
                    <tr>
                        <th>{{ __('register-service.price') }}</th>
                        <td>{{ $serviceUse->price }}</td>
                    </tr>
                    <tr>
                        <th>{{ __('register-service.start-date') }}</th>
                        <td>{{ date('d/m/Y', strtotime($serviceUse->start_date)) }}</td>
                    </tr>
                    <tr>
                        <th>{{ __('register-service.end-date') }}</th>
                        <td>{{ date('d/m/Y', strtotime($serviceUse->end_date)) }}</td>
                    </tr>
                    <tr>
                        <th>{{ __('register-service.note') }}</th>
                        <td>{{ $serviceUse->notes }}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('general.close') }}</button>
            </div>
        </div>
    </div>
</div>

<form action="{{ route('admin.register-services.destroy') }}" method="POST">
    @csrf
    <div class="modal fade" id="deleteSItemModal{{ $serviceUse->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{ __('service-use.info') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="text" name="id" value="{{ $serviceUse->id }}" style="display: none">
                    <p>{{ __('general.confirm-delete') }}</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('general.close') }}</button>
                    <button type="submit" class="btn btn-danger">{{ __('general.delete') }}</button>
                </div>
            </div>
        </div>
    </div>
</form>
