<div id="service-use-table">
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
            <tr>
                <th class="align-middle" style="width: 10px"><input type="checkbox" id="check-all"></th>
                <th class="align-middle">{{ __('service-use.id') }}</th>
                <th class="align-middle">{{ __('service-use.customer') }}</th>
                <th class="align-middle">
                    <p>{{ __('service-use.service-software') }}</p>
                    <p class="small-p">{{ __('service-use.domain-address') }}</p>
                </th>
                <th class="align-middle">
                    <p>{{ __('service-use.start-date') }}</p>
                    <p class="small-p">{{ __('service-use.end-date') }}</p>
                </th>
                <th class="align-middle">{{ __('service-use.status') }}</th>
                <th class="align-middle"></th>
            </tr>
            </thead>
            <tbody>
            @forelse($serviceUses as $item => $serviceUse)
                <tr >
                    <td class="align-middle"><input type="checkbox" class="btn-check" value="{{ $serviceUse->id }}"></td>
                    <td class="align-middle">{{ $serviceUses->firstItem() + $item }}</td>
                    <td class="align-middle">
                        <p>{{ $serviceUse->first_name.' '.$serviceUse->last_name }}</p>
                        <p class="small-p">{{ $serviceUse->customer_email }}</p>
                    </td>
                    <td class="align-middle">
                        <p>{{ isset($serviceUse->type_service) ?  $serviceUse->type_service : $serviceUse->software_name}}</p>
                        <p class="small-p">{{ $serviceUse->address_domain }}</p>
                    </td>
                    <td class="align-middle">
                        <p>{{ $serviceUse->start_date == "" ? "" : date('Y-m-d H:i', strtotime($serviceUse->start_date)) }}</p>
                        <p class="small-p">{{ $serviceUse->end_date == "" ? "" : date('Y-m-d H:i', strtotime($serviceUse->end_date) )}}</p>
                    </td>
                    <td class="align-middle">
                        @php
                            /** @var TYPE_NAME $serviceUse */
                            $now = \Carbon\Carbon::now('+07:00');
                            $resultCompare = true;
                            if ($serviceUse->end_date != "") {
                                $endDate = \Carbon\Carbon::parse($serviceUse->end_date);
                                $resultCompare = $endDate->greaterThanOrEqualTo($now);
                            }
                        @endphp
                        @if($resultCompare == true)
                            <span class="badge badge-primary">{{ __('service-use.active') }}</span>
                        @elseif($resultCompare == false)
                            <span class="badge badge-danger">{{ __('service-use.not-active') }}</span>
                        @else
                            <span class="badge badge-primary">{{ __('service-use.active') }}</span>
                        @endif
                    </td>
                    <td class="align-middle">
                        @if($serviceUse->type == 'software')
                            @include('admin.service-use.function-software')
                        @else
                            @include('admin.service-use.function-service')
                        @endif
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="7" class="text-center">{{ __('general.nodata') }}</td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>
    <div class="clearfix">
        <div style="float: right">
            {!! $serviceUses->links() !!}
        </div>
    </div>
</div>
