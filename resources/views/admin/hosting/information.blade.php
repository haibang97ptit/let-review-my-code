<div style="width: 120%">
    <table>
        <tr>
            <td>{{ __('hosting.price') }}</td>
            <td class="td-software">{{ $hosting->price }}</td>
        </tr>
        <tr>
            <td>{{ __('hosting.capacity') }}</td>
            <td class="td-software">{{ $hosting->capacity }}</td>
        </tr>
        <tr>
            <td>{{ __('hosting.bandwidth') }}</td>
            <td class="td-software">{{ $hosting->bandwith }}</td>
        </tr>
        <tr>
            <td>{{ __('hosting.sub-domain') }}</td>
            <td class="td-software">{{ $hosting->sub_domain }}</td>
        </tr>
        <tr>
            <td>{{ __('hosting.email') }}</td>
            <td class="td-software">{{ $hosting->email }}</td>
        </tr>
        <tr>
            <td>{{ __('hosting.ftp') }}</td>
            <td class="td-software">{{ $hosting->ftp }}</td>
        </tr>
        <tr>
            <td>{{ __('hosting.database') }}</td>
            <td class="td-software">{{ $hosting->database }}</td>
        </tr>
        <tr>
            <td>{{ __('hosting.adddon-domain') }}</td>
            <td class="td-software">{{ $hosting->adddon_domain }}</td>
        </tr>
        <tr>
            <td>{{ __('hosting.park-domain') }}</td>
            <td class="td-software">{{ $hosting->park_domain }}</td>
        </tr>
    </table>
</div>

