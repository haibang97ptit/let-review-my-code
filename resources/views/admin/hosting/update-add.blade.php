@extends('layout.master')
@section('css')
    <link rel="stylesheet" href="{{ asset('../css/default.css') }}">
@endsection
@section('content')
    <div class="body-content">
        <div class="card">
            <div class="card-header card-header-new">
                @if(isset($hosting->id))
                    {{ __('hosting.update') }}
                @else
                    {{ __('hosting.create') }}
                @endif
            </div>

            <div class="card-body">
                <form method="POST" action="{{ route("admin.hostings.store") }}" enctype="multipart/form-data">
                    @csrf
                    <input style="display: none" name="id" value="{{ isset($hosting->id) ? $hosting->id : old('id') }}">
                    <div class="clearfix">
                        <div class="form-group" style="float: left; width: 49%">
                            <label class="required" for="name">{{ __('hosting.name') }} <span class="content-required">*</span></label>
                            <input class="form-control" type="text" name="name" id="name" value="{{ isset($hosting->name) ? $hosting->name : old('name') }}">
                        </div>
                        <div class="form-group" style="float: right; width: 49%">
                            <label class="required" for="price">{{ __('hosting.price') }} <span class="content-required">*</span></label>
                            <input class="form-control" type="number" name="price" id="price" value="{{ isset($hosting->price) ? $hosting->price : old('price') }}">
                        </div>
                    </div>
                    <div class="clearfix">
                        <div class="form-group" style="float: left; width: 49%">
                            <label class="required" for="capacity">{{ __('hosting.capacity') }} <span class="content-required">*</span></label>
                            <input class="form-control" type="text" name="capacity" id="capacity" value="{{ isset($hosting->capacity) ? $hosting->capacity : old('capacity') }}">
                        </div>
                        <div class="form-group" style="float: right; width: 49%">
                            <label class="required" for="bandwith">{{ __('hosting.bandwidth') }} <span class="content-required">*</span></label>
                            <input class="form-control" type="text" name="bandwith" id="bandwith" value="{{ isset($hosting->bandwith) ? $hosting->bandwith : old('bandwith') }}">
                        </div>
                    </div>
                    <div class="clearfix">
                        <div class="form-group" style="float: left; width: 49%">
                            <label class="required" for="sub_domain">{{ __('hosting.sub-domain') }} <span class="content-required">*</span></label>
                            <input class="form-control" type="text" name="sub_domain" id="sub_domain" value="{{ isset($hosting->sub_domain) ? $hosting->sub_domain : old('sub_domain') }}">
                        </div>
                        <div class="form-group" style="float: right; width: 49%">
                            <label class="required" for="email">{{ __('hosting.email') }} <span class="content-required">*</span></label>
                            <input class="form-control" type="text" name="email" id="email" value="{{ isset($hosting->email) ? $hosting->email : old('email') }}">
                        </div>
                    </div>
                    <div class="clearfix">
                        <div class="form-group" style="float: left; width: 49%">
                            <label class="required" for="ftp">{{ __('hosting.ftp') }} <span class="content-required">*</span></label>
                            <input class="form-control" type="text" name="ftp" id="ftp" value="{{ isset($hosting->ftp) ? $hosting->ftp : old('ftp') }}">
                        </div>
                        <div class="form-group" style="float: right; width: 49%">
                            <label class="required" for="database">{{ __('hosting.database') }} <span class="content-required">*</span></label>
                            <input class="form-control" type="number" name="database" id="database" value="{{ isset($hosting->database) ? $hosting->database : old('database') }}">
                        </div>
                    </div>
                    <div class="clearfix">
                        <div class="form-group" style="float: left; width: 49%">
                            <label class="required" for="adddon_domain">{{ __('hosting.adddon-domain') }} <span class="content-required">*</span></label>
                            <input class="form-control" type="number" name="adddon_domain" id="adddon_domain" value="{{ isset($hosting->adddon_domain) ? $hosting->adddon_domain : old('adddon_domain') }}">
                        </div>
                        <div class="form-group" style="float: right; width: 49%">
                            <label class="required" for="park_domain">{{ __('hosting.park-domain') }} <span class="content-required">*</span></label>
                            <input class="form-control" type="text" name="park_domain" id="park_domain" value="{{ isset($hosting->park_domain) ? $hosting->park_domain : old('park_domain') }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="required" for="notes">{{ __('hosting.note') }}</label>
                        <input class="form-control" type="text" name="notes" id="notes" value="{{ isset($hosting->notes) ? $hosting->notes : old('notes') }}">
                    </div>
                    <div class="form-group">
                        <button class="btn btn-secondary" type="button" onclick="goBack()">{{ __('general.back') }}</button>
                        <button class="btn btn-danger" type="submit">{{ __('general.save') }}</button>
                    </div>
                </form>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul style="margin-bottom: 0px">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
    </div>
@stop
