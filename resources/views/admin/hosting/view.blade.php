<table class="table table-bordered table-striped table-hover" style="text-align: left">
    <tr>
        <td>{{ __('hosting.name') }}</td>
        <td class="td-software">{{ $hosting->name }}</td>
    </tr>
    <tr>
        <td>{{ __('hosting.price') }}</td>
        <td class="td-software">{{ $hosting->price }}</td>
    </tr>
    <tr>
        <td>{{ __('hosting.capacity') }}</td>
        <td class="td-software">{{ $hosting->capacity }}</td>
    </tr>
    <tr>
        <td>{{ __('hosting.bandwidth') }}</td>
        <td class="td-software">{{ $hosting->bandwith }}</td>
    </tr>
    <tr>
        <td>{{ __('hosting.sub-domain') }}</td>
        <td class="td-software">{{ $hosting->sub_domain }}</td>
    </tr>
    <tr>
        <td>{{ __('hosting.email') }}</td>
        <td class="td-software">{{ $hosting->email }}</td>
    </tr>
    <tr>
        <td>{{ __('hosting.ftp') }}</td>
        <td class="td-software">{{ $hosting->ftp }}</td>
    </tr>
    <tr>
        <td>{{ __('hosting.database') }}</td>
        <td class="td-software">{{ $hosting->database }}</td>
    </tr>
    <tr>
        <td>{{ __('hosting.adddon-domain') }}</td>
        <td class="td-software">{{ $hosting->adddon_domain }}</td>
    </tr>
    <tr>
        <td>{{ __('hosting.park-domain') }}</td>
        <td class="td-software">{{ $hosting->park_domain }}</td>
    </tr>
    <tr>
        <td>{{ __('hosting.note') }}</td>
        <td class="td-software">{{ $hosting->notes }}</td>
    </tr>
</table>
