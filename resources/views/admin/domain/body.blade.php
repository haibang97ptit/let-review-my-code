<div class="card">
    <div class="card-header">
        <div style="float: right">
            <button type="button" id="check-all" class="btn btn-xs btn-primary">{{ __('general.select-all') }}</button>
            <button type="button" id="cancel-check-all" class="btn btn-xs btn-secondary">{{ __('general.cancel-select-all') }}</button>
        </div>
    </div>
    <div class="card-body">
        <div id="domain-card">
            @include('admin.domain.card')
        </div>
    </div>
</div>
