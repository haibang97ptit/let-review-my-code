@extends('layout.master')
@section('css')
    <link rel="stylesheet" href="{{ asset('../css/default.css') }}">
@endsection
@section('content')
    <div class="body-content">
        <div class="card">
            <div class="card-header card-header-new">
                @if(isset($domain->id))
                    {{ __('domain.update') }}
                @else
                    {{ __('domain.create') }}
                @endif
            </div>

            <div class="card-body">
                <form method="POST" action="{{ route("admin.domains.store") }}" enctype="multipart/form-data">
                    @csrf
                    <input style="display: none" name="id" value="{{ isset($domain->id) ? $domain->id : old('id') }}">
                    <div class="clearfix">
                        <div class="form-group" style="float: left; width: 49%">
                            <label class="required" for="name">{{ __('domain.name') }} <span class="content-required">*</span></label>
                            <input class="form-control" type="text" name="name" id="name" value="{{ isset($domain->name) ? $domain->name : old('name') }}">
                        </div>
                        <div class="form-group" style="float: right; width: 49%">
                            <label class="required" for="fee_register">{{ __('domain.fee-register') }} <span class="content-required">*</span></label>
                            <input class="form-control" type="number" name="fee_register" id="fee_register" value="{{ isset($domain->fee_register) ? $domain->fee_register : old('fee_register') }}">
                        </div>
                    </div>
                    <div class="clearfix">
                        <div class="form-group" style="float: left; width: 49%">
                            <label class="required" for="fee_remain">{{ __('domain.fee-remain') }} <span class="content-required">*</span></label>
                            <input class="form-control" type="number" name="fee_remain" id="fee_remain" value="{{ isset($domain->fee_remain) ? $domain->fee_remain : old('fee_remain') }}">
                        </div>
                        <div class="form-group" style="float: right; width: 49%">
                            <label class="required" for="fee_transformation">{{ __('domain.fee-transformation') }} <span class="content-required">*</span></label>
                            <input class="form-control" type="number" name="fee_transformation" id="fee_transformation" value="{{ isset($domain->fee_transformation) ? $domain->fee_transformation : old('fee_transformation') }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="required" for="notes">{{ __('domain.note') }}</label>
                        <input class="form-control" type="text" name="notes" id="notes" value="{{ isset($domain->notes) ? $domain->notes : old('notes') }}">
                    </div>
                    <div class="form-group">
                        <button class="btn btn-secondary" type="button" onclick="goBack()">{{ __('general.back') }}</button>
                        <button class="btn btn-danger" type="submit">{{ __('general.save') }}</button>
                    </div>
                </form>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul style="margin-bottom: 0px">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
    </div>
@stop
