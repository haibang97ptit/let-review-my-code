<table class="table table-bordered table-striped table-hover" style="text-align: left">
    <tr>
        <td>{{ __('domain.name') }}</td>
        <td class="td-software">{{ $domain->name }}</td>
    </tr>
    <tr>
        <td>{{ __('domain.fee-register') }}</td>
        <td class="td-software">{{ $domain->fee_register }}</td>
    </tr>
    <tr>
        <td>{{ __('domain.fee-remain') }}</td>
        <td class="td-software">{{ $domain->fee_register }}</td>
    </tr>
    <tr>
        <td>{{ __('domain.fee-transformation') }}</td>
        <td class="td-software">{{ $domain->fee_transformation }}</td>
    </tr>
    <tr>
        <td>{{ __('domain.note') }}</td>
        <td class="td-software">{{ $domain->notes }}</td>
    </tr>
</table>
