<button type="button" class="btn btn-sm btn-info" data-toggle="modal" data-target="#viewSWModal{{ $cashBook->id }}">
    {{ __('general.view') }}
</button>
<button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#deleteSWItemModal{{ $cashBook->id }}">
    {{ __('general.delete') }}
</button>

<div class="modal fade" id="viewSWModal{{ $cashBook->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">{{ __('cash-book.cash-book') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-striped table-hover">
                    <tbody>
                    <tr>
                        <th>{{ __('register-software.code') }}</th>
                        <td>{{ $cashBook->code }}</td>
                    </tr>
                    <tr>
                        <th>{{ __('register-software.customer') }}</th>
                        <td>{{ $cashBook->first_name.' '.$cashBook->last_name }}</td>
                    </tr>
                    <tr>
                        <th>{{ __('register-software.email') }}</th>
                        <td>{{ $cashBook->customer_email }}</td>
                    </tr>
                    <tr>
                        <th>{{ __('register-software.software') }}</th>
                        <td>{{ $cashBook->software_name }}</td>
                    </tr>
                    <tr>
                        <th>{{ __('register-software.domain-address') }}</th>
                        <td>{{ $cashBook->address_domain }}</td>
                    </tr>
                    <tr>
                        <th>{{ __('register-software.price') }}</th>
                        <td>{{ $cashBook->price }}</td>
                    </tr>
                    <tr>
                        <th>{{ __('register-software.start-date') }}</th>
                        <td>{{ $cashBook->start_date }}</td>
                    </tr>
                    <tr>
                        <th>{{ __('register-software.end-date') }}</th>
                        <td>{{ $cashBook->end_date }}</td>
                    </tr>
                    <tr>
                        <th>{{ __('register-software.note') }}</th>
                        <td>{{ $cashBook->notes }}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('general.close') }}</button>
            </div>
        </div>
    </div>
</div>

<form action="{{ route('admin.cash-books.destroy') }}" method="POST">
    @csrf
    <div class="modal fade" id="deleteSWItemModal{{ $cashBook->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{ __('cash-book.cash-book') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="text" name="id" value="{{ $cashBook->id }}" style="display: none">
                    <p>{{ __('general.confirm-delete') }}</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('general.close') }}</button>
                    <button type="submit" class="btn btn-danger">{{ __('general.delete') }}</button>
                </div>
            </div>
        </div>
    </div>
</form>
