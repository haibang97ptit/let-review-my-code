<div id="cash-book-table">
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
            <tr>
                <th class="align-middle" style="width: 10px"><input type="checkbox" id="check-all"></th>
                <th class="align-middle">{{ __('cash-book.id') }}</th>
                <th class="align-middle">{{ __('cash-book.code') }}</th>
                <th class="align-middle"></th>
            </tr>
            </thead>
            <tbody>
            @forelse($cashBooks as $item => $cashBook)
                <tr >
                    <td class="align-middle"><input type="checkbox" class="btn-check" value="{{ $cashBook->id }}"></td>
                    <td class="align-middle">{{ $cashBooks->firstItem() + $item }}</td>
                    <td class="align-middle">{{ $cashBook->code }}</td>
                    <td class="align-middle">
                        @if($cashBook->type == 'software')
                            @include('admin.cash-book.function-software')
                        @else
                            @include('admin.cash-book.function-service')
                        @endif
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="4" class="text-center">{{ __('general.nodata') }}</td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>
    <div class="clearfix">
        <div style="float: right">
            {!! $cashBooks->links() !!}
        </div>
    </div>
</div>
