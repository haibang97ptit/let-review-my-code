@extends('layout.master')
@section('css')
    <link rel="stylesheet" href="{{ asset('../css/default.css') }}">
@endsection
@section('content')
    <div class="body-content">
        <div class="card">
            <div class="card-header card-header-new">
                @if(isset($website->id))
                    {{ __('website.update') }}
                @else
                    {{ __('website.create') }}
                @endif
            </div>

            <div class="card-body">
                <form method="POST" action="{{ route("admin.websites.store") }}" enctype="multipart/form-data">
                    @csrf
                    <input style="display: none" name="id" value="{{ isset($website->id) ? $website->id : old('id') }}">
                    <div class="clearfix">
                        <div class="form-group" style="float: left; width: 49%">
                            <label class="required" for="name">{{ __('website.name') }} <span class="content-required">*</span></label>
                            <input class="form-control" type="text" name="name" id="name" value="{{ isset($website->name) ? $website->name : old('name') }}">
                        </div>
                        <div class="form-group" style="float: right; width: 49%">
                            <label class="required" for="price">{{ __('website.price') }} <span class="content-required">*</span></label>
                            <input class="form-control" type="number" name="price" id="price" value="{{ isset($website->price) ? $website->price : old('price') }}">
                        </div>
                    </div>
                    <div class="clearfix">
                        <div class="form-group" style="float: left; width: 49%">
                            <label class="required" for="type_website">{{ __('website.type-website') }} <span class="content-required">*</span></label>
                            <input class="form-control" type="text" name="type_website" id="type_website" value="{{ isset($website->type_website) ? $website->type_website : old('type_website') }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="required" for="notes">{{ __('website.note') }}</label>
                        <input class="form-control" type="text" name="notes" id="notes" value="{{ isset($website->notes) ? $website->notes : old('notes') }}">
                    </div>
                    <div class="form-group">
                        <button class="btn btn-secondary" type="button" onclick="goBack()">{{ __('general.back') }}</button>
                        <button class="btn btn-danger" type="submit">{{ __('general.save') }}</button>
                    </div>
                </form>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul style="margin-bottom: 0px">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
    </div>
@stop
