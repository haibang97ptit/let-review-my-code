<table class="table table-bordered table-striped table-hover" style="text-align: left">
    <tr>
        <td>{{ __('website.name') }}</td>
        <td class="td-software">{{ $website->name }}</td>
    </tr>
    <tr>
        <td>{{ __('website.type-website') }}</td>
        <td class="td-software">{{ $website->type_website }}</td>
    </tr>
    <tr>
        <td>{{ __('website.price') }}</td>
        <td class="td-software">{{ $website->price }}</td>
    </tr>
    <tr>
        <td>{{ __('website.note') }}</td>
        <td class="td-software">{{ $website->notes }}</td>
    </tr>
</table>
