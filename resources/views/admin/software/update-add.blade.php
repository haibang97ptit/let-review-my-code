@extends('layout.master')
@section('css')
    <link rel="stylesheet" href="{{ asset('../css/default.css') }}">
@endsection
@section('content')
    <div class="body-content">
        <div class="card">
            <div class="card-header card-header-new">
                {{ __('software.create') }}
            </div>

            <div class="card-body">
                <form method="POST" action="{{ route("admin.softwares.store") }}" enctype="multipart/form-data">
                    @csrf
                    <input style="display: none" name="id" value="{{ isset($software->id) ? $software->id : old('id') }}">
                    <div class="form-group">
                        <label class="required" for="name">{{ __('software.name') }} <span class="content-required">*</span></label>
                        <input class="form-control" type="text" name="name" id="name" value="{{ isset($software->name) ? $software->name : old('name') }}">
                    </div>
                    <div class="form-group">
                        <label class="required" for="description">{{ __('software.description') }} <span class="content-required">*</span></label>
                        <input class="form-control" type="text" name="description" id="description" value="{{ isset($software->description) ? $software->description : old('description') }}">
                    </div>
                    <div class="form-group">
                        <button class="btn btn-secondary" type="button" onclick="goBack()">{{ __('general.back') }}</button>
                        <button class="btn btn-danger" type="submit">{{ __('general.save') }}</button>
                    </div>
                </form>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul style="margin-bottom: 0px">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
    </div>
@stop
