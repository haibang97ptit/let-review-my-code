<div class="clearfix">
    <div style="float: left">
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <label class="input-group-text" for="showRow">{{ __('general.show') }}</label>
            </div>
            <select class="custom-select" id="showRow">
                <option value="10" {{ isset($_GET['amount']) ? ($_GET['amount'] === '10' ? 'selected' : '') : '' }}>10</option>
                <option value="25" {{ isset($_GET['amount']) ? ($_GET['amount'] === '25' ? 'selected' : '') : '' }}>25</option>
                <option value="50" {{ isset($_GET['amount']) ? ($_GET['amount'] === '50' ? 'selected' : '') : '' }}>50</option>
                <option value="100" {{ isset($_GET['amount']) ? ($_GET['amount'] === '100' ? 'selected' : '') : '' }}>100</option>
            </select>
            @can('position-delete')
                <button id="btn-delete" style="margin-left: 5px" type="button" class="delete btn btn-danger" data-toggle="modal" data-target="#deleteModal">
                    {{ __('general.delete') }}
                </button>
                <form action="{{ route('admin.softwares.trash-force-select') }}" method="POST">
                    @csrf
                    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">{{ __('software.software') }}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <input type="text" class="allVals" name="allVals[]" style="display: none">
                                    <p style="text-align: left; margin-bottom: 0px">{{ __('general.confirm-delete') }}</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('general.close') }}</button>
                                    <button type="submit" class="btn btn-danger">{{ __('general.delete') }}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            @endcan
            <button id="btn-restore" style="margin-left: 5px" type="button" class="btn btn-success" data-toggle="modal" data-target="#restoreModal">
                {{ __('general.restore') }}
            </button>
            <form action="{{ route('admin.softwares.trash-restore-select') }}" method="POST">
                @csrf
                <div class="modal fade" id="restoreModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">{{ __('software.software') }}</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <input type="text" class="allVals" name="allVals[]" style="display: none">
                                <p class="text-confirm">{{ __('general.confirm-restore') }}</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('general.close') }}</button>
                                <button type="submit" class="btn btn-success">{{ __('general.restore') }}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="clearfix" style="float: right">
        <div style="float: left">
            <a style="margin-right: 5px" class="btn btn-secondary" href="{{ route('admin.softwares.index') }}" role="button"><i class="fa fa-chevron-left"></i></a>
        </div>
        @can('position-search')
            <div style="float: right">
                <input id="search" class="form-control" type="search" placeholder="{{ __('general.search') }}" value="{{ isset($_GET['key']) ? $_GET['key'] : '' }}">
            </div>
        @endcan
    </div>
</div>
