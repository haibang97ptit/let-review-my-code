@extends('layout.master')
@section('css')
    <link rel="stylesheet" href="{{ asset('../css/default.css') }}">
@endsection
@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @can('customer-view')
        <div class="body-content">
            <div class="clearfix" style="margin-bottom: 15px">
                @if(session('success'))
                    <div class="alert-crud alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif
                @if(session('fail'))
                    <div class="alert-crud alert alert-danger">
                        {{ session('fail') }}
                    </div>
                @endif
                @can('customer-create')
                    <div style="float: left">
                        <a href="{{ route('admin.vpses.create') }}" class="btn btn-primary">{{ __('general.create') }}</a>
                    </div>
                @endcan
            </div>

            @include('admin.vps.header')
            @include('admin.vps.body')
        </div>
    @endcan

    <script type="text/javascript">
        $(document).ready(function() {

            $("#check-all").click(function () {
                $(".card-body-vps").css('background-color', 'rgba(0,0,0,0.1)');
                $(".btn-check").prop('checked', true);
            });

            $("#cancel-check-all").click(function () {
                $(".card-body-vps").css('background-color', '#fafafa');
                $(".btn-check").prop('checked', false);
            });

            $("#btn-delete").on('click', function () {
                var allVals = [];
                $(".btn-check").each(function() {
                    if ($(this).is(":checked")) {
                        allVals.push($(this).val());
                    }
                });
                // console.log(allVals);
                $('#allValsDelete').val(allVals);
            });

            $("#search").on('keyup', function () {
                var key = $(this).val();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                    }
                });
                $.ajax({
                    type: 'POST',
                    url: '{{ route('admin.vpses.search-row') }}',
                    data: {
                        key: key,
                        amount: {{ isset($_GET['amount']) ? $_GET['amount'] : 'null' }}
                    },
                    success: function (response) {
                        $("#vps-card").empty();
                        $("#vps-card").html(response);
                    },
                });
            });

            $(document).on('click', '.pagination a', function (event) {
                event.preventDefault();
                var amountRow = $("#showRow :selected").val();
                var key = $("#search").val();
                var url = $(this).attr('href') + '&amount=' + amountRow + '&key=' + key;
                window.location.assign(url);
            });

            $("#showRow").on('change', function () {
                var amountRow = $("#showRow :selected").val();
                var url = '{{ route('admin.vpses.index').'?amount=:amount' }}';
                url = url.replace(':amount', amountRow);
                window.location.assign(url);
            });

        });
    </script>
@stop

