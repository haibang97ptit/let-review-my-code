<div style="width: 200%">
    <table>
        <tr>
            <td>{{ __('vps.price') }}</td>
            <td class="td-software">{{ $vps->price }}</td>
        </tr>
        <tr>
            <td>{{ __('vps.capacity') }}</td>
            <td class="td-software">{{ $vps->capacity }}</td>
        </tr>
        <tr>
            <td>{{ __('vps.bandwidth') }}</td>
            <td class="td-software">{{ $vps->bandwith }}</td>
        </tr>
        <tr>
            <td>{{ __('vps.cpu') }}</td>
            <td class="td-software">{{ $vps->cpu }}</td>
        </tr>
        <tr>
            <td>{{ __('vps.ram') }}</td>
            <td class="td-software">{{ $vps->ram }}</td>
        </tr>
        <tr>
            <td>{{ __('vps.technical') }}</td>
            <td class="td-software">{{ $vps->technical }}</td>
        </tr>
        <tr>
            <td>{{ __('vps.operating-system') }}</td>
            <td class="td-software">{{ $vps->operating_system }}</td>
        </tr>
    </table>
</div>

