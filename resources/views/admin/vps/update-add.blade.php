@extends('layout.master')
@section('css')
    <link rel="stylesheet" href="{{ asset('../css/default.css') }}">
@endsection
@section('content')
    <div class="body-content">
        <div class="card">
            <div class="card-header card-header-new">
                @if(isset($vps->id))
                    {{ __('vps.update') }}
                @else
                    {{ __('vps.create') }}
                @endif
            </div>

            <div class="card-body">
                <form method="POST" action="{{ route("admin.vpses.store") }}" enctype="multipart/form-data">
                    @csrf
                    <input style="display: none" name="id" value="{{ isset($vps->id) ? $vps->id : old('id') }}">
                    <div class="clearfix">
                        <div class="form-group" style="float: left; width: 49%">
                            <label class="required" for="name">{{ __('vps.name') }} <span class="content-required">*</span></label>
                            <input class="form-control" type="text" name="name" id="name" value="{{ isset($vps->name) ? $vps->name : old('name') }}">
                        </div>
                        <div class="form-group" style="float: right; width: 49%">
                            <label class="required" for="price">{{ __('vps.price') }} <span class="content-required">*</span></label>
                            <input class="form-control" type="number" name="price" id="price" value="{{ isset($vps->price) ? $vps->price : old('price') }}">
                        </div>
                    </div>
                    <div class="clearfix">
                        <div class="form-group" style="float: left; width: 49%">
                            <label class="required" for="cpu">{{ __('vps.cpu') }} <span class="content-required">*</span></label>
                            <input class="form-control" type="text" name="cpu" id="cpu" value="{{ isset($vps->cpu) ? $vps->cpu : old('cpu') }}">
                        </div>
                        <div class="form-group" style="float: right; width: 49%">
                            <label class="required" for="capacity">{{ __('vps.capacity') }} <span class="content-required">*</span></label>
                            <input class="form-control" type="text" name="capacity" id="capacity" value="{{ isset($vps->capacity) ? $vps->capacity : old('capacity') }}">
                        </div>
                    </div>
                    <div class="clearfix">
                        <div class="form-group" style="float: left; width: 49%">
                            <label class="required" for="ram">{{ __('vps.ram') }} <span class="content-required">*</span></label>
                            <input class="form-control" type="text" name="ram" id="ram" value="{{ isset($vps->ram) ? $vps->ram : old('ram') }}">
                        </div>
                        <div class="form-group" style="float: right; width: 49%">
                            <label class="required" for="bandwith">{{ __('vps.bandwidth') }} <span class="content-required">*</span></label>
                            <input class="form-control" type="text" name="bandwith" id="bandwith" value="{{ isset($vps->bandwith) ? $vps->bandwith : old('bandwith') }}">
                        </div>
                    </div>
                    <div class="clearfix">
                        <div class="form-group" style="float: left; width: 49%">
                            <label class="required" for="technical">{{ __('vps.technical') }} <span class="content-required">*</span></label>
                            <input class="form-control" type="text" name="technical" id="technical" value="{{ isset($vps->technical) ? $vps->technical : old('technical') }}">
                        </div>
                        <div class="form-group" style="float: right; width: 49%">
                            <label class="required" for="operating_system">{{ __('vps.operating-system') }} <span class="content-required">*</span></label>
                            <input class="form-control" type="text" name="operating_system" id="operating_system" value="{{ isset($vps->operating_system) ? $vps->operating_system : old('operating_system') }}">
                        </div>
                    </div>
                    <div class="clearfix">
                        <div class="form-group" style="float: left; width: 49%">
                            <label class="required" for="backup">{{ __('vps.backup') }} <span class="content-required">*</span></label>
                            <input class="form-control" type="text" name="backup" id="backup" value="{{ isset($vps->backup) ? $vps->backup : old('backup') }}">
                        </div>
                        <div class="form-group" style="float: right; width: 49%">
                            <label class="required" for="panel">{{ __('vps.panel') }} <span class="content-required">*</span></label>
                            <input class="form-control" type="text" name="panel" id="panel" value="{{ isset($vps->panel) ? $vps->panel : old('panel') }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="required" for="notes">{{ __('vps.note') }}</label>
                        <input class="form-control" type="text" name="notes" id="notes" value="{{ isset($vps->notes) ? $vps->notes : old('notes') }}">
                    </div>
                    <div class="form-group">
                        <button class="btn btn-secondary" type="button" onclick="goBack()">{{ __('general.back') }}</button>
                        <button class="btn btn-danger" type="submit">{{ __('general.save') }}</button>
                    </div>
                </form>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul style="margin-bottom: 0px">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
    </div>
@stop
