<div class="card">
    <div class="card-header card-header-new">
        {{ __('vps.list') }}
    </div>
    <div class="card-body">
        <div class="clearfix">
            <div style="float: left">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <label class="input-group-text" for="showRow">{{ __('general.show') }}</label>
                    </div>
                    <select class="custom-select" id="showRow">
                        <option value="4" {{ isset($_GET['amount']) ? ($_GET['amount'] === '4' ? 'selected' : '') : '' }}>4</option>
                        <option value="10" {{ isset($_GET['amount']) ? ($_GET['amount'] === '10' ? 'selected' : '') : '' }}>10</option>
                        <option value="25" {{ isset($_GET['amount']) ? ($_GET['amount'] === '25' ? 'selected' : '') : '' }}>25</option>
                        <option value="50" {{ isset($_GET['amount']) ? ($_GET['amount'] === '50' ? 'selected' : '') : '' }}>50</option>
                        <option value="100" {{ isset($_GET['amount']) ? ($_GET['amount'] === '100' ? 'selected' : '') : '' }}>100</option>
                    </select>
                    @can('customer-delete')
                        <button id="btn-delete" style="margin-left: 5px" type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteModal">
                            {{ __('general.delete') }}
                        </button>
                        <form action="{{ route('admin.vpses.destroy-select') }}" method="POST">
                            @csrf
                            <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">{{ __('vps.vps') }}</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <input type="text" id="allValsDelete" name="allValsDelete[]" style="display: none">
                                            <p class="text-confirm">{{ __('general.confirm-delete') }}</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('general.close') }}</button>
                                            <button type="submit" class="btn btn-danger">{{ __('general.delete') }}</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    @endcan
                </div>
            </div>
            <div class="clearfix" style="float: right">
{{--                <div style="float: left">--}}
{{--                    <a style="margin-right: 5px" class="btn btn-warning" href="{{ route('admin.software-packages.trash-index') }}" role="button"><i class="fa fa-trash"></i></a>--}}
{{--                </div>--}}
                @can('customer-search')
                    <div style="float: right">
                        <input id="search" class="form-control" type="search" placeholder="{{ __('general.search') }}" value="{{ isset($_GET['key']) ? $_GET['key'] : '' }}">
                    </div>
                @endcan
            </div>
        </div>
    </div>
</div>
