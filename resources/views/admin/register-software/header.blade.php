<div class="clearfix">
    <div style="float: left">
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <label class="input-group-text" for="showRow">{{ __('general.show') }}</label>
            </div>
            <select class="custom-select" id="showRow">
                <option value="10" {{ isset($_GET['amount']) ? ($_GET['amount'] === '10' ? 'selected' : '') : '' }}>10</option>
                <option value="25" {{ isset($_GET['amount']) ? ($_GET['amount'] === '25' ? 'selected' : '') : '' }}>25</option>
                <option value="50" {{ isset($_GET['amount']) ? ($_GET['amount'] === '50' ? 'selected' : '') : '' }}>50</option>
                <option value="100" {{ isset($_GET['amount']) ? ($_GET['amount'] === '100' ? 'selected' : '') : '' }}>100</option>
            </select>
            @can('customer-delete')
                <button id="btn-delete" style="margin-left: 5px" type="button" class="delete btn btn-danger" data-toggle="modal" data-target="#deleteModal">
                    {{ __('general.delete') }}
                </button>
                <form action="{{ route('admin.register-softwares.destroy-select') }}" method="POST">
                    @csrf
                    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">{{ __('register-software.software') }}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <input type="text" id="allValsDelete" name="allValsDelete[]" style="display: none">
                                    <p class="text-confirm">{{ __('general.confirm-delete') }}</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('general.close') }}</button>
                                    <button type="submit" class="btn btn-danger">{{ __('general.delete') }}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            @endcan
        </div>
    </div>

    <div class="clearfix" style="float: right">
        <div style="float: left">
            <a style="margin-right: 5px" class="btn btn-warning" href="{{ route('admin.register-softwares.trash-index') }}" role="button"><i class="fa fa-trash"></i></a>
        </div>
        @can('customer-search')
            <button class="btn btn-info" type="button" data-toggle="collapse" data-target="#collapseSearch" aria-expanded="false" aria-controls="collapseSearch">
                {{ __('general.search') }}
            </button>
        @endcan
    </div>
</div>
<form action="{{ route('admin.register-softwares.search-row') }}" method="POST">
    @csrf
    <input style="display: none" name="amount" value="{{ isset($_GET['amount']) ? $_GET['amount'] : null }}">
    <div class="collapse" id="collapseSearch">
        <div class="card card-body">
            <div class="clearfix">
                <div style="float: left; width: 49%">
                    <div style="float: left; width: 48%">
                        <select class="form-control" id="customer-select" name="customer_select">
                            <option></option>
                            @foreach($customers as $customer)
                                <option value="{{ $customer->id }}">{{ $customer->first_name.' '.$customer->last_name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div style="float: right; width: 48%">
                        <select class="form-control" id="software-select" name="software_select">
                            <option></option>
                            @foreach($softwares as $software)
                                <option value="{{ $software->id }}">{{ $software->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="clearfix" style="float: right; width: 49%">
                    <div style="float: left; width: 48%">
                        <select class="form-control" id="version-select" name="version_select">
                            <option></option>
                            <option value="official">{{ __('register-software.official') }}</option>
                            <option value="trial">{{ __('register-software.trial') }}</option>
                        </select>
                    </div>
                    <div style="float: right; width: 48%">
                        <select class="form-control" id="status-select" name="status_select">
                            <option></option>
                            <option value="paid">{{ __('register-software.paid') }}</option>
                            <option value="unpaid">{{ __('register-software.unpaid') }}</option>
                        </select>
                    </div>
                </div>
            </div>
            <div style="margin-top: 15px">
                <button style="float: right" type="submit" class="btn btn-danger">Xác nhận</button>
            </div>
        </div>
    </div>
</form>
