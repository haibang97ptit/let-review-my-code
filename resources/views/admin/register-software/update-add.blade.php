@extends('layout.master')
@section('css')
    <link rel="stylesheet" href="{{ asset('../css/default.css') }}">
@endsection
@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <div class="body-content">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                @if(isset($_GET['type']))
                    <li class="breadcrumb-item active" aria-current="page"><h2>{{ __('register-software.extend') }}</h2></li>
                @elseif(isset($registerSoftware->id))
                    <li class="breadcrumb-item active" aria-current="page"><h2>{{ __('register-software.update') }}</h2></li>
                @else
                    <li class="breadcrumb-item active" aria-current="page"><h2>{{ __('register-software.register-software') }}</h2></li>
                @endif
            </ol>
        </nav>

        <div class="card">
            <div class="card-body">
                <form method="POST" action="{{ route("admin.register-softwares.store") }}" enctype="multipart/form-data">
                    @csrf
                    <input value="{{ isset($registerSoftware->id) ? $registerSoftware->id : null }}" name="id" style="display: none" {{ isset($_GET['type']) ? 'disabled' : '' }}>
                    <input value="{{ isset($registerSoftware->end_date) ? $registerSoftware->end_date : null }}" name="end_date" style="display: none" {{ !isset($_GET['type']) ? 'disabled' : '' }}>
                    <div class="clearfix">
                        <div class="form-group left-col">
                            <label>{{ __('register-software.customer') }} <span class="content-required">*</span></label>
                            <select class="form-control" id="customer-select" name="id_customer">
                                <option></option>
                                @foreach($customers as $customer)
                                    <option value="{{ $customer->id }}" {{ (isset($registerSoftware->id_customer) && $registerSoftware->id_customer == $customer->id) ? "selected" : "" }}>{{ $customer->first_name.' '.$customer->last_name.' - '.$customer->email }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group right-col">
                            <label for="address-domain">{{ __('register-software.domain-address') }} <span class="content-required">*</span></label>
                            <input type="text" class="form-control" id="address-domain" name="address_domain" value="{{ isset($registerSoftware->address_domain) ? $registerSoftware->address_domain : old('address_domain') }}">
                        </div>
                    </div>

                    <div class="clearfix">
                        <div class="form-group left-col">
                            <label>{{ __('register-software.software') }} <span class="content-required">*</span></label>
                            <select class="form-control" id="software-select" name="id_software">
                                <option></option>
                                @foreach($softwares as $software)
                                    <option value="{{ $software->id }}" {{ (isset($registerSoftware->id_software) && $registerSoftware->id_software == $software->id) ? "selected" : "" }}>{{ $software->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group right-col">
                            <label>{{ __('register-software.software-package') }} <span class="content-required">*</span></label>
                            <select class="form-control" id="software-package-select" name="id_software_package">
                                <option></option>
                                @foreach($softwarePackages as $softwarePackage)
                                    <option value="{{ $softwarePackage->id }}" {{ (isset($registerSoftware->id_software_package) && $registerSoftware->id_software_package == $softwarePackage->id) ? "selected" : "" }}>{{ $softwarePackage->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="clearfix">
                        <div class="form-group left-col">
                            <label>{{ __('register-software.version') }} <span class="content-required">*</span></label>
                            <select class="form-control" id="version" name="version">
                                <option value="trial" {{ (isset($registerSoftware->version) && $registerSoftware->version == "trial") ? "selected" : "" }}>{{ __('register-software.trial') }}</option>
                                <option value="official" {{ (isset($registerSoftware->version) && $registerSoftware->version == "official") ? "selected" : "" }}>{{ __('register-software.official') }}</option>
                            </select>
                        </div>
                        <div class="form-group right-col">
                            <label>{{ __('register-software.start-date') }} <span class="content-required">*</span></label>
                            @if(isset($_GET['type']))
                                <input class="form-control" type="date" name="start_date" value="{{ isset($registerSoftware->start_date) ? \Carbon\Carbon::parse($registerSoftware->start_date)->format('Y-m-d') : old('start_date') }}" disabled>
                                <input class="form-control" type="text" name="start_date" value="{{ isset($registerSoftware->start_date) ? $registerSoftware->start_date : old('start_date') }}" style="display: none">
                            @else
                                <input class="form-control" type="date" name="start_date" value="{{ isset($registerSoftware->start_date) ? \Carbon\Carbon::parse($registerSoftware->start_date)->format('Y-m-d') : old('start_date') }}">
                            @endif
                        </div>
                    </div>

                    <div class="clearfix" id="version-table" style="display: none">
                        <div class="form-group left-col">
                            <label>{{ __('register-software.time-use') }} <span class="content-required">*</span></label>
                            <select class="form-control" name="time_use">
                                <option value="12" {{ (isset($registerSoftware->time_use) && $registerSoftware->time_use == "12") ? "selected" : "" }}>12 {{ __('register-software.months') }}</option>
                                <option value="24" {{ (isset($registerSoftware->time_use) && $registerSoftware->time_use == "24") ? "selected" : "" }}>24 {{ __('register-software.months') }}</option>
                                <option value="36" {{ (isset($registerSoftware->time_use) && $registerSoftware->time_use == "36") ? "selected" : "" }}>36 {{ __('register-software.months') }}</option>
                                <option value="48" {{ (isset($registerSoftware->time_use) && $registerSoftware->time_use == "48") ? "selected" : "" }}>48 {{ __('register-software.months') }}</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-row" style="margin-bottom: 20px">
                        <div class="form-group col-md-12">
                            <label>{{ __('register-software.note') }}</label>
                            <textarea rows="5" type="text" class="form-control" name="notes" value="{{ isset($registerSoftware->notes) ? $registerSoftware->notes : old('notes') }}"></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        @if(isset($_GET['type']))
                            <button class="btn btn-secondary" type="button" onclick="goBack()">{{ __('general.back') }}</button>
                            <button class="btn btn-danger" type="submit">{{ __('general.extend') }}</button>
                        @elseif(isset($registerSoftware->id))
                            <button class="btn btn-secondary" type="button" onclick="goBack()">{{ __('general.back') }}</button>
                            <button class="btn btn-danger" type="submit">{{ __('general.save') }}</button>
                        @else
                            <button class="btn btn-danger" type="submit">{{ __('general.register') }}</button>
                        @endif
                    </div>
                </form>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul style="margin-bottom: 0px">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function() {

            $("#customer-select").select2({
                placeholder: "{{ __('register-software.choose-customer') }}",
                allowClear: true,
                width: "100%",
            });

            $("#software-select").select2({
                placeholder: "{{ __('register-software.choose-software') }}",
                allowClear: true,
                width: "100%",
            });

            $("#software-package-select").select2({
                placeholder: "{{ __('register-software.choose-software-package') }}",
                allowClear: true,
                width: "100%",
            });

            $("#version").on('change', function () {
                var version = $("#version :selected").val();
                if (version == "official") {
                    $("#version-table").css('display', 'inline');
                }
                else {
                    $("#version-table").css('display', 'none');
                }
            });

            var version = $("#version :selected").val();
            if (version == "official") {
                $("#version-table").css('display', 'inline');
            }

        });
    </script>

@stop

