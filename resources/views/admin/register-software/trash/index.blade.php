@extends('layout.master')
@section('css')
    <link rel="stylesheet" href="{{ asset('../css/default.css') }}">
@endsection
@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @can('customer-view')
        <div class="body-content">

            <div class="clearfix">
                @if(session('success'))
                    <div class="alert-crud alert alert-success" style="margin-bottom: 15px">
                        {{ session('success') }}
                    </div>
                @endif
                @if(session('fail'))
                    <div class="alert-crud alert alert-danger" style="margin-bottom: 15px">
                        {{ session('fail') }}
                    </div>
                @endif
            </div>

            <div class="card">
                <div class="card-header card-header-new">
                    {{ __('register-software.list') }}
                </div>
                <div class="card-body">
                    @include('admin.register-software.trash.header')
                    @include('admin.register-software.trash.body')
                </div>
            </div>
        </div>
    @endcan

    <script type="text/javascript">
        $(document).ready(function() {

            $("#customer-select").select2({
                placeholder: "{{ __('register-software.choose-customer') }}",
                allowClear: true
            });

            $("#version-select").select2({
                placeholder: "{{ __('register-software.choose-version') }}",
                allowClear: true
            });

            $("#status-select").select2({
                placeholder: "{{ __('register-software.choose-status') }}",
                allowClear: true
            });

            $("#software-select").select2({
                placeholder: "{{ __('register-software.choose-software') }}",
                allowClear: true
            });

            $("#check-all").click(function () {
                $('input:checkbox').not(this).prop('checked', this.checked);
            });

            $("#btn-delete").on('click', function () {
                var allVals = [];
                $(".btn-check").not("#check-all").each(function() {
                    if ($(this).is(":checked")) {
                        allVals.push($(this).val());
                    }
                });
                // console.log(allVals);
                $('.allVals').val(allVals);
            });

            $("#btn-restore").on('click', function () {
                var allVals = [];
                $(".btn-check").not("#check-all").each(function() {
                    if ($(this).is(":checked")) {
                        allVals.push($(this).val());
                    }
                });
                // console.log(allVals);
                $('.allVals').val(allVals);
            });

            $(document).on('click', '.pagination a', function (event) {
                event.preventDefault();
                var amountRow = $("#showRow :selected").val();
                var key = $("#search").val();
                var url = $(this).attr('href') + '&amount=' + amountRow;
                window.location.assign(url);
            });

            $("#showRow").on('change', function () {
                var amountRow = $("#showRow :selected").val();
                var url = '{{ route('admin.register-softwares.trash-index').'?amount=:amount' }}';
                url = url.replace(':amount', amountRow);
                window.location.assign(url);
            });

        });
    </script>
@stop

