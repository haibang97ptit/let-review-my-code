<div class="table-responsive">
    <table class="table table-bordered table-striped table-hover">
        <thead>
        <tr>
            <th class="align-middle" style="width: 10px"><input type="checkbox" id="check-all"></th>
            <th class="align-middle">{{ __('register-software.id') }}</th>
            <th class="align-middle">{{ __('register-software.customer') }}</th>
            <th class="align-middle">
                <p>{{ __('register-software.software') }}</p>
                <p class="small-p">{{ __('register-software.domain-address') }}</p>
            </th>
            <th class="align-middle">
                <p>{{ __('register-software.start-date') }}</p>
                <p class="small-p">{{ __('register-software.end-date') }}</p>
            </th>
            <th class="align-middle">{{ __('register-software.status-register') }}</th>
            <th class="align-middle"></th>
        </tr>
        </thead>
        <tbody>
        @forelse($registerSoftwares as $item => $registerSoftware)
            <tr >
                <td class="align-middle"><input type="checkbox" class="btn-check" value="{{ $registerSoftware->id }}"></td>
                <td class="align-middle">{{ $registerSoftwares->firstItem() + $item }}</td>
                <td class="align-middle">
                    <p>{{ $registerSoftware->first_name.' '.$registerSoftware->last_name }}</p>
                    <p class="small-p">{{ $registerSoftware->customer_email }}</p>
                </td>
                <td class="align-middle">
                    <p>{{ $registerSoftware->software_name }}</p>
                    <p class="small-p">{{ $registerSoftware->address_domain }}</p>
                </td>

                <td class="align-middle">
                    <p>{{ $registerSoftware->start_date == "" ? "" : date('Y-m-d H:i', strtotime($registerSoftware->start_date)) }}</p>
                    <p class="small-p">{{ $registerSoftware->end_date == "" ? "" : date('Y-m-d H:i', strtotime($registerSoftware->end_date) )}}</p>
                </td>
                <td class="align-middle">
                    <input class="status-toggle" {{ $registerSoftware->status_register == "paid" ? "checked" : "" }} data-id="{{ $registerSoftware->id }}" type="checkbox" data-size="mini" data-width="100%" data-height="20%" data-onstyle="success" data-offstyle="warning" data-toggle="toggle" data-on="{{ __('general.paid') }}" data-off="{{ __('general.unpaid') }}">
                </td>
                <td class="align-middle">
                    @include('admin.register-software.trash.function')
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="8" class="text-center">{{ __('general.nodata') }}</td>
            </tr>
        @endforelse
        </tbody>
    </table>
</div>
<div class="clearfix">
    <div style="float: right">
        {!! $registerSoftwares->links() !!}
    </div>
</div>
