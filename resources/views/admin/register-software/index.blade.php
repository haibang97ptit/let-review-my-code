@extends('layout.master')
@section('css')
    <link rel="stylesheet" href="{{ asset('../css/default.css') }}">
@endsection
@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @can('customer-view')
        <div class="body-content">

            <div class="clearfix">
                @if(session('success'))
                    <div class="alert-crud alert alert-success" style="margin-bottom: 15px">
                        {{ session('success') }}
                    </div>
                @endif
                @if(session('fail'))
                    <div class="alert-crud alert alert-danger" style="margin-bottom: 15px">
                        {{ session('fail') }}
                    </div>
                @endif
            </div>

            <div class="card">
                <div class="card-header card-header-new">
                    {{ __('register-software.list') }}
                </div>
                <div class="card-body">
                    @include('admin.register-software.header')
                    @include('admin.register-software.body')
                </div>
            </div>
        </div>
    @endcan

    <script type="text/javascript">
        $(document).ready(function() {

            $("#customer-select").select2({
                placeholder: "{{ __('register-software.choose-customer') }}",
                allowClear: true
            });

            $("#version-select").select2({
                placeholder: "{{ __('register-software.choose-version') }}",
                allowClear: true
            });

            $("#status-select").select2({
                placeholder: "{{ __('register-software.choose-status') }}",
                allowClear: true
            });

            $("#software-select").select2({
                placeholder: "{{ __('register-software.choose-software') }}",
                allowClear: true
            });

            $("#check-all").click(function () {
                $('input:checkbox').not(this).prop('checked', this.checked);
            });

            $("#btn-delete").on('click', function () {
                var allVals = [];
                $(".btn-check").not("#check-all").each(function() {
                    if ($(this).is(":checked")) {
                        allVals.push($(this).val());
                    }
                });
                // console.log(allVals);
                $('#allValsDelete').val(allVals);
            });

            $(document).on('click', '.pagination a', function (event) {
                event.preventDefault();
                var amountRow = $("#showRow :selected").val();
                var key = $("#search").val();
                var url = $(this).attr('href') + '&amount=' + amountRow;
                window.location.assign(url);
            });

            $("#showRow").on('change', function () {
                var amountRow = $("#showRow :selected").val();
                var url = '{{ route('admin.register-softwares.index').'?amount=:amount' }}';
                url = url.replace(':amount', amountRow);
                window.location.assign(url);
            });

            $(".status-toggle").each(function () {
                $(this).on('change', function () {
                    var id = $(this).data("id");
                    if ($(this).is(":checked")) {
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                            }
                        });
                        $.ajax({
                            type: 'POST',
                            url: '{{ route('admin.register-softwares.change-status') }}',
                            data: {
                                id: id,
                                status: "paid",
                            },
                            success: function (response) {
                                alertify.success('{{ __('general.paid-success') }}');
                                console.log(response);
                            },
                        });
                    }
                    else {
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                            }
                        });
                        $.ajax({
                            type: 'POST',
                            url: '{{ route('admin.register-softwares.change-status') }}',
                            data: {
                                id: id,
                                status: "unpaid",
                            },
                            success: function (response) {
                                alertify.warning('{{ __('general.unpaid-success') }}');
                                console.log(response);
                            },
                        });
                    }
                });
            });

        });
    </script>
@stop

