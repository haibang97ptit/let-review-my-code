<button type="button" class="btn btn-xs btn-info" data-toggle="modal" data-target="#viewModal{{ $registerSoftware->id }}">
    {{ __('general.view') }}
</button>

<a href="{{ route('admin.register-softwares.update').'?id='.$registerSoftware->id }}" class="btn btn-xs btn-success">{{ __('general.update') }}</a>

<button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#deleteItemModal{{ $registerSoftware->id }}">
    {{ __('general.delete') }}
</button>

<div class="modal fade" id="viewModal{{ $registerSoftware->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">{{ __('register-software.info') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-striped table-hover">
                    <tbody>
                    <tr>
                        <th>{{ __('register-software.code') }}</th>
                        <td>{{ $registerSoftware->code }}</td>
                    </tr>
                    <tr>
                        <th>{{ __('register-software.customer') }}</th>
                        <td>{{ $registerSoftware->first_name.' '.$registerSoftware->last_name }}</td>
                    </tr>
                    <tr>
                        <th>{{ __('register-software.email') }}</th>
                        <td>{{ $registerSoftware->customer_email }}</td>
                    </tr>
                    <tr>
                        <th>{{ __('register-software.software') }}</th>
                        <td>{{ $registerSoftware->software_name }}</td>
                    </tr>
                    <tr>
                        <th>{{ __('register-software.domain-address') }}</th>
                        <td>{{ $registerSoftware->address_domain }}</td>
                    </tr>
                    <tr>
                        <th>{{ __('register-software.price') }}</th>
                        <td>{{ $registerSoftware->price }}</td>
                    </tr>
                    <tr>
                        <th>{{ __('register-software.start-date') }}</th>
                        <td>{{ $registerSoftware->start_date }}</td>
                    </tr>
                    <tr>
                        <th>{{ __('register-software.end-date') }}</th>
                        <td>{{ $registerSoftware->end_date }}</td>
                    </tr>
                    <tr>
                        <th>{{ __('register-software.version') }}</th>
                        @if($registerSoftware->version == "official")
                            <td>{{ __('register-software.official') }}</td>
                        @else
                            <td>{{ __('register-software.trial') }}</td>
                        @endif
                    </tr>
                    <tr>
                        <th>{{ __('register-software.note') }}</th>
                        <td>{{ $registerSoftware->notes }}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('general.close') }}</button>
            </div>
        </div>
    </div>
</div>

<form action="{{ route('admin.register-softwares.destroy') }}" method="POST">
    @csrf
    <div class="modal fade" id="deleteItemModal{{ $registerSoftware->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{ __('register-software.software') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="text" name="id" value="{{ $registerSoftware->id }}" style="display: none">
                    <p>{{ __('general.confirm-delete') }}</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('general.close') }}</button>
                    <button type="submit" class="btn btn-danger">{{ __('general.delete') }}</button>
                </div>
            </div>
        </div>
    </div>
</form>
