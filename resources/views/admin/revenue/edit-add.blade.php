@extends('layout.master')
@section('content')

    <!-- Main content -->
    <section class="content">
        <div class="">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h1 class="titleheader">Phiếu Thu</h1>
                            <form action="{{route('admin.revenues.store')}}" method="post">
                                <input type="hidden" name="id" value="{{isset($revenue->id) ? $revenue->id: ''}}">
                                @csrf
                                <div class="row">
                                    @if(session('success'))
                                        <div class="alert alert-success">
                                            {{session('success')}}
                                        </div>
                                    @endif
                                    @if(session('fail'))
                                        <div class="alert alert-danger">
                                            {{session('fail')}}
                                        </div>
                                    @endif
                                </div>

                                <div class="panel panel-default">
                                    {{--                                    <div class="panel-heading">--}}
                                    {{--                                        {{ __('general.create') }}--}}
                                    {{--                                    </div>--}}

                                    <div class="panel-body" style="font-family: Candara">
                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <div class="col-xs-6 form-group">
                                                    <label>Nhân viên <span class="aster">*</span></label>
                                                    <select class="js-example-basic-single form-control" name="id_staff">
                                                        @foreach($staff as $i)

                                                            <option value="{{$i->id}}" @if(@isset($revenue->id_staff))  selected @endif> {{$i->name}}-{{$i->email}}</option>
                                                        @endforeach
                                                    </select>
                                                    @if($errors->has('name'))
                                                        <p class="help-block text-danger">
                                                            {{ $errors->first('name') }}
                                                        </p>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <div class="col-xs-6 form-group">
                                                    <label>Số tiền </label>
                                                    <input type="text" class="form-control" name="price"
                                                           value="{{isset($revenue->price) ? old('name', $revenue->price) : old('price')}}">
                                                    <p class="help-block text-danger"></p>
                                                    @if($errors->has('name'))
                                                        <p class="help-block text-danger">
                                                            {{ $errors->first('name') }}
                                                        </p>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">

                                            <div class="form-group col-md-12">
                                                <div class="col-xs-12 form-group">
                                                    <label>Lí do </label>
                                                    <textarea  rows="5" type="text" class="form-control" name="description">
                                {{isset($revenue->description) ? old('notes', $revenue->description) : old('description')}}</textarea>
                                                </div>
                                            </div>

                                        </div>
                                        <hr>
                                        <button class="btn btn-info">{{ __('general.save') }}</button>
                                        <a href="{{ route('admin.revenues.index') }}"
                                           class="btn btn-default">{{ __('general.back') }}</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- /.card -->
                    </div>

                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>

    {{--    <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>--}}
    {{--    <script> CKEDITOR.replace('editor1'); </script>--}}

@stop
