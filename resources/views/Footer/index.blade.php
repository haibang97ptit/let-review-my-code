@inject('request', 'Illuminate\Http\Request')
@extends('layout.master')
@section('content')
    <div class="content">
        <h1 class="titleheader" >Quản Lý </h1>
        <div class="row">
            @if(session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            @if(session('fail'))
                <div class="alert alert-danger">
                    {{ session('fail') }}
                </div>
            @endif
        </div>
        <p>
            <a href="{{ route('admin.staffs.create') }}" class="btn btn-success">{{ __('general.create') }}</a>
        </p>
        {{--search--}}

        <div class="panel panel-default">
            <div class="card">
                <div class="accordion md-accordion accordion-blocks" id="accordion"
                     role="tablist"
                     aria-multiselectable="true" style="height: 40px">
                    <h5 class="panel-title">

                        <div id="color1" class=" panel-heading custom-panel-search" >

                            <!-- Card header -->
                            <div  role="tab" id="heading79">

                                <!--Options-->


                                <!-- Heading -->
                                <a class="collapsed bar-search" role="button" data-toggle="collapse" data-parent="#accordion"
                                   href="#addstudent" aria-expanded="false" aria-controls="schedule" style="font-size: 20px">
                                    <h5>
                                        <label class="registerservice" style="color: #177445;margin-top: 0.5%; margin-left: 1%">Tìm kiếm</label>

                                    </h5>
                                </a>

                            </div>

                            <!-- Card body -->

                        </div>

                    </h5>
                </div>

                <form method="get" class="search_frm" action="{{route('admin.staffs.search')}}">
                    @csrf

                    <div id="addstudent" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-mobile col-sm-6 col-md-6 form-group">
                                        <label>Nhân viên</label>
                                        <input autocomplete="off" placeholder="Nhân viên"
                                               type='text' name="name"
                                               class="form-control"
                                               value="{{$request->name ? $request->name : ''}}"/>
                                    </div>
                                    <div  class="col-mobile col-sm-6 col-md-6 form-group">
                                        <label>Điện thoại</label>
                                        <select class="js-example-basic-single form-control" name="phone_number">
                                            <option value="" disabled selected>Số điện thoại</option>
                                            @foreach($phones as $i)
                                                <option
                                                    value="{{$i}}">{{$i}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <div class="col-xs-6 form-group">
                                            <label>Email khách hàng</label>
                                            <select class="js-example-basic-single form-control" name="email">
                                                <option value="" disabled selected>Tìm kiếm theo email</option>
                                                @foreach($emails as $cus)
                                                    <option
                                                        value="{{$cus}}">{{$cus}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row" style="margin-bottom: 10px">
                                    <br>
                                    <button type="submit"
                                            class="btn btn-success search-schedule" >{{__('general.search')}}</button>

                                </div>



                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
        {{--end search--}}
        <div class="panel panel-default">
            <div class="panel-heading">

            </div>

            <div class="panel-body table-responsive">
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr  class="table_header">
                        <th class="thstyleform">ID</th>
                        <th class="thstyleform">Name</th>
                        <th class="thstyleform">Hình ảnh</th>
                        <th>&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($staffs as $staff)
                        <tr>
                            <td class="thstyleform">{{$staff->id}}</td>
                            <td class="thstyleform">{{$staff->name}}</td>
                            <td class="thstyleform">{{$staff->address}}</td>
                            <td class="thstyleform">{{$staff->email}}</td>
                            <td class="thstyleform">{{date('Y-m-d ',strtotime($staff->birthday))}}</td>
                            <td class="thstyleform">{{$staff->phone_number}}</td>

                            <td class="thstyleform">
                                <a href="{{ route('admin.staffs.show', $staff->id) }}" class="btn btn-xs btn-info">{{ __('general.view') }}</a>
                                <a href="{{ route('admin.staffs.edit', [$staff->id]) }}" class="btn btn-xs btn-success">{{ __('general.edit') }}</a>
                                <form style="display: inline-block" action="{{ route('admin.staffs.destroy', [$staff->id]) }}" method="post" onsubmit="return confirm('Bạn có chắc chắn muốn xóa?');">
                                    @csrf
                                    <button type="submit" class="btn btn-xs btn-danger">{{ __('general.delete') }}</button>
                                </form>
                            </td>

                        </tr>
                    @empty
                        <tr>
                            <td colspan="9" class="text-center">{{ __('general.nodata') }}</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
                <div class="text-right">
                    {{--                    hien thi phan trang--}}
                    {{ $staffs->links() }}
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $('.mdb-select').materialSelect();
        });
    </script>
    <script>
        // In your Javascript (external .js resource or <script> tag)
        $(document).ready(function() {
            $('.js-example-basic-single').select2();
        });

        // Reset search
        $(".btn_clear").click(function (e) {
            e.preventDefault();
            $('.search-dropdown-select2').each(function (index, value) {
                $(this).val('').trigger('change');
            });
            $('.search_frm').find('input[type=text], input[type=search], select, textarea').val('');
        });
    </script>
@stop
