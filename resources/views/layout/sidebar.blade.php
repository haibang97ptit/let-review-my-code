@inject('request', 'Illuminate\Http\Request')
<aside class="main-sidebar sidebar-dark-primary elevation-4 style-sidebar" style="">
    <!-- Brand Logo -->
    <a href="#" class="brand-link" style="background-color: #09203f !important;">
        <img src="{{ asset('dist/img/AdminLTELogo.png') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">Thiên Quân</span>
    </a>
    <a href="#" class="brand-link">
        <img src="{{ asset('dist/img/user1.jpg') }}" alt="User Image" class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">Admin</span>
    </a>
    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                @can('customer-access')
                    <li class="nav-background nav-item">
                        <a href="{{ route("admin.customers.index") }}" class="nav-link {{ $request->segment(2) == 'customers' ? 'active active-sub ' : '' }}">
                            <i class="nav-icon fa fa-child"></i>
                            <p>
                                {{ __('sidebar.customer') }}
                            </p>
                        </a>
                    </li>
                @endcan
                <li class="nav-background nav-item">
                    <a href="{{ route('admin.services-use.index') }}" class="nav-link {{ $request->segment(2) == 'services-use' ? 'active active-sub ' : '' }}">
                        <i class="nav-icon fa fa-cube"></i>
                        <p>
                            {{ __('sidebar.service-use') }}
                        </p>
                    </a>
                </li>
                @can('register-service-access')
                    <li class="nav-background nav-item">
                        <a href="{{ route("admin.register-services.create")}}" class="nav-link {{ ($request->segment(2) == 'register-services' && $request->segment(3) == 'create') ? 'active active-sub' : '' }}">
                            <i class="nav-icon fa fa-paper-plane" ></i>
                            <p>
                                {{ __('sidebar.register-service') }}
                            </p>
                        </a>
                    </li>
                @endcan
                @can('register-soft-access')
                    <li class="nav-background nav-item">
                        <a href="{{ route("admin.register-softwares.create") }}" class="nav-link {{ ($request->segment(2) == 'register-softwares' && $request->segment(3) == 'create') ? 'active active-sub' : '' }}">
                            <i class="nav-icon fas fa-laptop-medical"></i>
                            <p>
                                {{ __('sidebar.register-software') }}
                            </p>
                        </a>
                    </li>
                @endcan
                @can('invoice-access')
                    <li class="nav-background nav-item has-treeview {{ ($request->segment(2) == 'register-services' || $request->segment(2) == 'register-softwares') && $request->segment(3) != 'create' ? 'menu-open' : '' }}">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fa fa-cubes"></i>
                            <p>
                                {{ __('sidebar.order-management') }}
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{ route("admin.register-services.index") }}" class="nav-link {{ ($request->segment(2) == 'register-services' && $request->segment(3) != 'create') ? 'active active-sub' : '' }}">
                                    <i class="nav-icon fa fa-book"></i>
                                    <p>{{ __('sidebar.order-service') }}</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route("admin.register-softwares.index") }}" class="nav-link {{ ($request->segment(2) == 'register-softwares' && $request->segment(3) != 'create') ? 'active active-sub' : '' }}">
                                    <i class="nav-icon fas fa-arrow-down"></i>
                                    <p>{{ __('sidebar.order-software') }}</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                @endcan
                @can('cash-book-management-access')
                    <li class="nav-background nav-item has-treeview {{ ($request->segment(2) == 'cash_books' ||  $request->segment(2) == 'revenues' ||  $request->segment(2) == 'expenditures') ? 'menu-open' : '' }}">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-book-dead"></i>
                            <p>
                                {{ __('sidebar.cash-book-management') }}
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            @can('cash-book-access')
                                <li class="nav-item {{ $request->segment(2) == 'cash-books' ? 'active active-sub' : '' }}">
                                    <a href="{{ route("admin.cash-books.index") }}" class="nav-link {{ $request->segment(2) == 'cash_books' ? 'active active-sub' : '' }}">
                                        <i class="nav-icon fa fa-book"></i>
                                        <p>{{ __('sidebar.cash-book') }}</p>
                                    </a>
                                </li>
                            @endcan
{{--                            @can('revenue-access')--}}
{{--                                <li class="nav-item {{ $request->segment(2) == 'revenues' ? 'active active-sub' : '' }}">--}}
{{--                                    <a href="{{ route("admin.revenues.index") }}" class="nav-link {{ $request->segment(2) == 'revenues' ? 'active active-sub' : '' }}">--}}
{{--                                        <i class="nav-icon fas fa-arrow-down"></i>--}}
{{--                                        <p>{{ __('sidebar.revenues') }}</p>--}}
{{--                                    </a>--}}
{{--                                </li>--}}
{{--                            @endcan--}}
{{--                            @can('expenditure-access')--}}
{{--                                <li class="nav-item {{ $request->segment(2) == 'expenditures' ? 'active active-sub' : '' }}">--}}
{{--                                    <a href="{{ route("admin.expenditures.index") }}" class="nav-link {{ $request->segment(2) == 'expenditures' ? 'active active-sub' : '' }}">--}}
{{--                                        <i class="nav-icon fas fa-arrow-alt-circle-up"></i>--}}
{{--                                        <p>{{ __('sidebar.expenditures') }}</p>--}}
{{--                                    </a>--}}
{{--                                </li>--}}
{{--                            @endcan--}}
                        </ul>
                    </li>
                @endcan
                @can('service-management-access')
                    <li class="nav-background nav-item has-treeview {{ ($request->segment(2) == 'domains' ||  $request->segment(2) == 'hostings' ||  $request->segment(2) == 'vpss' ||  $request->segment(2) == 'emails' ||  $request->segment(2) == 'ssls' ||  $request->segment(2) == 'websites') ? 'menu-open' : '' }}">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fa fa-briefcase"></i>
                            <p>
                                {{ __('sidebar.service-management') }}
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview ">
                            @can('domain-access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.domains.index") }}" class="nav-link {{ $request->segment(2) == 'domains' ? 'active active-sub' : '' }}">
                                        <i class="nav-icon fab fa-battle-net"></i>
                                        <p>{{ __('sidebar.domain') }}</p>
                                    </a>
                                </li>
                            @endcan
                            @can('hosting-access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.hostings.index") }}" class="nav-link {{ $request->segment(2) == 'hostings' ? 'active active-sub' : '' }}">
                                        <i class="nav-icon fas fa-suitcase-rolling"></i>
                                        <p>{{ __('sidebar.hosting') }}</p>
                                    </a>
                                </li>
                            @endcan
                            @can('vps-access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.vpses.index") }}" class="nav-link {{ $request->segment(2) == 'vpses' ? 'active active-sub' : '' }}">
                                        <i class="nav-icon fab fa-vine"></i>
                                        <p>{{ __('sidebar.vps') }}</p>
                                    </a>
                                </li>
                            @endcan
                            @can('email-access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.emails.index") }}" class="nav-link {{ $request->segment(2) == 'emails' ? 'active active-sub' : '' }}">
                                        <i class="nav-icon fas fa-envelope-square"></i>
                                        <p>{{ __('sidebar.email') }}</p>
                                    </a>
                                </li>
                            @endcan
                            @can('ssl-access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.ssls.index") }}" class="nav-link {{ $request->segment(2) == 'ssls' ? 'active active-sub' : '' }}">
                                        <i class="nav-icon fab fa-expeditedssl"></i>
                                        <p>{{ __('sidebar.ssl') }}</p>
                                    </a>
                                </li>
                            @endcan
                            @can('website-access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.websites.index") }}" class="nav-link {{ $request->segment(2) == 'websites' ? 'active active-sub' : '' }}">
                                        <i class="nav-icon fab fa-accusoft"></i>
                                        <p>{{ __('sidebar.website') }}</p>
                                    </a>
                                </li>
                            @endcan
                        </ul>
                    </li>
                @endcan
                @can('software-management-access')
                    <li class="nav-background nav-item has-treeview {{ ($request->segment(2) == 'softwares' ||  $request->segment(2) == 'software-packages') ? 'menu-open' : '' }}">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-money-check"></i>
                            <p>
                                {{ __('sidebar.software-management') }}
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{ route("admin.softwares.index") }}" class="nav-link {{ $request->segment(2) == 'softwares' ? 'active active-sub' : '' }}">
                                        <i class="nav-icon fas fa-anchor"></i>
                                        <p>{{ __('sidebar.software') }}</p>
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a href="{{ route("admin.software-packages.index") }}" class="nav-link {{ $request->segment(2) == 'software-packages' ? 'active active-sub' : '' }}">
                                        <i class="nav-icon fa fa-gift"></i>
                                        <p>{{ __('sidebar.software-package') }}</p>
                                    </a>
                                </li>
                        </ul>
                    </li>
                @endcan
                @can('staff-management-access')
                    <li class="nav-background nav-item has-treeview  {{ ($request->segment(2) == 'staffs' ||  $request->segment(2) == 'positions' ||  $request->segment(2) == 'departments') ? 'menu-open' : '' }}">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-user-tie"></i>
                            <p>
                                {{ __('sidebar.staff-management') }}
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview ">
                            @can('staff-access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.staffs.index") }}" class="nav-link {{ $request->segment(2) == 'staffs' ? 'active active-sub' : '' }}">
                                        <i class="nav-icon fas fa-user-friends"></i>
                                        <p>{{ __('sidebar.staff') }}</p>
                                    </a>
                                </li>
                            @endcan
                            @can('position-access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.positions.index") }}" class="nav-link {{ $request->segment(2) == 'positions' ? 'active active-sub' : '' }}">
                                        <i class="nav-icon fas fa-users-cog"></i>
                                        <p>{{ __('sidebar.position') }}</p>
                                    </a>
                                </li>
                            @endcan
{{--                            @can('position-access')--}}
                                <li class="nav-item">
                                    <a href="{{ route("admin.departments.index") }}" class="nav-link {{ $request->segment(2) == 'departments' ? 'active active-sub' : '' }}">
                                        <i class="nav-icon fas fa-landmark"></i>
                                        <p>{{ __('sidebar.department') }}</p>
                                    </a>
                                </li>
{{--                            @endcan--}}
                        </ul>
                    </li>
                @endcan
                @can('account-management-access')
                    <li class="nav-background nav-item has-treeview  {{ ($request->segment(2) == 'roles' ||  $request->segment(2) == 'permissions' ||  $request->segment(2) == 'users') ? 'menu-open' : '' }}">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fa fa-users"></i>
                            <p>
                                {{ __('sidebar.user-management') }}
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview ">
                            @can('permission-access')
                                <li class="nav-item">
                                    <a href="{{ route('admin.permissions.index') }}" class="nav-link {{ $request->segment(2) == 'permissions' ? 'active active-sub' : '' }}">
                                        <i class="nav-icon far fa-user"></i>
                                        <p>{{ __('sidebar.permissions') }}</p>
                                    </a>
                                </li>
                            @endcan
                            @can('role-access')
                                <li class="nav-item">
                                    <a href="{{ route('admin.roles.index') }}" class="nav-link {{ $request->segment(2) == 'roles' ? 'active active-sub' : '' }}">
                                        <i class="nav-icon fas fa-user-secret"></i>
                                        <p>{{ __('sidebar.roles') }}</p>
                                    </a>
                                </li>
                            @endcan
                            @can('user-access')
                                <li class="nav-item">
                                    <a href="{{ route('admin.users.index') }}" class="nav-link {{ $request->segment(2) == 'users' ? 'active active-sub' : '' }}">
                                        <i class="nav-icon far fa-user"></i>
                                        <p>{{ __('sidebar.users') }}</p>
                                    </a>
                                </li>
                            @endcan
                        </ul>
                    </li>
                @endcan
                @can('account-management-access')
                    <li class="nav-background nav-item has-treeview  {{ ($request->segment(2) == 'support') ? 'menu-open' : '' }}">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-comments"></i>
                            <p>
                                {{ __('sidebar.support24-7') }}
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview ">
                            @can('permission-access')
                                <li class="nav-item">
                                    <a href="{{ route('admin.support.create') }}" class="nav-link {{ $request->segment(3) == 'create' ? 'active active-sub' : '' }}">
                                        <i class="nav-icon fas fa-paper-plane"></i>
                                        <p>{{ __('sidebar.send-new-request') }}</p>
                                    </a>
                                </li>
                            @endcan
                            @can('role-access')
                                <li class="nav-item">
                                    <a href="{{ route('admin.support.new') }}" class="nav-link {{ $request->segment(3) == 'new' ? 'active active-sub' : '' }}">
                                        <i class="nav-icon fas fa-share-square"></i>
                                        <p>{{ __('sidebar.new-request') }}</p>
                                        <span class="badge badge-primary right">{{$new_request}}</span>
                                    </a>
                                </li>
                            @endcan
                            @can('user-access')
                                <li class="nav-item">
                                    <a href="{{ route('admin.support.answers') }}" class="nav-link {{ $request->segment(3) == 'answers' ? 'active active-sub' : '' }}">
                                        <i class="nav-icon fas fa-reply"></i>
                                        <p>{{ __('sidebar.view-reply') }}</p>
                                        <span class="badge badge-warning right">{{$replied_request}}</span>
                                    </a>
                                </li>
                            @endcan
                            @can('user-access')
                                <li class="nav-item">
                                    <a href="{{ route('admin.support.processing') }}" class="nav-link {{ $request->segment(3) == 'processing' ? 'active active-sub' : '' }}">
                                        <i class="nav-icon fas fa-spinner"></i>
                                        <p>{{ __('sidebar.processing') }}</p>
                                        <span class="badge badge-success right">{{$processing_request}}</span>
                                    </a>
                                </li>
                            @endcan
                            @can('user-access')
                                <li class="nav-item">
                                    <a href="{{ route('admin.support.closed') }}" class="nav-link {{ $request->segment(3) == 'closed' ? 'active active-sub' : '' }}">
                                    <i class="nav-icon fas fa-check-circle"></i>
                                        <p>{{ __('sidebar.done') }}</p>
                                    </a>
                                </li>
                            @endcan
                            @can('user-access')
                                <li class="nav-item">
                                    <a href="{{ route('admin.support.index') }}" class="nav-link {{ ($request->segment(2) == 'support' && $request->segment(3) == '') ? 'active active-sub' : ''}}">
                                    <i class="nav-icon fas fa-list"></i>
                                        <p>{{ __('sidebar.request-list') }}</p>
                                    </a>
                                </li>
                            @endcan
                        </ul>
                    </li>
                @endcan
                <li class="nav-background nav-item">
                    <a class="nav-link" href="javascript:" onclick="$('#logout').submit();">
                        <i class="nav-icon fa fa-power-off"></i>
                        <p>{{ __('sidebar.logout') }}</p>
                        <span class="pull-right-container"></span>
                    </a>
                    <form method="post" id="logout" action="{{ route('logout') }}" style="display: none">
                        @csrf
                    </form>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
