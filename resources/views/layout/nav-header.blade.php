<!DOCTYPE html>
<html lang="en">
@include('layout.header')
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white nav-header">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link header-top" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="https://ceonail.com/vi/phan-mem-quan-ly-tiem-nail/" target="_blank"
                   class="nav-link header-top">Home</a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="https://thienquantech.com/lien-he/" class="nav-link header-top">Contact</a>
            </li>
        </ul>

        <!-- SEARCH FORM -->

        <!-- Right navbar links -->
        <ul class="navbar-nav ml-auto">
            <!-- Notifications Dropdown Menu -->
            @php
                $db_staffs = \App\Models\Staff::where('email','like',Auth::user()->email)->first()
            @endphp
            <li class="nav-item dropdown">
                <a class="nav-link" data-toggle="dropdown" href="#">
                    <i class="fas fa-bell text-white notification-icon"
                       data-count="{{count($db_staffs->unreadNotifications)}}"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                    <span
                        class="dropdown-item dropdown-header">{{count($db_staffs->unreadNotifications)}} Notifications</span>
                    <div class="dropdown-divider"></div>
                    <div id="notifications_panel" style="overflow-y: scroll; max-height: 400px">
                        @forelse($db_staffs->notifications->slice(0, 10) as $notification)
                            <a class="dropdown-item {{$notification->read_at == null ? 'unread' : ''}}"
                               href="{{route('admin.notify.mark_read',['id'=>$notification->id,'code'=>$notification->data['code']])}}">
                                {{ $notification->data['title'] }}
                                <br>
                                <span class="d-inline-block text-truncate text-secondary text-sm"
                                      style="max-width: 150px;">{{ $notification->data['content'] }}</span>
                                <span
                                    class="float-right text-muted text-sm">{{ $notification->created_at->diffForHumans() }}</span>
                            </a>
                            <div class="dropdown-divider"></div>

                        @empty
                            <div class="text-center">
                                <a class="dropdown-item" href="#">No unread notifications</a>
                            </div>
                        @endforelse
                    </div>
                    <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>

                </div>
            </li>

        </ul>
    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
@include('layout.sidebar')

<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        @yield('content')
        <div id="list_toast"></div>
    </div>
    <!-- /.content-wrapper -->
    @include('layout.footer')

</div>

<!-- /page container -->
<!-- Footer -->
@yield('javascript')
@yield('css')
</body>
</html>
