@extends('layout.nav-header')
@section('content')
    <div class="content-wrapper">
        @yield('content')
    </div>
@endsection
@section('javascript')
    <script type="text/javascript">
        var data_count = parseInt($("i[data-count]").attr('data-count'));
        var pusher = new Pusher('{{env('PUSHER_APP_KEY')}}', {
            cluster: 'ap1',
            encrypted: true
        });

        Pusher.logToConsole = true;
        // Subscribe to the channel we specified in our Laravel Event
        var channel = pusher.subscribe('Notify');
        // Bind a function to a Event (the full Laravel class)
        channel.bind('staff.{!! \App\Models\Staff::where('email','like',Auth::user()->email)->first()->department_code !!}', function (data) {
            // $('#list_toast').empty();
            let toast_item = '';
            toast_item = '            <div class="toast" role="alert" aria-live="assertive" aria-atomic="true" data-autohide="false">' +
                '                        <div class="toast-header">' +
                '                            <i class="mr-2 icon_bell far fa-bell"></i>' +
                '                            <strong class="mr-auto">' + data.title + '</strong>' +
                '                            <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">' +
                '                                <span aria-hidden="true">&times;</span>' +
                '                            </button>' +
                '                        </div>' +
                '                        <div class="toast-body">' + data.content + '</div>' +
                '                    </div>';
            $('#list_toast').append(toast_item);
            $('.toast').toast('show');

            let dropdown_item = `<a class="dropdown-item unread" href="{{asset('')}}/admin/support/detail/` + data.code + `" >
                                ` + data.title + `
            <br>
            <span class="d-inline-block text-truncate text-secondary text-sm" style="max-width: 150px;">` + data.content + `</span>
                                <span class="float-right text-muted text-sm">Vừa xong</span>
                            </a>
                            <div class="dropdown-divider"></div>`;

            $('#notifications_panel').prepend(dropdown_item);
            data_count +=1;
            $("i[data-count]").attr('data-count',data_count);
            $("span.dropdown-header").text(data_count + " Notifications");
        });
    </script>
@endsection
